Thoughts on Collaboration


I have been thinking about about web3 projects and their relationship with the community or society in a broader sense for some time : https://our.status.im/computing-to-p2p-society/
Recently a discussion with the Akasha project made things once more clear. Or lets say it put the spotlight on a issue once more. 

Our society is so complex and I think sometimes, just to deal with reality people tend to ignore that complexity. To illustrate the issue I will use the thinking frame of developers and the community. 
The developers would be represented by a web3 project. Even if many more types of people are involved lets say the people of a web3 project have a connected and embedded understanding of "the space" but also of their own project.

Now from my perspective it seems that the role of community manager is to be a kind of interface between the developers("the project") and the community ("the -other- people"). As such a CM is responsible for building a relationship between 
the project and the people. From what I have seen so far there seems to be the following strategy prevalent:
The project offers a communication channel, often this channel would ran on a currently existing platform ("social media") such as twitter, facebook, youtube etc...
Now this is interesting as these sites do not offer any meaningful way to actually interact or learn.

How can it not be seen that so much effort is used to create content aka fill peoples precious attention with yet meaningless information? How are people supposed to learn if they can not interact in a meaningful way with something?

A way way forward 

I think there are ways in which things could be improved. I think a single person can not possibly a meaning full interface between a larger group of people with coherence ( project) and a even larger group of people with yet no relation to the project itself.
Chat-Channels, Blog posts seem inappropriate. A major step towards a meaningful relation in my option would be if the communitymanager or a group from the project helps a small group of people from the community to setup a self learning group.
In this kind of P2P-learning environment the community group would articulate a stable method of interaction which then could be used to communicate with the project. And from there on a dynamic process could grow in which project and community build trust and meaningful interactions may emerge. 


	
