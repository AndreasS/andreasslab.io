[[_TOC_]]

# Paul Mason - Postkapitalismus - Worum geht es im Buch - Context

- #tags: #context #paul_mason #postcapitalism #postkapitalismus #book
- wie bin ich auf das Buch gekommen? Jamie Dobbs Talk Scala Days 2016
- es geht um die Bedeutung der Arbeit in der Gesellschaft (Sinnstiftung)
- historische Betrachtungen und einige Ausblicke in die Zukunft 

# Paul Mason - Postcapitalism

- #tags: #paul_mason #postcapitalism #youtube #talk
- cambridge October 2015
- https://www.youtube.com/watch?v=EM1IOe51NZo
- jeremy rifkin: zero marginal cost society
- david greber athropolgist : bullshit jobs
- digital technology making goods _non_ scarce
- QA: universal basic income
- berlin social economy
- reference to the movie elyisum, "its not far off" : https://youtu.be/EM1IOe51NZo?t=3571
- "our problems in the west" : 1. oil companies 2. bust banks 3. gone pension funds
- networked human beeing , Book : why is it kicking of everywhere
- OECD: need people to keep up with standard (migration) , 50 years study
- funny note at the end UK politics also ignores these issues..

# Links Resoruces - Postcapitalism book

- #tags: #postcapitalism #karl_marx #maschinenfragment #postkapitalismus
- Karl Marx Grundrisse der Kritik der Politischen Ökonomie- Das sogenannte Maschinenfragment":  http://www.wildcat-www.de/dossiers/empire/maschinenfragment.pdf
- https://en.wikipedia.org/wiki/Cognitive-cultural_economy
- http://www.springer.com/us/book/9788132236351
- https://blog.p2pfoundation.net/p2p-planetary-futures/2016/12/14

# Jeremy Rifkin on the Fall of Capitalism and the Internet of Things | Big Think

- #tags: #jeremy_rifkin #zero_marginal_cost_society #postcapitalism #youtube
- https://www.youtube.com/watch?v=3xOK2aJ-0Js zero marginal cost society , three internets !!!

# Gedanken Notizen 

- #tags: #gedanken #thoughts #postcapitalism #book #postkapitalismus
- Auch sie(Arbeiter? Um 1900?) schwankten zwischen Reform und Revolution 
- In DE erzwangen sie durch Kriegs Marine das Ende des Krieges 
- Sie wollten parallel Gesellschaft 

# Chomsky 

- Working class press 

# Einleitung

- #tags: #notes #postkapitalismus 
- bis 2050 Veränderung
- Kap. -> Neoliberaismus : Doktrin unkontrolierter Märkte
- was ist Kapitalimus: umfassendes System: gesellschaftlich, wirtschaftlich, demographisch, kulturell und idiologisches System
- kap verändert sich, ist aber an seine Grenzen gestoßen
- S.16 drei Auswrikungen ermöglichen den Postkapitalismus: 
    1. Arbeitsaufwand sinkt , Arbeit/Freizeit Grenzen verwischen   
    2. Märkte, Preisgefüge kaputt -> Knappheit / Informationen -> Überfluss , Konterversuch mit Monopole und Patente
    3. Spontane Peer Produktion
- S. 15 das scheitern der Linken, Individualismus hat über Kollektivismus triumphiert
- S. 16 Vorraussetzung: Einstellung zu Technologie, Eigentum und Arbeit ändern
- S. 17 Keine Insel Lösung möglich
- S. 17 Konsum zu einer Form der Selbstverwirklichung geworden
- Allmende Produktion
- S. 18 auf der einen Seite bringt er Argumente warum "es" nicht aufzuhalten ist -> Überwachung und staatliche Zensur nimmt aber _zu_ und Informationsüberfluss
- S. 20 "In der Vergangenheit wäre eine intellektuelle Radikalisierung ohne Macht sinnlos gewesen. Generationen von Rebellen vergeudeten ihr Leben in Dachstuben, wo sie wütende Gedichte schrieben, die Ungerechtigkeit der Welt verfluchten und ihre eigene Machtlosigkeit beklagten. **In einer Informationsökonomie ändert sich die jedoch _die Beziehung zwischen Denken und Handeln_"**
- S. 20 "Der größte Widerspruch des heutigen Kapitalismus ist zwischen der Möglichkeit  eines unerschöpflichen Angebotes an kostenlosen Gütern !! <==> !!
    und einem System von Monopolen, Banken und Staaten die alles tun damit diese Güter  knap, kommerziell nutzbar und im Privatbeseitz bleiben
- S. 21 Die Auseinandersetzung zwischen Netz und Hierarchie

# Teil 1

## 1. Der Neoliberalismus ist kaputt

- S. 28-29 zukünftige Generationen: Rentenalter angehoben heutige Hochschulabsolventen **bis 70 arbeiten**, Bildung privatisiert
- S. 29 noch eine Bankenrettung unmöglich?

### _Eine weitere Droge_

- S. 30 Formulierungen aus 2008
- S. 31 Was 2008 passiert ist
- S. 34 Zweifel an der Globalisierung?
- S. 34 Aussichten für die Globalisierung
- S. 35 4 Elemente ermöglichen und werden dem Neoliberalismus zum Verhängnis:
    1. Fiatgeld -> auf Pump
    2. stagnierende Einkommen der Arbeitskräfte in der entwickelten Welt durch Kredit ersetzt
    3. globales Ungleichgewicht und Risiken durch Schulden
    4. Informationstechnologie

###  _Fiatgeld_

- S. 35 Fiatgeld: Geld beliebig nachdruckbar -> Entkopplung von realen **Gegenwerten** !
- S. 37 Basel II und Basel III
- S. 39 Illusion des Neolib: man brauche nur Geld um Geld zu machen
- S. 42 "Fiatgeld in Kombination mit der freien Marktwirtschaft eine Maschine ist, die Zyklen von Expansion und Rezession produziert" langfristige Stagnation widerspricht Wachstum?

### _Finanzialisierung_

- Finanzialisierung seit den 80ern:
    1. Unternehmen werden selbst auf Finanzmärkten aktiv, ohne Banken
    2. Einnahmequellen für Banken: Privatkunden -> mit riskanten Produkten, investment Banking
    3. Kreditkarten, Dispo, Hypothek, Studiendarlehen,Autofinanzierung, Gewinne werden heute NICHT erzielt in dem Arbeitskräfte oder Güter oder Dienstleistungen bereitgestellt werden welche diese mit ihrem Einkommen bezahlen, Sie Werden erziehlt in dem man den Arbeitskräften das Geld für den Erwerb von Gütern und Dienstleistungen leiht.
    4. finanzielle TRansaktionen heute komplex, mobilfunkvertrag verpackt die stetigen Zinsen für Investoren und jemand wettet schlielich dafür/dagegen
- S. 44 die Reallöhne von Industriearbeitern stagnieren in den USA seit 1973
- S. 46 Finanzliasisierung : zerstört die Beziehung zwischen Ausleihungen und Ersparnissen
- S. 46 in finanzialisierten Gesellschaften führt eine Bankenkrise nicht dazu das die Kunden die Banken stürmen, weil sie einfach nicht viel Geld auf dem konto haben !!

### _Die Welt ist aus dem Lot_

- S. 49 DE und Japan betreiben Merkantilismus und nicht Neoliberalismus, sie manipulieren Handel, Investitionen und Währungspolitik um große Fremdwährungsreserven anzuhäufen
- S. 51 in ihrer derzeitigen form hat die globalisierung Designfehler, wenn es hierbei wachstum gibt dann nur mit untragbaren Verzerrungen die Finanzkrisen erzeugen

### _Die Informationstechnologierevolution_

- #tags: #notes #postcapitalism #monopolies #markets #networkeffects
- Netzwerkeffekte sind mit dem heutigem Marktsystem nicht vereinbar, Monopol Bildung -> Amazon

### _Das Zombiesystem_

- S. 56 globale Elite "muss": 
    - Stabilisierung des Fiatgeldes
    - Abkehr von Finanzialisierung
    - Beseitigung des Ungleichgewichts
    - ===> aber die gessellschaftlichen und poitischen Hindernisse sind zu Hoch!!
    - "Die herrschende Elite Deutschlands profitiert von der Schulden kolonisierung Griechenlands und Spanies
- S. 57 2050 Schweden so ungerecht wie heute die USA

## 2. Lange Wellen kurzes Gedächntnis

- #tags: #notes #postkapitalismus #nikolai_kondratiev #model #economy #trotski
- **Kondratjew** https://en.wikipedia.org/wiki/Nikolai_Kondratiev
- S. 65 Wellentheorie
- S. 66 Gleitender Mittelwert, K. Methode: er verwendete Zinssätze , Löhne, Rohstoffpreise, Zahlen zur Kohle und Stahlproduktion sowie zum Außenhandel
- S. 67 50 Jahre Zyklus
- S. 68 Zyklus Definition: 
    - die Einführung neuer Technologien
    - der Siegeszug neuer Geschäftsmodelle
    - der Eintritt neuer Länder in den Weltmarkt
    - eine Ausweitung der Geldmende und ein erleichterter Zugang zu Kapital
- Ursachen?
- S. 70 **Trotski** zu K. These
- S. 72 Kritik an  K. Thesen
- S. 73 (unten) zentrales Problem von K. Thesen
- S. 74 Wellen und Gelitender Mittelwert für Vorhersagen
- S. 76 die Welle für heute (2010) nach berechnet
- S. 77 K. These als Werkzeug zum Verständnis für die Mutationen des Kapitalismus
- S. 78 **Joseph Schumpeter**, Wie K. thesen von JS übernommen wurden und Mainstream wurden
- S. 79 **Carlota Perez**
- S. 80 Die vier, fünf Zyklen des Industriekapitalismus:
    1. 1790 - 1848 Fabrik System Dampfmaschine in UK, Frankreich und USA
    2. 1848 - 1895 Eisenbahn, Telegraphie, Ozeandampfer,stabile Währungen 
    3. 1895 - 1945 weiterführende Entwicklung in: Schwerindustrie, Elektrotechnik, Telefon, wissenschaftliche Betriebsführung und Massenproduktion
    4. 1945 - 2008 transistoren, Kunststoffe, Massenkonsumgüter, Fertigungsautomation, Atomkraft und automatische Kalkulation ( was für eine schlechte Übersetzung..)
    5.  Ende der 90er : Vernetzung, mobile kommunikation, globaler markt und informations güter

## 3. Hatte **Marx** am Ende doch recht? Finanzsystem <==> Kapitalismus

### Was Marx sagte

- #tags: #notes #postkapitalismus #karl_marx
- S. 87 die drei Krisen in "Das Kapital" : 1. Überproduktionskrise 2. Investitionsstillstand Krise  3. Finanzkrise: - 2008 sehr ähnlich beschrieben, Phänomene wie oxfam 2016 vorhergesagt 
- S. 88 entscheidene Phänomene des 20 Jh. nicht enthalten: staatskapitalismus, monopole, komplexe finanzmärkte, globalisierung --> Informations technologie??
- S. 89 **Karl Kautsky**

### Der Kapitalismus unterdrückt den Markt

- #tags: #notes #postkapitalismus #market #culture #technology #change
- S. 90 Kultur Technologischer Wandel
    - das Verhältnis vin Innovation zu Preisentwicklung
    - Beispiele amerikanischer Telefonkonzerne, Bell Telephone
    - Mechanismen welche die Unternehmen fanden : Monopole, Preisabsprachen, Schutz der Märkte
- S. 91 Staatskapitalismus beispiele für Deutschland und Japan
- S. 92 Def. Staatskap: "Der Finanzsektor sicherte sicheine Kontrollmehrheit an der Industrie und baute Monopolpositionen auf und unterdrückte Märkte wo möglich. Mit Hilfe des Staates"

### Der Kapitalismus mutiert

- S. 93 **Hilferding** https://en.wikipedia.org/wiki/Rudolf_Hilferding
- S. 97 **Rosa Luxemburg**

### Die große Desorientierung 

- Es wird die Geschichte beleuchtet wie im russischen Kommunismus der Kapitalismus gesehen wurde
- S. 101 "der Imperialismus als höchtes Stadium des Kapitalimus" - Lenin
- die hochgradige Organisation durch vertikal integrierte Konzerne Kartelle und den Staat - bedeutete, dass die Wirtschaft schon im Staat vergesellschaftet werde
- **Bucharin** https://de.wikipedia.org/wiki/Nikolai_Iwanowitsch_Bucharin "... nur eine Form des Wettbewerbs, und zwar den Krieg"
- Kautsky und der "ultraimperialismus" ( ein von transnationalen Konzernen beherrschter Weltmarkt)
- S. 105 Eugen Varga Stalins Chefsvolkswirt
- S. 107 die Theorie der Unterkonsumtion
- Bucharin erklärt der Kapitalismus habe sich in den 1920er Jahren stabilisiert, durch die Entsteheung des Staatskapitalimus - eine Verschmelzung von Monopolen, Banken und Kartellen mit dem Staat, keine vorrübergehende Stabilisierung

### Das Problem der Krisentheorie

- die Theorie konnte nicht erklären wohin sich der Kapitalimus entwickelte und warum
- Kapitalismus als offenes, sich veränderndes System
- wann kriesen kritisch werden und wie schnell der System wechsel kommt
- S. 108 Marx erkannte das die maschinen die menschliche Arbeitskraft verdrängen...
- veränderungen auf der "mesoebene" können nicht erklärt werden das heißt zwischen mikro und makroökonomischen Ebene

### Der perfekte Zyklus

- normative Neuformulierung der Theorien der langen Zyklen:
- S. 111    1. Dem Begin eines Zyklus geht normalerweise eine Anhäufung von Kapital und Märkten
-           2. technologisches Paradigma im Einklang , "kooperativer Wettbewerb" , sozialer Frieden , durch überdurchschnittliches Wachstum
-           3. während des gesamten zyklus wird die menschliche Arbeitskraft durch maschinen ersetzt, Profit geht zurück wird aber durch ausweitung der Produktion ausgeglichen, wodurch Gesamtprofit steigt
-           4. Wirtschaftsaufschwung (wachstum?) weil die Euphorie zu übermäßigen Investitionen , traumatischer  Umbruch durch Ungewissheit der Geschäftsmodelle, Währungssysteme
-           5. Druck auf Löhne steigt, es wird versucht Tätigkeiten so zu gestalten das sie weniger Kenntnisse benötigen, Umverstilungsorijekte (sozialstaat) geraten unter druck, Staat wird gedrängt einen beschleunigten Wandel zu organisieren, Rezessionsphasen werden häufiger
-           6. Wenn der erste Anpassungsversuch scheitert wird das kapital aus dem produktiven Sektor und in das Finanzsytem geleitet. Preise sinken. Auf eine Panik folgt eine Depression. Die Suche nach vollkommen neuen Technologien , geschäftsmodellen, Geldquellen beginnt. Die globalen machtstrukturen werden instabil
- S.112 das Konzept der sozialen Agenten, zusammenhang technologische Innovation und Klassen

### Was erzeugt einen Zyklus?

- S.116 Marx und Kondratjews Darstellung der Fünfzig jahres zyklen, "einfach ausgedrückt, sind die fünfzig jahres Zyklen der langfristige Ryhtmus des Profitsystems"
- S.117 "Eine Lösung die den Unernehmen erlaubt, Arbeitskräft rasch durch maschinen zu ersetzen, funktioniert wine weile und steigert die Profite. Doch irgendwan versagt sie"
- S.117 der Kapitalismus gewzungen mit der Außenwelt zu interagieren
- S. 118 "Das Muster des langen Zyklus ist gestört worden. Der vierte Zyklus wurde von Faktoren, die in der Geschichte des Kapitalismus neu waren, verlängert, verzerrt und schließlich unterbrochen.
- Diese Faktoren waren:
    - die Niederlage und moralische Kapitulation der organisierten Arbeiterklasse
    - der Aufstieg der Informationstechnologie
    - die Entdeckung das eine unangefochtene Supermacht lange Zeit Geld aus dem Nichts schöpfen kann

## 4. Ein unterbrochener langer Zyklus

- S. 120 Was passierte nach dem WW2: "über all in der entwickelten Welt war das neue techno-ökonomische Paradigma zu erkennen, auch wenn jedes Land seine eigene Versionentwickelte"
- In der gesamten Volkswirtschaft setze sich genormte massenproduktion durch, Arbeitseinkommen waren so hoch das sie die Bevölkerung die von den Fabriken erzeugten Güter leisten konnte
- männliche erwebsbevölkerung genoss vollbeschäftigung, ggf. nach kultur auch jungend und frauen, zunehmende urbanisierung

### Die Macht klarer Regeln

- #tags: #notes #postkapitalismus #models #money #society #coordination
- S. 122 Bretton Woods: auf der konferenz wurde ein System fester Wechselkurse vereinbart, alle Währungen an den Dollar gekoppelt und der Dollar an das Gold
- John maynard Keynes sprach sich für separte Weltwährung aus, welches die USA jedoch ablehnten
- S. 123 das System war offenkundig zum Vorteil der USA gestaltet worden
- Regel für Hochfinanz, Grenzen für Verschuldung
- "Das Ergebnis war ein Kapitalismus mit ausgepräft nationalem Charakter" Banken und Rentenfonds wurden gesetzlich verpflichtet die Anleihen ihrer Länder zu kaufen
- Finanzrepression: der Staat hält die Zinsen unterhalb der Inflaiton womit Sparer für das Privileg bezahlen Geld zu besitzen (Geld außer landes wird so verhindert)

### Der nachkriegsaufschwung als Zyklus

- S. 125 Im krieg hatte eine weitere bedeutsame Veränderung stattgefunden : der Staat hatte die Kontrolle über die Innovation übernommen
- Bis ende de Krieges wußten die nationalen Verwaltungen wie das Verahlten des Privatsektors druch Staatseigentum und staatliche Kontrolle, sowie Massenkommunikaiton , gelenkt werden konnte
- S.126 Zentren für Forschung werden zentralisiert und industrialisiert OSRD => holte **Claude Shannon** nach Princeton
- S.126  1942 GM **Peter Drucker** "The Concept of the Corporation" - Das Großunternehmen, sprach er sich aus die hierarischen Befehlsstrukturen aufzubrechen und die Unternehmensleitung zu dezentralisiern
- GM folgte dem nicht, aber japanische Unternehmen
- Die managementlehre war kein geheimes Wissen mehr sondern verwandelte sich in eine Disziplin die allgemein angewandt wurde.
- Kriegswirtschaft brachte: Problemlösung durch kühne Technologie sprünge. experten aus verschiedenen disziplinen, verbreiterte die bewährten Methoden in einem ganzen Sektor und änderte die Betriebsabläufe wenn sich das Produkt änderte
- in den dreißger Jahren war der Finanzsektor was komplett abgeschaltet worden
- der Kapitalismsu ging stark verändert aus dem Krieg hervor
- aufkommen der Schlüsseltechnologie: Informationstechnologie -> Informationsgeellschaft
- S. 128 **Andrew Glyn** der außergewöhnliche Aufschwung kam durhc einzigartige Mischung aus wirtschaftlicher, sozialer , geopolitischer Faktoren die während des Aufschwungs harmonisch wirkten, mitte der 60er jedohcin Konflikt gerieten
- daher von 1948-1973 starke Aufschwungphase

### Warum überschritt die Welle ihren Scheitelpunkt?

- S. 129 ende der 60er bereits Krisen , 1973 dann auslöser für Krise
- klassischer phasenwechsel Kondratjew Zyklus
- S. 130 "die Nachkriegsregelungen hatten die Instabilität auf zwei Bereiche beschränkt, die kontrolliert werden mussten: die Beziehungen zwischen Währungen und die Beziehungen zwischen Klassen
- S. 132 "Profitklemme" sinkende Gewinne, steigende löhne, 2 Optionen - 1. Wert der REallöhn e verringer oder 2. Sozial ausgaben steigern bei unternehmens entlastung
- Nixon 1971 entkopplung Dollar vom Gold, Konkurrenten der USA holten bei Produktivität auf und Kapital floß aus dem USA ab , handelsüberschuss schrumpfte
- USA verwenadelten sich in den großen Verlieren , konnten nicht mehr sozial reformen udn Vieatnam krieg finanzieren

### eine Parodie auf Keynes

- S. 132 ab jetz war es tatsächlich möglich Geld aus dem nichts zu schaffen

### Der Angriff auf die Arbeiter

- S. 133 "Ende der 70er zerstörte sich das keynesianische System selbst , durch alle beteilligten: Arbeiter, Bürokraten, Technokraten, Politiker
- S. 134 Wandel der gewerkschaften und "Atomisierung der Arbeiterklasse"
- S. 135 japan Arbeiterproteste täglich verprügelt
- Deutschland "lößte" das Arbeitsmarkt Problem mit einer Randgruppe von osteuropäischen Gastarbeitern
- Fluglotsen in den USA, Bergarbeiter in UK
- S. 136 Mason argumentiert das durch das "brechen" der organisierten Arbeiterschaft in den 80er Jahren die Kapitalisten mit Lohund auf geringem Wert beruhendem Produktionsmodellen, einfach weiterzumachen
- ==> hierdurch war das 1% nciht gezwungen zu "revolutionärer" technologischer innovation (ist es genau dieser zustand bzw. Mangel den Alan Kay beschreibt? !!! )

### Die unterbrochene Welle in Bildern

- Grafiken:
- S. 137 Anteil der Arbeitseinkommen am "globalen BIP" sank deutlich 
- 2. Zinsen 3. Rohstoffpreise: Nickel , china extrem starke nachfrage für STahlproduktion, alternative technologie bauweise? Holz? Kunstoffe?
- 4. Staatsschulden gemessenam BIP 5. Geldmenge , "Finanzinstrumente"
- 6. Ungleicheit in den USA, einkommen weniger als 341.000 Dollar und mehr, WIEVIEL mehr!  7. Finanzialisierung (sieheStichwort Arbeitswerttheorie)
- 8. Globale Investitionsströme, Perspektiven der "Globalisierung" 9. Pro-Kopf BIP weltweit, entwicklungsländer, transformationsökonimien
- 10. die Gewinner der Globalisierung , S.146 unterdrückung des Globalen Südens durch den Kapitalismus, was mit Ländern passiert di eWiderstand leisten
- 11. Verdopplung der globalen Erwerbsbevölkerung , drei Viertel verdienen zwischen 4 und 13 Dollar am Tag und arbeiten im Dienstleistungssektor
- Dienstleisutng "natürliche Etnwicklung des Arbeitsmarktes im modernen Kapitalismsu: Verlagerun der Arbeitsplätze ins Ausland , Callcenter , It-Abteilungen , Backoffice Funktionen"
- die Grafik zeigt Grenzen des Offshoring, Tage des leichten profits durch Auslandsverlagerung für unternehmen sind "gezählt"
- Massen statt Klasse produktion ( extensiv vs intensiv Methoden zur Produkt weiterentwicklung)

### Wie wurde das Muster durchbrochen?

- S. 151 die Faktoren auf welchen der Neoliberalismus beruht: Fiatgeld, Finanzialisierung, Verdoppelung der Erwerbsbevölkerung, globaleUngleichgewichte einschließlich der deflationären Wirkung der Verbilligung der Arbeit
- sowie der Verbilligung von allem anderen durch die Informationstechnologie
- superreiche Elite die im Finanzkapitalismsus von Finanz erträgen lebt
- kann das folgende System kapitalistisch sein?

# Teil 2

## 5. Die Propheten des Kapitalismus

- S. 155 Düsentriebwerk und Simulation
- S. 158 "die Beziehung zwischen körperlicher Arbeit und Information hat sich verändert" , alte objekte "intelligent" machen
- in den 90er Jahren aufkommen der Informationstechnologie , verändert die Welt , interdisziplinärer Gedanke: der Charakter des Kapitalismus wandelt sich von Grund auf
- neue Begriffe: Wissensökonomie, Informationsgesellschaft, kognitiver (Informations?) Kapitalimus // schon wtizig das ich im Sozialkunde unterricht immer noch lerne das wir in der Dienstleistungsgesellschaft sind, so alt war das Lehr material...
- Problem mit Grundlegenden Mechanismen des Kapitalismus: zersetzt Marktmechanismen, hölt Eigentumsrechte aus und zerstört die Beziehung zwischen : Einkommen, Arbeit und Profit
- *die ersten die das begriffen, Philosophen, Managementgurus und Rechtsanwälte*
- radikale These: **die Informationstechnologie führt uns in ein postkapitalistisches Wirtschaftssystem**

### Peter Drucker: die richtigen Fragen

- #tags: #notes #postkapitalismus #peter_drucker #questions #structure #society #peter_drucker
- S. 159 1993 Drucker: "die Tatsache das Wissen *die* Resource geworden ist und nicht mehr bloß eine Resource unter anderen, macht unsereGesellschaft zu einer *postkapitalistischen*. "
- "Sie verschiebt die Struktur unserer Gesellschaft, und zwar Grundsätzlich" 
- Drucker letzter überlebender Schüler von **Joseph Schumpeter** 
- Beobachtung: herkömmliche Produktionsfaktoren: Boden, Arbeit und Kapital treten gegenüber der Information in den Hintergrund
- #book Peter Drucker: die postkapitalistische Gesellschaft , Grundlagen der Netzwerkökonomie https://en.wikipedia.org/wiki/Post-Capitalist_Society
- Informationskapitalismus (Neoliberalismus + Informationstechnologie) nur als Übergangsphase
- Drucker stellte fest: es gibt keine Theorie wie die Information in wirtschaftlichen Abläufen verhält
- seine erste frage: Wie können wir die Produktivität des Wissens erhöhen?
- seine Antwort: Verküpfung verschiedene Bereiche, S. 160 seine Antwort aus dem Handbuch der management theorie
- Die Menschheit fand eine besere Lösung: das Netzwerk
- nicht Resultat eines zentralisierten Plans oder Arbeit einer Managementgruppe, sondern entstand aus :
- **"spontanen Interaktionen von Menschen die Informationspfade und Organisationsformen(open source? Konsens) nutzen die bis vor 25 Jahren noch nicht existierten"**
- Druckers zweite Frage: Welches ist der gesellschaftliche Archetyp des Postkapitalismus? "die gebildete universelle Person"
- verschmelzung von Intelektuellen und Managern
- Drucker zitat: "eine einigende Kraft[...], des es gelingt, lokale, die Menschen voneinander trennede Traditonen gemeinsamen, von allen Menschen geteilten Wertvorstellungen einem gemeinsamen Konzept herausrander Leistung und gegenseiter Achtung zu verpflichten"
- => Kooperation über Konkurrenz, gegenseitige Achtung und Fortschritt über Tradition
- gibt es eine solche Gruppe von Personen? Mason schreibt hier das es der "smartphone" mensch ist, wenig überzeugend...
- S. 163 zurück zu Drucker: warum sollte der Postkapitalismus stattfinden?
- Druckers Einteilung zur Geschichte des Industriekapitalismus (Stichwort Industrie 4.0):
  1. eine mechanische Revolution, 19. jahrhundert
  2. aufkommen wissentschaftliche Betriebsführung, beginnende Produktivitätsrevolution
  3. nach 1945 durch Anwendung des Wissens auf die Betriebsabläufe -> Managementrevolution
  4. Informationsrevolution durch "Anwendung des Wissens auf das Wissen"
- Drucker Betrachtung Kondratjew Zyklen, diese sind nicht zu verstehen ohne die Ökonomie der Arbeit
- Was macht die Information mit der Arbeit und wie Grenzen zwischen Arbeit und Freizeit verschwimmen (Prosumer?) -> umwälzender Wandel
- Abschließend zu Drucker: Gute Fragen aber: Es gibt immer noch keine umfassende Theorie zum Informationskapitalismus oder Postkapitalismus

### Die Informationsgüter verändern alles

- #tags: #notes #postkapitalismus #influence #technology #economy
- S. 164 **Paul Romer** https://en.wikipedia.org/wiki/Paul_Romer Konzept der Charter Cities
- wiederlegte zentrale Annahme der Volkswirtschaftslehre , führte frage des Informationskapitalismus in den Mainstream
- Innovation sollte Teil der wachstumstheorie sein
- Inofrmationsprodukte unterscheiden sich von physischen Gütern
- **zero marginal cost**, Informationen können ohne "Mehrkosten" ( auf erzeuger seite) beliebig oft wiederverwendet werden, Null Grenzkosten !
- Informations als "nicht rivalisierende Güter"
- S. 166 Kopierschutz um Eigentumsrechte durch zu drücken 
- S. 167 Sein Beispiel für 0,99 € Itunes Songs stimmt so nicht, Amazon hat schon andere preise in unterschieldichen regionen , andere Beispiele?
- der eingezäunte Garten von Apple
- stärkere Monopolbildung im digitalen Sektor? (Sirenserver jaraon Lanier), "google muss die einzige Suchmaschine sein, FB, twitter, itunes" ach ja? hier zeigt sich wieder Masons mangelndes Technik verständnis
- was er meint ist der Dienst mit der größten Abdeckung wird sicherlich von den meisten Menschen favorisiert, das aber nicht undbedingt EINE firma hinter einem dienst stehen muss ( Blockchain) scheint ihm nicht denkbar
- man könnte allerdings auch argumentieren das es technisch tatsächlich einfacher war einen monolithischen monopol Service auf zu bauen als einen dezentralen (blockchain) Service der die selbe dienstleistung erbringt aber beliebige anbieter untertzützt
- David Warsh: "Nach 200 jahren waren Boden, Arbeit und Kapital plötzlich nicht mehr die grundlegenden Kategorierne der ökonomischen Analyse. Ersetzt wurden sie
- durch Menschen Ideen und Dinge [...], das vertraute Prinzip der Knappheit wird durch das wichtige Prinzip des Überfluss ergänzt"
- S: 168 Informationstechnologie untergräbt das normale Funktionieren des Preismechanismus
- eine Informationstechnologie macht eine nicht-marktwirtschaft möglich

### Der Aufstieg von OpenSource

- #tags: #notes #postkapitalismus #technology #transparancy #open_source
- Betriebssysteme funktionieren tadels los? Oh je was für ein User...
- S.170 Verkauf von Hardware und Software im Privatbereich , die Anfänge
- Gates Argument, Stallmans Argument - Gnu Manifest 1985 
- (fragwürdige) Open Source erklärung
- veränderte Eigentums verhältnisse: Google "gehört" android nicht und trotzdem kontrolliert es Android
- warum google und Samsung OpenSource verwenden, hoffnungslose naive beschreibung, erversteht zu wenig von Software ( es geht um kosten optimierung, sicherheit und verteilte REchtssicherheit - Lizenz)
- siehe Scala center , sehr fragil, was ist lohnenswert? wie funktionieren Communites? Welche Ideen werden umgesetzt? Alan Kay: the computer revolution hasn't happend yet
- Kann eine auf Informationsnetzen beruhende Wirtschaft eine "neue Produktionsweise" jenseits des Kapitalismus hervorbringen?

### "Am Rand des Chaos dahingleiten"

- Kevin Kelly: "die Ära des Computers ist vorbei , vielversprechendes jetzt durch Kommunikation zwischen Computern" aha Captain obious...
- nicht der Computer sondern das netzwerk sei "die intelligente Maschine", compositionality nicht verstanden
- S. 174 der start von eBay löste die dtomcom blase aus??? Mr Paul Mason verwendet siten weise auf Arbetiswert Therorie aber hier schlampt er ohne ende
- das "plötzliche" (falsch!) Auftauchen billiger, standardisierter digitaler Werkzeuge , das web 2.0 (falsch bzw. stark mangelnde technisch historische perspektive)
- moores Law und anstieg de rinformationsmenge 
- tablets und Streaming stellen also einen durchbruch dar? und welcher Art von Synergie S.175 ?
- "Mittlerweile leben wir in dem unausgesprochenen Bewußtsein, dass das Netzwerk die Maschine ist" zum Zeitpunkt des Buches war der überwiegende Anteil an Mehrwert, Diensten und Inhalten im Netz noch von Menschen erzeugt worden
- => also ist die Aussage falsch, das Netzwerk ist **nicht** die Maschine, es sind die Menschen di eKomplexität und Vielfalt erzeugen
- Kelly akzeptierte als normal was Romer "nur" sieben Jahre zuvor als neuartig betrachtete: Die Tendenz  der Informationstechnologie Daten und physische Produkte zu verbilligen ,so das die Grenzkosten ihrer Produktion gegen null sinken
- wenn Mason das Buch als Ratgeber für traditionelle Unternehmen in der Netzwerkökonmie ansieht, wie kann es da interesant für den Postkapitalismus sein? Übergang??

### Eine neue Produktionsweise?

- #tags: #notes #postkapitalismus #influence #technology #economy #production
- S.176  **Yochai Benkler** Buch "The wealth of Networks" Rechtsrahmen **Creative Commons**
- man sieht hier deutlich das Mason für seine Recherche überwiegend Bücher verwendete, scheinbar hatte jemand vergessen eine "geschichte der Creative Commons" zu schreiben und dann als Buch zu veröffentlichen
- armer Paul Mason kein Bezug auf **Lawrence Lessig oder Aaron Swartz** ?
- Allmendemodelle und nicht gelenkte Produktion?
- sein Beispiel für digitale selbstvermarktung ist real , Andy Weir the Martian, aber , wie wir gerade an diesem Beipsiel sehen kam der Erfolg erst mit amazon obwohl es zuvor schon auf dem Blog kostenfrei war
- Wikipedia hat Angestellte und das hosting kostet auch geld, sie sammeln mehrere millionen € spenden jedes Jahr, Großteil SIND personal kosten
- sein wikipedia beispiel zeigt das er die internen Strukturen der wikipedia nicht versteht, für mehr DEtails zur Wikipedia Finanz und produktions problemantik : siehe wikipedia.md
- S. 178 ja ein kind hat mehr zur Auswahl als in einer kleinstadt bilbiothek aber hat es die Kompetenzen um alle Informationen auf Quellengehalt zu prüfen? Willkommen im Zeitalter de rInformitonsexplosion
- "Verzicht auf eine Mangementhierarchie" ?? Zu dum das das GEld dann doch lieber ZENTRAL verwaltet wird und lediglich die Produktion dezentral "ausgeleagert" ist
- kollaborative Allmende Produktion
- S. 179 "es kann frei genztt werden , ist jedoch unmöglich zu dassen, zu besitzen und auszubeuten" naive simplifikation
- Allmende Produktion und Perr to Peer Produktion rücken menschliche Bedürfnisse mehr in den Vordergrund
- S. 180 "In dem Augenblikc als es möglich wurde Dinge ohne den Markt und Unternehmen zu erzeugen begannen die Menschengenau dies zu tun"
- Mason sagt das die Dynamik des Informationskapitalismus von Benkler nicht beschrieben wird was problematisch sei da er Arbeitskräfte aus dem Produktionsprozess verdrängt, Marktpreis von Gütern drückt, zerstört einige Profitmodelle und bringt eine Generation von Konsumenten( HA! was is tmit Prosumenten?) die an kostenlose Produkte gewöhnt sind
- und trotzdem: neoliberales Wirtschaftssystem und Nwtzwerktechnologie harmonieren nicht

### Die Ökonomie der kostenlosen Dinge

-  **externe Effekte**
-  schlechte externe Effekte: Energieversorung mit Kohle => Luftverschmutzung
-  gute externe Effekte: Cluster von ähnlichen Unternehmen?
-  S. 182 zweck von Patenten? Profitmaximierung
-  Geschäftmodelle der Giganten des Informationskapitalismus beruhen fast vollständig auf der Neutzung positiver externer Effekte
-  S. 183 Was wäre wenn supermärkte Kundendaten anonymisiert veröfentlicht würden? Gesellschaft würde proditiren aber die supermärkte würden "ihren" Vorteil einbüsßen ( sollte es nicht richtiger heißen: sie würden ihren Vorteil dann teilen?)
-  freie Marktwirtschaft mit geistigem Eigentum vs Informationswirtschaft
-  Was wäre wenn eine Person vorrausgesagt hätte das die Möglichkeit Preise festzulegen verloren geht  wenn Wissen kollektiv verteilt und auf Maschinen verteilt wird? => Karl Marx

### Der allgemeine Verstand

- #tags: #notes #postkapitalismus #atomisierug #karl_marx
- S. 184  **Grundrisse der Kritik der politischen Ökonomie - Das Maschinenfragment** 1858 Karl Marx
- englische Übersetzunhg von "grundrisse" erst 1973
- interessant das die Bausteine auf denen das Kapital basiert so lange verborgen blieben
- Ausgangspunkt Maschinenfragment: die Entwicklung der Industrie verändert die Beziehung zwischen Maschine und Arbeiter
- "Marx stellt sich eine Volkswirtschaft vor , in der die vorrangie Funktion der Maschine in der Produktion und die Hauptaufgabe des Menschen in der Steuerung und Beaufsichtigung der Maschinen besteht"
- er bemerkt dass das "Wissen" hierbei die wichtigste Produktivkraft ist
- S. 185 "Organisation und Wissen leisten also einen größeren Beitrag zur Produktion als die Arbeit, die in Herstellung und Betrieb der Maschinen investiert wird"
- das Wissen als eigenständige Produktivkraft, dann stellt sich nicht mehr die Frage in welchem Verhältnis Lohn und Profit stehen sondern: **Wer kontrolliert das Wissen** ( patente , open source, komplexität?)
- Marx: "In einer Wirtschaft in der die Maschinen den Großteil der Arbeit leisten und die menschliche Arbeit eigentlich in der Gestaltung, Steuerung und Wartung der Maschinen besteht, muss das in den Maschinen gespeicherte Wissen, **gesellschaftliches Wissen** sein"
-  Standing on the shoulder of giants am Beipsiel von Softwareentwicklung
-  S. 186 Marx kannte keine Webserver aber das Telegrafensystem, er Schlussfolgert: 
-  **die Produktivitätssteigerung durch besseres Wissen ist sehr viel attraktiver als Verlängerung der Arbeitszeit(verbraucht mehr Energie) oder Beschleunigung der Arbeit(grenzen menschlicher geschicklichkeit und Belastbarkeit). Eine auf Wissen beruhende Lösung hingegen ist billig und hat unbegrenztes Potential**" 
- <- ist ein wenig wieder sprüchlich, also eigentlich ist der Mensch IMMER der limitierende faktor , bei körperlicher Arbeit schneller als Maschinen, bei geistiger etwas langsamer da wir uns rekursiv ( in bestimmten umfang) verbessern können
- weiter erklärt marx: das der auf dem Wissen beruhende Kapitalismus nicht mit einem Preismechanismus funktionieren kann in dem der Wert eines Guts vom Wert der für seine Erzeugung benötigten Inputs abhängt
- "es ist unmöglich den Wert der Inputs richtig zu bestimmen wenn sie die Form gesellschaftlichen Wissens  haben" <- ach ja? Blockchains track everything, mapping de rInputs?
-Marx: Widerspruch im auf Wissen beruhenden Kapitalismus zwischen Produktivkräften und den gesellschaftlichen Beziehungen
- arbeiter müssen vom System weiterentwickelt werden (jaja im präzisen rahmen des einsatzzwecks, bsp Callcenter schulungen)
- Marx Konzept: **general intellect** " Wenn wir die Entwicklung der Technologie messen, erklärt er, messen wir, bis zu welchem Grade das allgemeine gesellschaftliche Wissen, knowledge,
- zur unmittelbaren Produktivkraft geworden ist und daher die Bedingungen des gesellschaftlichen Lebensprozesses selbst unter die Kontrolle des general intellect gekommen sind"
- S. 188 in den 60er erkannte die Linke im Maschinenfragment einen Gegenentwurf zum klassischen Marxismus, bis dahin hatten die **Linken geglaubt der Kaptalismus könne nur durch staatliche Kontrolle überwunden werden**
- "Sie nahmen an, Ursache für die inneren Widersprüche des Kapitalismus sei die chaotische Natur des zu katastrophalen Zusammenbrüchen neigenden Marktes, der in ihren Augen **unfähig war, die Menschlichen bedrüfnisse zu erfüllen.**"
- Im Maschinenfragment wird ein anderes Modell für den Übergang  beschrieben: **der Kapitalismus bricht zusammen weil seine Existenz nicht mit dem gesellschaftlichen Wissen vereinbar ist** , aus dem Klassen kampf wird ein Kampf um Menschlichkeit und Bildung in der Freizeit
- <- gegenmaßnahmen laufen hiefür: Atomisierung des selbst, der Arbeiter (Klassen), zunahme nationalismus, populismus, Propaganda
- <- das "System"- Der Kapitalismus? hat einen Weg gefunden das zwar die Informationen: das der Kapitalismsus für alle/das System untragbar sei, IM System bekannt ist aber einfach die Menge an Informationen so groß ist das es nicht bemerkt wird, außerdem werden weitere Antroposopische Faktoren ausgenutzt zur "Ich zersplitterung"
- Die Frage: Warum geht das Konzept **der allgemeine Verstand** im Kapital verloren? Marx verwarf die Ideen...
- S. 189 "Gibt es einen Weg zum Postkapitalismus der auf der Informationstechnologie Beruht?, das maschinen fragment zeigt das Marx darüber nachdachte" 
- "Er stellte sich vor, dass sich das gesellschaftlich produzierte Wissen in den Maschinen vergenständlichen werde. Er stellte sich vor das dies eine neue Dynamik auslösen werde, welche die alten Mechanismen zur Erzeugung vin Preisen und Profiten zerstören konnte"
- "Er stellte sich vor das der Kapitalismus dazu gezwungen wäre die geisteigen Fähigkeiten des Arbeites zu entwickeln" 
- "und er stellte sich vor das die Informtionen im _general Intellect_ / _allgemeinen Verstand_ gespeichert und geteilt würden"
- "Darüber hinaus stellte er sich vor welches das Hauptziel der Arbeiterklasse sein würde, sollte es dies eWelt je geben: **Freiheit von der Arbeit**"
- Marx zitat "die freie zeit [...] hat ihren Besitzer natürlich in ein andres Subjekt verwandelt, und als dies Subjekt tritt er dann auch in den unmittelbaren Produktionsprozess"
- "Dieser Produktionprozess ist materiell, schöpferische und vergegenständlichende Wissenschaft im Bezug auf den gewordenen Menschen, in dessen Kopf das akkumulierte wissen der GEsellschaft existert" etwas naiv, aber im prinzip geht es darum alles zu teilen
- marx revolutionäre Idee: durch Reduzierung der Arbeit auf minimum könne der Mensch das gesamte akkumulierte Wissen der gesellschaft einsetzen , anteil der "freizeit" überzeigt "Arbeitszeit" --> Abscheid von der Arbeitsgesellschaft/ethik
- S. 190 kein weiter weg von marx arbeiter wie im maschinenfragtment beschrieben hinzur universellen gebildeten Person bei Peter Drucker

### Eine dritte Art von Kapitalismus

- neoliberale sahen inofrmationskapitalismus als Erfolg
- **Antonio Negris** seine Schüler nannten das System "kognitiver,dritter Kapitalismus"
- dessen Säulen sind: globale Märktefinanzialisierter Konsum, immaterielle Arbeit und immaterielles Kapital
- damit der kog. Kapitalismus funktionieren könne müssen sich die unternehmen die externen Effekte Aneignen (siren server)
- S. 191 die gesellschaft als Fabrik
- wie ein paar nke Schue 179,99 $ Wert sein kann
- S. 192 **"mitlerweile hängt es vom Verhalten des Kunden und von seiner Interaktion mit der Marke ab ob ein Unternehmen Gewinn erziehlen kann"**
- **"Produktion und konsum verschmelzen"**
- es geht um Markenwert, Protestbewergungen eher "Stämme" als Proletariat 
- theoretiker des kognitiven kapitalismus sagen: die vorrangie Aktivität der Arbeitskräfte ist die Produktion von Wissen durch Wissen

### Der Postkapitalismus : Eine Hypothese

- **WICHTIG** dies ist ein zusammenfassungskapitel seiner Ansichten und Aussichten
- S. 193 Postkapitalismus bestandsaufnahme
- **Jeremy Rifkin** : Die null grenzkosten Gesellschaft, allerdings gesellschafts( politische?) Dimension vernachlässigt
- um zu verstehen müssen wir die neue Info-Tech Ökonomie, die 2008 Krise und das muster der langen Zyklen im zusammenhang verstehen
- die netzwerk ökonomie untergräbt für digitale Güter den Mechanismus zur Festlegung der Preise für digitale Güter, wie ihn die herkömmliche Ökonomie versteht
- Güter bekommen hohen Informationsgehalt, der Wert dieser Güter hängt weniger von den Kosten der eigentlichen Produktion ab als von den gesellschaftlich erzeugten Ideen ( der marke)
- finanzialisierung erforderlich und erzeugt zwei profit ströme die von der Bevölkerung zu den Kapitalisten fließen: menschen produzierne Güter, Wissen, Dienstleistungen als Arbeitskräfte , als Kreditnehmer zahlen sie Zinsen
- Ausbeutung: 1. die Löhne 2. der Kredit 3. gesitige Beihilfe zum Aufbau des Markenwerts
- die Unernehmen reagieren mit folgenden maßnahmen: 1. sie errichten Informationsmonopole mit geisteigem Eigentumsrecht 2. sie versuchen "Am Rande des Chaos dahinzugleiten" zwischen erweitertem Angebot und sinkenden Preisen zu überleben 3. sie versuchen gesellschaftlich erzeugte Information für eigene zwecke zu benutzen
- und trotzdem aufstieg der Allmende Produktion ( ausnutzung von Open Source?)
- S. 195 " der kapitalismus beginnt sich in einen Verteidigungsmechanismus gegen die Peer Produktion zu verwandeln" durch Informationsmonopole, schwächung Lohnbeziehung , irrationale Nutzung von geschäftsmodellen die auf nutzung fossiler Energien beruhen
- Produktion und Güteraustausch außerhalb des Marktes nutzen die neigung der Menshcne zur Kooperation?
- technologischer Wandel verwischt die Grenzen zwischen Arbeit und Freizeit, an eignen von mehren ökonomischen Persöhnlichkeiten
- die technologie Ausrichtung dieser Revolution **widerspricht** ! der gesellschaftlichen Ausrichtung
- technologisch: auf dem Weg zu kostenlosen Gütern, nichtbessbare Arbeit, exponentielle Produktivitätszuwächse , umfassende Automatisiserung physikalisscher Prozesse
- gesellschaftlich gefangen in : einer welt von monopolen, Ineffizenz, , den Ruinen eines vom Finanzsektors beherrschten freien markts und der Ausbreitung von _bullshit_ jobs
- **==>**  _"der wesentliche innere Widerspruch des modernen Kapitalismus ist der zwischen der Möglichkeit kostenloser , im **Überfluss** vorhandener Allmende produkte und einem System System von Monopolen , Banken und Regierungen , die versuchen ihre Kontrolle über die Macht und die Informationen aufrecht zu erhalten. Es tobt ein Krieg zwischen Netzwerk und Hierarchie"_
- warum jetzt? weil Neoliberaismsus den Zyklus durchbrochen hat, es bedeutet das der 240 jährige Zyklus des Industrie kapitalismsus sich dem Ende nähert
- 2 richtungen möglich für die weitere Entwicklung: 
- entweder entsteht ein neuer kognitiver Kapitalismus und er stabilisiert sich
- oder das Netzwerk zerstört die Funktionsfähigkeit als auch Legitimität ! des Marksystems, wenn das geschieht wird ein Konflikt ausbrechen der das Marktsystem kollabierne läßt und den Postkapitalismus entstehen lässt
- verschiedene Formen des Postkapitalismus möglich
- wir wissen das wir im Postkapitalismus leben wenn: die Auflösung zwischen Arbeit und Freizeit sowie zwischen Arbeitszeit und Arbeitseinkommen **intitionionalisiert** ist ! ! ! --> die Frage ist wer diese Institution bauen wird :))
- Überfluss ist die Vorraussetzung für den Postkapitalismus
- während kapitalistische Gesellschaften stets über "Butter und Gewehre" nachdenken mussten müssen
- postkapitalistische über "wachstum und nachaltigkeit" nachdenken, oder über den Zeitrahmen für die ereitstellung grundlegender sozialer Güter etc..
- S. 197 wir brauchen Modelle für die Wirtschaft in der Transitionsphase
- sollten lehren von Fehlgeschlagenen transitionen Sowejt 1928 beachten
- Wir müssen verstehen welche Gesellschaftsgruppen interesse an der Transition hat, brauchen bessere Definition von Wert und detaiierte GEschichte der Arbeit
- **Untersuchung zwei kritischer Begriffe: _Arbeit_ und _Wert_ **
- **Was hat der Kapitalismus geschafft: in 240 Jahren Übergang von einem System basierend auf Knappheit bzw. Mangel hinzu einem System in dem zumindest die Grundbedürfnisse im Überfluss verfügbar sind durch den gesamtgesellschaftlichen Fortschritt**
- **Von Scarcity/Mangel hin zu Abundance/Überfluss** (Das beinflusst die Motivation!)

## 6. Auf dem Weg zur kostenlosen Maschine

- Proteste m Gez Park in Istanbul
- heute kaum noch knappe Dinge
- der Fortschritt von der Knappheit ist eine bedeutsame Entwicklung in der Geschichte der Menschheit
- Diese entwicklung stellt allerdings eine große Herrausforderung für die Wirtschaftstheorie dar 
- den er suggeriert das der PReismechanismus ein natürlicher Teil des Systems sei
- Das Problem von Angebot und nachfrage
- genauerer Blick auf die Arbeitswerttheorie, als Werkzeug um zu unersuchen wo in einer Wissenökonomie **Wert** erzeugt wird und wo er landet
- diese Theorie zeigt wie wir den Wert messen können in einer Volkswirtschaft in der Maschinen kostenlos gebaut werden können und ewig halten

### Die Arbeit ist die Quelle des Werts

- S. 201 Adam Smith:  die Arbeit ist die Quelle des Wohlstandes
- **klassische Arbeitswerttheorie** : sie besagt das die Arbeit , die eingesetzt werden muss , um ein Gut zu erzeugen , den Wert dieses Gut bestimmt
- in herkömmlichen Lehrbüchern ließt man: Im kapitalismus sei der "Wert" das gemeinsame Produkt von Löhnen , Arbeit und Boden ( falsch nach: John F. Henry - Adam Smith  and the theory of value : chaper six considered)
- **David Ricardo** Hauptwerk zur Arbeitswert Theorie, Aussage: Die Maschinen übetragen lediglich den WErt auf ihr Produkt , nur die Arbeit fügen neuen Wert hinzu
- Die Bedeutung der Maschinen besteht demnach darin das sie die Produktivität erhöhen
- S. 202 Ricardos Nachfolger machten die Theorie zur theoretischen Grundlage desndustriekapitalismjus
- Arbeiter intelektuelle: Wenn die Arbeit die Quelle des Wohlstands ist, dann muss die Frage nach der Vereilung legitim sein
- Volkwirtschaftslehe mitte des 19. jahrhunderts auf "Beschreibung und Zählung" reduziert

### Die Arbeitswertheorie in Zahlen

- Es wird erklärt wie die Lohnarbeit engeführt wurde und das alle im System die PRämissen akzeptieren, erinnert ein wenig an Chomsky
- S. 205 "Der Wert eines Wirtschaftgutes kann anhand des durchschnittlichen Arbeitsaufwande bestimmt werden, der notwenig ist um dieses Gut zu erzeugen"
- "Der Wert hängt **NICHT** von der tatsächlichen Zahl der Arbeitsstunden ab , sondern von den in einer Industrie oder Volkswirtschaft festgelegten _gesellschaftlich notwendigen_ Arbeitsstunden"
- zwei dinge tragen zum Wert eines wirtschaftsgut bei:
- 1. die im Produktionsprozess geleistete Arbeit (beinhaltet Marketing, Forschung, Entwicklung)
- 2. alles Ürbige was benötigt wird um waren herzustellen ( Maschinen, Anlagen, Rohstoffe, Transport)
- "die Arbeitswerthteorie  behandelt Maschinen, Energie und Rohstoffe als _vergegenständlichte_ Arbeit und überträgt ihren Wert auf das neue Produkt
- "tatsächlich" im Produktionprozess aufgewandete Arbeit bezeichnet Marx als "lebendige Arbeit"
- S. 206 "Preise weichen immer vom zugrundeligenden Wert der Dinge ab"
- "Aber wovon hängt der Wert der Arbeit ab? Vom Arbeitseinsatz anderer Menschen " (aha? - bereistellung Arbeitsumfeld)
- S. 207 " Woher kommt der Profit?" (wieder indirekte beantwortung ohne Fakten begründung :/)
- "Diese mangelnde Übereinstimmung zwischen In- und Outputs der menschlichen Arbeit ist der Kern der Theorie"
- "Wenn Nazma sechzig stunden pro Woche Arbeitet und dreißig stunden Arbeit anderer erforderlich sind unm sie zum Fabrik tor zu bringen[..]dann ist der Output ihrer arbeit doppelt so hoch wie der Input" (hier wird implizit angenommen das die erforderliche  _arbeit anderer_ ihren vollständigen Lohn darstellt )
- "Die Differenz streicht der Arbeitgeber ein. Das ist was marx den **Mehrwert** bezeichnet. Der Mehrwert ist die Quelle des Profits" 
- "Arbeit sit nicht einwach der Maßstab des Wertes, sondern die Quelle, aus der der Profit geschöpft wird"
- Ausnutzungsbeosipiele: amerikanisches gefängnissystem , nationalsozialistische Konzentrationslager (siehe schindlers Liste)
- S. 208 "Warum sollte ich sechzig stunden arbeiten wenn der reale , wöchentliche Wert eminer Arbeit dem von dreißig Arbeitsstunden anderer Menschen entspricht?"
- **-->** "Der Arbeitsmarkt ist nie frei. Er ist durch Zwang entstanden und wird täglich mit Gesetzen, Vorschriften, Verboten, geldbußen und **Angst vor Arbeitslosigkeit** stabilisiert"
- "Niemand sagt uns: du musst Schlittschulaufen oder die Gesellschaft wird zusammenbrechen. Die **Lohnarbeit** ist das Fundament des Systems. Wir akzeptieren sie..."
- S. 209 Minuten sind Goldwert Beispiele: Callcenter, Einsatz plan heimpflege, Lager Interneteinzel händler
- "Auf der Ebene wo hörhere Kenntnisse gefordert werden, wird der zwang nicht mit Zeitplänen oder Disziplin sondern mit Zielvorgaben und Qualitätskontrolle  ausgeübt " ( Janet 120% Auslastung min)

### Einige legitime Einwände...

- Frage: Warum Theorie? Reichen nicht zahlen wie BIP?
- Antwort: Erklärung komplexerer Vorgänge
- Frage: Man kann Wert, Mehrwert und Arbeitszeit nicht sehen? (sehen?? Arbeitszeit???)
- "Nur der Markt kann sagen welches die _gesellschaftlich_ notwendige Arbeitszeit ist" ?
- Theorie genauso abstrakt wie Adam Smith "unsichtbare Hand" 
- auch die Arbeitswerttheorie beschreibt zyklus der zum Zusammenbruch führt
- S. 211 Marx wollte Theorie die konkrete Realität beschreiben konnte
- hierfür entwickelte er ein (in **Das Kapital** )  auf zwei Sektoren (Konsum und Produktion) basierendes Wirtschaftsmodell im dritten band: Bankensystem
- **Marx versuchte zu zeigen wie die grundlegenden Werte auf der konkreten Ebene in Preise umgewandelt werden** <-- Das wäre tatsächlich ein simulierbares modell
- -> ein sogenanntes **Transformationsproblem** -- Wie entstehen aus Werten und Bedrüfnissen Koknrete Preise?!
- die theorie ist eungeeignet Preisbewegungen zu messen oder vorherzusagen, hilft aber beim Versträndnis dieser
- Einstein ziel der Wissenschaft: das Wesen der Realität in einer einfachen Aussage zusammenzufassen , die Zusammenhänge zwischen allen messbaren Daten in ihrer Gesamtheit zu efassen anhand einer möglichst geringen Zahl primärer Konzepte und Beiehungen
- Einstein weiterhin: die Grundkonzepte sind umso wieter von den Daten entfernt je klarer und logischer sie seien. Quelle [15 im kap 6]: Einstein - Physics and reality jornal of Franklin institute
- "Aber die Beziehung zwischen Theorie und ERfahrung kann nur _intuitiv_ verstanden werden" ...
- "Wir stehen also vor der Aufgabe **die Ungereihmtheit des Kapitalismus und die mangelnde Konkretheit des Marxismus** zu überwinden"

### Die Produktivität in der Arbeitswerttheorie

- "Die Arbeitswert theorie besagt das die Produktivität auf zwei Arten erhöht werden kann"
- 1. Arbeitskräfte besser ausbilden
- 2. Durch einführung neuer Maschinen, Reorganisation des Produktionsprozesses
- Tendenz der fallenden Profitrate durch Innovation?
- Was bedeutet es wenn dinge "normal" werden? --> **Sättigung** , Rogers Bell Curve
- Mainstream-Ökonomie Preistheorie: Grenznutzentheorie

### Die Abkehr von der Zukunft

- Grenznutzentheorie: Nichts an sich hat wert, Wert hängt davon ab was käufer in einem bestimmten Augenblick ist bereit zu zahlen
- **Leon Walras** einer der Begründer Grenzkosten schule
- **"Marginal"** bedeutet einfach das der Wert nicht im ganz allgemeinen Nutzen eines Produktes steckt sondern in dem Nutzen einer zusätzlichen einheit
- **William Stanley Jevons** englischer Vertreter der Theorie: Der Wert konnte als "Tauschverhältnis" ausgedrückt werden , er schlug vor **vollkommen** auf den Begriff "Wert" zu verzichten
- S. 217 "Auf den Ersten Blick scheint es so als wollten die Vertreter der Grenznutzenschule die Öknonomie aus de rUmklammerung der Philosophie befreien."
- "Walras erklärte man könne den Kapitalismus nicht mit dem Argument verteidigen das er _natürlich_ sei."
- "Die Grenznutzentheorie enthält jeodch eine ideologische Aussage: sie behauptet der Markt sei _rational_"
- "Walras hielt den Gedanken nicht aus,dass die wirtschaftlichen Gesetzmäßigkeiten unabhängig vom menschlichen Willen wirkten" 
- **<<-** tatsächlich ist dies aber so da wir als menschen veerschiedene kognitive verzerrungen haben, deren wir nicht immer vollständig bewußt sind, wenn man diese noch mit der kompexität menschlicher Interaktionen in der masse kombiniert, entsteht die unberechenbare komplexität der "wirtschaftlichen" realität
- seien (walras) Theorie ging vom Rationalen willen des menschlichen verstandes aus  ( :))))) Das sagt schon alles
- interessanter nebengedanke: vielleicht mußte de rMensch erst auf die großen perspektiven der menschlichen Wirtschaft blikchen um seine eigene irrationalität besser zu begreifen
- "Die Grenznutzentheorie leistet einen bedeutetnden beitrag: das Märkte auf denen ein freier und vollkommener Wettbewerb herrscht, ein _Gleichgewicht_ finden werden"
- **==>** Faktoren die Walras nicht berücksichtigte: sein modell beruhre darauf das alle akteuere vollkommene über Informationen verfügen, keine Unsicherheit bezüglich Zukunft UND das de rMarkt nicht von äußeren Faktoren ( Monopole, Gewerkschaften, Einfuhrzölle) beeinflusst wird
- Grenznutzentheorie schlechter Umgang mit Krisen, sie behauptet Krisen seien Ergebnis nicht wirtschaftlicher Faktoren
- S. 218 "Die heutige Volkwirtschaftslehre beruht auf den Entdeckungen der Grenznutzenlehre"
- Klassen werden ignoriert
- die Theorie entstand weil etwas benötigt wurde was über Buchhaltung hinaus gehen sollte jedoch aber keine Geschichte mit einbeziehen sollte
- **Carl Menger** Im Gegenaufsatz zur Grenznutzentheorie: nur auf abstrakten Menschen bedacht, Ziel der Ökonomie müsste es sein: die Realität zu untersuchen die der Kapitalismus hervorbringt und gegen eine **einseitige rationalistische Neuerungssucht** zu verteidigen ...
- gegenwärtige Vertreter der Grenzkostenschule fixiert auf alle Formen des Kapitalismus die sich nicht verändern und nicht sterben

### Warum das bedeutsam ist

- " **Steve Keen** (ja der von ericT ) meint die gegenwärtige Grenznutzentheorie habe zum Zusammenbruch beigetragen, in dem sie alles auf die Doktrin  der _effizienten Märkte_ reduziert habe"
- "Die Mainstreamökonomie habe <<eine bereits erschütterte Gesellschaft noch schlimmer gemacht: ungleicher, instabiler und wenige reffizient>>" Quelle: Steve Keen debunking economics
- Weiteres Problem: Informationsgüter stellen die Grenznutzenthoerie infrage, die theorie beruht auf Knappheit, Information ist jedoch im Überfluss vorhanden
- Walras sagte: "Es gibt keine Produkte die unbegrenzt vervielfacht werden können. Alle Dinge die Teil des sozialen Wohlstands sind [..] existieren in begrenzter Menge"
- Informationsgüter existieren in potentiell unbegrenzter Menge, dies läßt auch die Grenzkosten physischer güter z.b. Bandbreite und Datenspeicher gegen null gehen
- in der gegenwärtigen Volkswirtschaft gibt es knappe als auch im Überfluss vorhandene güter 
- Teilen und Kooperation als Sabotageakt in der GRenznutzenschule
- wenn die Anzhal der Inhalte mit entsprechender CC Lizenz einen kritischen anteil in der Nutzung einer kritischen Masse von Leuten in der Gesellschaft erreicht funktionieren die GRenzkosten nicht mehr, bzw. die Preisbildung
- "Die Marxsche Arbeitswerttheorie prognostiziert, dass die Automatisierung den notwendigen Arbeitseinsatz derart verringert das die Arbeit(menschliche?!) **optional** werden kann"

### Karl Marx und die Informationsmaschinen

- Marx - Wertgesetz: Der Preis jedes Wirtschaftguts spiegelt die Gesamtmenge der Arbeit wider, die aufgewendet wurde, um es zu erzeugen
- "In der Praxis verhinder der Kapitalismus, dass die Innovation den Arbeitsgehalt der Wirtschaft verringert( Profit runter) in dem neue Märkte/Industrien erschaffen in denen die Arbeitskosten hoch sind damit Arbeitskräfte genügend Geld verdienen um die erzeugten Güter zu konsumieren"
- S. 222 "die kosten für Wartung und Aktualisierung von Software sinken dramatisch" aha dafür hätte ich gerne eine Quelle, in der Realität sind wir immer sehr nah an der Grenze dabei an der Komplexität der eigenen Software zu ersticken
- er bringt sinktende Hardwarekosten als Beispiel, bringt noch ein Beispiel das auch die Information "abwärme" erzeugt und irgendwo physikalisch hinterlegt ist
- S. 223 "Die Information tut alles, was eine Maschine tut: Sie ersetzt billige ungelernte durch qualifizierte Arbetiskräfte. sie beseiteigt das Erfordernis von Arbeit in manchen Tätigkeiten vollkommen, ermöglicht neue Tätigkeiten die keine frührere vom bewältigt hätte"
- "Informationsfügter nutzen auf natürliche Art und Weise die allgemeinen wissenschaftlichen Erkenntnise"
- "Die Nutzer speisen in Echtzeit (ha?) kostenlose (haha?) Daten ein, mit denen diese Informationsgüter verbessert werden können"
- "Jede an einem beliebigen Ort vorgenommene Verbeserung kann augenblicklich (siehe _echtzeit_) in jeder Maschine an jedem Ort der Welt (der Internetzugang besitzt) übernommen werden"
- Marx in Grundrisse: "Führt man den Gedaanken zu Ende, so wäre es ideal, eine Maschine zu haben , die sich nie abnutzt oder kostenlos ersetzt werden kann"
- "Könnte das Kapital das Produktionsinstrument ohne Kosten, für 0 haben, was wäre die Folge" der Merhwert würde erhöht "ohne das es dem Kapital das geringste kostete"
- zwei Beispiele hierfür von Marx aus dem 19. Jahrhundert: 
- 1. durch Neuanordnung der Arbeitsabläufe
- 2. durch wissenschaftlichen Fortschritt
- "Maschinen, bei denen Teile des Werts kostenlose Inputs an geselschaftlichem Wissen und wissenschaftlichen Erkenntnissen sind, die öffentlich zur Verfügung stehen, sin der Arbeitswerttheorie nicht fremd"
- Mason simplifiziert an dieser Stelle deutlich die moderne Chipherstellung ist extrem aufwendig, also ist die moderne Hardware in keinem Fall _kostenlos_ , sie rechnet sich lediglich dadurch das die Verkaufszahlen sehr hoch sind und der produktionsprozess optimiert ist
- er übersieht auch Phänomene wie die ansteigende "geplante Obsolenz" im heutigen System, die aber auch durchaus die Symptome des jetztigen Systems darstellen könnte :)
- S. 225 "Natürlich halten physische Maschinen in der Realität noch nicht ewig. [..] Und nur die Arbeitswertthorie kann richtig beschreiben, was es wirtschaftlich bedeutet, wenn die werlt der materiellen Objekte von Informationen belebt wird"

### Wenn Maschinen denken

- Metallpresse Geschichte 1981 und heute - fast (3D Druck??) vollständig automatisiert
- In der Metallverarbeitung hat also eine REvolution stattgefunden, doch niemand hat sich theroetisch ( wirtschafts) damit auseinandergesetzt
- S. 226 "Der Grunddafür ist, dass niemand weiß, wie der Wert der Information wirtschaftlich gemessen werden kann."
- Aber Arbeitswerttheorie, Software als Maschine und Daten als Werkzeuge oder Werkstücke ( oh je aber Software sind auch nur daten... das weiß er wohl nicht..)

### Kostenlose maschinen in einer gelenkten Volkswirtschaft

- S. 227 "Wir haben gesehen was geschieht, wenn man Produkte mit Null-Grenzkosten in das Preismodell einführt: es zerfällt"
- jetzt: ansehen was passiert wenn kostenlose Maschinen in den Zyklus von Kapitalinvetionen einführt
- ==> sein null kosten beosiel sollte eigentlich anschaulich sein... ist es aber nicht, Spalten und veränderungen ??? Warum keine Tabellen im Text zur veranschaulichung?

### Wie würde der Informationskapitalismus aussehen?

-  der Aufstieg der kostenlosen Informationen und freien Maschinen ist etwas neues, Verbilligung der Inputs durch Poduktivitätssteigerungen bereits bekannt
-  Kapitalismus überlebt durch Erschaffung neuer Märkte und Bedürfnisse
-  S. 231 für Realisierung Informationskapitalismus müßte folgendes geschehen: Der Preisrückgang der Informationsgüter müßte gebremst werden
-  noch stärkere Monopol bildung wäre notwendig, z.b. durhc verknappung von Rohstoffen/Physikalischen Güter/Energie
-  neue Märkte in den Dienstleistungen
-  es Müssten neue Arbeitsplätze für die Millionen von menschen die durch die Automatisierung ihre Arbeitsplätze verlieren , geschaffen werden
-  **das menschliche leben müßte komplexer werden**
-  S. 232 Grundlagen dafür bereits heute vorhanden: Apple und Amazon heute berits auf dem WEg dahin
-  Informaiton als Technologie ist jedoch anders, Veränderungen auf der Meta ebene ( Einfluss auf alle Gesellschaftsbereiche- Null Preis Dynamik)
-  Technologie Verbieten wie in **Dune** ??
-  Oxford 47% Studie
-  S.233 "Doch um den Verlust der Hälfte aller Arbeitsplätze auszugleichen , müsste das menschliche Arbeitsleben umfassend kommerzialisiert werden(IoT, Blockchain??"
-  ==> Masons gegenbeispiel ist schwach , er kann sich einfach die neuen Formen der Dienstleistungen nicht vorstellen, es gibt aber durhcaus Szenarien (Full scale individual vs environ tracking?)
- S. 234 "Der Widerstand gegen die Kommerzialisierung müßte unter Strafe gestellt werden" <<-- ist das nicht bereits so??  Kostenlos küssen?? -_- was für ein schlechtes Beispiel...
- **Mason sagt**: _"Eine auf Wissen beruhende Volkswirtschaft kann aufgrund ihrer Tendenz zu kostenlosen Produkten und schwachen Eigentumsrechten (Blockchain Eigentums rechte für die digitale welt) keine kapitalistische Volkswirtschaft mehr sein"_
- S. 235 die Arbeitswerttheorie berücksichtigt das (sagt mason) und definiert  **Ziele** für den Übergang ( zum Postkapitalismus): eine Welt in der Maschinen nichts kosten, der Preis grundlegender Güter bei null liegt und notwendige Arbeitszeit auf ein minimum reduziert wird


## 7. Wunderbare Störenfriede

- 1980 auflösung der Arbeiterklasse/Solidarität
- S. 238 das Proletariat wird nicht die gesellschaftliche Entwicklung bis zu Überwindung des Kapitalismus vorrantreiben
- zusammenhalt wie in den westlichen Ländern kann im globalen Süden nicht entstehen
- S. 239 "Die Arbeit, die definierende Aktivität im Kapitalismus, verliert ihre zentrale Bedeutung für die Ausbeutung aud der einen und den Widerstand auf der anderen Seite"
- die Spähre _jeinseits_ der Arbeit ist nun das wichtigste Schlachtfeld
- S. 240 "Wie wir gesehen haben, verdrängt die Informationstechnologie Arbeitskräfte aus der Produktion, sie zerstört die Preisbildungsmechanismen und begünstigt den wirtschaftlichen Austausch abseits des Marktes."
- "Und irgendwann wird sie die Verbindung zwischen Arbeit und Wert endgültig kappen" <- das mag zwar wie ein phrase klingen aber es passt zur Analyse hararis: das es im 21. Jahrhundert darum geht Intelligenz von Bewußtsein zu entkoppeln, körper und gehirne zu bauen...
-  was folgt der Arbeiterklasse?
-  die Gruppe ( welche dem Proletariat "folgt") von menschen kämpft und verkörpert "ähnliche" von der Technologie bestimmte Werte
-  S. 241 "Der Marxismus hat sich in Bezug auf die Arbeiterklasse getäuscht ...Die Erfahrung der letzten 200 Jahre hat gezeigt das es dem Proletariat nicht darum ging den Kapitalismus zu stürzen. Vielmehr wollte es _trotz_ des Kapitalismus leben"
- linke ist voll von Rechtfertigungen für die seit 200 Jahren andauernde Geschichte von Niederlagen
- alle Erklärungen laufen auf: ungünstige Bedingungen oder unfähige Führer , hinaus
- Strategie der Arbeiterklasse war: "die andauernde Bedeutung von Fähigkeiten, Autonomie und Status im Leben der Arbeiter"
- "Haben wir einmal verstanden, was in den vier langen Zyklen des Industriekapitalismus wirklich mit der Arbeit geschah, wird die Bedeutung ihrer Wandlung im fünften Zyklus klar"
- **"Die Informationstechnologie macht die Abschaffung der Arbeit möglich"**
- "Verhindert wir ddies nur durch die gesellschaftliche Struktur, die wir als Kapitalismus kennen"

### 1771 - 1848 Die Fabrik als Schlachtfeld

- S.242 1771 erste Fabrik in Cromford
- Arbeiter durften nicht länger innerer Uhr folgen sondern mußten sich nach der Uhr des Arbeitgebers richtgen
- 13h Arbeitstag
- 1818 Aufschwung endet im ersten langen Zyklus , organisierte Massenstreiks
- Marx und Engels fanden in der Arbeiterklasse eine passende Lösung für ein philosophisches Problem
- die linken angehörigen des deutschen Bürgertums waren begeisterte Kommunisten träumten von: klassenlose Gesellschaft, ohne Eigentum, ohne Religion und ohne Arbeit
- Marx sieht die Arbeiterklasse als Kraft, welche diese Gesellschaft verwirklichen kann
- S. 243 Zusammenfassung der Beziehung des Proletariats zum Schicksal der Gesellschaft: es ist kompliziert
- #tags:  #gabor_mate #interview #russel_brand
- S. 243 Peterloo-Massaker , ein dutzend Tote , markierte den Beginn der organisierten Arbeiterbewegung
- die Arbeiter sammeln sich als Arbeiterklasse, Bildung, politische
- bereits damals soziale Probleme mit technologischer Innovation angehen(selbsttätige Spinnmaschine)
- "Die Geschichte der Arbeit kann nicht auf _Ökonomie plus Technologie_ reduziert werden" gesellschaftliche/solziale Aspekte welche mit Technologie/Ökonomie in Wechselwirkung stehen sind nicht vernachlässigbar
- Engel Studie: Die Lage der arbeitenden Klasse in England (1845)
- Engels als Anthropologe
- "Die automatisierung festigte am Ende die Position der qualifizierten männlichen Spinner und schuf neue Arbeitsplätze für sie"
- S. 245 " Marx glaubte ,die Arbeiter würden das Eigentum abschaffen, weil sie selbst keines hatten. Sie würden die Klassentrennung beseitigen, weil sie davon nicht profitieren konnten. Und beides würden sie tun, ohne innerhalb des alten Systems ein alternatives Wirtschaftssystem errichten zu müssen"
- **==>** Masons these: dadurch das die Arbeiterklasse sich **IM** Kapitalismus eine eigene Kultur schaffen konnte sank ihre Motivation eine andere Gesellschaftsform als den Kapitalismus(Kommunismus) zu etablieren
- "Engels anthropologische Studie zur englischen Arbeiterklasse ... ist detailiert,komplex und spezifisch. Die marxistische Theorie über das Proletariat ist es nicht "

### 1848 - 1898 Menschen gegen Maschinen

- S. 247 Engels Buch (1892) über die Arbeiterklasse mit Vorwort von Engels in dem er einräumte sein Werk sei längst überholt
- 1848 von qualifizierten Arbeitern geführten Gewerkschaften blieben bestehen
- "der autonome Facharbeiter war bald die Norm. Radikalismus und utopischer Sozialismus wurden an den Rand gedrängt"
- 1848 erkannte Engels das "eine neue industrielle Ära" begonnen hatte, dies beschrieb Kondratjew später als zweiten langen Zyklus 
- Engels bemerkt ein wesentliches merkmal des technologischen Paradigmas dieser Ära: die Kooperation zwischen Arbeiterschaft und Kapital
- Methoden von Oliver Twist waren nicht mehr nötig
- Arbeitstag wurde auf 10 Stunden verkürzt
- S. 248 "nach Ansicht von Engels waren die Arbeiter gemäßigter geworden, weil sie an de rimperialen Macht Großbritanniens beteiligt wurden" **!**
- "Den Wettbewerbsvorteil Großbritanniens hielt er für vorrübergehend" <= richtig, hello Brexit!
- "Aber sobald die Welt aufholte, würde die Mäßigung ein Ende haben"
- Faßbinder autonomie und Fallstudien details
- bis zur Abschwungphase des zweiten langen  Zyklus 1870-1890

### 1898 - 1948 Hebt einen Barren auf und geht damit

- Taylor zerlegung der Arbeit in einzelne Schritte um qualifikation für Teilaufgaben zu senken
- S.250/251 ">>Ein Mann, der sich in dem Beruf eines Roheisenverladers aud die Dauer wohlfühlt, muß natürlich geistig sehr tief stehen und recht gleichgültig sein. Ein aufgeweckter intelligenter Mann ist deshalb ganz ungeeignet zu einer Arbeit von solch zerreibender Eintönigkeit<<"
- "Diese Erkenntnis wurde zur Grundlage der wissenschaftlichen Betriebsführung" 

### Lenin und die Aristokraten

- S. 253 Marx sieht das Proletariat als eigenständigen (historischen) Agenten (um sich aus ihrer Lage zu befreien) während hingegen Lenin eher als Re-Agenten sieht der noch auf externe Hilfe (revolutionäre avantgarde) angewiesen ist
- " die revolutionären Sozialisten müssten die Arbeiterbewegung von ihren kurzfristigen Orientierungen und ihrem gemäßigten Weg _abbringen_"
- warum es nicht das Proletariat sein wird, das Konzept von Klassenzusammenhalt zerfällt, die Abschaffung der Arbeit wird möglich, der Letze Satz/Absatz
- S. 254 Lenin macht die Arbeiteraristrokratie für Patriotismus (1914) und Mäßigung verantwortlich
- "zum Glück gebe es ein großes Reservor an ungelernten Arbetiern diese würden das _rohmaterial_ für die Revolution liefern"
- die Spaltung zwischen Reform und Revolution sei Ausdruck der Spaltung der Arbeiterklasse - Lenin

### Eine furchtbare Schönheit: 1916-39

- S. 255 "am 1. Mai 1916 streikten die Berliner Fabrikarbeiter gegen den Krieg..."
- Obmann des Arbeitterausschusses wurde von Belegschaft gewählt und war unabhängig von Gewekschaftsführungen die den Krieg befürworteten
- verhaftung Clyde Workers Comittee ".., deren Ziel es war, die Rüstungsindustrie unter die Kontrolle der Arbeiter zu bringen"
- S. 256 Februar 1917 Streikwelle in den Waffenfabriken von Petrograd, erfasste ganze Russland, zwang den Zar zur Abdankung
- Kondratjew wurde kurzzeitig stellvertretender Minister für Nahrungsmittelversorgung
- Telegraf, Telefon und Militarfunk verbreiteten Nachrichten von den Erhebungen
- Im Mai 1917 meuterte die französische Armee, nur noch 49 von 113 Divisionen einsatzfähig
- "Da die Männer zum Kriegsdienst eingezogen wurden, ... füllten sich Werften und Fabriken .. mit Frauen und Jugendlichen" 
- "Die Gewerkschaften unterstützen den Krieg und lehnten Streiks ab"
- Die Figur des Obmanns des Arbeiterausschusses
- Als die Revolutionen ausbrachen, stellten die Obmänner die Führung der Basisbewegung
- "In keinem Programm der Bolschewiki war je die Rede davon gewesen, den Arbeitern die Kontrolle zu übertragen. Lemom fürchtete sich vor der Arbeiterselbstverwaltung"
- zusammenbruch deutsches Reich durhc Arbeiterklasse 
- im November1918 organisierten Linkssozialisten eine Meuterei in der Kriegsmarine, die Teile der Flotte innerhalb von 24 Stunden zur Rückkehr in die Häfen zwang
- tausende Seeleute zogen bewaffnet durch Deutschland
- innerhalb weniger Tage nach Beginn der Meuterei hatten sie die Ausrufung der Republik und die Unterzeichnung eines Waffenstillstandes durchgesetzt
- 1919 Massenausstand Italien
- S. 258 Facharbeiter strebten Kontrolle über ihren Arbeitsplatz an und wollten eine Parallelgesellschaft innerhalb des Kapitalismus ( Reform ansatz)
- Obmänner der Arbeiterausschüsse erkennen Notwendigkeit für langfristige Strategien
- Fraktionsbildung innerhalb der meisten kommunistischen Parteien ( Leninisten, Gewerkschaftsobmänner)
- Lenin hatte recht das der revolutionäre Kommunismus nicht die spontane Ideologie der Arbeiter war
- Forderungen der Arbeiter konnten nicht erfüllt werden, New Yorker Börsenkrach 1929
- zerstörung der deutschen Arbeiterbewegung durch den Nationalsozialismus => Die Antowrt des deutschen Kapitalismus auf die macht de rorganisierten Arbeiterklasse
- 1933 wurden die Gewerkschaften aufgelöst und die linken Parteien zerschlagen, 1934 Östereich
- 1936-1939 Franco führt totalen Krieg gegen die Arbeiterschaft und radikale Bauern
- 1936 Metaxas Diktatur verbietet sozialistische Parteien und Gewerkschaften(sogar Musik)
- In Polen, Ungarn, baltischen Ländern wurde die Arbeiterbewgung( einschließlich der jüdischen) zunächst unterdrück und später im Holocaust ausgelöscht
- Nur in Großbritannien(Empire), Frankreich und den USA 

### Das Massaker an den Hoffnungen

- in russischen Gulag wurden 1,4 Millionen Menschen festgehalten, NS KZ 6 Mio Juden ermordet, 3,3 Mio russische Kriegsgefangene, spanischer Bürgerkrieg ca. 350000
- In Spanien  wurden die Gewerkschaften, Kooperativen und Milizen der Linken durch Massenmord zerstört
- In sowietunion Gulag und Massenhinrichtungen aktivisten de rArbeiterklasse ausgerottet
- S. 261 "Abschlachten der politisierten Arbeiter"
- Menschen und Hoffnungen wurden massakriert
- Klassenkampf wird in den Hintergrund gedrängt , vor dem Überlebenskampf

### 1948-89: Die Arbeit wird "absurd"

- die globale Erhebung von 197-21 wiederholte sich nicht
- die furcht vor einer Wiederholung zwang den Kapitalismus zur anpassung
- S. 262 "Dazu kam, das die Alliierten den unterlegenen Kriegsgegnern Italien, Deutschland und Japan tatsächlich Sozialsysteme aufzwangen um ihre Elite zu bestrafen..."
- Wirtschaftswachtum in den fünfzigern beschlunigt, höhere belastung von ober und mittelschicht ermögichte finanzierung von Gesundheitssystem und Sozialleistung
- im gegenzug gaben Arbeiter die Ideologie des Widerstandes auf => Koexistenz mit dem Kapitalismus
- Daniel Bell 1955 das Proletariat ersetzt durch "Salariat"
- Unterschied Angestellter - Arbeiter?
- Arbeiterkultur  der nachkriegszeit konnte nie _humanismus der Arbeit_ hervorbringen
- Arbeiterklasse wuchs, Entstehung neuer schichten, Automatisierung und dezentralisierung langfristigepsychologische Veränerungen
- Fiat arbeiter beschreiben arbeit als _absurd_ , das ende der manuellen arbeit absehbar
- S. 265 "Fast unmerklich änderte sich die Antwort auf eine grundlegende Frage: was bedeutetes es ein arbeiter zu sein?"
- **" Gorz erklärte, die Masse der Arbeiter orientierte sich nicht mehr an der utopie der Arbeiter macht, sondern an der Utopie, das sie aufhören könnten arbeiter zu sein"**
- Akzent weniger Befreiung _in_ der Arbeit als Befreiung _von_ der Arbeit

### 1967-76 Das heiße Jahrzehnt

- S. 266 67 westlicher Kapitalismus gestört
- persöhnliche Geschichte zur Zeit vor dem zweiten Weltkrieg
- Die Arbeiterwelt (in England) war wirklich distanzier vom der Bürgerlichen (medien- Radio - Zeitung) Welt
- zwei Quadrat kilometer
- S. 268 1957 Richard Hoggart  Studie - The use of Literacy , Arbeiter klasse erfährt Information, Logik und die Fähigkeit alles infrage zustellen
- 1960 england anti baby pillee, frauen drängen auf en arbeitsmarkt
- S. 269 "Schließlichlich wurden wurden die Arbeiter durch das Wissen gebremst, dass die Revolution der zwanziger und dreißiger Jahre gescheitert waren  und dass der Faschissmus nur mit Unterstützung des demokratischen Kapitalismus besiegt worden war" ... naja

### Italien: Eine neue Art von Kontrolle

- "Bis 1967 lockte das italienische Wirtschaftswunder millionen Arbeitskräfte ... in die Industriezntren des Nordens"
- zwischen 1950-1960 waren Reallöhne um 15% gestiegen
- große Industrieunternehmen investierten : Kantienen, Sportvereine und soziale Clubs, Sozialfonds
- steigendes Arbeitseinkommen und schlechte Lebensbedingungen außerhalb er Fabriken war die erste Auswirkung des Aufschwungs
- 1968 450000 Studenten italienische Hochschulen, doppelt so viele wie 10 jahre zuvor, die meisten mittelslos aus Arbeiterfamilien
- sie fanden an den Hochschulen nutzlose Lehrbücher und archaische Regeln vor
- "Indem man ein mit deratigen Mängeln behaftetes Hochschulsystem frei zugänglich machte, deponierte man eine Zeitbombe an den Universitäten"
- ende 1967 besetzen von Universitäten , 69 Streikwelle
- S. 271 slogan bei einem Aufmarsch 1969: "Was wir wollen? Alles!" ... klingt sehr reflektiert...
- "sie beginnen sich langsam zu befreien. Sie zerstören die bestehende Autorität der Fabrik"
- Zusammenbruch des Keynesianismus in diesem zusammenhang
- "Als der **Ölpreis** im Jahr **1973** plötzlich in die Höhe schoss und eine zweistellige Inflation verursachte, **löste sich der Zusammenhang zwischen Arbeitseinkommen, Preisen und Produktivität einfach auf"
- S. 271 50-60er Sozialeinkommen bei ca 8% des BIP, Mitte der 70er auf 16% BIP angestiegen
- Staatsausgaben 50-60er bei 28% BIP -> 70er 41%, Anteil der Profite der Industrie/ Dienstleistungen filen  um 24%
- Arbeiterklasse Einbindung in Regierungsarbeit
- Italien, spanien, GB -> "Social Contract" Kompromisse
- Arbeiter gewinnen wieder an Einfluss
- Ölkrise 1979: Massenarbeitslosigkeit, Zerschlagung ganzer branchen, Lohnsenkungen, Einschnitte öffentliche Ausgben
- _ein Teil der Arbeiter war bereit mit  konservativen Politiker gemeinsame Sache zu machen_
- US: Ronald Regan, Gb: Margaret Thatcher
- S. 273 1979 konservative Teile der Arbeiterschaft verliert Vertrauen in Keysinusmus
- Mitte der 80er erstmals Verlagerung der Produktion in Billiglohnländer
- Gewerkschaften schwächer, _unablässiger Krieg mit der Linken_

### Digitale Rebellen, Analoge Sklaven

- Arbeiterklasse hat überlebt sich gewandelt
- in den Industrie Ländern Kern-Perepherie Modell
- Qualifiziert-Ungelernt
- Flexibilität gefragt. S. 274 **"wird großer Wert auf die Fähigkeit gelegt, sich ständig neu zu erfinden, sich den kurzfristigen Unternehmenszieloen anzupassen"**
- "müssen sie in der Lage sein einen automatisierten Ablauf rasch zu erlernen.. Bsp. **Heimpflege, strikte Checkliste 15 min**"
- _erzwungenes Lächeln und Anfassen_
- Außer in den Exportnationen Dienstleistungen mehr anteil als Industrie
- S. 274 der Anteil der Arbeitseinkommen am globalen BIP schrumpft 19070 -> 53%, "heute" 44%
- S. 275 hat zur folge "finanzialisiertes" Verhalten der Arbeitskräfte
- dies fällt schwächer bei exportorientierten Wirtschaften aus
- "Viele Arbeitnehmer, kommen eher durch Konsum und Kreditaufnahme als durch Arbeit in Berührung mit Kapital"
- **Abmachung lautet: zu Haus eMails, auf Reisen arbeiten, Überstunden, außerhalb der Dienstzeiten arbeiten um _die Ziele der Firma_ zu erreichen**
- "der hochwertige mitarbeiter wird im Grunde dafür bezahlt, dass er existiert und seine Ideen beiträgt, um die Ziele der Firma zu erreichen"
- pendeln (länger) wird normal
- S. 276 "Für die jüngeren, die in unsicheren Arbeitsverhältnissen leben, gewinnt stattdessen die phsische Nähe im urbanen Raum an Bedeutung. Sie sammeln sich in den Stadtzentren und akzeptieren einen deutlich eingeschränkten Lebensraum, weil sie ein Netz von Kontakten brauchen in dem sie Partner , sporadische Arbeitsverhältnisse und Unterhaltung finden können.""
- "Ein neuer Arbeiter entsteht, wenn die Arbeit _Bindungslosigkeit_ und oberflächliches Engagement belohnt", Richard Sennet
- **Der neue Arbeiter konzentriert sich im privaten _und_ beruflichen Leben auf _den kurzfristigen Erfolg_**
- dieser fühlt sich weder Hierarchien und Strukturen noch als Arbeitskraft und Aktivist verpflichet
- Sennet und Wellman: Ausprägung mehrerer Identitäten nimmt zu
- S. 277 Arbeiter in der keynesianischen Ära: Arbeitsplatz, Stammkneipe, Arbeiterverein, Fussball
- neuer Arbeiter: führt in der Arbeit, in zahlreichen bruchstückhaften Subkulturen und im Internet mehere parallele Leben
- Wie sieht die Zukunft der Arbeiterklasse aus? ==> die frage wird NICHT beantwortet
- S. 278 "Die Sozioloagen studierten die jungen (chinesichen) migranten und stellten fest, das das sie das Internet für zwei DInge nutzen: Sie knüpfen Kontakte zu anderen Arbeitern aus ihren Heimatstädten und ließen bei computerspielen Dampf ab"
- Erweriterung der Perspektive , jenseits von Bauhof, Fabrik und Wohnheim
- S. 279 **"Wenn man davon ausgeht, dass die wichtigste Verwerfungslinie in der modernen Welt jene zwischen Netzwerken und Hierarchien ist **, dann verläuft diese Line genau durch China ""
- digitale Rebelen, analoge Sklaven
- ein nues historisches Subjekt: die vernetzte Menscheit
- " Die vielfältigen Interessen dieser Gruppe fließen zu dem Bedürfniss zusammen den Postkapitalismus herbeizuführen"
- "In diesem System wird viel kollaborativ und kostenlos für die gemeinsame Nutzung produziert"
- S. 280 **" Der neoliberalismus kann Ihnen nur stagnierendes Wachstum und den Staatsbankrott offerieren "**
- **"über ihre Köpfehinweg wird die Politik in  vielen Ländern von einer kleptokratischen Mafia gesteuert..."**

# Teil 3

## 8. Transitionen

- S. 283 es gab eine Zeit vor dem Kapitalismus, so kann es eine Zeit danach geben? Wie funktionieren Transitionen?
- IT hat die tragenenden Säulen des Kapitalismus untergraben: Preise, Eigentum und Arbeitseinkommen
- zwei Betrachtungen: der Aufstieg des Kapitalismmus und der Zusammenbruch der Sovietunion, was kann aus diesen beiden Prozessen gelernt werden und wie könnte ein Projekt "zur überwindung des Kapitalismus" aussehen

### Ein Bolschewik auf dem Mars

- S. 284 Alexander Bogdanow - Der rote Planet 1909
- "Da es keinen Mangel an Gütern gibt wird die Nachfrage nicht gemesen"
- Bogdanow wollte eine Wissensgesellschaft
- S. 285 "Er sagte vorraus, die geistige Arbeit werde diekörperliche ersetzen und alle Arbeit werde technologisch werden"
- "Wenn es einmal so weit sei, werde unser Verständnis der Welt über die dialektische Methode hinausgehen müssen, die Marx von Hegel übernommen hatte"
- "Die Wissenschaft werde an die Stelle der  Philosophie treten und die Menscheit werde lernen, die Realität als untereinander verbundene _Erfahrungsnetze_ zu betrachten"
- "Verschiedene Disziplinen würden in einer _universellen Organisationswissenschaft_ aufgeben, das heißt im Studium von Systemen"
- der Kommunismus im Roman basiert auf Überfluss, .. einer transparenten Echtzeitberechnung der Nachfrage(sollte diese nicht egal sein?)
- weitere Eigenschaften des Kommunismus im Roman: friedlicher übergang über 100 jahre, Arbeit schrittweise reduktion auf 0
- die technologische Reife als Vorbedingung!
- nicht nur die Wirtschaft, sondern auch der Mensch müse sich verändern ( z.b. das Geschlecht ;) )
- nachhaltige Resourcen nutzung, extrem: marsianer begehen selbstmord bei drohender Überbevölkerung

### Der russische Alptraum

- S. 288 1918-21 Banken und wichtige industrieen verstaatlicht, Folge Produktion viel auf ein fünftel des Vorkriegsniveaus, Hungersnöte, Rubel verlor rasch an wert (Intern oder Extern?)
- Konflikt zwischen drei inner Parteilichen (Kommunistische Partei) Strömungen:
1. von Trotzki geführte linke Opposition mehr Demokratie und Planung
2. von Bucharin geführt, für mehr Marktwirtschaft und eine verzögerte Industrialisierung
3. von Stalin geführte Gruppe welche die Interessen der Bürokratie verteidigte
- Stalin versetzte Konkurenten ins Exil
- innerhalb von drei Jahren starben etwa 8 Millionen Menschen
- durch allgemeine Resourcenknappheit konnte stalin Fortschritte nur durch brutale Umverteilung erreichen: vom Land zur Industrie, von der Konsum zur Schwerindustrie
- Industriealisierungsziele wurden erreicht aber Preis dafür waren: Hungersnöte, Massenexekutionen und Sklavenarbeit in vielen Wirtschaftssektoren
- S. 290 zwischen 1928 und 1985 wuchs soviet Wirtschaft pro Jahr um 4,2% laut RAND Studie
- die Studie zeigte das jedoch nur ein viertel davon auf technologischen Verbesserungen basierte, stattdessen wurde auf erhötem Input bei Maschinen, Rohstoffen und Energie gesetzt
- ab 1970 stagnierte die Produktivität
- die inneren Probleme hätten mittelfristig zum kollaps(UDSSR) geführt
- "Anarchisten, Agrarsozialistn und marxistische Dissidenten erkannten frühzeitig das es ein Fehler war, den Sozialismus in einem rückständigem Land einzuführen"
- starres Festhalten an Plänen und ignorierte Fehleranalyse führten immer wieder zu Fehlschlägen
- Mises und Hayek - Propheten des Neoliberalismus, sagen Scheitern der Planwirtschaft vorraus, selbst in einem entwickeltem Land

### Die Debatte über die Wirtschaftsrechnung

- S. 291 Grenznutzenschule hält den Markt für den vollkommenen Ausdruk der menschlichen Vernunft
- der allwissende Staat könne die selben ergebnise erzihelen?
- Enrico Barone "es wäre eine ungeheure, eine gewaltige Arbeit..., aber es ist nicht unmöglich"
- ein Staat mit der Fähigkeit alle Variablen in Echtzeit zu berechnen wäre ebenso gut wie ein vollkommener Markt
- bayrische Räterepublik Diskussionen über die Abschaffung des Geldes
- 1921 Ludwig von Mises " Die Wirtschaftsrechnung im sozialistischen Gemeinwesen" "Ohne Wirtschaftsrechnung keine Wirtschaft. Im sozialistischen Gemeinwesen kann es, da die Durchführung der Wirtschaftsrechnung unmöglich ist, _überhaupt keine Wirtschaft in unserem Sinne geben_"
- Mises: Geld wichtig für Preissignale
- S. 293 Drei kritische Probleme der Planwirtschaft nach Mises:
1. der staat kann die wirtschaftlichen Variablen nicht so schnell berechnen wir der Markt
2. der Staat kann die Innovation nicht belohnen
3. die Verteilung des Kapitals unter den Wirtschaftssektoren wird ohne Ffinanzsystem schwerfällig und willkürlich
- Mises sagt Chaos und Überproduktion minderwertiger Güter voraus
- Hayek stimmt zwei von drei Punkten zu, außer Punkt 1
- Oskar Lange glaubte das der Sozialismus auf der Grundlage der Grenznutzentheorie errichtet werden könne
- S. 294 Das RechenProblem mit "Superrechner und BigData" lösen, eine million mal eine million mal eine million, ein Exaflop
- Hayek und Lange, beide bevorzugten die Grenznutzentheorie der Arbeitswerttheorie, somit ein System das auf Knappheit basiert und nicht Überfluß ( Menschliche Arbeitskraft könnte man als überfluss bezeichnen aber was ist mit begrenzten Resourcen?)
- Wenn Kapitalismus und Staatssozialismus lediglich zwei verschiedene Systeme zur rationalen Zuteilung von Gütern sind, um einen Gleichgewichtszustand zu erreichen, dann ist der Übergang zwischen ihnen keine Revolution sondern einfach eine _technische Herrausfprderung_
- S. 295: Auch Kritikpunkte 2 und 3 können innerhalb der Arbeitswerttheorie gelöst werden
- "wenn es eine objektive erkennbare Wertgröße geben würde...Als solche könnte aber ... nur die Arbeit in Betracht kommen"
- Ha meine Frage von oben beantwortet: Mises Lehnte die Arbeitswerttheorie ab weil: 
1. es sei unmöglich mit den Arbeitswerten unterschiedliche Qualifikationsniveaus zu messen
2. _sie seien ungeeignet um den natürlichen Resourcen einen Marktwert  zuzuordnen_
- Mason sagt man könne diese zwei Punkte jedoch auflösen: "der in den Rohstoffen enthaltene Arbeitswert einfach der Arbeit entspricht die aufgewandt werden muss, um die Rohstoffe zu gewinen und zu transportieren"
- Mises: Die Zuteilung von Angebot und Nachfrage in einer Marktwirtschaft erfolgt nicht im handel sondern im Finanzsysstem
- das ist eine wichtige Erkenntnis, **für eine Postkapitalistische Gesellschaft  brauchen wir bessere Lösung als den Markt um Güter zu verteilen, sondern wir brauchen auch eine bessere Lösung als das Finanzsystem, um (das) Kapital zu verteilen**

### Transitionen haben ihre eigene Dynamik

- S. 296 Russland in den 1920er Jahren mangelte es an allem: für Konsumgüter wurden  Schwerindustrie und Elektrizität benötigt, um die Bevölkerung zu ernähren Industrielle Landwirtschaft
- "eine Übergangsphase ihre eigene Dynamik entwickelt: Die Transition ist nie einfach die Auflösung eines Systems und der Aufstieg eines anderen" => Kognitive grenzen des Menschen teile Probleme bis sie tatsächlich greifbar und lösbar werden
- die Resource welche heute wohl am wenigsten verfügbar ist, ist: Zeit
- ".. das Geld musste sowohl als Tauschmittel als auch als Wertspeicher erhalten werden" Store of Value vs Medium of Exchange
- Trotzki Passage: "Gäbe es einen universellen Verstand..., der sämtliche natürlichen und geselschaftlichen Prozesse gleichzeitig registrieren, die Dynamik ihrer Bewegung messen und die Ergebnisse ihrer Interaktionen vorraussagen könnte - ein solcher Verstand könnte selbstverständlich im Vorraus einen fehlerfreien und umfassenden Wirtschaftsplan entwerfen, von der für den Weizenanbau benötigten Fläche bis zum letzen Westenknopf"
- Preobraschenski, Trotzki und ihre Mitarbeiter waren die letzten Marxisten mit politischer Macht , P. wurde 1937 hingerichtet, T. 1940 ermordet
- heute ist die Situation anders: Negri die Gesellschaft als Fabrik
- Gesellschaft in der die identität der Menschen von den Dingen abhängt die sie kaufen
- "Jeder Versuch, über den Markt hinauszugehen, muss an einem anderen Ort beginnen als in den dreißiger Jahren des 20. Jahrhundert" <== WARUM hat er sich dann 100 seiten an der Geschichte aufgehalten? Hat das Buch *Seiten* benötigt???
- es folgt nun eine erklärung warum die Planwirtschaft auch mit Computern nicht funktioniert

### Angriff der Cyberstalinisten

- Paul Cockshott Allin Cottrell versuchen  Planwirtschaft mittels Computer umzusetzen
- S. 299 in ihrem Modell existiert Geld als "Arbeitswertzeichen" 
- Supercomputer solle den Wert einer Arbeitsstunde berechnen 
- "mit modernen Computern könnte man täglic aktualisierte Arbeitswerte berechnen und wöchentlich einen neuen Perspektivenplan entwerden - womit der Plan etwas schneller reagieren würde als eine Marktwirtschaft"
- Vorschlag für die EU, "in ihrem Modell wird die Abschaffung des Marktes in Europa nicht in erster Linie durch Verstaatlichungen , sonern durch eine Reform des monäteren Systems bewerkstelligt, damit das** Geld beginnen kann die Arbeitswerte wiederzuspiegeln" **
- S. 301 Abschaffung des Profits
- Kapitalbildung ohne Banken
- ".. Sie zeigen, dass man eine entwickelte Volkswirtschaft im 21. Jahrhundert nur umfassend planen kann, indem man sie ihrer **Komplexität** beraubt, den Finanzsektor vollkommen beseitigt und eine radikale Änderung des Konsumverahltens, der Mitbestimmung am Arbeitsplatz und der Investitionsmuster erzwingt."
- "Die Frage, wo in dieser Planwirtschaft wirtschaftliche Dynamik und Innovation herkommen sollen, **beantworten die beiden Forscher nicht**" 
- S. 302 "Die Forscher werfen den sowjetischen Planern dogmatische Idiotie vor, halten selbst jedochan einem hierarchischen Gesellschaftskonzept, physichen Produkten und einem einfachen System fest, in dem Veränderungen nur langsam Stattfinden"
- **"Ihr Modell ist der bisher beste Beleg dafür, das jeder Versuch den Postkapitalismus durch staatliche Planung und Unterdrückung des Marktes zu erreichen , zum Scheitern verurteilt ist"**
- S. 303 "Andre Gorz erklärte, die Überlegenheit des Kapitalismus gegenüber dem sowjetischen Sozialismus beruhe auf seiner Instabilität, seiner Vielfalt, seinem komplexen vielgestaltigen charakter, der dem eines **Ökosystems** vergleichbar ist"
- "...(vergleichbar Ökosystem) in dem es unentwegt zu neuen Konflikten zwischen **teilweisen autonomen Kräften** kommt, die weder kotrolliert noch ein für allemal in den Dienst einer stabilen Ordnung gestellt werden können" 
- "Diese   Transition wird lange dauern, sie wird Wirren auslösen, und das gesamte Konzept des **_Wirtschaftssystems_** wird neu definiert werden müssen"

### Umwälzungen: Shakespeare oder Marx

- Shakespeares Werke von König Johan bis Heinrich VIII als Beschreibungs bzw. Beobachtung der Übergangsphase vom Feudalismus zum Frühkapitalismus
- S. 304 "Welches ist die Grundlage des vorherrschenden Wirtschaftssystems?"
- Feudalismus => Verpflichtung
- "..das Geld  die auf Loyalität beruhende Macht destabilisierte"
- ca. 150 Jahre nach Shakespeare Merkantilismus ebnet den Weg für den Industriekapitalismus
- Shakespeare beschreibt den Wandel der Produktionsweise
- "Und im Grunde wird die Veränderung durch neue Technologien angetrieben"
- "Was reproduziert sich spontan?" => Im Feudalismus das Konzept von Treue  => im Kapitalismus der Markt
- "Shakespeares Könige und Herzöge töteten einander, weil das Geld dieauf der Loyalität beruhende Macht destabilisiert hatte."
- "Er ( Shakespeare) sah , was das neue Wirtschaftssystem mit dem menschlichen Wesen machte: Es gab dem Menschen Eigentständigkeit durch Wissen, machte ihn jedoch in einem bis dahin ungekannten Ausmaß anfällig für Gier, Leidenschaft, Selbstzweifel und Machthunger."
- 150 Jahre später Merkantilismus ( Handel, Eroberung, Sklaverei) ebnet weg für   Industriekapitalismus
- Idden und Verhaltensweisen - Werte
- S. 305  Shakespeare als Beobachter einer Transition in der Produktionsweise
- "Marx definiert eine Produktionsweise als ein Gefüge aus wirtschaftlichen Beziehungen, Gesetzen und Traditionen, die das(? ein ?) Regelwerk der Gesellschaft darstellen"
- Feudalismus: Macht des Herrn und Verpflichtung
- Kapitalimus: Markt, Privateigentum, Löhne
- "Marx leitet aus dem Kenzept der Produktionsweise eine klare historische Sequenz ab.."
- Keynes und Marx sagen das selbe: "Eines Tages wird es _ausreicheinde_ (für wen? Maßstäbe? Überleben? Selbstverwirklichung? Maslow? ) Güter geben, womit das Problem der Ökonomie(hm?) sein wird."
- Keynes Zitat: "Zum ersten Mal seit seiner Schöpfung, wird der Mensch mit seinem wirklichen, seinem dauerhaften Problem konfrontiert: Wie kann er seine Freiheit von drängenden wirtschaftlichen Sorgen nutzen ... um klug, verträglich und gut zu leben." das bringt es auf den Punkt: **Die Sinnfrage** , die Frage nach dem Sinn des Lebens für jeden Menschen indivduell wird zum Zentrum des wirtschaftlichen (motivations-kooperations) Systems.

### Was die Transition vorantreibt

- Was verursachte den Zusammenbruch des Feudalismus und den Aufstieg des Kapitalismus? -> Debatte (also unklar? was nicht unwahrscheinlich bei einem so komplexen Prozess erscheint), ich würde auf eine vielfach geschichtete dialektische Entwicklung tippen, getrieben durch Technologie verändern sich Wertvorstellungen welche die Struktur der gesellschaft verändern , welche neue Technologien hervorbringen welche wiederum...
- Paul Mason skizziert 3 verantwortliche Faktoren:
- 1. Faktor: Veränderungen um die Arbeitskraft
- Im 14. Jahrhundert Hungersnöte, Produktivität der landwirtschaftlichen Flächen im Feudalsystem konnte nicht mit dem Bevölkerungswachstum Schritt halten.
- Pestepidemie 1347 , spiritueller Wandel, wirtschaftliche Auswirkungen: Landarbeiter konnten aufgrund schrumpfenden Arbeitskräfteangebeotes höere Löhne verlangen
- S. 308 Nach der Epidemie: Bauernaufstände, Historiker: "allgemeine Krise des Feudalismus", "Renteneinkommen der Grundherren brachen ein während die Löhne in den Städten auf das doppelte und teilweise dreifache des Niveaus vor der Epidemie stiegen"
- traditionelle Armeen wurden durh Söldnerheere ersetzt
- Aufgrund Arbeitskräftemangel wurden Geräte erfunden die den Arbeitsaufwand verringerten
- Pest als externer Schock um ein innerlich geschwächtes System zum Einsturz zu bringen
- 2. Faktor: Bankwesen
- Aufstieg des Bankwesens, ermöglichte zwischen den offiziellen Klassen des Feudalismus- Adel, Ritter, städtisches Bürgertum, Bauern ein Vermögen anzuhäufen
- Medici -> Fugger, inoffizieller Einfluss auf die Könige
- Banken nicht nur Kredite sondern auch als alternativ Netzwerk von MAcht und Geheimhaltung
- S. 309 "Alle Beteiligten machten gemeinsame Sache, um innerhalb der offiziellen Feudalwirtschaft einen unterschwelligen Kapitalimus zu erreichten"
- 3. Faktor: Plünderung Amerikas
- 1,3 Millionen Unzen Gold aus Peru (was für ein Werte system...)
- 4. Faktor: Gutenbergs Druckerpresse
-  in den 50 Jahren nach der Erfindung wurden 8 Millionen Bücher gedruckt
- "Mit dem gedruckten Buch wurden überprüfbares Wissen und nachweisbare Autorenschaft eingeführt."
- "Es begünstigte die Verbreitung des Protestantismus, der wissenschaftlichen Revolution und des Humanismus"
- Mason Schlussfolgert: die Auflösung des Feudalismus beruhte nicht in erster Linie auf Technologie sondern "Sie erfolgte in einem komplexen Wechselspiel von wirtschaftlichem Zerfall und äußeren Schocks"
- S. 310 wir müssen davon ausgehen das uns ein ähnlich komplexes Wechselspiel zwischen Technologie, gesellschaftlichen Konflikten, neuen Ideen und externen Schocks bevorsteht
- "Aber das Ausmaß einers solchen Umbruchs macht uns Angst, so wie es uns Angst macht, wenn man uns zeigt wie gewaltig unsere Galaxie und wie unermesslich das Universum ist."
- unterschiedliche Prodduktionsweisen -> beruhen auf verschiedenartigen Strukturen (wiedereinmal erinnert an Luhman: eine funktion wird in einem System gebraucht also bildet sich eine Struktur)
- Vorrausetzung für den Postkapitalismus: Überfluss -> Ich glaube eben weil es nicht nur um wirtschaftliche Bedürfnisse geht dürfen Aspekte wie Souveränitat des Individuums in der Gesellschaft sowie Bedeutungswertung und Bedeutungsschöpfung bei dieser Betrachtung nicht fehlen
- "Aber wir können uns nur eine grobe Vorstellung davon machen, wie er aussehen wird"
- "Wenn das Gefüge dieser Gesellschaft jedoch nicht auf Wirtschaft, sondern auf dem befreiten Menschen beruhen wird, werden unvorhersehbare Dinge geschehen."
- S.311 "Vielleicht wird sich das Wesen der Medien, die wir verwenden, um Geschichten zu erzählen , bis dahin (2075) vollkommen geändert haben ... erste öffentliche Theater zu Shakespeares zeiten"
- der Marxismus lässt die Frage außer Acht : wi emüssen sich die Menschen ändern damit der Postkapitalismus entstehen kann?
- "So wie sich Shakespeare Doyce nicht vorstellen konnte, können wir uns nicht vorstellen, welche Art von Menschen die Gesellschaft hervorbringen wird, sobald sich nicht mehr das ganze Leben um die Ökonomie dreht" viele Ansätz davon sind bereits heute zu sehen, Selbstverwirklichung und selbst Transzendenz - Weiterentwicklung des Individuums durch weiterentwicklung der Umgebung auf einem bewußten Niveau
- hier fasst Mason nocheinmal die Gründe zusammen für Zusammenbruch (Übergang) des Feudalismus
    - feudale Landwirtschaftsmodell zunächst natürliche Grenzen dann ein massiver externer Schock - die Pest (könnten heute negative Klimakatastrophen diese Rolle spielen?)
    - daraus folgte ein demografischer Einbruch, Arbetiskräftemangel , Löhne steigen und das System der feudalen Verpflichtungen kann nicht aufrechterhalten werden
    - ebenseo wegen Arbeitskräftemangel technologische Innovation -> Merkantilismus: Anregung Wirtschaft ( Druck und Buchführung), Wohlstand durch Handel ( Bergbau, Kompass, schnellere Schiffe), erhöhte Produktivität (Mathematik und wissenschaftliche Methode)
- "Bestandteil des gesamten Prozesses ist etwas , das im alten System nebensächlich erscheint, jedoch die Grundlage des neuen Systems bilden wird: Geld und Kreditwesen" Kreditwesen ist ein komplexes Thema schade oder erstaunlich das Mason hier nicht Graebers (Schulden die ersten 5000 Jahre) Perspektive herranzieht , auch Harari erläutert die Bedeutung des Kreditwesens besser. Als gesamtgesellschaftliche Projektionsfläche entstehen andere Verhaltensdynamiken durch veränderte Wertvorstellungen ("erfindung von Fortschritt siehe Alan Kay Francis Bacon etc..)
- allerdings sind einfache Metaphern schwierig da es sich um eine Verschiebung/Veränderung auf meheren Ebenen und aus einer immensen Viehlzahl aus kleinen Veränderungen (size of an Argument) handelt
- S. 312 "Geldverleih im Feudalismus noch als Sünde => Revolution hinzu einem System das mit Geld und Finanzwesen ein Marktsystem begründet" auch hier wäre es interessant genauer zu hinterfragen was Märkte sind ihre Beziehung zu individuellen und gesellschaftlichen Bedrüfnissen (sind sie eine adäquate Repräsentation? Einige Naval tweets können hier als gute beispiele für starkes Markt denken gelten)
- "das neue System profitiert zusätzlich von der Entdeckung ener praktisch unerschöpflichen Quelle kostenlosen (Ethik???) Wohlstands in Amerika" ich frage mich häufig ob der Weltraum oder Asteroid-Mining diese Rolle in unserer Epoche einnehmen?
- "eine marginalisierte Gruppe erlangt die Führungsrolle ber der Verwandlung der Gesellschaft"
- "In entscheidenen Momenten behindert der Staat den Wandel nicht mehr, sondenr förderte ihn..." <- das ist heute immer noch eine der entscheidenen Fragen, wie wird sich der Staat bei solchen Transitionen verhalten? diese Transitionen sind auch immer Veränderungen in der Machtausübung und Machtverteilung, kann das konzept von Nationalstaaten immer noch funktionieren?
- " die Krakt die den Kapitalimus zersetzt ..., ist die Information" etwas zu einfach, ja digitalisierung spielt eine wichtige Rolel aber ebenso das Klima und die veränderten Bedürfnisse in Bezug auf die Bedeutung von Arbeit
- "Das moderne Gegenstück zur Druckerpresse..." ja hierbei mus sich immer an das Interview von Bilbo Calvez mit Jörg Platzer denken wo er Blockchain Technologie mit der Technologie der Druckerpresse gleichsetzt
- "Es kommt nicht zur Vollautomatisierung sondern es entstehen nur noch geringbezahlten Bullshit-Job" <- die Bedeutung geht verloren! Eigentlich auch interesant das Mason nicht Marx Theorie der Entfremdung (u.a. auch von der Arbeit) miterwähnt
- gegenstück zu Amerika als quelle kostenloser Reichtümer -> Weltraum ( laut mason jedoch das Internet)
- S. 313 "Haben wir einmal verstanden, dass die Transition begonnen hat, so brauchen wir keinen von Supercomputern berechneten Fünfjahresplan, sondern ein graduelles (inkrementelles) und modulares (Dunnbar und bias minimierung) Projekt"
- "Unser ( ach ja _wir_ ) Ziel sollte sein, die Technologien, Geschäftmodelle und Verhaltensweisen zu verbreiten, die den Markt auflösen, die Notwendigkeit der Arbeit beseitigen und die Weltwirtschaft zum Überfluss führen." - die Notwendigkeit der Arbeit beseitigen - ein hoch komplexes Thema , zum einem die Bedeutungstangente über John Vervaeke(meaning crisis) aber auch über Graeber und Jordan Peterson -  Verantwortung und Geschlechterrollen spielen ebenso eine große Rolle
- ".. bedarf es einer Mischung aus Planung, staatlichen Beiträgen, Marktwirtschaft und Peer-Produktion"
- "Raum für moderne Gegenstücke zu Gutenberg(Satoshi Nakamoto) , Columbus(Musk Besoz Brandson..) Shakespeare (infinite serious games?)"
- "die linke muss wieder lernen, positiv zu handeln" sogar Precht bestätigte dies nicht nur im Rahmen von Sarah Wagenknecht
- S. 314 "eine anpassungsfähige Linke..." absolut tiefgreifend durchdacht sollte die Kategorisierung rechts/links Transzendenz verschwinden es geht um Souveränität und einklang mit dem Umfeld eine gewisse Art der harmonieren
- "elemente des neuen sind bereits hier im alten vorhanden: Kooperativen, die Genossenschaftsbanken, Peetnetzwerke, die Unternehmen die ohne Management auskommen und ide Parallelwirtschaft der subkultur"
- gewaltige externe Schocks

## 9. Rationale Gründe zur Panik

- S.315  Wo auch immer - fragn zur Wirtschaft - Antworten zum Klima
- Beispiele für Klima Probleme , China , Manila , New Orleans
- IPCC 2013 es wird wärmer 
- S. 317 Klimakrise als weiterer Faktor für den Postkapitalismus
- S. 318 "In gewisser Weise haben sie verstanden: Wenn der Klimawandel real ist, ist der Kapitalismus am Ende"( deswegen leugnet trump und andere nationalisten ihn, siehe Harari)
- "Wirklich absurd sind nicht die Theorien derer...sondern die  Vorstellungn jener Politiker und Ökonomen (Sinn) die glauben, die bestehenden Marktmechanismen könnten den Klimawandel stoppen, Markt müsse GRenzen für klimaschonende Maßnahmen ziehen und die Marktstruktur könnte angepasst werden , um das größte Umbauprojekt in der Geschichte der Menscheit zu bewältigen"
- "John Ashton Ein sich selbst überlassener Markt kann das Energieversorungungssystem und die Wirtschaft nicht innerhalb einer Generation umgestalten"
- "Und wie haben die Mächtigen ...reagiert? ... Da sie im Markt den höchsten Ausdruck der menshclichen Vernunft sehen, glauben sie, er werde die richtigen Resourcenallokation anregen, um das Ziel einer Begrenzung des Temperaturanstiegs auf zwei Grad zu erreichen. Doch das is tpure Ideologie..."
- 886 Milliarden Tonnen um zwei Grad einzuhalten , es sind aber min 2,8 Billionen Tonnen verfügbar
- S. 319 steigende Energiepreise als Marktsignal  -> mehr investionen in neue Fördermethoden Fossiler brennstoffe 
- falsche Signale für mehr autos und mehr verbrauch 
- Investitionsproblem - ich würde es das monopol Problem nennen: börsenwert der 200 konzerne mit den größten Vorräten an öl, kohle und gas sind vier billionen Dollar - und hier die Crux - ein großteil dieses werts würde sich in Luft auflösen
- "Daraus können wir folgende Lehre ziehen: eine auf dem Marktmechanismus beruhende Strategie gegen den Klimawandel ist utopisch"
- "Welches sind die Hindernisse für eine Strategie, die nicht auf den Markt setzt? Lobbymacht der Energiekonzerne"
- 2003 -2010 usa lobbygruppen für leugnung des Klimawandel erhaltrn 558 Millionen Dollar
- S. 320 Deutschland als positiv beispiel - das muss vor den änderungen gewesen sein
- Verhaltensveränderungen mit Preiserhöhungen zu erzwingen ist problematisch - besser das gesamte System umbauen

### Wie wir die Klimakatastrophe abwenden können

- nur ein Weg für 2 Grad Ziel: CO2 Emmison bis 2050 halbieren
- Greenpeace Szenario, geldsparen und Arbeitsplätze
- S. 322 "Selbst wenn man Kosten misst, ist es also technologisch machbar und wirtschaftlich vernüftig den Planeten zu rette(story of separation) das einzige hindernis ist der markt(was ist mit lobby und anderen monopolen?)
- geo politische Spannungen
- S. 322 " Wenn wir die lebenswichtigen Emissionsziele erreichen wollen, werden wir jedoch um ein gewisses maß an zentraler Kontrolle nicht herumkommen"
- "Verstaatlichung grosser CO2 Emittenten" - mason kann nicht jenseits von National Staaten denken
- "...so müssen wir"
- "Es liegt auf der Hand dass der Aufbau einer Nicht-Marktwirtschaft und die Bemühungen um ein System mit geringerem CO2 ausstoss zusammenhängen. Aber während es zahlreiche Wege zu einer postkapitalistischen Wirtschaft gibt, ist die Banbreite an Maßnahmen , die wir ergreifen können, um Klimakatstrophe abzuwenden , beschränkt. Wenn es um den Klimawandel geht gibt es also rationale Argumente für Panik"

### Die demografische Zeitnombe

- S.324 "In der Frühzeit des Kapitalismus fielen die Menschen den schmutzigen, ungesunden Städten zum Opfer"
- "Die Zukunft der Menschheit wird sich also vor allem in Städten wie Manila, Lagos und Kairo entscheiden"
- im "erwerbsfähigen Alter"..
- Erwerbsquotient : verhältnis erwerstätige - ruhestand, heute 3:1, 2050 1:1
- er tanzt ziemlich umher: die instiutionen sind nicht auf den demografischen Wandel vorbereitet, das Soziale system würde kollabieren
- "...die rasante Alterung vieler Gesellschaften stellt eine existentielle Bedrohung für das nach dem zweiten Weltkrieg  errichtete Gesellschafts und Wirtschaftssystem dar " Volkswirt George Magnus
- Mason beschreibt verschiedene Symptome die von Wachstumszwängen ausgehen (börsen) , Rentenfonds
- alles kummuliert in der 2007/2008 Krise
- S. 328 Mason erklärt wie die Renten sich auf die Staatsschulden auswirken und das damit Mittel und langfristig (2050) die Kredit Bewertungen der Staatsschulden negativ beeinflusst werden, ich denke das erklärt auch den Versuch der Bundesregierung auf eine schwarze Null zukommen
- S. 329 Das (Wohlstandsneid) Flüchtlingingsproblem am Beispiel Nordafrika, 2050 Bevölkerungsprognosen für Niger und Tschad
- die Hälfte des Bevölkerungswachstums bis 2050 wird in nur 8 Ländern stattfinden 
- S.331 „Würden der Klimawandel,, die alternde Bevölkerung und der Arbeitsplatzmangel.. nicht mit der Stagnation eines fragilen Wirtschaftsmodell einhergehen, so könnten diese Probleme vielleicht einzeln gelöst werden. Sie hängen jedoch zusammen“ Im Kapitalismuskritiker wird alles mit Geld bemessen und somit gelöst ... so mit scheinen große Veränderungen nur mit Geld möglich..

### Die globale Elite im Zustand der Verleugnung 

- #tags: #change #society #meaning_crisis
- masons Vermutungen zu Ursachen für die Verleugnung der Probleme durch die Elite
- Gesellschaftliche Veränderungen ohne Kriege?
- „Das Psychologische Nebenprodukt in den köpfender politischen Elite war die Vorstellung, das es keine unkontrollierbaren Situationen mehr gab“ Angela merkels - alternativlose Politik/Sprache
- S.333 „Das Streben nach energiepolitischer Unabhängigkeit führt zu einer Regionalisierung der globalen Energiemärkte- das wäre doch gut?
- Sprung zu Internetzensur
- Schottlandsreferendum als verpasste Chance 
- auf einmal spricht er vom Neoliberalismus, ohne klare Definition oder Bezug, und vom „wir“
- Mason sagt das Klimawandel und Demografie problematisch für die weitere Existenz des Kapitalismus sind 
- S. 334 „...rechtliche Schranken für Informationsmonopole“ ?? Schlechter Scherz.. immer wieder Staat..
- „Die äußeren Schocks machen jedoch zentralisierte, strategische und rasche Eingriffe erforderlich“ Warum? History is not just ... we may screw it up
- „...dass wir Keine finanziellen, sondern strukturelle Lösungen brauchen.“ aber Geld und Staat sind als Strukturen gesetzt?
- mason sagt: Ursache  (für Klima, Demografie , Finanzen) ist ein Wirtschaftsmodell das mit der Umwelt nicht im Gleichgewicht ist und die Bedürfnisse der Menschen nicht mehr erfüllen kann, ich denke es geht noch tiefer , meaning Crisis, Meta Crisis, GameB
- er beschreibt Symptome kognitiver Dissonanz, was sind nicht ungewöhnlich ist für extrem reiche bzw. im Kapitalismus erfolgreiche Leute , hm ich muss an musk denken... und die Entfremdung 
- masons folgende Beschreibung ist romantisch im besten Falle
- postkapitalismus als langfristiges und dringendes Vorhaben 

## 10. Das "Projekt Null"

- S. 338 mason: “Der Neoliberalismus (ist das seine Definition für den aktuellen Kapitalismus?) hat den Postkapitalimus möglich gemacht.“
- das erwähnte Modell ist so albern das ich es hier nicht erwähne 

### Die fünf Prinzipien der Transition 

- S. 340 „eine neue Möglichkeit zur Überwindung des Kapitalismus ... gestützt auf die Informationstechnologie die Allmendeproduktion und den Austausch abseits des Marktes zu fördern“
- Projekt null, großangelegte postkapitalistisches Projekt - Ziele:  Energieversorgung mit null Emissionen, die Erzeugung von Maschinen, Produkten und Dienstleistungen mit null-grenzkosten und die weitgehende Beseitigung der Arbeit
- Vorschläge im kleinen Maßstab testen, makroökonomische Auswirkungen zuvor mehrmals virtuell durchspielen 


