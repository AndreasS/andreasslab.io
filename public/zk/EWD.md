[[_TOC_]]

# Why

- #tags: #motivation #context #dijkstra #book
- Dijkstra leads to a collection of interesting things: antromorphism in computing, radical change/novelity ( see his examples like: pille), computing and computing industry, computing and society, hypathia, gauss, and the history of different geometries, scientific thinking method
- EWD describes in his diaries with extraordinary discipline , reflections about his work and the context of his work
- especially about his profession and its relation to society
- reflections to practicing science and the science community
- how do computers influence society
- what a computer is
- Interesting math book -  ref from dijkstra: http://www.amazon.com/Introduction-Metamathematics-Stephen-Cole-Kleene/dp/0923891579
- Interview: https://www.youtube.com/watch?v=EL97C8C53ZM
- #book: A Method of programming ( Inhaltsverzeichnis) http://www.amazon.com/Method-Programming-Dijkstra/dp/0201175363
- #book: A Discipline of Programming
- on dijkstra EWD Mathematics Inc.

# Warum

- #tags: #motivation #dijkstra
- EWD beschreibt in seinen Tagebüchern, mit außerordentlicher Disziplin, Gedankenreflektionen über seine Arbeit
- während in derfrühen Phase natürlich Fachliche Diskussionen im Vordergrund stehen, wird später viel über sein Berufsbild reflektiert
- im besonderen über sein Berufsbild in Beziehung zur Gesellschaft
- Reflektionen zum Wissenschaftlichen arbeiten und der Wissenschaftlichen Gemeinde
- in wiefern der Computer die Gesellschaft beeinflusst hat
- was ein Computer ist

# Dijkstras Handwriting

- #tags: #article #dijkstra
- https://joshldavis.com/2013/05/20/the-path-to-dijkstras-handwriting/
- via https://news.ycombinator.com/item?id=20791049

# Video Interview

- #tags: #youtube #interview #dijkstra
- https://www.youtube.com/watch?v=RCCigccBzIU&app=desktop
- Complexity is in software is our own fault
- his experience comes from a time where he wrote the software for a computer system, when the hardware was just beeing assembled, so he had to write it without the hardware

# Hypatia

- #tags: #hypatia #cross_reference #dijkstra
- Mathematikerin , pagan , von einem christlichen Mob getötet, als politsches opfer , als intellektueller nicht christ
- http://de.wikipedia.org/wiki/Hypatia
- => Synkretismus : versuchte Vereinigung von poly und monotheistischen Religionen/Philosophien außer Epikureismus (garten Metapher), welcher übernatürliche Phänomene ablehnt bzw. durch natürliche Erklärungen beschreibt <- Hier bereits wurzeln der modernen Wissenschaft? -> nein trotz ablehnung des glaubeb/höheren fehlt der sich selbt verbessernde , rekursive Verbesserungsansatz der Wissenschaft
- stoiker verlieren einfluss zu beginn der  chirstlichen Zeit in Rom, synkretische verschmelzungen
- Verfilmung von 2009: agora
- Wenn die beschriebenen Freiheiten wirklich so wahren ist es verständlich das sie lieber allein als Philosoph blieb als ohne und mit noch mehr eingeschränkten rechten Hausfrau zu werden.
- Vgl. Besuch im Theater , sprach und Redefreiheit ?
- ref via heise TP article Spengler
- ref: find EWD number - EWD 1041

# Gauß -  warum hat er nicht bereits die nicht euklidsche Geometrie erklärt? 

- #tags: #dijkstra #ewd #carl_friedrich_gauß
- EWD ref. 1012,1036
- lebte bis 1855, seine Arbeit(Tagebücher) ist nicht vollständig erhalten
- http://de.wikipedia.org/wiki/Gau%C3%9F#Arbeitsweise_von_Gau.C3.9F
- seine Arbeitsweise war schüchtern  er veröffentlichte beabsichtigt nur vollständiges und bewußt aus Angst vor den Böotien öl
- Schüler von Gauß : http://de.wikipedia.org/wiki/Bernhard_Riemann
- [Graßmann](http://de.wikipedia.org/wiki/Hermann_Gra%C3%9Fmann) gilt als begründer der Vektor und Tensor rechnung, er rang um anerkennung ( bbzw. Kommunikation da er selbst geschaffene und fachhistorisch nicht sanktionierte Begriffe verwendete. Graßmann verfasste ein Wörterbuch zur Rigveda.
- Also wurden in der Geschichte Menschen die radikale Sprünge vollzogen, oft nicht verstanden. Oder Menschen entwickelten implizit entgegengerichtete Verhaltensmuster(Gauß). 

# Letters with Backus

- #tags: #article #ewd #dijkstra
- https://news.ycombinator.com/item?id=11796557
- https://medium.com/@acidflask/this-guys-arrogance-takes-your-breath-away-5b903624ca5f#.wurlt0kvs

# Self stabilization - Self repairing systems

- #tags: #ewd #dijkstra #programming
- via http://www.mail-archive.com/fonc@vpri.org/msg04573.html
- https://web.archive.org/web/20170611101855/http://dijkstra.cs.virginia.edu/genprog/

# EWD - Chronicle - Overview

- #tags: #ewd #dijkstra
- http://www.cs.utexas.edu/users/EWD/indexChron.html

|EWD no.|	time spanned~|
| -------- | -------- |
|  0–99|	 1962–1964|
| 100–199|	 1964–1967|
| 200–299|	 1967–1970|
| 300–399|	 1971–1973|
| 400–499|	 1973–1975|
| 500–599|	 1975–1976|
| 600–699|	 1976–1979|
| 700–799|	 1979–1981|
| 800–899|	 1981–1984|
| 900–999|	 1984–1987|
|1000–1099|	 1987–1991|
|1100–1199|	 1991–1995|
|1200–1299|	 1995–2000|
| 1300–1318|	 2000–2002|
 
 ~ Years shown are approximate, because manuscripts were not always finished in the order in which they were begun.

# EWD 117 - introducing structured programming

- #tags: #ewd #dijkstra
- http://fermatslibrary.com/s/programming-considered-as-a-human-activity#email-newsletter

# EWDs read without comment

- #tags: #ewd #dijkstra
- EWD 123 Cooperating Sequential Processes
- EWD 215 A case against the GO TO statement
- EWD 236 Complexity controlled by hierarchical ordering of function and variability
- EWD 237 A preliminary investigation into Computer Assisted Programming
- EWD 238 Computation versus program
- EWD 239 On trading storage against computation time
- EWD 240 The moral of EWD237 - EWD239
- EWD 241 Towards correct programs 
- EWD 249 Notes on Structured Programming
- EWD 268 Structured programming
- EWD 275 Structure of an extendable operating system
- EWD 292 Letter to professor C.A.R. Hoare (31 August 1970)
- EWD 417 On the abolishment of the subscripted variable [see EWD428]
- EWD 428 Array variables. [see EWD417]
- EWD 443 A multidisciplinary approach to mathematics
- EWD 450 Correctness concerns and, among other things, why they are resented
- EWD 451 Lord, deliver us from charlatans!
- EWD 456 Determinism and recursion versus non-determinism and the transitive closure
- EWD 459 The pattern matching problem.
- EWD 464 A new elephant built from mosquitos humming in harmony
- EWD 465 Monotonic replacement algorithms and their implementation
- EWD 469 Programming methodologies, their objectives and their nature
- EWD 473 On the teaching of programming, i.e. on the teaching of thinking
- EWD 476 Concurrent programming: a preliminary investigation
- EWD 480 Craftsman or scientist?
- EWD 482 Exercises in making programs robust
- EWD 485 Marketing questionnaire "A Discipline of Programming"
- EWD 487 Letter to the Burroughs recipients of the EWD-series
- EWD 492 On-the-fly garbage collection: an exercise in multiprocessing
- EWD 498 How do we tell truths that might hurt?
- EWD 507 On a gauntlet thrown by David Gries
- EWD 512 Comments at a symposium
- EWD 525 On a warning from E.A.Hauck
- EWD 527 On units of consistency
- EWD 528 More on Hauck's warning
- EWD 539 Mathematics Inc., a private letter from its chairman
- EWD 554 A personal summary of the Gries-Owicki theory
- EWD 562 The effective arrangement of logical systems
- EWD 564 A superficial book
- EWD 566 Programming: from craft to scientific discipline
- EWD 568 A programmer's early memories
- EWD 573 A great improvement
- EWD 598 A bug in my book!
- EWD 788 What we seem to have learned (with A.J.M. van Gasteren)
- EWD 899 A review of a paper in AI
- EWD 923a Where is Russell's "Paradox"?
- EWD 993 The nature of my research and why I do it
- EWD 1070 For brevity's sake (Mathematical Methodology)
- EWD 1078 Leibniz's Principle (Mathematical Methodology)
- EWD 1104 So much for scientific visualization
- EWD 1186 Ping-pong arguments and Leibniz's principle
- EWD 1206 An unfortunate use of symbols
- EWD 1213 Introducing a course on calculi

# EWD 279 - The programming laboratory project

- #tags: #ewd #dijkstra
- sounds interesting but didn't read

# EWD 316 - A Short Introduction to the Art of Programming (value / Identity / state)

- #tags: #ewd #dijkstra
- http://www.cs.utexas.edu/~EWD/transcriptions/EWD03xx/EWD316.2.html
- http://www.infoq.com/presentations/Value-Identity-State-Rich-Hickey
- ITs always good to see common concepts paraphrased by different individuals.  
- maids wearing the same dress?
- Many implications arise from the perception, management and communication of V I S, see javas equals vs ==  
- this is part of a longer EWD: http://www.cs.utexas.edu/users/EWD/transcriptions/EWD03xx/EWD316.html

    0. Contents

    1. Preface

    2. Some Fundamental Notions

    3. Programming Languages and their Implementation

    4. Variables and relations between their values

    5. Programs corresponding to recurrence relations

    6. A first example of step-wise program composition

    7. The shortest spanning subtree of a graph

    8. The towers of Hanoi

    9. The problem of eight queens

    10. A rearranging routine

# EWD 340 - The humble programmer

- #tags: #ewd #dijkstra
- why programmer,ALGOL60,LISP,BNF,definetly an appell to manageable programs: "We must not forget that it is not our business to make programs, it is our business to design classes of computations that will display a desired behaviour."
- Turing Award 72

# EWD 361 - Programming as a dicipline of mathematical nature 

- #tags: #ewd #dijkstra
- ref: http://www.cs.utexas.edu/users/EWD/transcriptions/EWD03xx/EWD361.html
- Figure that out: "quo modo" than on the "quod"
- Programm Design via Dijkstra 361
- N Number of components
- p Propability of an individual component beeing corret
-  P ≤ pN
- es ist klar wenn P nicht sehr nah bei 1 ist das, Dass gesamte Programm sehr instabil werden wird -> deswegen Techniken wie "defensive Programmierung" entstanden?

# EWD 447 - On the role of scientific thought 

- #tags: #ewd #dijkstra
- http://www.cs.utexas.edu/users/EWD/transcriptions/EWD04xx/EWD447.html
- Die Betrachtung eines Aspekts in Isolation und definierten Bedinungen:
- 1.interne : Koheränz Linguistik (vgl Konsistenz) das Wissen muss die Fähigkeiten unterstützen und die Fähigkeiten müssen es erlauben das Wissen zu erweitern
- 2.externe: eine schmale Schnittstelle, der Grad der Losgelöstheit von anderen Forschungsgebieten
- Präzise Unterscheidung von programmier sprachen als Spezifikation und als implementierung
- LISP 1.5 Manual als negativ Beispiel. (hierzu gibt es verschiedene Links, ich würde sagen zum Zeitpunkt als er 447 schreib verstand er ES oder die meta zirkuläre Definition nicht)
- Business Data processing probleme durch zu wenig formale spezifikation. Wie und in welchem Umfang lernen die Leute mit “worse is better” zu leben. Weil es billiger ist.
- Als interessante neben betrachtung: viele am “anfang” (50,60er?) hinzugekommene Wissenschaftler kannten den wissenschaftlichen gedanken bereits aus anderen Diziplinen, jedoch wenige übertrugen das Konzept entsprechend.
- Die Bestrebungen der Informatik (CS) sollten daruf hinarbeiten das die Fundamente noch unzureichend erforscht sind. 

Functional programming and layers of complexity
The famous there isn't a problem which can't be solved by adding another layer of abstraction(although it's arguaby not defined in the sense dijkstra defined it) is also true for functional programming, many abstractions are used on top of it. But since RT is guaranteed at the same time it's opaque from a certain perspective

# EWD 463 - Some questions

- #tags: #ewd #dijkstra
- He lost the book "Introduction to Metamathematics", and thus was never concerned with Gödel? again? human nature 

# EWD 500 - After many a sobering experience

- #tags: #ewd #dijkstra
-  On the Fly Garbage Collection

# EWD 508 - A synthesis emerging?

- #tags: #ewd #dijkstra
- an Argument against Neumanns architecture? Page 7, 3 implementation pos. for org.: a) normal sequential techniques b) elephants built from mosquitos c) datadriven machine

# EWD 540 - Two views of programming

- #tags: #ewd #dijkstra
- "understanding, beeing resposibile" can not be delegated, which comes to the heart of what understanding is, seems that far thought didnt cross
dijkstras mind at that time, warning of talking with mehtopds carries the potential for missinterpretation

# EWD 619 - Essays on the nature and role of mathematical elegance 

- #tags: #ewd #dijkstra
- experiements

# EWD 622 - On making solutions more and more fine-grained (In gratitude dedicated to C.A.R.Hoare, D.E.Knuth, and J.F.Traub.)

- #tags: #ewd #dijkstra
- "non-operational" semantics? connection between this and the term denotational semantics? Connection to the "on the fly garbage Collection" problem?

# EWD 690 - The pragmatic engineer versus the scientific designer

- #tags: #ewd #dijkstra #perspective #programming
- Understanding purpose and value of documentation, rant on the term "software engineering",dijkstra explains why specification can't
come from management as" when we turn to our native tongues the specs become unwieldy, blaming again "false" analogies, but how to
know if your analogy is false if out basic thinking model is the analogy(making sense)

# EWD 692 - A review of the 1977 Turing Award Lecture by John Backus

- #tags: #ewd #dijkstra #turing_award
- Programm semantics, understanding programs in terms of unerlying computation model can not work?, denotational semantics?? Massage objects??

# EWD 735 - A mild variant of Combinatory Logic

- #tags: #ewd #dijkstra
- SKI Kombinator, 4 people involved,acknowledged:D.A.Turner for writing, Hamilton Richards for attention, C.S. Scholten for understanding, himself

# EWD 750 - American programming's plight

- #tags: #ewd #dijkstra #programming
- Mathematical developement in the US, most important skills for Programmer, observation on company strategy, role of the mathematical thinking in programming blurs?

# EWD 791 - The psychology of the user

- #tags: #ewd #dijkstra #psychology #programming #dialectic
- If programming is, it may be: a dual of theory and practice, the reviewed paper try to navigate in the dark without light

# EWD 800 - A bagatelle for the left hand

- #tags: #ewd #dijkstra #begriffs_genese
- Glossary on spurious words which appeal to superficial acknowledgement, real programmers, natural...

# EWD 854 - The fruits of misunderstanding - Gedanken Schulen - Thought schools 

- #tags: #ewd #dijkstra #antromorphism #words #meaning #programming #thoughts
- inspired through: http://www.cs.utexas.edu/users/EWD/transcriptions/EWD08xx/EWD854.html
- ref: http://de.wikipedia.org/wiki/Anthropomorphismus
- Es lassen sich zwei Hauptströmungen in den heutigen Programmiersprachen als Einflüsse aufzeigen:
- Dijkstra vertritt die Prämisse das Programme Abstraktionen sind und je mehr desto besser(Genau wie tony,brianMc). Er vertritt eindeutig die Meinung das Antromorphismus einen negativen Einfluss auf Software hat.
- Die Versuche von worrydream die Feedback loop zu schließen werden von D. verworfen, er sagt es sei bis auf wenige triviale Fälle unmöglich!(warum..-> rekursive programe NP Probleme?) der entscheidene Punkt mag hier sein das sich 80% der heutigen “benötigten” Software mittels trivialer Fälle abbilden ließe. Was uns wieder dahin führt das die Werkzeuge mit denen Wir denken selbstverständlich den Prozess des Denkens in ihrem Rahmen einschränken (Zitat ref http://worrydream.com/quotes/ David Hestenes The mathematical modeling tools we employ at once extend and limit our ability to conceive the world.)
- Die Werkzeuge die wir verwenden erweitern und begrenzen gleichzeitig unsere Warnehmung der Welt/ Gedanken.
- https://www.youtube.com/watch?v=yzUuCwyk5DA (Three Strange Definitions of Computer Programming from Edsger Dijkstra - Ron Burk)
- Difficult problems must be solved outside the brain"
- Alan kay verweist auf die Mengenverhältnisse bzw. Skalierbarkeit von Mikroorganismen. (siehe Verhältnis von Bakterien Molekühlen etc.)
- Sein Ansatz scheint zu sein ähnliche Mengen und Verhaltensmuster in Software nachzubilden als alternative zu make & fix. welches zu Objekten (in seinem bzw. Aktoren sinne) führte, “maybe we thought of objects beeing to small”
- -> letzter Absatz
- Schluss endlich scheint D. A.K. beizupflichten das es zu wenig Innovation gibt?
- Wörtlich betrachtet bestätigt dies aber nur wieder die “Analogie-in unserem Denken ”  These -> wie sollen wir mit dem Vokabular von Gestern die Gedanken von heute und morgen Denken?
- comments from Spreadsheet: COBOL Rant, warnung in die 90er,What is understanding, how to understand and define something "radically" new, problematic antromorphism,set example for understanding, the axiomatic method, the failure of the invention of the term "software engineering,
maintanace also not appliable, loc "spent,what can computers actually do,"With yesterdays words we have to plan today for tomorrow"...
meaning :)

# EWD 898 - The threats to computing science

- #tags: #ewd #dijkstra
- homogenizing forces have been stronger in computing (english lingua france),numerical mathematics was seen as only applciance for computers in the beginning, complexity as a thread to CS, mankinds scientific endeavors have been a spectacular failure,the youth elixir and phil stone metaphor, the importance of "notational experiments" see Worrys dreams tools shape our thoughts,software engin: "how to programm if you
can not", how to argue against user friendliness

# EWD 913 - On a cultural gap (Draft.)

- tags: #institutions #ewd #dijkstra
- communication (gap) on a "typical" university campus between typical mathematican and typical computing scientist
- radical novelity, reasoning by analogy-methaphers, why does it take 50 years for mathematicans and CS to grow to gether, surgery as knife science as analogy to why computer science is such a bad name for it

# EWD 1036 - On the cruelty of really teaching computing science

- #tags: #ewd #dijkstra #change #thoughts #programming 
- so much for the mathematicans : see D. Engelbarts efforts to apply his "augmentation" to other fields
- so much for education:
again inability for meta discussion, how we learn, how our perceptions works
Die Fähigkeit bewußt linear und exponentiell/logarithmisch zu denken.  lernen Maß nehmen , einschätzen zu können
- Warum hat Gauß nicht bereits die "nicht-euklidsche" Geometrie postuliert(zur diskusion gegeben)?
- Computing has 2 radical novelities
109 scale ,example Storage and Computation time, we cann not even slightly think in or  understanding that scale
Computer digital, discrete Device, our analouges thinking leads us to a strange foundation: small change , small impact. This breakes completly since 1 wrong bit can turn a correct program into an error.  Interestingly we STILL build today abstractions to come along this property: more faliure tollerant systems OTP, see also alan kays make & fix vs. grow and negoiate ( despising dijkstra?)
- sein divide and rule, mein HWi Vortrag, teile und herrsche skaliert schlecht aber was dagegen tun
- Linguistic suggestions( so we _have_ to take into account our own perception, communication?, habitus-bad wording
to blame John von neuman for what? -> can computing liberated from von neuman style?
- Geschichte zum Begriff der Rekursion in der Mathematik?
- Leibnitz beitrag , idee?
- software Engeneering - is self contradicting in its definition
- What does the “true name” analogy here mean?
- What are Programs: Abstract symbol interpreter ( human or mechanised but not automatic?)
<<what could dijkstra mean here with automatic programming?>>
-> see WDs prog. is blindly manipulating symbols
The purpose of of machines today is to execute our programs.

# EWD 1012 - "Real mathematicians don't prove"

- #tags: #ewd #dijkstra
- postulational-operational thiking about a programm, how those mehtods compare in scale,example
Gauss example,medieval thought patterns in mathematics today, euclid geometry as a failed(false)
example of a deductive system

# EWD 1018 - To the members of the Budget Council

- #tags: #ewd #dijkstra
- Related to Alan Kays Musings about how funding works today, Industry and gov want to direct
research, Tools shape thoughts , again as alan kay Dijkstra says one should be careful about
departments because they force thinking into a certain direction, while it should be fluent

# EWD 1024 - A new science, from birth to maturity

- #tags: #ewd #dijkstra #science
- Nice Overview : 4 Decades in Computing: 50,60,70,80 still going, CS to full fill Leibniz Dream, referenced complexity theoriy? P NP Problems?

# EWD 1036 - On the cruelty of really teaching computing science

- #tags: #ewd #dijkstra
- so much for the mathematicans : see D. Engelbarts efforts to apply his “augmentation” to other fields
- so much for education: 
- again inability for meta discussion, how we learn, how our perceptions works
- Die Fähigkeit bewußt linear und exponentiell/logarithmisch zu denken.  lernen Maß nehmen , einschätzen zu können
- _Warum hat Gauß nicht bereits die “nicht-euklidsche” Geometrie postuliert(zur diskusion gegeben)?_
- Computing has 2 radical novelities :
1. 109 scale ,example Storage and Computation time, we cann not even slightly think in or  understanding that scale
2. Computer digital, discrete Device, our analouges thinking leads us to a strange foundation: small change , small impact. This breakes completly since 1 wrong bit can turn a correct program into an error.  Interestingly we STILL build today abstractions to come along this property: more faliure tollerant systems OTP, see also alan kays make & fix vs. grow and negoiate ( despising dijkstra?)

(sein divide and rule, mein HWi Vortrag, teile und herrsche skaliert schlecht aber was dagegen tun)

Linguistic suggestions( so we _have_ to take into account our own perception, communication?, habitus-bad wording
to blame John von neuman for what? -> can computing liberated from von neuman style? )

Geschichte zum Begriff der Rekursion in der Mathematik?

Leibnitz beitrag , idee? <= antworten hierzu heute: siehe Leibniz und Stephen Wolfram, Symbolic Computation

software Engeneering - is self contradicting in its definition
What does the "true name" analogy here mean?

What are Programs:
Abstract symbol interpreter ( human or mechanised but not automatic?)

-  what could dijkstra mean here with automatic programming? => he did see the "analog" computer so the "digital" was "automatic" to him
- -> see WDs prog. is blindly manipulating symbols
- The purpose of of machines today is to execute our programs.

- http://www.cs.utexas.edu/~EWD/transcriptions/EWD10xx/EWD1036.html
- Erläuterungen zu den Begriffen “radikal” wann die grenze Überschritten ist
- wie sollte man CS Unterrichten
- Spreadsheet comment: thinking proceses,how tools of thought limit perception and podnering by enforcing their rules to the
thought(thinking of today in terms of yesterday words), better definiton of radical novelity and how to
cope(axiomatic method), Gauss non-euclidian geometry as example why individual does not speaks
open to soeciety about research,rant against mathematicians,even disbelief in the existense of radical
novelity,we should find methods to cope with radical novelity,middle ages thinking

# EWD 1041 - By way of introduction

- #tags: #ewd #dijkstra #metaphor #hypatia
- EWD on "Reactive"-2013 Design,Society <-> Education interrelationship (A.K. Thermostat metaphor?),it is actually quite bitter,Hostility towards scientists? -> re human universals, how sciences emerge....hm, science can not be managed,programming boils down to: training of misfits, I am now certain that it IS bitter, I guess it is because this time he cant blame society in the large (although to a degree he does: managers reliying on prog...) but the real pain this time seems to stem from the university system he is actually part of, he openly declares its failure, fortunately we can say that although his observations are true, the reasons for situation in education system in the first place
is in deep entanglement with societies demans, I think his first Reference to historic science martyrs shows that, but the critique seems unconstructive ("easier said than done"), bitter?

# EWD 1043 - A few comments on "Computing as a discipline"

- #tags: #ewd #dijkstra #programming
- How to learn programming:" Everybody knows that programming is most easily and effectively taught to students that have never touched a machine and are not given access to a computer as long as the course lasts.", dont tell truth that hurt? "No" required equippement?

# EWD 1051 - The next forty years

- #tags: #ewd #dijkstra #society #progress #outlook
- how fast can society absorb progress? EWD claims at least 30 years, is learning _only_ to blame?
- Eigentlich müsste man bestrebt sein weniger "Dokumentation" zu erzeugen, leider ist das gegenteil der fall "so we better learn how to not introduce complexity, in the first place", Computing community :Artificial Complexity Preservation

# EWD 1059 - 0 Preface (Mathematical Methodology)

- #tags: #ewd #dijkstra
- Many mathematicians seem to be in 'favor of progress without change' ; why is teaching thinking
dangerous? gilde vs unviversity teaching, mathematicans feel "tickled" when finding gaps in proofs,
what does it mean to DO Mathematics, in fact EWD states that computing is transforming human
thought process (does he make a direct connection to the social implications?) , colleguaes anectode
of teaching thinking,MM is EWDs description of how to teach Math

# EWD 1095 - Are systems people really necessary?

- #tags: #ewd #dijkstra #guilds #teaching
- Observations on cultural/Social phenomena, quote from 1969, diffrence in teaching, guilds vs university

# EWD 1100 - The computing habit

- #tags: #ewd #dijkstra #society #risk
- computer addiction risks and chances in futures society developement, electronic dependence,computers sould NOT only be used as multimedia
machines,CS can be anything

# EWD 1126 - Who failed?

- #tags: #ewd #dijkstra
- Misinterpretation,miscommunication

# EWD 1137 - Computing the future?

- #tags: #ewd #dijkstra #apps #sovereignty #society
- society influence on university,"applications" limit the general purpose capabilities of computers

# EWD 1284 - Computing Science: Achievements and Challenges

- #tags: #ewd #dijkstra #lisp #programming
- http://www.cs.utexas.edu/users/EWD/transcriptions/EWD12xx/EWD1284.html
- recursion
- fortran doesnt allow neseted array indices
- IBM influence operating systems
- unterschiedliche CS US und EU einfluss der wirtschaft in US, -> employer education
- LISP functional programing
- influence of the "ProgramingLanguage" on the thoughts of the programmer
- boolean types as leap forward
- BNF
- predicate calculus
- lattice theory
- dogma problem , warning "as long computing science is not allowed to save the computer industry, we had better to it that computer industry does not kill computing science"
- comments spreadsheet: recursion,fortran doesnt allow neseted array indices,IBM influence operating systems,unterschiedliche CS US und EU
einfluss der wirtschaft in US, -> employer education,LISP functional programing,influence of the "ProgramingLanguage" on the thoughts of the programmer,boolean types as leap forward,BNF,predicate calculus, lattice theory,dogma problem , warning "as long computing science is not allowed to save the computer industry, we had better to it that computer industry does not kill computing science"

# EWD 1298 - Under the spell of Leibniz's Dream

- #tags: #ewd #dijkstra #leibniz
- 50 years review,annecdote of his mother to not understand boolean statements, historical review of evolution of the
educational system of university in the netherlands ( 68er Einfluss

# EWD 1303 - My recollections of operating system design

- #tags: #ewd #dijkstra
- the 3 problems with operating systems, extra hardware monitor zum debuggen: crude and silly way of proceeding

# EWD 1304 - The end of computing science?

- #tags: #ewd #dijkstra
- Die Kluft zwischen theoretischem Verständnis und praktischen Bedürfnissen verliert sich immer noch in Komplexität,
Separation of Concerns

# Unsorted

- #tags: #ewd #dijkstra
- Open in FF EWD:1018,1024,1026, 1248, 1305,  447, 273, _1298_ , 361, 256, 526, 480, 1043, 
- EWD Argument against IDE's ? https://twitter.com/michaeljforster/status/414809839294873601

# blog - 28.09.2013 

- #tags: #dijkstra #ewd
- http://www.cs.utexas.edu/users/EWD/transcriptions/transcriptions.html
- http://www.cs.utexas.edu/users/EWD/transcriptions/EWD08xx/EWD898.html
- Influence from PLs like BASIC, corporate influence
- http://www.cs.utexas.edu/~EWD/transcriptions/EWD08xx/EWD854.html


# Quotes 

- #tags: #twitter #quotes #dijkstra
- via Susan Potter:  https://github.com/polynomial/fortunes/blob/master/dijkstra

"For a number of years I have been familiar with the observation that the quality of programmers is a decreasing function of the density of go to statements in the programs they produce. More recently I discovered why the use of the go to statement has such disastrous effects, and I became convinced that the go to statement should be abolished from all "higher level" programming languages."
-- Dijkstra (1968)

"Our intellectual powers are rather geared to master static relations and that our powers to visualize processes evolving in time are relatively poorly developed. For that reason we should do (as wise programmers aware of our limitations) our utmost to shorten the conceptual gap between the static program and the dynamic process, to make the correspondence between the program (spread out in text space) and the process (spread out in time) as trivial as possible."
-- Dijkstra (1968, "A Case against the GO TO Statement")

"Testing shows the presence, not the absence of bugs"
-- Dijkstra (1970, "Software Engineering Techniques", April 1970, p. 16.)

"A convincing demonstration of correctness being impossible as long as the mechanism is regarded as a black box, our only hope lies in not regarding the mechanism as a black box."
-- Dijkstra (1970s, EWD249)

"When we take the position that it is not only the programmer's responsibility to produce a correct program but also to demonstrate its correctness in a convincing manner, then the above remarks have a profound influence on the programmer's activity: the object he has to produce must be usefully structured."
-- Dijkstra (1970s, EWD249)

"The art of programming is the art of organizing complexity, of mastering multitude and avoiding its bastard chaos as effectively as possible."
-- Dijkstra (1970s, EWD249)

"Program testing can be used to show the presence of bugs, but never to show their absence!"
-- Dijkstra (1970s, EWD249)

"The competent programmer is fully aware of the strictly limited size of his own skull; therefore he approaches the programming task in full humility, and among other things he avoids clever tricks like the plague."
-- Dijkstra (1970s, EWD340)

"On Our Inability To Do Much."
-- Dijkstra (1970s, "Structured Programming", Chapter title in O.J. Dahl, E.W. Dijkstra, and C.A.R. Hoare. Academic Press)

"Please don't fall into the trap of believing that I am terribly dogmatic about [the go to statement]. I have the uncomfortable feeling that others are making a religion out of it, as if the conceptual problems of programming could be solved by a simple trick, by a simple form of coding discipline!"
-- Dijkstra (1970s, in personal communication to Donald Knuth , quoted in Knuth's "Structured Programming with go to Statements")

"Don't blame me for the fact that competent programming, as I view it as an intellectual possibility, will be too difficult for "the average programmer" — you must not fall into the trap of rejecting a surgical technique because it is beyond the capabilities of the barber in his shop around the corner."
-- Dijkstra (1970s, EWD512)

"Several people have told me that my inability to suffer fools gladly is one of my main weaknesses."
-- Dijkstra (1970s, EWD690)

"Write a paper promising salvation, make it a 'structured' something or a 'virtual' something, or 'abstract', 'distributed' or 'higher-order' or 'applicative' and you can almost be certain of having started a new cult."
-- Dijkstra (1970s, EWD709)

"For me, the first challenge for computing science is to discover how to maintain order in a finite, but very large, discrete universe that is intricately intertwined. And a second, but not less important challenge is how to mould what you have achieved in solving the first problem, into a teachable discipline: it does not suffice to hone your own intellect (that will join you in your grave), you must teach others how to hone theirs. The more you concentrate on these two challenges, the clearer you will see that they are only two sides of the same coin: teaching yourself is discovering what is teachable."
-- Dijkstra (1970s, EWD709)

"We must be very careful when we give advice to younger people: sometimes they follow it!"
-- Dijkstra (1970s, 1972 Turing Award Lecture)

"The major cause [of the software crisis] is that the machines have become several orders of magnitude more powerful! To put it quite bluntly: as long as there were no machines, programming was no problem at all; when we had a few weak computers, programming became a mild problem, and now we have gigantic computers, programming has become an equally gigantic problem. In this sense the electronic industry has not solved a single problem, it has only created them, it has created the problem of using its products."
-- Dijkstra (1970s, 1972 Turing Award Lecture)

"FORTRAN's tragic fate has been its wide acceptance, mentally chaining thousands and thousands of programmers to our past mistakes."
-- Dijkstra (1970s, 1972 Turing Award Lecture)

"LISP has been jokingly described as "the most intelligent way to misuse a computer". I think that description a great compliment because it transmits the full flavor of liberation: it has assisted a number of our most gifted fellow humans in thinking previously impossible thoughts."
-- Dijkstra (1970s, 1972 Turing Award Lecture)

"If you want more effective programmers, you will discover that they should not waste their time debugging, they should not introduce the bugs to start with."
-- Dijkstra (1970s, 1972 Turing Award Lecture)

"The effective exploitation of his powers of abstraction must be regarded as one of the most vital activities of a competent programmer."
-- Dijkstra (1970s, 1972 Turing Award Lecture)

"The use of COBOL cripples the mind; its teaching should, therefore, be regarded as a criminal offense."
-- Dijkstra (1975, EWD498)

"In the good old days physicists repeated each other's experiments, just to be sure. Today they stick to FORTRAN, so that they can share each other's programs, bugs included."
-- Dijkstra (1975, EWD498)

"It is practically impossible to teach good programming to students that have had a prior exposure to BASIC: as potential programmers they are mentally mutilated beyond hope of regeneration."
-- Dijkstra (1975, EWD498)

"Simplicity is prerequisite for reliability."
-- Dijkstra (1975, EWD498)

"Programming is one of the most difficult branches of applied mathematics; the poorer mathematicians had better remain pure mathematicians."
-- Dijkstra (1975, EWD498)

"About the use of language: it is impossible to sharpen a pencil with a blunt axe. It is equally vain to try to do it with ten blunt axes instead."
-- Dijkstra (1975, EWD498)

"Thank goodness we don't have only serious problems, but ridiculous ones as well."
-- Dijkstra (1980s, EWD475)
%
"How do we convince people that in programming simplicity and clarity —in short: what mathematicians call 'elegance'— are not a dispensable luxury, but a crucial matter that decides between success and failure?"
-- Dijkstra (1980s, EWD648)

"The question of whether Machines Can Think... is about as relevant as the question of whether Submarines Can Swim."
-- Dijkstra (1980s, EWD898)

"Simplicity is a great virtue but it requires hard work to achieve it and education to appreciate it. And to make matters worse: complexity sells better."
-- Dijkstra (1980s, EWD898)

"Probably I am very naive, but I also think I prefer to remain so, at least for the time being and perhaps for the rest of my life."
-- Dijkstra (1980s, EWD923A)

"I mean, if 10 years from now, when you are doing something quick and dirty, you suddenly visualize that I am looking over your shoulders and say to yourself "Dijkstra would not have liked this", well, that would be enough immortality for me."
-- Dijkstra (1995, EWD1213)

"Elegance is not a dispensable luxury but a quality that decides between success and failure."
-- Dijkstra (1990s, EWD1284)

"Industry suffers from the managerial dogma that for the sake of stability and continuity, the company should be independent of the competence of individual employees. Hence industry rejects any methodological proposal that can be viewed as making intellectual demands on its work force. Since in the US the influence of industry is more pervasive than elsewhere, the above dogma hurts American computing science most. The moral of this sad part of the story is that as long as computing science is not allowed to save the computer industry, we had better see to it that the computer industry does not kill computing science."
-- Dijkstra (1999, EWD1284)

# tweet erik meijer 

- #tags: #erik_meijer #dijkstra #twitter
- https://twitter.com/headinthebox/status/1339065478702981120?s=20
- "I was sitting next to EWD at a dinner. After I said something, he cried "oh my, you talk with your hands!" and for the rest of the night every time I opened my mouth, EWD closed his eyes."
