# Definition

A Zettelkasten (die Zettelkästen in plural) is a methodology for thinking in writing. It combines note taking while reading, personal thoughts and a bibliographic system.
It's an approach for a tool working more like the human brain, being processor and storage in once.

# Zettelkasten methode

- via: https://www.youtube.com/watch?v=4veq2i3teVk and https://www.youtube.com/watch?v=NbncA7bDl70
- entscheidend ist was aus dem gelesen für das vorhandene verwendtbar ist
- im vordergrund steht nicht die letzliche vollendeung des aktuellen zettels/gedankens sondern das sich das was auf dem zettel steht erst im kontextanderer anderer zettel herrausstelt !
- Wert eines zettels erst durch die verbindung!
- klärgrube wieder käuer metapher => Grothendieck learning metapher
- einziges kretierium für folgezettel: er sollte an den vorhergehenden passen? (TODO read blog luhman folge zettel)
- Themen sind durch multiple storage , zwangsläufig in verschiedene  andere Themen eingebetet -> mehrfach bedeutung, Zukunftsoffenheit durch vermeidung von festgelegter Ordnung
- Klumpenbildung und schwarze Löcher => die Ordnung passt sich der Gedankenentwicklung an! (bietet somit eigentlich Potential zur Reflektion der Gedankenstruktur weil sie dadurchgreifbar, serialisiert ist)
- branching Nummerierung ...TODO
- interne verweise
- 3 verweistypen 2- sammelverweise semantische zusammenhänge , verbindungen schaffen zwischen verwandten begriffen oder nur direkte wort nennung?
- Schlagwort register (tags) begriffsabbildung "relevanter" begriffe mit beispielhaftem verweis
- Zettelkasten IST ein Denkwerkzeug, eine Technik, jedoch primär bzw. wie Luhman ihn verwandte zur **Selbst** Reflektion

# More Intro Material

- https://strengejacke.files.wordpress.com/2015/10/introduction-into-luhmanns-zettelkasten-thinking.pdf 
- http://www.dansheffler.com/blog/2015-05-05-the-zettelkasten-method/
- http://www.zettelkasten.danielluedecke.de/doc.php (java desktop programm) 
- https://www.lesswrong.com/posts/NfdHG6oHBJ8Qxc26s/the-zettelkasten-method-1
- Zettelkasten app in verwendung: https://www.youtube.com/watch?v=T7zdyyTGPaI https://twitter.com/ctietze
- Erfahrungsbericht von obiger App hinzu notion: https://forum.zettelkasten.de/discussion/404/moving-on
- die Autoren des Blogs haben auch ein Buch geschrieben: https://zettelkasten.de/book/de/
- andere Tools: https://zettelkasten.de/tools/

# What is a Zettelkasten - Flow

- "Divide composing from editing your texts to reach a state of flow and get faster at finishing your projects."
- => its a writing/organising technique to get into the **flow state of writing/organising (thinking!)**
- https://zettelkasten.de/posts/overview/


# Zettelkasten resources from dgov

- http://www.dansheffler.com/blog/2015-05-05-the-zettelkasten-method/
- https://notepad.diglife.coop/pEnNb9nKT7CixwpMy1cbjQ#
- http://andersaamodt.com/binder.php 

# Hackernews

- via https://news.ycombinator.com/item?id=23386630
- https://eugeneyan.com/2020/04/05/note-taking-zettelkasten/
- trillum sql lite markdown notes but way to overloaded and of course a javaScript  night mare https://news.ycombinator.com/item?id=23335759

# Zettelkasten note-taking in 10 minutes

- https://blog.viktomas.com/posts/slip-box/
- 1) make notes as you read
- 2) write atomic, self-contained, permanent notes
- 3) link permanent notes together
- 4) tag permanent notes
- example zettelkasten by Andy Matuschak https://notes.andymatuschak.org/About_these_notes

# Podcast deutsch

- https://zwischenzweideckeln.de/zettelkasten-prinzip-soenke-ahrens/
- => Tobi, Ronny

# Thinking about Zettelkasten

- it came to me today: that the way guy sengstock talks about _distinctions_ in the context of circling, it is that the _atomicity_ of zettel within a zettelkasten plays a similar role
- when reading this https://emvi.com/blog/luhmanns-zettelkasten-a-productivity-tool-that-works-like-your-brain-N9Gd2G4aP it occured to me that you can't or shouldn't really flattening the structure, it is that off branching that grows distance between, in that example zettel 2 and 3, zettels - this makes the distinction more explicit

# Tools Discussion

- #tags: #software
- runar started a discussion relating to vimwiki, then I went to transclusion then roamresearch jumped in and introduced gtoolkit:
- zettlr.com is open source and OK for a start but many draw backs (see forum TOC discussion) and in genereal the tool idea is too much locked down: https://twitter.com/sahiralsaid/status/1277605825989705734
- via https://twitter.com/RoamResearch/status/1276049217434161152
- gtoolkit looks nice but since I can't code in smalltalk im using https://github.com/AndreasS2501/markdown-writer-fx for now and started with Philip a small effort to improve it

# Tools Summary

- #tags: #software #tools
- https://www.zettlr.com , markdown editor with simple ZK functions,open source and free to use, desktop app uses electron
- https://obsidian.md, markdown editor with slightly more features than zettlr free to use closed source , desktop app electron
- https://foambubble.github.io/foam/#how-do-i-use-foam free open source , builds on vscode via https://news.ycombinator.com/item?id=23666950
- https://roamresearch.com/ 15€ a month browser based, best feature support for ZK so far also transclusion
- https://emvi.com , browser based , number reference system?, intro https://emvi.com/blog/luhmanns-zettelkasten-a-productivity-tool-that-works-like-your-brain-N9Gd2G4aPv via https://news.ycombinator.com/item?id=22085837
- https://gtoolkit.com Pharo smalltalk spin off(image based) see moldable Developement
- notion.so https://matthiasfrank.de/zettelkasten-for-notion/