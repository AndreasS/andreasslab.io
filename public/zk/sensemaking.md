# Question

Can I ask what the circling community is? and/or the "sense making" space?? sounds super interesting.


# An Attempt to answer

hey 
sure! I just realised that the question is quite context dependent. I think there are at least 3 entries into the space: the wisdom traditions(buddhism, stoicism), self developement and social media channels with relations to 1 and 2. For me the Journey started with the YouTube Channel Future thinkers. The Channel started out with a variety of topics. Vitalik Buterin and Vinay Gupta were also a guests. I think the topics eventually changed from more technology focused episodes(AI, Blockchain) to more personal developement topics. Three people I want to highlight here are:
- john vervaeke
- jordan hall
- guy sengstock

I heard this podcast with guy Sengstock about circling: 
https://anchor.fm/emerge/episodes/Guy-Sengstock---Circling-Towards-Meaningfulness--Collective-Intelligence-e5g8j7
John Vervaeke is a cognitive scientist from the university of toronto he created a series in 2019 which is called: Awakening from the meaning crisis. This changed my perspective quite profoundly, as I changed from being a consumer (videos,podcasts, books etc) to a practioner. I discovered a discord server where people gathered a community arround the ideas from the series. With a focus on practice, eventually I found there a opportunity to practice circling once a week for 90 minutes. After almost a year now it has been really nuturing experience for me.
John Vervaeke is is conversation with a lot of people, so his discussions (voices with vervaeke) are a nice intro into the sensemaking space. In his series he introduces also thr term "psycho-technology" and I would call circling an example of such. 
there is also the rebel wisdom YouTube channel (they also do retreats). Future thinkes just started their smart/eco village project in canada(they did  a NFT crowdsale). There is also the stoa.
jordan hall has/had some connection to game b but lately also tries to get more involved with web3.