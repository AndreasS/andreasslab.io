# Ep. 17 - Awakening from the Meaning Crisis - Gnosis and Existential Inertia

- #tags: #john_vervaeke #youtube #aftmc #lecture #practice #play #ritual #sacred #personal #transformation #psychology #cross_reference
- #cross_reference: #andy
- https://www.youtube.com/watch?v=mXfK4nicygA
- sensibility transcendence -unthinkable (its something like my #impossible_thought maybe its an impossible feeling or experience?)
- the problem (suffering of being) existentially trapped
- vampire example (~28min):
- "I don't know what I'm gonna lose if I become a vampire I don't know what I'm going to lose once I go through this change I will have lost
a way of being it will become unthinkable to me I can't get back to it but I don't know what I'm gonna lose until I go through it so oh well then I shouldn't do it ah but if I don't do it I don't know what I'm missing I don't know what I'm missing there could be a way of being here that is amazing and wonderful I don't know what I miss it and I'm caught I equally don't know what I'm going to lose and I don't know what I'm going what I'm missing and I can't do any calculations I don't know what my values are going to are my values now the right set of values are my values then the right set of values is the kinds of experience I'm having now or the like there is no place above I can make the comparison
I can't reason my way through it" 
- from vampire to "should you have a child"
- pet - child example
- **ritual**: "that's what ritual properly understood is people are **playing serious** playing in order to try and put themselves into a _**liminal place a place between two worlds**_ the normal world and the sacred world they want to dwell within they're playing there in order to see **how** and **whether** they should go through the change in world and self that the religion is demanding and affording"
- the problem of not only being stuck (don't know how to transform) also stupified (should I transform)
- practice to get out of existentially trapped state: therapy, jeepform, martial arts
- therapy with an agapic element
- jeepform: - therapy with an agapic element 
- https://rpg.stackexchange.com/questions/107600/what-is-jeepform-vi-%C3%A5ker-jeep 
- "Jeepform is the name of a group of Swedish roleplayers and game designers and, by extensions, the brand of the games authored by members of the group. Because of the gaming culture inside the group, Jeepform games are often similar in theme and often share some techniques and ideas.

Their games often border experimental/improvisational theatre, as can be seen by several entries in their Dictionary.(http://jeepen.org/dict/)

The Freeform and Jeepform entries of the dictionary, when combined together, help understand what Jeepform really is to them. It is their own brand of freeform games, that are games where **the emerging story takes precedence over the form**.
- #book 10:29 - The Sovereignty of Good by Murdoch
- #book 16:40 - The Reasons of Love by Frankfurt
- #book 24:51 - Transformative Experience by Paul
- rediscover play
- **gnosis** : "gnosis is to have a **set of psycho technologies** that create a **ritual context** like **jeepform** like **martial arts** like **therapy** that allows us to **overcome** being existentially stuck existentially stupefied and that is being powered by an altered state of consciousness that's induced by chanting sleep deprivation psychedelics and what this does what **gnosis does is it frees me from being existentially trapped** it's this combination this integration of psycho technologies that activate and transform perspectival and participatory knowing and give us a sense of a greater reality that we want to live within and thereby liberate sus from being existentially
trapped and heals us from our fractured suffering our fragmented agency our broken world that's gnosis"
- talking to andy about this, especially about the vampire example helped me again to get the motivation to watch this again and to finally embody the knowledge
