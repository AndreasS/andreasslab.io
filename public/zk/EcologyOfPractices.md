# Harmonizing to Emerge: A New Ecology of Practices w/ Seishin, Daniel Thorson, and John Vervaeke

- #tags: #monastic_academy #john_vervaeke #daniel_thorson #practice
- the stoa
- https://www.youtube.com/watch?v=-0Tsg6SzK7Y
- https://growingwillow.org/
- Daniels approach to a new ecology of practices:
- 1. body energy
- 2. psyche or psycho-emotional life 
- 3. the third is relationships
- 4. the fourth is ethics 
- 5. the fifth is perception 
- JV asks: how do you do it
- How to do it:
- "sleep well"
- "eat right"
- "be surrounded by good people that you like
- walk be in nature
like you're doing right now
move your body and then we can talk
about like
all these other pieces uh on top of that 

# Developing an Ecology of Practices for the Meaning Crisis (w/ Daniel Thorson)

- #tags: #john_vervaeke #daniel_thorson #interview #youtube #ecology_of_pracices #meaning_crisis
- https://www.youtube.com/watch?v=xytWaEuvFVU
- daniels 5 pracises:
- 1. energy pracises: primary practice is https://en.wikipedia.org/wiki/Samadhi practice
- inspired by Rob Burbea http://www.robburbea.com/
- harmozining energy in the body through meditation, breaathing, body posture harmony
- DT: " breathe in such a way hold the body in such a way relate to it all in such a way that it comes into harmony that kind of energetic tensions and knots get worked out and we start to like settle in a deeper and deeper deeper way"
- 1 hour exercise
- eating healthy food
- get adequate sleep but not too much
- 2. psycho emotional : three main practices
- Gendlin Eugene Focusing https://en.wikipedia.org/wiki/Focusing_(psychotherapy)
- Dr. Douglas J. Tataryn - The Bio-Emotive Framework https://anchor.fm/emerge/episodes/Dr--Douglas-Tataryn---Emotions-in-Meditation-and-Human-Development-e423ti
- " and then we hold all that within the meta model of internal family systems" https://westcoastrecoverycenters.com/what-is-the-internal-family-systems-therapy-model/
- 3. relationships
- circling
- "eight of us full time living together"
- 4. Ethics
- joe edelman https://nxhx.org/ human systems https://anchor.fm/emerge/episodes/Joe-Edelman---Designing-Systems-for-Human-Flourishing-e4mbde 
- clearly articulated values and how they function in your life - JVs relevance landscape
- 5. perception
- rob Burbea's approach to emptyness DT: "ways of particiating with your experience that reducing clining" deconstructing the reality of perceptions
 samadi? Biomotive Framework going straight to the most painful emotional resvoir
- disenchantment - what does it mean to take practices "out of place and or context"?
- called to awakening or enchanment of the world
- DT: "you want to exapt the individual that's a construction obviously for the sake of the re-enchantment of the world and that's awesome 
- Q: was the connection to narrative with the ecology of pracises? Is there a connection to myth? ( is that the fear of fragmentation jonathan pageau talks about?)  DT: for me the call to planetary crisis
