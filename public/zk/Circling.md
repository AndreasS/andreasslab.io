# Guy Sengstock - Circling Towards Meaningfulness & Collective Intelligence

- #tags: #podcast #circling #guy_sengstock #monastic_academy
- https://anchor.fm/emerge/episodes/Guy-Sengstock---Circling-Towards-Meaningfulness--Collective-Intelligence-e5g8j7
- emerge podcast with Daniel Thorson, emerge podcast as a part of the monastic academy
- the role and meaning of distinctions (impact and experience)
- fed ex sign negativ space arrow
- the notion of the "horizion", forground background switching/merging
- that which is most near , consealed becomes clear
- alethia? the moment something veiled becomes unveiled
- stages of development, moving beyond self interest

# 7 Stages of Circling Webinar Recording

- #tags: #guy_sengstock #circling
- https://circlinginstitute.com/thanks-webinar-7stages-recording/

# Circling Principles from Ben

- #tags: #circling #principles #procedual_knowledge
- Welcoming everything
- Assuming nothing
- Owning your experience
- Honouring self and other
- Staying at the level of sensation
- Being in the world of the other
- Reveal your experience
- Commit to connection
- Trusting your Experience
