# EinLeitung

- Urpsrülich bin ich zu dem Thema über folgenden blog/Zettel gekommen:
- **Developing ethical, social, and cognitive competence**
- 

- Ich würde auf deutsch als Einführung hiermit starten:
- **Markus Fischer Podcast - Entwicklungstheorie nach Prof. Robert Kegan: Wie erwachsen sind Erwachsene? Teil 1 - Mehr Wissen oder mehr Sein Teil 2 - Wie Sie Persönlichkeitsentwicklung fördern  Teil 3**
- 
- diese Animation zeigt das Konzept nochmal sehr gut in Bildlicher Form:  **Robert Kegan's 5 Orders of Consciousness | A Animated Guide**
- 
- dieses 45 min Interview mit dem Autor erklärt nochmal in seinen Worten seine Theorie: **Robert Kegan: The Evolution of the Self**

- **# Future Thinkers podcast 062, 063: Jordan Greenhall – Deep Code: Learning How To Learn** zeigt wie man diese Theorie in Verbindung mit lernen bringen kann

- **Ken Wilber: Jordan Peterson and the evolution of thought** Ken Wilber hat die _Integral Theory entwickelt und zeigt oder möchte argumentieren das die Entwicklung von Bewußtsein sich auf viele Bereiche auswirkt, seine Farbliche/konzeptionelle Unterteilung ist interessant


# Markus Fischer Podcast - Entwicklungstheorie nach Prof. Robert Kegan: Wie erwachsen sind Erwachsene? Teil 1 - Mehr Wissen oder mehr Sein Teil 2 - Wie Sie Persönlichkeitsentwicklung fördern  Teil 3

- #tags: #robert_kegan #podcast #youtube #stages_of_developement #personal #change #adult_developement
- #file location: C:\cygwin64\home\Nutzer\connectingdots\CognitiveScience\CognitiveScience2021.md
- https://knotenloesen.com
- Teil 1: https://www.youtube.com/watch?v=8jNfzWmKCbU 30min
- Teil 2: https://www.youtube.com/watch?v=3WdeM_PQ-pg 22min
- Teil 3: https://www.youtube.com/watch?v=EddX5PsH43k 26min
- ich bin nicht eine bestimmte identität => ich habe iene bestimmte identität 
- die 6 stufen : 
- 0: der einverleibende geist - consuming mind, Körper und geist sind inkorporiert/vereinigt
- 1: impulsiver geist - impulsive mind : impulse ausleben aber wirklich ausleben auch unkontrolliert und nicht bewusst
- 2: souveräner geist - imperial mind - 6% Erwachsene: die ebene wo wir schon bewusst werden was wir ausleben aber dass er eben auf uns zentriert sind das kann man ein stück weit vergleich mit
dem was sie vielleicht kennen als die sogenannte egozentrische ebene aus anderen modellen
- 3: zwischen menschlicher geist - inter personal mind - 58% Erwachsene: sehr stark damit beschäftigt sich zu vergleichen sich einzufügen zu verstehen wo gehöre ich dazu mich zu orientieren an meiner umgebung 
- 4: selbst gestaltender geist - institutional mind 35% Erwachsene: bewusst sind dass sie sich selber auch verändern können die ihre gefühle bedürfnisse bewusst wahrnehmen und auch abgrenzen
können von denen anderer und nicht sozusagen immer anpassen und vergleichen müssen mit den anderen deswegen selbst gestaltende geist das hat also mehr von dem selbstbewussten wahrnehmen als das auf der ebene 3 möglich wäre
- 5: über individuelle geist - inter-individual individual mind 1% Erwachsene: see emerge podcast
- bildungssystem(de?) häufig erstmal so offensichtlich aufgebaut ist es geht um um input und inhalt von theorien um wissen und daten und fakten
- "transformation bedeutet eben dass es nicht darum geht den geist und unseren geist mit immer mehr wissen zu füllen sondern die grenzen dieses geistes zu erweitern"
- "diese transformation kann man so stück weit wird es auch nennt als eine persönliche kopernikanische verschiebung bezeichnen"
- "durch diese verschiebung ändert sich die wahrnehmung komplett plötzlich war die wahrnehmung des menschseins eine andere weil er nicht mehr per se im zentrum des ganzen stand und so was ähnliches passiert immer wieder auch beim erwachsenen werden wo sich ihm die das zentrum dessen was bin ich eigentlich radikal verändert"
- subjekt objekt verschiebung => ich bin katholik => ich habe eine katholische überzeugung
- Teil 2:  was ist der unterschied zwischen lernen und persönlichkeitsentwicklung
- lernen und trasformation
- horizontale entwicklung => translation lernen
- vertikale entwicklung => tranformation
- "es geht eben in der transformation nicht darum wissen anzusammeln, sie werden durch wissen durch input also auch durch bücher durchlesen und selbst durch diesen podcast höchstens dazu angeregt sich bestimmte fragen zu stellen aber wenn sie sich diese fragen nicht stellen und sie ehrlich beantworten und innerlich wirklich suchen dann passiert ja eben auch nichts **selbstreflexion muss man selber machen** damit sie wirkt damit bewusstsein entsteht"
- 3 einfache fragen zur möglichen Transformation
- was denke ich 
- was möchte ich
- was motiviert mich
- Teil 3
- Fähigkeiten und Erkenntnisse sind notwenidg 
- "nicht zu fordernd werden dürfen"
- "wir(müssen) wahrscheinlich vor allem eins entwickeln in diesem bereich dieses geduld, geduld und verständnis denn wir können entwicklung zwar fördern aber wir können sie nicht machen wir haben da sehr wenig kontrolle"
- "entwicklungsumgebungen zu gestalten also im seminar das so zu gestalten dass jeder in seinem rhythmus wirklich arbeiten kann" "bedürfnisorientiert" - jeden abholen dort wo er ist, erinnert mich an das was der Zen meister dazu sagte
- "der prozess erwachsenen des erwachsenwerdens ist ein prozess des immer bewusster werden zu über die eigene innenwelt"
-" also dieser prozess der bewusstwerdung dass das im zentrum steht der persönlichkeitsentwicklung"
- "diese arbeit im grunde eine erfahrungs arbeit" möglichkeiten schaffen => kontroll abgabe
- "ein stück weit wieder entspannen weil mir lernen egal wie ich es mache es ist quasi nie richtig oder nie perfekt das gefällt nicht allen"
- "das heißt im coaching oder der einzelberatung ist die anregung zur selbstreflexion das zentrale element geworden"

# Robert Kegan's 5 Orders of Consciousness | A Animated Guide

- #tags: #book #robert_kegan #consciousness #stages_of_developement
- #file location: C:\cygwin64\home\Nutzer\connectingdots\CognitiveScience\CognitiveScience_untilEarly2021.md
- #book: In Over Our Heads
- https://www.youtube.com/watch?v=mW4LTqRJDW8&feature=share
- 13 min
- Development models(sketch note)
- prezi https://prezi.com/yave1wdy4p_l/the-five-orders-of-consciousness-according-to-robert-kegan/
- three different lines of development: (1). Cognitive ((2)). Inter-Personal (((3))). Intra-Personal
- 1. Perceptions, Social Perceptions, Impules
- 2. Duarable Categories - Concrete, Point of View, Enduring Dispositions, Needs, Preferences
- 3. Traditionalism (historical Epic) - Cross-Categorial <-> Trans-Categorial, Abstractions, Mutuality, Interpersonalism, Inner States, Subjectivity, Self-Consciousness
- 4. Modernism (historical Epic) - Abstract System, Institution, Relationship-Regeulating Forms, Self-Regulation, Self-Authorship, Self-Formation
- 5. Postmodernism (historical Epic) - _Dialectical_ , Inter-Institutional, Relationship between Forms, Transformational Interprenetration of Selves
- Geometry Analogy: 
- 1st Order: Point
- 2nd Order: Line
- 3rd Order: 2D-shape
- 4th Order: 3D-Object
- 5th Order: Tesseract
- comment from December 2018: 5 stages, Level 3 tribal Level , the urge of many programmers to become AI coders => Level up, getting out of comfort zone, environment global consciousness

# Robert Kegan: The Evolution of the Self

- #tags: #youtube #interview #rebel_wisdom #robert_kegan #adult_developement
- #file location: C:\cygwin64\home\Nutzer\connectingdots\CognitiveScience\CognitiveScience_untilEarly2021.md
- https://youtu.be/bhRNMj6UNYY
- proving that development of the brain continues after 25
- constructive development theory graphic -- see over our heads 
- in 2nd half of interview , to apply this process as means to another end (not enjoying, realising the phases) can not work

# Future Thinkers podcast 062, 063: Jordan Greenhall – Deep Code: Learning How To Learn

- #tags: #future_thinkers #podcast #jordan_hall #robert_kegan #learning #mastery #sovereignty
- #file location: C:\cygwin64\home\Nutzer\connectingdots\Education.md
- https://futurethinkers.org/jordan-greenhall-deep-code/
- https://neurohacker.com/
- Robert Kegan (Evolving Self)
- the most important meta skill is to deconstruct learning
- Discernment 
- Attunement
- Coherence
- Clarity
- Insight
- Embodiment
- also mastery and sovereignty 

# Developing ethical, social, and cognitive competence

- #tags: #models #david_chapman #robert_kegan #stages_of_developement
- #file location: C:\cygwin64\home\Nutzer\connectingdots\Principles.md
- meaningness from David Chapman 2010-2019 : Meaningness is a hypertext book (in progress)
- https://news.ycombinator.com/item?id=12521772
- https://vividness.live/2015/10/12/developing-ethical-social-and-cognitive-competence/
- 5 stages, relations and skills within them
- http://meaningness.com/meaningness-history
- #books: evolving self, in over our heads
- http://meaningness.com/choiceless-mode
- http://meaningness.com/systematic-mode
- http://meaningness.com/nebulosity http://meaningness.com/pattern
- http://meaningness.com/self
- http://meaningness.com/neither-objective-nor-subjective
- http://meaningness.com/systems-crisis-breakdown
- http://meaningness.com/monism http://meaningness.com/dualism
- http://meaningness.com/boundaries-objects-connections
- http://meaningness.com/resolution
- http://meaningness.com/participation
- Hegel http://meaningness.com/metablog/bad-ideas-from-dead-germans
- why STEM// software is eating the world: http://www.wsj.com/articles/SB10001424053111903480904576512250915629460
- http://meaningness.com/metablog/pop-spirituality-monism-goes-mainstream
- http://meaningness.com/meaningness-and-time
- https://en.wikipedia.org/wiki/Lawrence_Kohlberg%27s_stages_of_moral_development
- https://en.wikipedia.org/wiki/Robert_Kegan

# Ken Wilber: Jordan Peterson and the evolution of thought

- #tags: #youtube #jordan_peterson #ken_wilber #integral #postmodern #interview #arthur_koestler #perception #stages_of_developement
- #file location: C:\cygwin64\home\Nutzer\connectingdots\CognitiveScience\CognitiveScience2021.md
- https://www.youtube.com/watch?v=bDjCnFvz11A
- Arthur Koestler https://en.wikipedia.org/wiki/Holon_(philosophy)#Holarchy
- missing distincting from peterson : growth hierachies and dominator hierachies
- "which is the world isn't just a perception it's a conception it's an interpretation, we have to interpret these things that we see"
- mapping stages of development :
- "Piaget tended to focus on cognitive development, Kohlberg tended to focus on moral development,  Maslow tended to focus on motivational
development,  Jane Loevinger focus on ego development" https://en.wikipedia.org/wiki/Loevinger%27s_stages_of_ego_development#Stages
- #book: the origins and history of consciousness by erich neumann foreword c.g. jung
- men, women and their changing roles throughout the civlizational developmental stages - agricultural, industrial, information
- sam harris and petersons misunderstanding - but both know waking up


