# Context 

- #tags: #book #yuval_noah_harari
- did come to this book through the long TED interview
- but also this: http://davidbyrne.com/journal/eliminating-the-human such as nations, money, religions and legal institutions
- and naval knowledge podcast

# thoughts on gossip

- #tags: #thoughts
- Somehow I think that much of the media crisis is related to our gossip traits

# Death is optional - excellent interview - between sapiens and Homo deus with kahneman

- #tags: #yuval_noah_harari #daniel_kahneman #youtube #article #interview #death
- https://www.edge.org/conversation/yuval_noah_harari-daniel_kahneman-death-is-optional 
- youtube: https://www.youtube.com/watch?v=JKOQvn0OY-w
- via http://lesswrong.com/lw/m0a/sapiens/

# Yuval Harari - Sapiens: A Brief History of Humankind

- #tags: #youtube #sapiens #yuval_noah_harari #book
- https://www.youtube.com/watch?v=eOO5xrEiC0M
- its not enough to "just" develop technology, we need new ways of thinking , new stories

# Part One the Cognitive Revolution

- #tags: - #tags: #sapiens #yuval_noah_harari #book #notes #perspective #frame #technology #stories
- frame: Physics => Chemistry => Biology => History
- Fire - Language - Gossip
- Cooking
- Fictions(stories) - **Imagined _orders_** ==> order <=> control relationship

# The Prison Walls

- #tags: #sapiens #yuval_noah_harari #book #notes #perspective #structure #relationship #society #individual #group
- P. 127 a. The imagined order is embedded in the material world
- P. 128 a. The imagined order shapes our desires
- P. 131 a. The imagined order is intersubjective
- Australia as a example of sapiens greatest devourer 
- p. 131 /imagined orders are inter subjective :Objective - Subjective - inter subjectiv
- dunnbars number
- p. 41 "the immense diversity of imagined realities that Sapiens invented, and the resulting diversity of behavior patters, are the main components of what we call ,cultures'"
- p. 43 playing ever more complex Games 
- Fictions -> Art Lion Man - Peugot-> "Limited" Cooperations
- using Function to invoke Cooperation, the level of complexity of things people can do raised dramatically, single -> dozen -> 150 -> 1000
- imagined orders help dealing with strangers

# 1 An Animal of No Significance

# 2 The Tree of Knowledge

# 3 A Day in the Life of Adam and Eve

# 4 The Flood

# Part Two the Agricultural Revolution

- #tags: #sapiens #yuval_noah_harari #book #notes #perspective #money #religion
- Domestication of Animals (success for chicken?): Shepard -> Religion
- Race Discrimination - Gender Discrimination
- The influence and self Perspective of Empires
- Currency -> Coins -> fraud with coins : Majäestäts Beleidigung

# 5 History Biggest Fraud

- #tags: #sapiens #yuval_noah_harari #book #notes 
- Why agricultural Revolution? For reproduction...
- Population explosion
- thinking about the future more important than myths

# 6 Building Pyramids

# 7 Memory Overload

- #tags: #sapiens #yuval_noah_harari #book #notes #technology #desires
- dunnbars number , humans 150, primates 50 => social devide : (us) vs (them) ===> so would be feel "lonely" if humankind was unified completely? Kennt den find hat de rtag struktur?

# 8 There is no justice in History

- #tags: #sapiens #yuval_noah_harari #book #notes #perspective
- Memes, Postmoderism, Game Theory

# Part Three the Unification of Humankind

- #tags: #sapiens #yuval_noah_harari #book #notes #money #religion
- 1. monetary Order
- 2. imperial Order
- 3. religious Order
- monotheism <-> Dualism
- Mono | Dual (mighty God , free Evil) | poly (saints)| Ani (Ghosts)
- Liberal vs Social ( Precht Feiheit vs Gleichheit - ein fundamentaler kampf in der Gesellschaft)
- Sykretismus: Liberal, Social, Nazis
- Budhismus - craving - begehren
- Idiologies as religion
- Hamurabi - US Constitution Eqal men but Women and Slaves?
 
# 9 The Arrow of History

- #tags: #notes #sapiens #book #order #control
- P.182  "Unlike the laws of physics, which are free of inconsistencies, every man-made
order is packed with internal contradictions."
- P. 191 "The first universal order to appear was economic: the monetary order. The second universal order was political: the imperial order. The third universal order was religious: the order of universal religions such as Buddhism, Christianity and Islam."


# 10 The Scent of Money

- #tags: #sapiens #yuval_noah_harari #book #notes #money

# 11 Imperial Visions

# 12 The Law of Religion

- #tags: #sapiens #yuval_noah_harari #book #notes #religion #buddhism #desires
- remove craving ((Buddisim)) child experience, live in the moment

# 13 The Secret of Success

# Part Four the Scientific Revolution

- #tags: #sapiens #yuval_noah_harari #book #notes #science
- Where did come from? When did it happen?

# 14 The Discovery of Ignorance 

- #tags: #sapiens #yuval_noah_harari #book #notes #science

# 15 The Marriage of Science and Empire

- #tags: #sapiens #yuval_noah_harari #book #notes
- Knowledge and terrain as power factors
- the extraordinary european sailors
- imperial scientits

# 16 The Capitalist Creed

- #tags: #sapiens #yuval_noah_harari #book #notes #money #value
- Credit => The value of the imagined future, acutally a tight system of control for (future) impossible thoughts

# 17 The Wheels of Industry

- #tags: #sapiens #yuval_noah_harari #book #notes #industry
- industrial revolution => Energy Conversion Revolution
- industrial time scale

# 18 A Permanent Revolution

- #tags: #sapiens #yuval_noah_harari #book #notes
- history is not just
- Communism, Capitalism => whats happens with feminists after death? importance of death degrades [[**Death**]] is optional
- Postmodernism, Game Theory, Science and Technology

# 19 And they lived Happily ever After

- #tags: #sapiens #yuval_noah_harari #book #notes
- he said the winner makes up the rules, so what does that make of capitalism and communism?

# 20 The End of Homo Sapiens

- #tags: #sapiens #yuval_noah_harari #book #notes
- 1. Brain Computer Interface
- 2. Cyborg
- 3. Full Artifical, anorganic "life"
- Happines?

# The Animal that became a God

- #tags: #youtube #sapiens #yuval_noah_harari #book #notes #meaning
