[[_TOC_]]

# Psychologie

## Jordan Peterson

- jungian framework
- life is suffering
- try not to reach for happiness as a goal but meaning => meaning crisis ==> eudamonia
- chaos vs order
- and order means hierarchy (what about networks?)

- start : may 2018 interview Anne McElvoy, see telegram source

**motivation**

- https://www.youtube.com/watch?v=eMo_20J1J1Y

**interview Anne McElvoy**

- https://www.youtube.com/watch?v=7QRQjrsFnR4&feature=share
- different expressions of aggression for men and women
- defining attractiveness in dressing when men and women work together (its not a solved problem)

**on Jung, Freud and Nietzsche**

- https://www.youtube.com/watch?v=qZKV-Bii5Mg&feature=share
- Göthe Faust
- what nietzsche meant by god is dead
- even Piaget is mentioned at the end -> evolving self

**Nietzsche - Beyond Good and Evil**

- https://www.youtube.com/watch?v=MCOw0eJ84d8&feature=share
- IDEAS Authors of great works unconsciously collect patterns from their experience of living in the world. The patterns can be deep and multi-level. Their initial (conscious) formulations of these patterns become seeds for future ideas. The more poetic the author, the more likely his/her works contain seeds for future ideas. In this particular example, Nietzsche is not only ‘creating seeds’, he’s also telling us how he’s doing it.
- VALUES Nietzsche was very concerned with values—how we should act in the world. And, given that he wrote at a genius level, many of his insights are useful and relevant to how we live our lives. In this passage he is particularly interested in the relationship between individual and collective motivations, how each interacts, influences and constrains the other, and how this process results in the co-organisation of the individual psyche and society at large. By taking the time to organise our values, recognising and restructuring our short, medium and long term motivations (that are often in conflict with one another), we’re better able to function in the world—maximising the likelihood of a productive life, whilst minimising the damage we inflict along the way.
- UNCONSCIOUS BIOGRAPHY The philosopher is not a ‘rational’ being capable of perceiving objective truth or thinking and acting rationally, but, rather, a LIVING being, biased for survival, where any and all truth perceived is (unconsciously and consciously) used to further both implicit and explicit life goals. No matter what the philosopher thinks he’s doing (e.g. ‘rational’ discourse), what he’s actually doing (due to a deep human motivation to tell stories) is revealing aspects of his ‘being’—his individual, biological, evolutionary, collective, historical biography. 
- THE HUMAN PLANT GROWS FROM MORAL PURPOSE The philosopher always has a purpose, an agenda—even if they don’t know it. And the philosopher tries to reveal and articulate that purpose (to themselves and others). And this purpose/agenda emerges from an individual with an ancient evolutionary history, who’s integrated within a shared social and cultural heritage. 
- KNOWLEDGE IS A TOOL NOT A PRIMARY MOTIVATOR The desire for knowledge isn’t the primary motivation of a philosopher (though many mistakenly believe it is). Knowledge is a tool a philosopher uses (both unconsciously and consciously) to further implicit and explicit life goals.
- IMPULSE DOMINATION The human psyche is a dwelling place of creative and destructive ‘spirits’—impulses, independent of one another, desirous of domination and of ruling the psyche unopposed. Further, society reflects this same competitive process through structured hierarchy. And the nature of a specific hierarchy is dependent upon which impulse (creative or destructive) rules from its peak.﻿

**Why do young people like to party**

**debate on the gender pay gap, campus protests and postmodernism channel 4**

- https://www.youtube.com/watch?v=aMcjxSThD54
- staying calm when confrontet with: your saying , yoursaying....
- JP: so your not very aggreable and thats what you got into your position
- JP: so your making me uncomfortable and thats your job ...
- some analysis of the interviewer : https://www.youtube.com/watch?v=nS9W-wlJHPA
- general analysis of JP => get your life straigt - according to 12 rules - is ok, but do we need to set this straigt in TV interviews?
- her reaction when he makes her ackowledge her role in macking other people uncomfortable as a result of "doing her job"
- JP says a lot is wrong when young men seek confirmation from him over youtube, but thats the point: what are the real causes, not sypmtoms of this? Why are instituions failing? which instituions are failing and ==> How will JP engage in the process of redesining /creating new instiutions which do not have such negative influences?

**Jordan Peterson and Susan Blackmore - Do we need God to make sense of life?**

- https://www.youtube.com/watch?v=syP-OtdCIho
- good and calm conversation
- her definition of memes vs his - conflicting

**Life of Buddha**

- https://www.youtube.com/watch?v=ycIAW0M2-NQ
- old video his style is different

**Jordan Peterson - The Tragic Story of the Man-Child- Peter Pan**

- https://www.youtube.com/watch?v=JjfClL6nogo
- Pan the God of eveything?
- the boy that is magical, Childrean are magical, they are nothing but potential
- Captain Hook , the crocodile (time!) already has a part of him
- Wendy vs tinkerbell the fairy of porn , she doesnt exist
- sacrifice the potentiality for the actuality of a frame
- the child potential is still low resolution , you could be anything but you are not anything
- jung : proper path of developement in the last half of life is to rediscover the child that you left behind as you where appretecing and then you get to be something and regain that potential at the same time
- shift in time that you can slack of (it was shorter when jordan was young)
- corporate jobs vs part time starbucks? - too bad his students don't challange him..

**cathy newman**

- https://www.youtube.com/watch?v=aMcjxSThD54
- when he said : ha I got you he later said he should have used that opening to start a real conversation

**a (good) critique on jordan peterson/maps of meaning**

- https://medium.com/@alexanderdouglas/jordan-petersons-maps-of-meaning-no-it-s-not-legit-763d9373db65
- The beginning of the maps of meaning lecture was in some sense exhausting because of the way JP _imposes_ his believe structure as absolute truth
- but I think in some sense thats because of his framework, his foundational believe of chaos and order, he draws sharp lines IMO too sharp 

**Why People Are So Unhappy - 2017**

- https://www.youtube.com/watch?v=P-8XbT97Meo&feature=share
- at least part of it is due to complexity of society, the future is uncertain and its uncertain that what you know about the past is going to help in the future
- society is philisophically unstable
- people are doubtful about wether their lives have meaning
- issues with diet
- cravings of bacteria influencing mood and other things in people

### Dune

- https://www.reddit.com/r/dune/comments/a20qzd/i_swear_jordan_peterson_believes_in_the/
- Joe Rogan (#1208) https://www.youtube.com/watch?v=vIeFt88Hm8s
- 24:40-26:40 minutes 
- from jewish: https://en.wikipedia.org/wiki/Kefitzat_Haderech in Dune Kwisatz Haderach

### Rebel Wisdom - YT Channel

- as a big (meta) reflection on JP

**Glitch in the matrix**

- https://www.youtube.com/watch?v=trhTbEs2GGE
- short analysis of the cathy newman video
- at 33 min :her anima - male archetype - possession
- 2 M views on JPs YT 
- blue church - the definitve influece of TV in 
- too simple conclusion

**integrating shadow**

- rebel wisdom Zen master JP : as a projective figure between surfaced conflicts => just had a thought here about FFF Luisa Neubauer- twitter always a pack of hate in your pocket => so the internet allowed for all that to rise (plus the negative media) now what to do about i- 

**Telling the Story of Jordan Peterson**

- https://www.youtube.com/watch?v=CZC9byuBjXA
- Interview with Filmmaker Patricia Marcoccia 
- JPs hardening after all these public battles
- his friendship withe indian man

### Maps of Meaning - The Architecture of Belief 2017 PSY 434 

- https://www.youtube.com/watch?v=I8Xc2_FtpHI&list=PL22J3VaeABQAT-0aSPq-OKOpQlHyR4k5h
- this series is an example represenstation of JPs believe system
- the aspect of interpretation: the pinoccio movie is disected as in the movie "The Da Vinci Code " , yes one may interpret the symbols in the movies as such but it still is _intepretation_ !

#### Transcription

- https://www.yousubtitles.com/2017-Maps-of-Meaning-1-Context-and-Background-id-1138062
- https://beyondhumannature.wordpress.com/2018/02/01/maps-of-meaning-2017-lecture-9-patterns-of-symbolic-representation-jordan-peterson-transcript/

#### 2017 Maps of Meaning 01: Context and Background

- ICBMs and communist
- he says Bill gates and Steve jobs did great things and thats why they have a lot of money, but that seems just absolutely too simplistic, what if the great that bill gates has done was through unfair means?
- Who judges what is "good" the market?
- His comments and judgements 
- his style is very much a story telling style
- lets create a story - argument map
- Sentence 1 ==> Sentence 2 (story - argument - Reference)
- he praises the current system and his selectivly highlights only the negative aspects of what he percieves as "communism", he seems like will in "good will hunting" as he has read a lot of books, but does he derive his value judgements from experience?
- He only mentions China and North Cora but not Cuba
- he names our current system as reliable but isn#t the UNSUSTAINABILITY of the system , in other words its un-reliability is increasing because of its contradictional relatioship with nature
- colonising, resource wrs are complety ignored by his picture - I guess he needs this for his message to be **seemingly** coherent
- believe systems value systems 
- Britian was "not that bad off" ( look in argument map for exact wording) see hans rosling turkey and other state in the world that WERE way better off at that time, not bad off on what degree? Life expectancy? GDP? How to even messure
- he uses all his threads to "spin" a story of singluar truth
- somehow he is missing different perspectives - practices?
- Pain and Chaos as fundamental base points (he uses a different term in his lecture)
- the relation between Order and Chaos the need to know "of where to be"

#### 2017 Maps of Meaning 02: Marionettes and Individuals (Part 1)

- Hobes and Rousseau
- Jean Piaget 
- sharing war/conflict affinity with chimpansees 
- presupposition social contract 
- playing games and acting out stories
- stories and pieces of puzzles
- name of that movie he is looking for?
- little knight story
- different kinds of games for different personality types : TODO list them
- the purpose of university education is to make (young) people more articulate !
- disney pinoccio

#### 2017 Maps of Meaning 03: Marionettes and Individuals (Part 2)

#### 2017 Maps of Meaning 04: Marionettes and Individuals (Part 3)

- ==> possibly mentioned before : a structure (even badly) is better than no structure

#### 2017 Maps of Meaning 05: Story and Metastory(Part 1)

- I misunderstood it, he said it right : the relationship between sex and procreation had become taboo
- Object  : the object in the world - but also TO object to something
- Matter: the world is made out of matter, but it also matters to you

#### 2017 Maps of Meaning 06: Story and Metastory(Part 2)

#### 2017 Maps of Meaning 07: Images of Story & Metastory

- Why harry potter was so successful, how to get kids to read 600 pages books, drawing from a intercultural mythological substrate
- "whats the ultimate order?" => no fixed state but the process and the will for further improvement
- can you let go of your old/puppet self without getting killed(trauma getting stuck)?
- **A Billion Wicked Thoughts** Book - https://www.youtube.com/watch?v=-xJqWXKyhlc&feature=emb_logo
- someone looked into what he said about the book: https://alexandbooks.com/archive/what-jordan-peterson-got-right-wrong-about-a-billion-wicked-thoughts
- Men search for visuals of pornography: true
- Women search for literary representations of pornography: true
- There are 5 types of classic male monsters: JBP said: Vampire, Werewolf, Billionaire, Pirate, Surgeon
- Book: Doctor , Cowboy, Boss, Prince, Rancher, Knight, Surgeon, King, Bodyguard, Sheriff
- why Peterson said werewolves and vampires were part of the list: These paranormal heroes basically have all the alpha male traits women desire, but cranked to the max.
- Women want men who are aggressive yet civilized:  true!

#### 2017 Maps of Meaning 08: Images of Story & Metastory

- are things that survive in culture _real _ even if they exist only in the cultural context?
- ~ 4 min he is hinting towards a fixed neuropsychology of Symbolic Representation - "and there is no way of changing them as far as I can tell without us being radically and Incomprehensibly different" <- so Nietzsches Übermensch should stay in the _loop_ and would be incomprehensible? Relation to posthuman/transhuman?
- So what are these elements? "they're expressed in mythology, but they're not merely symbolic"
- our senses are tuned for a particular duration raning from: roughly speaking"a tenth of a second to three three years"


- mesopotamian Gods  lecture 8 cut
- https://www.youtube.com/watch?v=JodNMjXphKA
- gilgamesh song
- https://www.youtube.com/watch?v=QUcTsFe1PVs&feature=share
- "In those days, in those distant days, in those nights, in those remote nights, in those years, in those distant years; in days of yore, when the necessary things had been brought into manifest existence, in days of yore, when the necessary things had been for the first time properly cared for, when bread had been tasted for the first time in the shrines of the Land, when the ovens of the Land had been made to work, when the heavens had been separated from the earth, when the earth had been delimited from the heavens, when the fame of mankind had been established..."

#### 2017 Maps of Meaning 09: Patterns of Symbolic Representation

- "Platonic forms are archetypes, essentially"
- Why be virtuous?, So you can bare the suffering of life without becoming corrupt

#### 2017 Maps of Meaning 10: Genesis and the Buddha

- JPB Views: Chaos and Order, and Order means Hierarchy 
- in you are things that think
- the dialectic of thought patterns :
- ~ 58 min _"waking thoughts sacrifices completeness for coherence"_
-  _"dream thought sacrifices coherence for completeness"_
-  "the richness, the unarticulated (would) be lost in the premature attempt to bring logical closure to the phenomena"
-   "precise thought excludes too much and imprecise thought is not sufficiently coherent so we do _**both**_" 
- ==> I am so reminded by this dialectic of the programming static/dynamic dialectic: you need both!
- its so much a sign of that we need more and better techniques to transcended between multiple leves fluently, john vervaekes technique efforts remind me of that 

#### 2017 Maps of Meaning 11: The Flood and the Tower

#### 2017 Maps of Meaning 12: Final: The Divinity of the Individual

**Das BIG FIVE Persönlichkeitsmodell (Aspektskala von Dr. Jordan Peterson)**

- https://www.youtube.com/watch?v=CejSMAsFwgI&feature=youtu.be&t=84

## Carl Gustav Jung

- Begründer der analytischen Psychologie 
- anima und animus https://de.wikipedia.org/wiki/Animus_und_Anima
- arche typen https://www.youtube.com/watch?v=wywUQc-4Opk
- Synchronizität
- Evil 
- his personal Moment of "self" awakaning
- Biografie , Beziehung zu Freud

### Atom and Archetype

- via  https://twitter.com/cryptowanderer/status/1189221918072803328
- https://www.brainpickings.org/2017/03/09/atom-and-archetype-pauli-jung/
- book: https://www.goodreads.com/book/show/1039648.Atom_and_Archetype
- Wolfgang Pauli and Carl Gustav Jung correspond and develop the concept of **synchronisity** 

### Arche types

- https://en.wikipedia.org/wiki/Jungian_archetypes
- came here for a list, but it doesn't make sense to create one
- some could be: Hero, King (grown up King)
- deeply connected to the stages of life: heros journey - the transition of adolescence to adulthood
- initiation rituals in our culture, separating the male infant/child from the mother, its a strong bond 
- if you still want a list: Jung described archetypal events: birth, death, separation from parents, initiation, marriage, the union of opposites;
- archetypal figures: great mother, father, child, devil, god, wise old man, wise old woman, the trickster, the hero; and archetypal motifs: the apocalypse, the deluge, the creation
- its us! the archetypes are our cultural heritage, people at different stages in their life and how we remember, relate to them 
- The **anima** archetype appears in men and is his primordial image of woman. It represents the man's sexual expectation of women, but also is a symbol of a man's possibilities, his contrasexual tendencies
- The **animus** archetype is the analogous image of the masculine that occurs in women
- The **shadow** is a representation of the personal unconscious as a whole and usually embodies the compensating values to those held by the conscious personality. Thus, the shadow often represents one's dark side, those aspects of oneself that exist, but which one does not acknowledge or with which one does not identify

**Venturing into Sacred Space | Archetype of the Magician - wise old man, wise old woman**

- https://www.youtube.com/watch?v=4unDD4OUUNQ aborigines and space

## Jean Piaget

- assimilation and accommodation(die and reborn god, let go of old self go new..) ==> related to thomas kuhn - Paradigmshift
- Robert Kegan
- Jordan Peterson on the logos, Piaget, Carl Jung and ideology 2018 https://www.youtube.com/watch?v=l8itHnfREIg&feature=share

- Piaget structualism: https://pdfs.semanticscholar.org/7bf3/15983f70851b649e88ac26be31cdf3676594.pdf

## Robert Kegan 

- Books: over our heads, the evolving self
- antagonist to Peterson?
- Development models(sketch note): https://www.youtube.com/watch?v=mW4LTqRJDW8&feature=share
- 5 stages, Level 3 tribal Level , the urge of many programmers to become AI coders => Level up, getting out of comfort zone, environment global consciousness 

**Rebel wisdom Interview**

- https://youtu.be/bhRNMj6UNYY
- proving that development of the brain continues after 25
- constructive development theory graphic -- see over our heads 
- in 2nd half of interview , to apply this process as means to another end (not enjoying, realising the phases) can not work

**emerge podcast Robert Kegan - The Five Stages of Adult Development (And Why You Probably Aren't Stage 5)**

- https://anchor.fm/emerge/episodes/Robert-Kegan---The-Five-Stages-of-Adult-Development-And-Why-You-Probably-Arent-Stage-5-eb8gug
- when he started his work the concept of adult mental developement was scientifically unexplored , so when the physical developement of a person was complete at arround 25 similar assumptions about peak and decay for mental?!
- to date only > 40 people have been stage 5 according to RK
- notes?

## Ken Wilber

- integral - relation to jamie Weal?
- see integral altitudes http://pialogue.info/definitions/Integral_Altitude.php
- jordan peterson - awakening 
- future thinkers https://www.youtube.com/watch?v=py07pZ_3Yn4

### Why Wake Up 

- https://www.youtube.com/watch?v=auKqzbhXZGM
- Future Thinkers
- TODO : watch

### the rise and fall of Ken Wilber/integral

- https://www.youtube.com/watch?v=TK9PWQeAQZg&feature=share
- https://markmanson.net/ken-wilber
- failed coherence - practice - impact - individual to group relationships ( ego and other negative aspects STILL did prevail)
- "But ultimately, he was done in by his pride, his need for control and, well, ironically his ego"
- "But what he seems to have missed is that worshipping consciousness development itself, Wilber’s so-called “second-tier” thinking, leads to the same disastrous repercussions Wallace warned of: vanity, power, guilt, obsession"

## John Vervaeke

**Overcomming the meaning Crisis - Future Thinkers Episode 98-99**

- https://futurethinkers.org/john-vervaeke-overcoming-the-meaning-crisis/
- notes and refs !!
- see memetic tribes post

### Memetic Tribes

**memetic tribes post**

- written by https://twitter.com/Ideopunk (leaning towards  Eliezer Yudkowsky term of rationality) and  https://twitter.com/peternlimberg (stoic?) By Peter N. Limberg and Conor Barnes
- letter wiki conversation: https://letter.wiki/conversation/116
- https://medium.com/s/world-wide-wtf/memetic-tribes-and-culture-war-2-0-14705c43f6bb
- memetic tribes https://docs.google.com/spreadsheets/d/11Ov1Y1xM-LCeYSSBYZ7yPXJah2ldgFX4oIlDtdd7-Qw/edit#gid=0

**Rebel Wisdom - Culture War 2.0, Peter Limberg**

- https://www.youtube.com/watch?v=jHGbA5XcjIo
- from conservative vs liberal ( civil rights movement)
- from bi polar conflicts(cons/lib , us/sovj) to multipolar conflicts see spreadsheet tribes
- Intelectual explroers club (created spreadsheet)
- ~ 16 Global Village from Marshall McLuhan is finally there , tension because throgh technology proximity is increased in a - naturally? - unpleasent way, disagreements on all fronts
- dissimilarity casscades- the more you know about someone the more you dislike them - corrupting environments - social media
- memetic mediation?
- blue pill - culture / red pill anti thesis to culture, grey pill: _relative relaxation_
- in some sense this, _relaxation_ rembered me of integral Levels and also of adult developmental stages according to robert kegan
- possible ? technique for improving the situation: anti-debate - authentic dialog
- comfort with uncertainy- 

**Rebel Wisdom - Flow states and Wisdom**

- https://www.youtube.com/watch?v=5jS2AiQEz-E
- is the psychadelic experience objective or subjective => its transjective... its changes your understanding of you, it changes you and so you can percieve the world in a different way

**A Quality Conversation with John Vervaeke and Sevilla**

- https://www.youtube.com/watch?v=8d2vlpieEH8&feature=youtu.be
- Zen and the art of motorcycle maintenance 

**The Meaning Crisis and Silicon Valley with John Vervaeke**

- https://www.spreaker.com/user/10197011/the-meaning-crisis-and-silicon-valley-wi
- very good podcast/Interview
- JV explaining our desire to develop/change (aristotle)
- 3 negative points about Jordan Peterson , plus his hierarhy argument via biology is at least doubtful
- video game exodus, why people play video games ..list!, and they believe they can't find it/these values anymore in reality

### Series - Awakening from the Meaning Crisis

**Episode 1**

- https://www.youtube.com/watch?v=54l8_ewcOlY
- cognitive processes and cognitive mechanisms
- Upper Paleolithic transition it occurs around 40,000 BCE 
- (shamans) noticing patterns in nature ( moon phases, even seasons)
- throwing as a deep metaphor in our language still until today
- how religion evolved (book: supernatural selection Rossano) and the near extinction event for humans
- near extinction event => how did humans adapt? In all ways! Settling, diet but especially social technology/change invention
- rituals, shaking hands, trust

**Episode 2 - Flow, Metaphor, and the Axial Revolution**

- https://www.youtube.com/watch?v=aF9HeXg65AE
- Video Games and the Flow state
- implicit learning and the flow state
- (Upper Paleolithic transition) https://en.wikipedia.org/wiki/Behavioral_modernity - when humans nearly went extinct
- shamans masters of the flow state!
- Rock Climbing as a flow state
- Kuneai?! Alphabet and after the invention of vowels
- reading gilgamesh and ancient egpyt myth stories

**Episode 3 - Continuous Cosmos and Modern World Grammar**

- https://www.youtube.com/watch?v=C1AaqD8t3pk
- Axial Revolution https://en.wikipedia.org/wiki/Axial_Age
- second order thinking - reflective
- the meaning of myth

**episode 4 Socrates**

- asking questions to people in public to reveal the "obvious"contradiction between their current executed action and their stated long term goal


**episode 5 plato and the cave**

- want to share experience 
- eu...
- why people do stupid things? Because of inner conflict 
- Plato's theory :
- =+> man in your head - represents reason , truth/false , abstract thoughts, long term goals
- <+= opposed to it is a monster: works in terms of pleasures and pain,stomach genitals, appetite superficial thoughts , short term goals quick reactions 
- <=> Lion, chest,thymos ,Social cultural Domain , cultural aspects shared meaning 
- the myth of the cave: coming to light, JV explains very precise and nicely the context for the metapher that this story should convey
- the matrix as a neo (ha!) platonist  reformulation of the allegory of the cave
- how the process of enlighment is, coming to enlightment is central to plato and also a reason why he is so popular and often revisited

**Episode 6 aristotles**

- be-cause : giving an explanaition that _causes_ it to _be_
- from aristotle to newtons view - pushed by Kant, (causal order?) if A then B then C , Backtracking of C gets to B gets to A and thus helps to avoid circular explanaitions
- circular explanation example: little man triangle - infinite regress
- potential <=> actuality in-(form)ation
- change as an anology for development
- form - eidos
- In Formation: you put a form into something and you actualize its potential ==> you will give it a structural functional organisation

**Episode 7 - Aristotle's World View and Erich Fromm**

- self deception and rationality
- rational does not ONLY mean to be locigal
- rationality core capacity: reflective realising - self deception and self correction
- for aristotle => realising the potentential through cultivation the character
- the meta drive : need to be in contact with reality (core motivation of rationality)
- the books aristotle wrote: physics, meta-physics (philosiphy), psychology, animals movement, how to write books
- ~ 30 arena - agent

**Episode 8 - The Buddha and mindfulness**

**Episode 9 - Insight**

- agent arena relationship
- how contemplation and meditation are at opposite ends
- the coordinate system
- PCE
- https://en.wikipedia.org/wiki/Sati_(Buddhism) - mindfulness or awareness
- mindfulness (definition) is doing:  basically teaching us how to appropriate and train a flexibility of attentional scaling so that we can intervene effectively in how we are framing our problems and increase the chances of insight

### Future thinkers watch party 

- https://docs.google.com/document/d/1WhacRQY-Cv34HSnfAAnBJEABET-8I__Q7RNkypf43QU/mobilebasic

### David Chapman letter exchange about - Joker

- https://letter.wiki/conversation/209

### Jordan Hall, John Vervaeke, Christopher Mastropietro & Guy Sengstock in Diologos

- https://www.youtube.com/watch?v=7g6rwOa-pGs 1 
- https://www.youtube.com/watch?v=UovLJTLbFhU 2
- https://www.youtube.com/watch?v=lcDNqys1YgQ 4

### Collective intelligence into collective wisdom w/ Taylor Barratt - Voices with Vervaeke

- via https://twitter.com/vervaeke_john/status/1259348701211955200?s=12
- https://www.youtube.com/watch?v=NpaN59gxE28&feature=youtu.be
- JV: "learning dialogos" by play , pretend and then comprehend very much like kids
- JV how to create a pedagogic programm to create the "right kind of mood" for dialogos
- question for TB: why isn't circling enough? Guy Sengstock still calls it Circling 2.0
- how to do dialogos experiments
- emotional gym
- Virtual socrates, distributed influece? https://www.youtube.com/watch?v=0GjdjccFJVg
- putting something forward which then not understood

# Evolutionary Biology 

**Bret Weinstein on the Dawkins Debate**

- link to dawkins debate
- John Vervaeke mentioned the psycho technics , BW sees Religions as a evolutionary adaption, so there are biological factors which favor the application of religions but the question is could this principle be applied for mindfull ness?
- ~ 15 BW clearly articulates it:  "we are programmed we are culturally programmed for something that cannot be defended" (--I think thats a very nice way of saying that we are currently something very bad that we don't understand)
- "we need to figure that out very quickly, so that we can get on to reauthorize towards objectives that actually match the values we think we hold our conscious minds need to reauthorize purpose" 
- "so that we can not simply be the the the slaves of an evolutionary purpose that is not honorable and will lead to our extinction"
- ==> so he is basically wishing for the uncovering of the shadow - but from a biological perspective

# Neuroscience 

**Dream state - how to create a mind**

- https://news.ycombinator.com/item?id=16903826
- "I was introduced to this phenomenon through Kurzweil's book "How to create a mind". He claims that the brain is massively parallelized pattern recognition machine, with consciousness being a censor that filters results. While in hypnagogia, this censoring function is suppressed and you are able to make "unthinkable" connections between ideas you normally think are unrelated. Of course, you should just take notes of these connections and later evaluate them rationally to see if they have any merit. It's very exciting that technology might be able to prolong and enhance this creative state of mind."

## Gerald Hüther

**#NWX19 - Gerald Hüther - Die Wiedererweckung von Intentionalität und Co-Kreativität**

- https://www.youtube.com/watch?v=66aQoRlF-eQ
- März 2019

**MANN SEIN 2018 Konferenz**

**Momente gelingender Beziehungen**

**Wieso die Schulen versagen**

- https://www.youtube.com/watch?v=cVOwIRv0Fs0
- Transition von Homogenen leistungsklassen => Förderung von Wettbewerbsdenken - hinzu Inhomogenen , z.B.  Altersübergreifende Jahrgänge oder durch inklusions anteile (Khan teaching for mastery , not fixed length of study but fixed outcome!)
- diese Transition überfordert: es kann nicht mehr der gleiche stoff vermittelt werden , es kann nicht mehr auf die selbe - alte Art und Weise bewertet werden
- es entstehen parallele Bildungswelten: es gibt sehr gut entwickelte Schulen ... aber weitweg von Standard Schule und dem alten Bayern vorsitzende Lehrer verband: mehr disziplin
- aufmüpfige Lehrer und Schüler
- was hilft ? neue Lehrerausbildung? Hochschulen - Lehrerausbildung veraltet, "Perfekte Vorbereitung für Unterrichtsstunden", Improvisation fehlt -> siehe SOLE's
- Hochschulen sind Ausbildungstätten geworden, mann muss so durch das man eine "gute Anschlusskarriere" machen kann, private Hochschulen mit versprechen
- "die einzigen die das verhindern könnten wären die Staatlichen Hochschulen, sie könnten sagen wir gehen nicht mit" ho? Precht hat mehr als deutlich gezeigt das, dass staatliche schul - ideal /prinzip eben NICHT das humboldsche /humanistische Bildungs ideal ist sondern zweck bezogen (beamte und so "gut" wie nötig) verfolgt
- erwin wagenhofer, film:  alphabet
- drehte zuvor " lets feed the world" "lets make money" -> wer sind die verantwortlichen? die hochschul absolventen
- Was für menschen werden an den Hochschulen erzogen? geissen losen umgang , bereicherung auf Kosten anderer

**Schule und Gesellschaft - die Radikalkritik**

- https://www.youtube.com/watch?v=EpIXYHAh3cQ
- wer wird gebraucht


# Philosophie

- Wer sagte das einteilung in fächer keinen sinn macht? es ist wahr-> Hegel -> Marx , Nietzsche -> Jung etc.. Russell -> Wittgenstein

"Philosophy is critical thinking: trying to become aware of how one’s own thinking works, of all the things one takes for granted, of the way in which one’s own thinking shapes the things one’s thinking about."

- Was kann uns die bisherige Entwicklung sagen?
- Wie lässt sich die bisherige Entwicklung darstellen?
- auch das Konzept "Zeitgeist" betreffend, siehe Zuse, Ford, 
- Nietzsche, Hegels Dialektik
- http://de.wikipedia.org/wiki/Demiurg
- http://en.wikipedia.org/wiki/George_Braque "I do not believe in things. I believe only in their relationships"

- http://de.wikipedia.org/wiki/Apologie_(Platon)
- Wahrheit als Gesellschaftlicher Konsens, in Wie weit ist eine Verteidigung möglich oder notwenig, bzw. eine Kommunikation?
- following entries via school of life

**How Any Idiot Can Memorize The Entire History of Philosophy -  Dr. Paul Maxwell  Map - Orientation**

- https://www.youtube.com/watch?v=G5i2y5w8Dzc&feature=youtu.be
- the author,  Dr. Paul Maxwell  ,  selfwire.org (dead) seems in the lines of jordan peterson 
- commonly used categories used to make sense or distinct philosophy:
- **Meta Physics** - study of ultimate reality , immaterial , condistions in which a material world could exist
- **Eistomology** - study of what we can know example: god doesnt exist - meta phsysical claim, we can't know if god exists epistomilogical claim 
- **axiology** - statements of relativelity and degree
- pre modernism - modernism - postmodernism
- JPB?chaos  pre modernism - order modernism - chaos postmodernism
- pre modernism prioritising meta physics
- mondernism -prioritising epistomology in focus
- decard - even doubt is thinking
- hume - distingion impressions and ideas causalitx as an impressions
-   morality
-   cant go from ought(meta py) to is(py)
- kant -  tries to reconcile decards rationalism and humes sceptiscm
-   logic common rules of perception
- hegel - critiques kant: nomonal world userhers into perceptional world progressively through dialectic (sarah w was right all along) 
-         thesis and anti-thesis over generations
-         thesis gen 1
-         anti thesis gen 2
-         synthesis gen3
-         hegels followers split into two camps:
-         the commoners - all think the same
-         the individualists: only the individual is soverign
-         (looks like a job for synthesis?)
- kierkegard - 
-           childish : happyness pursing
-           adult: religous ethic modernismman encoutering with god (appeal to a higher level of virtue or inspiration )
- postmodernism:  axiological
-   rooted in hegel extentialism
-   holocost happened because the nazis rejected the real (nominal) world?
-   pm sees any  claim to objective truths as a attempt to get power over another human beeing
-   can not dicuss problems of materialism
- aothors argumentation about postmodernism besides the comment on materialism feelt shallow and inconclusive to me, I'm missing here an integral or awake(john vervaeke) perspektive, quality, zen ...
- some kind of other perspective which show a way towards syntheses/integration?

**Argument Browser**

- https://www.denizcemonduygu.com/philo/browse/

## Zizek

- https://www.youtube.com/watch?v=Jmq3imrHPMk Quantum realm IS a different reality

## Charles Eisenstein

- originally from POC 21

**The stuff that makes us feel alive - POC 21**

- https://vimeo.com/135338336

**Future Thinkers Interview**

- https://futurethinkers.org/charles-eisenstein/ 81/82
- https://www.youtube.com/watch?time_continue=1&v=bIMNJA6Mlpg
- Civilization Has Completed Its Hero's Journey. What's Next? https://www.youtube.com/watch?v=FHkatuX9CDU
- archetypes - heros journey - super hero movies - tobi avatar realization

## Plato

- politea, der Idealstaat der phil. Herrscher, Kinder erziehung
- Buch : Republik
- https://de.wikipedia.org/wiki/Platon#Staatsphilosophie
- via : https://twitter.com/anjiecast/status/595661143416434689
- Ideen lehre - theory of forms: Grundlage für Objects as in Object Oriented Programming
- Höhlengleichniss
- Platon Nietzsche Dialog https://www.youtube.com/watch?v=7NXdY-vq4CQ&feature=share

## Socrates

- why he hated democracy: only educated people can form an apropriate opinion make a "good" descion
- voting as a skill, democracy only as effective as the education system that surrounds it

## Aristotelis

- => file

## Heidegger

- trigger via Jordan Greenhall - Guy sensestock video
- Seinsvergessenheit
- Bodenständigkeit
- Wesensverfassung
- Geworgenheit, (Un) Eigentlichkeit
- Buch: Sein und Zeit => Abkehr

## Stoiker

- how to deal with: Life is difficult
- practical 
- helps to deal with: 
   1. anxiety, assume the worst case BUT you will get through it
   2. anger : use intelectual argument instead, anger emergees when misplaced hope smashes into unforseen reality
- Seneca
- Marcus Arleius : the meditations

## Hegel

**School of Life**

- https://www.youtube.com/watch?v=H5JGE3lhuNo

1. Important parts of ourselves can be found in history
2. learn from ideas you dislike
- specific example: the importance for people to be proud of where they come from
3. progress is messy
- the moves to find the perfect balance, a process which he called: dialektik: hegels dialektik
4. art has a purpose - JBP : acting something out before we understand it, the artists get it first

### Hegels Dialektik

- via Dr. Paul Maxwell SelfWire https://www.youtube.com/watch?v=G5i2y5w8Dzc&feature=youtu.be&t=511
- hegel - critiques kant: nominal world userhers into perceptional world progressively through dialectic => sarah wagenknecht was right all along we need a improvent in the dialektik process to get to synthesis
- thesis and anti-thesis over generations
- thesis gen 1
- anti thesis gen 2
- synthesis gen3
- this blends nicely with JPB notion of eternal cycles ( ouroborss) of chaos and order - while hopefully moving up levels in order

## Kant

- der kategorische Imperativ
- doing good without religion
- state as a imposed value system
- selbst überwindung, übermensch

** School of Life**

- recovered via: 864823f65676dc71768c863c64c264739970fe8e
- 4 principles (via school of life YT) :https://www.youtube.com/watch?v=nsgAsw4XGvU

1. Own up to envy: Christianity says : feel ashamed of feelings of envy, he says used it as a envisoned target of yourself as a better yourself
2. don't be a christian: christanity as a cult who make virtue of their cowardness which he called: sklavenmoral, christian as "die herde" , turning their inabilites in to certain values:
sexlessness = purity
weakness = goodness
submission to people one hates = obiedience
not being able to take revenge = forgiveness
=> chritanity amounted into a giant machine for bitter denial
3. never drink alcohol : both christianity and alcohol , both numb pain , reassure that everything is OK
4. god is dead : culture should replace scripte ( religion), relgious believes where false but benefitial in the sense of allowing us to cope with the difficulties of life,
- he was deeply suspicous (HA! rightfully so!!) of how his own area handled culture, universites were killing the humanites turning them into dry exercises
- he admired how the greeks used tragic play in a practical way, as a therapeutic tool
- for him his centurie had two major influences:
1. mass demoracy : unleashing tons of unguided envy -> OHO!!
2. atheism : leaving humans without guidance or morality

## Nietzsche

- auch via Xenosaga: der Wille zur Macht, jenseits von gut und böse , also sprach Zarathustra 


**school of life**

- Konzepte wie: Jenes von Gut und Böse (Moral), der Wille zur Macht
- Auswirkungen und Ausblick Psychologischer Phänomene auf die Geschichte des Menschen
- Zarathustra erster historischer Mensch der gut und böse unterscheidet
- Zarathustra der Gestank (Gedanken?) der Menschen, leben auf dem Berg
- selbst überwindung, übermensch
- 4 principles (via school of life YT) : 
    1. Own up to envy: Christianity says : feel ashamed of feelings of envy, he says used it as a envisoned target of yourself as a better yourself
    2. don't be a christian: christanity as a cult who make virtue of their cowardness which he called: sklavenmoral, christian as "die herde" , turning their inabilites in to certain values:
        - sexlessness = purity
        - weakness = goodness
        - submission to people one hates = obiedience
        - not being able to take revenge = forgiveness
        - => chritanity amounted into a giant machine for bitter denial  
    3. never drink alcohol : both christianity and alcohol , both numb pain , reassure that everything is OK
    4. god is dead : culture should replace scripte ( religion), relgious believes where false but benefitial in the sense  of allowing us to cope with the difficulties of life, 
    - he was deeply suspicous (HA! rightfully so!!) of how his own area handled culture, universites were killing the humanites turning them into dry exercises
    - he admired how the greeks used tragic play in a practical way, as a therapeutic tool
    - for him his centurie had two major influences: 
        1. mass demoracy : unleashing tons of unguided envy -> OHO!!
        2. atheism : leaving humans without guidance or morality

**acedemy of ideas**

- influenced Sigmund Freud, Carl Jung, Alfred Adler ( ==> cogn sci cognetion psycho - phil)
- TODO: more notes ... got lost, re write

## Wittgenstein

- https://de.wikipedia.org/wiki/Tractatus_logico-philosophicus
- Mind map https://de.wikipedia.org/w/index.php?title=Datei:Tractatus.png&filetimestamp=20060501152657&
- on the value of money : https://www.reddit.com/r/dredmorbius/comments/77krxk/wittgenstein_on_the_source_of_value_in_money/ ==> the bitcoin/Blockchain idea
- also Logicomix

### Podcast Bayern 2 - Ludwig Wittgenstein und die Wahrheit

- https://www.br.de/radio/bayern2/programmkalender/ausstrahlung-2149450.html
- the interesting relation ship between language - logic and the world... but that also in relation to the persons age (nice context example)
- Wortspiel? in Wittgensteins later work but the shapes (plato?) of the impossible ( how to think an impossible thought) are there, was also thinking about this when listening to daniel schmachtenbergers podcast ref a chinese india book? DAO?

## Betrand Russell

- also Logicomix


### Icarus

- see Principles.md Zusammenhänge zwischen menschlichen Eigenschaften und Gesellschaft


## Denis_Diderot

### Von es war einmal

- http://de.wikipedia.org/wiki/Denis_Diderot
- Enzykloopädie
- Frühe visualisierung/Karte von menschlichem wissen
- http://upload.wikimedia.org/wikipedia/commons/4/49/Encyclop%C3%A9die_Figurative_System_of_Human_Knowledge.jpg
- http://upload.wikimedia.org/wikipedia/commons/5/58/ENC_SYSTEME_FIGURE.jpeg
- http://de.wikipedia.org/wiki/Baum_des_Wissens
- Yggdrasil?

## Ivan Illisch

- via Future of Coding Slack
- works: Deschooling Society, Tool for Conviviality

## Philosophie - (logischer)Positivismus

- https://groups.google.com/forum/#!topic/philosophy-in-a-time-of-software/Fj0LsOhvjZk
- http://plato.stanford.edu/entries/comte/
- http://www.existentialprogramming.com/2008/03/logical-positivists-were-test-infected.html
- https://twitter.com/substack/status/207233076873539584
- also podcast: http://rationallyspeakingpodcast.org/show/rs109-rebecca-newberger-goldstein-on-plato-at-the-googleplex.html

## Great Men of Predicate Logic

- Alte papier notizen gefunden, leider kein context/quellen bezug
- Gottlob Frege -> anti semitische Haltung -> Zeitgeist?
- debating meaning of existence with others
- debating meaning of meaning with _himself_
- "positivism": replacing old certaines (king, faith, duty) with new ones 
- largely resonsible for farming "natural philosophy" into science
- in progressive positive utopia ( siehe Friedrich Eduard Bilz 1904, Das Volk im Zukunftsstaat)
- knowledge would be formal on objective facts, evidence, eason and the _scientific method_
- technocrates:  liked pos. because it justified rule by an intelectual elite, people who  best masterd the facts evidence reason( white male european) -> social darwinism
- not all pos. fell for SD
- Betrand Russel -> in welchel Umfeld wuchs er auf ?
- in frühen .. <-> ähnlich zu Frege
- Russels Schüler Ludwig Wittgenstein -> Sein Umfeld!
- sein Werk Tractatus Logico philosophicus
- Grundsätze:
- 1. the world is build of facts
- 2. an atomic fact describes a true relation between objects
- 3. the truth of a simple sentence lies in its relation to an atom in a fact in the world -> faktische relation zum GEB : Bedeutung kann nur durch Relation entstehen
- 4. the truth of  complex sentences is afunction of the truth of simple ones -> Rekursion, Abstraktion, Subsitution
- 5. we can pin down facts with sentences of predicate Logic
- 6. Facts are everything, if we cant talk about it in facts, we should not
- => Manifest d. prog.
- Leading to Logical Positivism
- "new Positivsm": angrier, more radical , more evangelical
- Wiener Kreis: Gödel wider legt princ. Mathematicas Grundlegenden Anspruch
- Karl Popper (founding "post positivism") <-> Friedrich Hayek
- Verification <-> Falsification

## Notizen

- nochmal alte papier notizen
- Karl popper "the Logic of scientific discovery" 1934
- popper claimed positivism died but "Many scientists are not aware that these are assumptions"
    - ability of objective truth
    - value of empirism
    - universslity of scientific knowledge
    - and the scientific method ( itself adjusting/adopting recursive so it can handle all/ fit in all)
- Logical Positivism -> Declarative Programming
- Planner (EWD?) -> Carl Hewit
- postmodern science
- contemporary culture zeitgeist
- pomo science example: CS ( same thoughsts as EWD?)
- Thesis: modern mainstream science commit to _what ever works_ -> which translates here to : erforschen des menschlichen wesens um Umgang mit dem Computer
- EWD (objective truth) vs Alan Kay (education as a thermosthate), methods?

## Gesellschaftlicher Zeitgeist zu Beginn des 20. Jahrhundert

- http://de.wikipedia.org/wiki/Der_internationale_Jude
- http://de.wikipedia.org/wiki/Harry_Graf_Kessler
- http://de.wikipedia.org/wiki/Walther_Rathenau#Werke
- http://de.wikipedia.org/wiki/Organisation_Consul
- http://de.wikipedia.org/wiki/Untergang_des_Abendlandes
- Philosophische Grundlagen- Grudbegriffe - Nietzsche : Der Wile zur Macht
- Kulturverlauf
- Kultur-Charakteristik
- Einzelfragen
- Oswald Spengler
- <= Diese geschichtliche und gesellschaftliche Einordnung sollte teil jeder Schulsausbildung sein :/
- via: http://www.heise.de/tp/artikel/39/39652/1.html

**Peter Scholl Latour Interview**

- http://www.heise.de/tp/artikel/41/41168/1.html
- der untergang des abendlandes

**Hedonismus und Dekadenz**

- Singularität, Keimzelle
- Konrad Zuse, über sein Studium, Rosa Luxemburg, Henry Ford, den Verlauf seines Studiums und die Reflektion über diesen Verlauf  
- Artikel ref: http://www.heise.de/tp/artikel/40/40576/1.html
- http://www.heise.de/tp/foren/S-Der-Zeitgeist-der-Wirtschaftskrise/forum-271510/list/

## Thomas Paine Common Sense

- was können wir zum Zeitgeist dieser Epoche finden?

# Anthropologie 

https://www.nytimes.com/2018/08/03/opinion/airbnb-is-the-new-nato.html

- "Airbnb, without fundamental human goodness, would not work,"

- "From all the data the company has accumulated, no major country anomalies, in terms of patterns of behavior, have emerged. People from Japan, Brazil, Nigeria, Russia, the United States, Mexico and France are equally respectful and honest. There are no national outliers, Chesky said, on the goodness or trustworthiness scale. **There are no enemies.**"



# Emergence 

- Emergence as a Phenomenon, as a Movement


http://www.whatisemerging.com/opinions/the-rise-of-the-emergentsia-meaning-making-in-a-time-between-worlds 

- Maja Göpel
- Terry Patten 

## Jordan Green Hall

- https://www.deviantart.com/jordangreenhall/journal/Events-and-Memes-307237258:
Start running copies of the lesson and the shape of the Meme fitness landscape changes dramatically.  Rule #3 still applies, but the variables are novel.  There are two factors that constrain the fitness landscape of "learnable Memes" in a high mediation environment.

1.  The absolute size of the mediation environment.
2. The share of the mediation environment that the Meme occupies.
- via Future thinkers discord "chat off topic"
- This is another recent Jordan Hall video.
https://www.youtube.com/watch?v=I7TMrCCQmBY
This devientart profile may have the first articles from 2012 Jordan Hall was talking about in the video.
https://www.deviantart.com/jordangreenhall
https://www.deviantart.com/jordangreenhall/posts
I also found an interview with him from that site from around that time.
https://www.deviantart.com/techgnotic/journal/What-Superheroes-Should-Today-s-Tech-Inspire-298464237

## Daniel Schmachtenberger

- neurohacker

### Humanity's Phase Shift

- https://www.youtube.com/watch?v=nQRzxEobWco
- open vs closed loops, nothing in nature is a closed loop, but with chemistry and toxic waste we create open loops
- his definition of economics ~ 5:33 !  "economics is our value system codified as value equations that determines how much we value one thing relative to another thing that determines what we're incentivized to do and what we confer power" 


joe rogan russel brand dmt https://www.youtube.com/watch?v=jiBgI0VzLfI

### war on sensemaking

- https://www.youtube.com/watch?v=7LqaotiGWjQ
- "news is mostly propaganda"
- beware of the exponential lever
- trust and truthfulness - relating his terms to other thinkers?
- how to get to post warfare/post gametheoretic interactions
- breaking down how the traditional institutions and media are broken, basically it all comes down to wrong biological signaling and the current incentive paradigm: capitalism
- the story about how he almost got invited to an event , where the event creator was (of course) optimizing for max. attendees?
- (capitalism's) growth is not sustainable
- articulating why 
- start sensemaking with trust and vulnerability then create bigger groups


### podcast jim rutt - EP7 Daniel Schmachtenberger and the Evolution of Technology

- https://jimruttshow.blubrry.net/daniel-schmachtenberger/
- run governance and organisational experiments by respecting dunbars number
- future social/economics model _can not_ have (private) property, it unermindes responsibility for other people and the commons
- we have exceeded the limits of growth
- Anti rivalrous behaviour and game B

### Designing A Win-Win World for Everyone

- https://shows.pippa.io/futurefossils/episodes/51
- starting with a personal background
- home schooling and Buckminster fuller, preservation projects african elefants parks, 
- realizing the issue of generator functions
- robert kegan and the stages of developement of human beeings
- omni considerative 
- sense making is central to the process, to scale this up
- Books:  Timothy Morton’s book Hyperobjects ,  James P. Carse’s book Finite & Infinite Game

### Wellness Opt Podcast

- https://www.youtube.com/watch?v=6fWfvO72vAw
- NOOTROPICS | EMOTIONAL RESILIENCE | OUTSMART THE MODERN WORLD w/ Daniel Schmachtenberger | QUALIA
- first 10 minutes some more details on his early education/home schooling

### Portal Podcast Episode 027

- Daniel Schmachtenberger on The Portal (with host Eric Weinstein), Ep. #027 - On Avoiding Apocalypses
- https://www.youtube.com/watch?v=_b4qKv1Ctv8
- capitalism as a paperclip maximiser
- buddhist could/can teach abstract smypathy, extending the dunnbars number, the usual zone of engagement and trust further
- groups like Silicon Valley not just epistemic bias but epistemic inadequacy (Silicon Valley - Money - Markets - Naval)
- Addiction kills sense making -  hyper normal stimuli , for society
- neither capitalism nor socialism something different
- non rivalry sex?

# Understanding Spirited Away Consumption and Identity

- Removed from youtube, old link in telegram
- Blog with transscript : https://plainflavoredenglish.com/2019/09/18/understanding-spirited-away-consumption-and-identity-transcript/

# Circling

- first heard mention in Future Thinkers Video
- https://www.youtube.com/watch?v=0xSUw_QhJkc
- Had a call with Guy Sengstock (kind of applied philosopher)
- he has a hunch for Heidegger
- Circling Video with Guy https://www.youtube.com/watch?v=76UIf6DSvKQ&feature=youtu.be&t=1979

**About**

- Circling is a meta psycho technique for improving certain aspects of speech conversations
- there are various improvements over a normal "debate", speed, trust!, "notion" of inquiry 
- "hm", "oh", "ah"

## Guy Sengstock

**Guy Sengstock - Circling Towards Meaningfulness & Collective Intelligence**

- https://anchor.fm/emerge/episodes/Guy-Sengstock---Circling-Towards-Meaningfulness--Collective-Intelligence-e5g8j7
- listen AND TAKE notes
- the role and meaning of distinctions (impact and experience)
- fed ex sign negativ space arrow
- the notion of the "horizion", forground background switching/merging
- that which is most near , consealed becomes clear
- alethia? the moment something veiled becomes unveiled
- stages of develeopement, moving beyond self interest 


# Avatar - the last Airbender - Everything is Connceted

- https://www.youtube.com/watch?v=AOjJ4w9USDE&feature=share
- onion and banana juice to clear the chakras
- 7 chakras
- earth chakra below spine - blocked by fear , deal with survival
- water chakra deals with pleasure and is blocked by guilt - look at all the guilt that burdens your sould => forgive yourself
- fire chakra - stomach - will power blocked by shame accept all aspects of your life evven the mistakes
- love charka in the heart blocked by grief release all sadness and loss
- sound chakra throat deals with truth and is blocked by lies - release denial and the lies you tell yourself
- light chakra center of the forhead it deals with insight and is blocked by illusion - realease all illusions within yourself - everything is connected - collective consciousness
- thought chakra located at the crown of the head and deals with pure cosmic energy and is blocked by earthly attachment - letting go doesnt mean it does disappear
- I read a youtube comment somewhere: and when earth, fire, water, wind and love unit then captain planet emerges, it neccured to me because it was a kind of poor acting out box in strange stories, but the combination of elements was actually present in captain planet

# Restoring Humanity: Exploring Our Connections to Earth & Each Other with Charles Eisenstein - Neurohacker Podcast Daniel Schmachtenberger

- https://www.youtube.com/watch?v=EfNgW-On6hw
- danger of narrow focus
- the compelling case for earth as a living being, because of its complexity!
- ...

# Wait but why - Sensemaking

- https://twitter.com/waitbutwhy/status/1214283303857065984
- either I didn't notice or its just been now gradualy that waitbutwhy gravitated towards sense making

## Wait but why - Sensemaking Structure

**Chapter 0 - Introduction**

### Part 1 The Power Games

**Chapter 1 - The Great Battle of Fire and Light**

**Chapter 2 - A Game of Giants**

**Chapter 3 - A Story of Stories**

### Part 2 The Value Games

**Chapter 4 - The Enlightment Kids**

**Chapter 5 - The Mute Button**

**Chapter 6 - The American Brain**

### Part 3 Thinking, in 3D

**Chapter 7 - The Thinking Ladder**

**Chapter 8 -Idea Labs and Echo Chambers**

### Part 4 Politics, in 3D

**Chapter 9 - Political Disney World**

### Part 5 A Dangerous Trend

**Chapter 10 - A Sick Giant**


# Ice Shaman Wim Hof

- breathing!!
- seeking discomfort
- 300 JBP lion initiation rituals self noting wim Hof Russel podcast 1h >

**Heal Yourself with The Ice Shaman | Wim Hof & Russell Brand**

- he can see chakras
- science studies directions!
- his kids made him survive, nature - the ice healed him
- 8 senses

# yes theory wimhof

- #tags: #health #discomfort
- BECOMING SUPERHUMAN WITH ICE MAN - Wim Hof https://www.youtube.com/watch?v=8cvhwquPqJ0

# Sense Making with Nick Redmark

- First zoom Session on 17.12.2019
- Sense making and Game B
- links from the zoom call on 30.04.2020: 
- https://lecticalive.org/
- book http://www.zakstein.org/education-in-a-time-between-worlds-book-release/
- https://medium.com/@theo_dawson
- https://web.facebook.com/groups/gamebparenting/
- did ask: community bridging => answer build a dialouge culture first, then export it
- nick has really good ideas on how to improve, or even make possible "dialogos"(the kind of software needs to behave) https://twitter.com/nickredmark/status/1246793184488890371?s=20
- wanted to ask him about Hanzi freinacht
- nicks markdown list archive : https://github.com/nmaro/notes/blob/master/streem.mdl

# Jim Rutt podcast - EP36 Hanzi Freinacht on Metamodernism

- https://www.jimruttshow.com/hanzi-freinacht/
- in the comments ref to  David Chapman meaningness.com
- controviersial? 
- todo : part 2
- https://jimruttshow.blubrry.net/the-jim-rutt-show-transcripts/transcript-of-episode-36-hanzi-freinacht-on-metamodernism/

# Generating Archetypal Mind States with Michael Taft

- I was reading the annoucement of a workshop with Michael Taft, when Euvie wrote: ". I can say for myself that when I figured out how to do that, it blew my mind. **It's a bit like discovering you have superpowers**!"
- I had a few hunches about Arche Types and some went in a similar direction
- It must be at least 20 years since I watched this and still the TV show pretender "came up" in my thoughts

# Jim Rutt podcast - EP65 Tyson Yunkaporta on Indigenous Complexity

- https://www.jimruttshow.com/tyson-yunkaporta-1/
- https://jimruttshow.blubrry.net/the-jim-rutt-show-transcripts/transcript-of-episode-65-tyson-yunkaporta-on-indigenous-complexity/
- on the book: sand talk - How indigenous thinking can save the world
- language -> for him (aboriginal context) society and environment/nature are the SAME
- language -> sapir whorf 
- language -> rituals
- Richness ( Growth?) in Connections in the Brain and Between People
- Dolphin Collaborative Fishing at the Coast
- What is Civlization -> the old way (GameA/Western ref. to Musk Civlization) - the City that grows (negative conotation) if it stops growing it dies
- TY: " like grinding flour for bread is a sign of civilization. I don’t know why that is" I immediately  felt a connection to that aragon black interview with James C. Scott about his book "Against the Grain: A Deep History of the Earliest States" sadly the interview is not anylonger available , found it see ref. Blockchain 2020, lets see , but the book still would contian the main message and that it/was: certain types of corn made a certain type of agriculture possible which inflicted a certain culture - hierarchy, taxes , control => Civlisation, as viewed in the old/western sense this would be a good thing?
- TY " But for me, a civilization is basically a community that must be constantly growing or it will collapse. And so, therefore, it relies on the importation of resources. That’s it. That’s all I can find in common across all these places that call themselves civilizations. And they all behave the same way and they have thatt growth-based imperative, that growth- based paradigm. And it’s impossible. It’s a denial of reality. It’s a denial of physics. It’s a denial of everything. If you want a definition of evil, I think it’s probably a good one. It’s just denial of reality."
- JR: Indian cast System , Invaders stack on top,  Norman French invented the idea of the nobility to sit on top of the English
- narcissism , JR hasn't heard of it, precht was asked about it and replied: because its now possible, but I think there the term is used in a more broad way like a trade of human psychology
- JR: 10 % C level executives in America Socio Paths ( see Pieter Hintjens :book )
- TY "The narcissism is dealt with early on, because your life is in almost in these 15-year segments" I was so struck by this because when I listen to that, finishing this podcast in the evening I watched Ocean Waves Video by Beyond ghibli just before and it felt like a strong connection/relation
- Ordeal , Learning from Mistakes, How permanent crime records make such thing impossible
- **Obsession** with security/savety, no word for savety in aborigine language only for protection because that involves agency
- the agency of violence - women, the domestication of women , ref. Harari Sapines fight or flight response
- TY: to come into relation with things
- Civlization as Self-domestication
- Tysons re imagining of Civilization
- yarn: aggregate stories non linear , overlapping talk, the goal is to find a loose Consensus, Flood
- indigenous as (human) factory settings
- TY "So your stories are maps as well." refering to the deer hunting stories of JR

# Joscha Bach

- #tags: #AI #Consciousness
- I had this file: ML_AI but now I create this section here since I found a somewhat better angle/perspective to approach the AI topic, and that angle is Joscha Bach


# Joscha Bach: Artificial Consciousness and the Nature of Reality | AI Podcast #101 with Lex Fridman

- #tags: #AI #Consciousness
- via : https://news.ycombinator.com/item?id=23923183
- https://www.youtube.com/watch?v=P-2P3MSZrBM
- arielle - undine andersen
- https://en.wikipedia.org/wiki/Thomas_Aquinas

- #tags: #tractatus #Wittgenstein
- ~16 min
 - I think putting computation at the center of the the worldview is actually the right way to think about it and Wittgenstein could see it 
- Wittgenstein basically preempted the largest program of AI that Minsky started later like thirty years later Turing was actually a pupil of Wittgenstein
- Wittgenstein even cancel some classes when Turing was not present because he thought it was not worth spending the time
- if you read the Tractatus it's a very beautiful book like basically one thought on 75 pages 
- it's very non typical for philosophy because it doesn't have arguments in it and it doesn't have references in it
- it's just one thought that is not intending to convince anybody it's mostly for people that had the same insight as me just spelled out
and this insight is **there is a way in which mathematics and philosophy ought to meet**
- mathematics tries to understand the domain of all languages by starting with those that are so formaliseable that you can prove all the properties of the statements that you make
- **but the price that you pay is that your language is very very simple so it's very hard to say something meaningful in mathematics**
- it looks complicated to people but it's far less complicated than what our brain is casually doing all the time (when) it makes sense of reality
- and philosophy is coming from the top so it's mostly starting from natural languages with
vaguely defined concepts
- and the hope is that mathematics and philosophy can meet at some point
 and Wittgenstein was trying to make them meet and he already understood that for instance you could express everything with the NAND calculus that you could produce the entire logic to NAND gates as we do in all modern computers
- so in some sense he already understood turing universality before turing spelled it out
-  I think he when he wrote the Tractatus he didn't understand yet that the idea was so important and significant 
-  and I suspect then when turing wrote it out nobody cared that much
 turing was not that famous when he lived it was mostly his work in decrypting the German codes
that made him famous and or gave him some notoriety but this same status that notoriety he has to computer science right now in
the eye is something that I think he acquired later
 LF: it's kind of
 interesting and do you think of computation and computer science and you
represent that to me is maybe that's the modern-day you in a sense are the new
philosopher by sort of the computer scientist who dares to ask the bigger
questions that philosophy originally started is the new philosophy is the new
philosopher

- ~ 43 min category theory: " we discover that mathematics itself is the domain of all languages and then we see that most of the domains of mathematics that we have discovered are in some sense describing the same fractals"
"this is what category theory is obsessed about that you can map these different domains to each other so they're not that many fractals and some of these have interesting structure and symmetry breaks"
- climate/temperature tipping points
- neural link why we might not need it
- when the non symbolic communicates anxiety in the chest 
- TODO ... consciousness .. make notes
-  ~ 150 min"the us is a society of cheaters it's basically cheating so indistinguishable from innovation" "
 it's acceptable to do things that you know know are wrong " (to a certain degree)
- "so the us is not optimizing for sustainability but innovation"
- democracy is: "democracy is the rule of oligarchs, they are the people that
currently own the means of production
that is administered not by the oligarchs themselves because they
there's too much distraction right here so much innovation that we have in every
generation new means of production let me invent and corporations dive usually
after 30 years or so and something either takes the leading role in our
societies so it's administered by institutions and
these institutions themselves **are not elected but they provide continuity** and
they are led by electable politicians and this makes it possible that you can
adapt to change _without having to kill people_ right so you can tell for
instance of a change in government's if people think that the current government
is too corrupt or is not up-to-date you can just elect new people or if a
journalist finds out something inconvenient about the institution and
the institution is has no plan B"

# Further CCC material

- via https://news.ycombinator.com/item?id=23925091
- Computational Meta-Intelligence (32C3): https://www.youtube.com/watch?v=WRdJCFEqFTU
- Machine Dreams (33C3): https://www.youtube.com/watch?v=K5nJ5l6dl2s
- The Ghost in the Machine (35C3): https://www.youtube.com/watch?v=e3K5UxWRRuY&t=2516s
- german fefe podcast  https://alternativlos.org/42/

# On Minsky

- http://bach.ai/on-marvin-minsky/
- "believe that today, the need for working on AI is as pressing as ever. Psychology is still unable to formulate and test theories, neuroscientists are entirely focused on nervous systems and not on minds. Artificial Intelligence is our best bet at understanding who we are, and it is time to continue Marvin’s work, to recognize and describe the the richness of our minds, and to build machines that think, feel, perceive, learn, imagine and dream."

# Jim Rutt Podcast - EP72 Joscha Bach on Minds, Machines & Magic

- Dietrich Dörner and Stanisław Lem
- motivation and sacrificing cats
- PSI theory and practice, 10 years to get mini psi to work
- version 3 internally developed - berlin?

# Josch Bach - Wittgenstein - Games - language

- https://twitter.com/Plinz/status/1296997008096415744
- "Part of Wittgenstein's program is that the rules of linguistic and epistemic games can be universally deduced, so the games can be played symmetrically."
- other "Not an expert but I think he means that both people in a conversation are trying to figure out what the other one is saying and what it means. Winning would be gaining a better mutual understanding of each other & better approximation of whatever truth they're trying to deduce."
- "Yes, symmetric debates are not won because one side has hacked the world view of the other, but because two autonomous worldviews got closer by becoming more consistent."

# Learning to Ask The Right Questions

- #tags: #learning #p2p_learning
- I was drawn to this so I made a note in my paper notebook, the note contains the aspects: this reference, zak stein edu hubs and p2p_learning via jöran, I think if these principles could be teached in a dynamic p2p way, then this would be a important step towards a better understanding of ourselves and eventually society itself
- https://futurethinkers.org/the-right-questions/
- MG: When I ask myself "What is the right question to ask?", usually this is what I come up with:
    - What is the most interesting or useful conversation to have?
    - What’s going on in the world? What are the facts?
    - What do we do about all the division? How do we get people on the same page?
    - What do we do about the planet?
    - What is the best way to live?
    - What do I do about my anger?
    - What should I do with my life?
    - What action should I take in the world?
    - What are my obligations to my fellow human beings?
    - Why am I afraid of death?
    - How can I deal with the difficult situations I face?
    - How should I handle the success or power that I hold?
- MG: Whichever questions resonate the most at that moment are the ones I focus on answering. If I’m not able to get any useful answers right away, I’ll stretch these questions further, by going meta. "What does this mean at a meta level?"
    - Where does it lead?
    - What are the underlying reasons for it?
    - What is my experience of it? 
    - How does it work? 
    - What are the societal and human level implications if we all do it?

- MG: So how do I know what is worth thinking about, and how do I identify what seems urgent but isn’t actually important? 
- MG: Most people pay attention to what is salient:
    - What feels most urgent or immediate
    - What is loudest or scariest
    - What feels good
    - What their social groups pay attention to
    - What is easiest to pay attention to
- MG: Naturally, they tend to avoid
    - What makes them feel shame or embarrassment
    - What causes pain or dread
    - What requires a lot of effort, focus, and attention
    - What makes them feel conflicted or confused
    - What is ugly to look at or think about
    - What they feel they can do nothing about
    - What nobody else seems to care about
    - What is boring
