[[_TOC_]]

# Alan Kay some Context

- #tags: #alan_kay #context
- need a way to at least partially extract and connect the meta of Alan Kays work
- TODO : from all his talk which books and persons he mentions?! put a list here!!
- find reference talk of his : ahh , aha, and haha metapher - it's from Arthur koeslter book: the act of creation
- I think this was menntioned in one of his talks http://de.wikipedia.org/wiki/Walden (verfiy)
- on arrogance: https://www.youtube.com/watch?v=9KivesLMncs
- how to dicuss, he tried , reached for public discussion: http://stackoverflow.com/questions/432922/significant-new-inventions-in-computing-since-1980
- unix vs smalltalk : https://twitter.com/JBezivin/status/452884450544607233
- linux is a distraction
- build your own, custom hardware
- "inventend" OO to  express better intentions, see his other OO quotes on Java/C++
- Konzept von MA(japanese, in Between) => wo bezug in seinen talks? (siehe transscript)
- internet VS web : http://programmers.stackexchange.com/questions/191738/why-did-alan-kay-say-the-internet-was-so-well-done-but-the-web-was-by-amateur
- creative think talk 82, notes: https://twitter.com/bob_burrough/status/695061451426238464
- the center of why, why only 1 book per subject in school, why... https://news.ycombinator.com/item?id=13347771
- 200.000 Miles a year in the air (pocket interview), the origins of the student exchange programm, since then , find source video(2012 SCIx Keynote)
- unread: http://mythz.servicestack.net/blog/2013/02/27/the-deep-insights-of-alan-kay/
- unwatched: https://www.youtube.com/watch?v=HAT4iewOHDs !!! how comüplex is personal computing via : https://news.ycombinator.com/item?id=10333755
- unwatched: https://www.youtube.com/watch?v=WJzi9R_55Iw
- https://www.youtube.com/watch?v=aqotNrIp-Ik
- http://mythz.servicestack.net/blog/2013/02/27/the-deep-insights-of-alan-kay/
- don't know where to put so here: cicero , kultur -> siehe weltall , erde mensch
- parc https://news.ycombinator.com/item?id=14108797
- check : https://m.youtube.com/watch?feature=youtu.be&v=QboI_1WJUlM

VPRI Archive:

- #tags: #vpri #alan_kay #youtube
- archive digger: https://www.youtube.com/channel/UCkdJ2NwjI8LcgyvfU7PyaVA
- https://www.youtube.com/watch?v=G8X-yvDWClc
- https://www.youtube.com/watch?v=BUud1gcbS9k
- https://www.youtube.com/watch?v=GainW30_wUU
- https://www.youtube.com/watch?v=pUoBSC3uoeo
- https://www.youtube.com/watch?v=44Rd_qMigLg
- https://www.youtube.com/watch?v=0Rin0fCE9nM
- https://www.youtube.com/watch?v=SoW-1lVP9DI

# the world of null - Neil postman 2009

- #tags: #tags #alan_kay #neil_postman
- http://lowerquality.com/kay-archive/transcripts/154578-The_World_of_null-W_-_Alan_Kay_Keynote.doc
- Alfred Korzybski : #book: science and sanity , the map is not the territory 
- Gregory Bateson
- McLuan: Until I believe it, I can't see it
- Jacque hadamard: #book: the psychology of invention, feeling math , formula is the last part of communication 
- Brahms impossible plays , wanted to hear what it sounds when people _tried_ to play it
- Jerome Bruner learning :, kinesthetic, figurative, symbols 
- David Macaulay Cathedral: the storey of it's construction 
- Elan vital by Henri Bergson
- David Goodsell molecular paintings, 
- Charles Sanders Peirce, - Russel/ Math connection, scientific method , #book: philosophy of math 

# Talks

- #tags: #alan_kay #talk #list
- http://vpri.org/html/words_links/links_ifnct.htm

# Alan Kay public wiki - like my notes but public and bigger

- #tags: #alan_kay #wiki #glossary #concepts #influences
- https://alan-kay.wikia.com/wiki/Alan_Kay_Wiki
- source: https://news.ycombinator.com/item?id=18085842
- of course its offline now but the web archive has our back:
- https://web.archive.org/web/20160610043904/http://alan-kay.wikia.com/wiki/Glossary
- Some glossary entries:
- the invention of science
- the american constitution
- citizenship
- systems thinking
- object oriented thinking
- reality is a construction
- human perception is flawed
- stories, narrative and theater
- theater
- Gulley or Canyon model
- Kahneman System 1 vs System 2
- tinker toys/ clocks /apis
- empire state building
- microsoft word justification bug
- the internet and TCP/IP
- the web
- biological systems
- IQ vs. Knowledge vs Point of View 
- Point of view is worth 80 IQ points
- Qualitative leaps or thresholds
- pink plane vs blue plane
- Pyramids
- Software Engineering
- 7 +- 2 things
- the moon program set back space travel by 50 years
- tinkering vs. Engineering vs Math/Science
- List maxwell equations of computing
- pop music /pop culture- CS becoming a pop culture
- ARPA
- Research funding
- Problem solving vs problem finding
- Literacy
- Human Universals
- Tribal Beings
- Learn French by going to France (Hello Seymour)
- Early childhood education
- training wheels
- esau
- STEPs Project
- Technology vs. the Arts
- Odor of perfume
- Past vs Present vs future - mcLuhan 
- **influences, colleagues, and friends (links to references to talk in the web archive link):** 
- seymour papert
- isaac newton
- bob barton
- chuck thacker
- dan ingalls
- ted nelson
- J. C. R. Licklider
- Douglas Engelbart
- Marshall Mcluhan
- Neil Postman
- Leonardo da Vinci
- Henry Ford
- Paul MacCready
- jerome bruner
- lev vygotsky
- jean piaget
- john holt
- maria montessori
- John Dewey
- Ivan Sutherland
- John McCarthy
- Marvin Minsky
- Vannevar Bush
- Plato
- Donald Brown - human universals
- Alan Peris
- Jacques Hadamard
- Daniel Kahneman
- Charles Hampden-Turner
- Francis Bacon
- Tim Galloway 
- Betty Edwards 
- Konrad Lorenz  

# Persons

- #tags: #context #alan_kay #influences
- Seymor Papert
- Marvin Minsky
- Neil Postman
- Marshall McLuan
- ==> so many more need a time line and people / material / influences / projects 
- not even OLPC ( with negroponte) http://www.vpri.org/pdf/rn2007006a_olpc.pdf

# 2017 - The Heidelberg Laureate Forum Foundation presents the HLF Portraits: Alan Kay

- #tags: #youtube #alan_kay #interview #book 
- notes were made in february 2019
- https://www.youtube.com/watch?v=fn3roWiLTtQ
- read at the age of 2 years
- #book: 75th annyversary illustration https://www.amazon.com/Mythology-Timeless-Heroes-Anniversary-Illustrated/dp/0316438529/ref=sr_1_1_sspa?ie=UTF8&qid=1548850296&sr=8-1-spons&keywords=edith+hamilton+mythology&psc=1
- https://www.amazon.de/Das-gro%C3%9Fe-Buch-klassischen-Mythen/dp/3442217237/ref=sr_1_1?ie=UTF8&qid=1548850559&sr=8-1&keywords=Das+grosse+Buch+der+klassischen+Mythen
- https://www.amazon.com/Ancient-Times-History-Early-World/dp/149938307X/ref=sr_1_1?ie=UTF8&qid=1548850940&sr=8-1&keywords=ancient+times+history+of+the+early+world
- grandfather really big book store in bookstore, his uncle had many books
- story about his 4th grad teacher how she setup situiation for the childrens to find something they are interested in, to setup their own reasearch groups
- class theme ( griech Theater
- #book: https://www.amazon.com/Rockets-missiles-space-travel-Willy/dp/B0007DV4XW/ref=sr_1_1?ie=UTF8&qid=1548853458&sr=8-1&keywords=Rockets%2C+missiles%2C+and+space+travel
- ==> introduction to non linerary thinking
- Jazz music playing engineering throw out
- #book; https://www.amazon.com/Faster-Than-Thought-Symposium-Computing/dp/B000V7882Y/ref=sr_1_2?ie=UTF8&qid=1548854530&sr=8-2&keywords=faster+than+thought
- Computing inspiration - Airforce
- Airforce => dont pmake plans Lenon quote
- Theater in Colorado? when to be fooled - science as a way to teach people to see through fooling
- david evans - father computer graphics
- technology had a lot todo with world war 2
- Radar technology -> MIT -> Vanevar Bush
- Russian bomber detection system -> todays flight control system
- Licklider - ARPA -> he was enlightend ;) the value of different perspectives
- two periods
- Lickdiers dream
- AKs starting budegt there: budget for two months reading on what computing so far
- Inspiration: Marvin minsky turing speech
- Minsky and Paperts work on LOGO
- Small talk - inspirations for objects/desktop ..: the early history of small talk ( book/paper)
- Ivan Sutherland thesis ( kinda use of objects)
- biology: can think of cells as objects because they are container of processes
- big idea: simulation ( sketchpad : build something that doesnt exist and simulte it)
- the romance of the personal ( at your disposal) 
- the medium is the massage, what has to happen in the brain to be able to use a computer fluent
- papert interested in math
- AK:  interested in science -> francis bacon
- 50: What is a computer?
- Q:What are you looking for in future of computation/education?
- The main parts of the population have to be brought in into the discussion (~80%)
- school learning vs. culture learning ( montessori gets this right)

# Interview about mobile technology

- #tags: #alan_kay #technology #narrative #society #writing #oral #interview
- https://www.fastcompany.com/40435064/what-alan-kay-thinks-about-the-iphone-and-technology-now
- via https://news.ycombinator.com/item?id=15261691
- montessorie, the role of culture in learning
- very few adults learn reading
- human universals book by donald brown and the TV amplifier
- #book https://www.goodreads.com/book/show/1080960.Human_Universals
- neil postman
- the pen on the ipad
- the web is a mess because tim berners lee didn't look at engelbarts work
- going back to a oral society?
- papert, logo, and the right mathematical context for a child to learn
- GUI as a supstitude for AI as a input - learning method
- Kahneman System 1 and 2
- the irony that the device that should have brought more "writing" actually lead to more "oral" percieved reality
- quit smoking at 26 

# Slogans

- #tags: #alan_kay #meeting #futureofcoding
- nobody unpacks slogans
- steve krouse - edit lunch notes - https://futureofcoding.org/notes/alan-kay-lunch
- my future of coding backup, **INCLUDING** the original notes from the lunch : https://gitlab.com/AndreasS/futureofcodingbackup

# Quote 

- #tags: #alan_kay #quote
In natural science, Nature has given us a world and we’re just to discover its laws. In computers, we can stuff laws into it and create a world. – Alan Kay
- Via http://cognitivemedium.com/qc-a-science

# marvin minsky's idea that CS has already a grammer but it neets a literature

- #tags: #alan_kay #marvin_minsky #computer_science #narrativ
- https://www.quora.com/How-would-you-interpret-Marvin-Minskys-idea-that-%E2%80%9CComputer-science-already-has-a-grammar-What-it-needs-is-a-literature-%E2%80%9D/answer/Alan-Kay-11?share=f96ab6f2
- todo find the media where minsky was stating this

# Interview

- #tags: #alan_kay #interview #article
- http://factordaily.com/alan-kay-apple-steve-jobs/ Computing pioneer Alan Kay on AI, Apple and future
- er fängt endlich an die Werte hinter dem System anzusprechen
- ref: http://techland.time.com/2013/04/02/an-interview-with-computing-pioneer-alan-kay/

# Y Combinator - Alto 

- #tags: #talk #silicon_valley #youtube #alan_kay
- https://www.youtube.com/watch?v=id1WShzzMCQ How to Invent the Future I
- https://www.youtube.com/watch?v=1e8VZlPBx_0 How to Invent the Future II

# Now at Y combinator HARC

- #tags: #alan_kay #ama #rich_hickey
- https://twitter.com/HARCommunity
- apparently SAP money did run out?
- rebuilding Alto: http://www.righto.com/2016/06/y-combinators-xerox-alto-restoring.html?m=1
- AMA: https://news.ycombinator.com/item?id=11939851
- between 16000 and 20000 books, Betrand Russell 23000
- Rich hickey on that AMA discussion about "how data could be a bad idea" : https://news.ycombinator.com/item?id=11941656 claude shannon, commiunicating with aliens problem
- very good links: https://tekkie.wordpress.com/2010/07/05/sicp-what-is-meant-by-data/
- also find some other threads by him on ycombinator

# Project at SAP - CDG Labs

- #tags: #alan_kay #youtube #github #article #talk
- to recreate PARC Atmosphere and results?
- what ever happend to the results of the STEPS Project?
- http://www.fastcodesign.com/3046437/5-steps-to-recreate-xerox-parcs-design-magic-from-the-guy-who-helped-make-it
- https://www.youtube.com/watch?v=NdSD07U5uBs
- https://github.com/cdglabs
- aparaturs -> strange loop talk
- SAP talk, no slides, : https://www.youtube.com/watch?v=vXjpA9gFX5c
- private link from alan to his last talk at SAP: https://www.youtube.com/watch?v=wKA-jrOT7i0&feature=youtu.be

# OOP Definition

- #tags: #programming #oop #alan_kay #quora #c2wiki
- https://www.quora.com/What-is-Alan-Kays-definition-of-Object-Oriented/answer/Alan-Kay-11?srid=cgo&share=2eb158d4
- more good content on quora
- c2wiki http://wiki.c2.com/?AlanKaysDefinitionOfObjectOriented

# Learning from Seymour Papert

- #tags: #youtube #marvin_minsky #seymour_papert #alan_kay #nicholas_negroponte
- https://www.youtube.com/watch?v=Pvgef9ABDUc
- Mitch Resnick (Scratch), Marvin Minsky, Alan Kay, and Nicholas Negroponte


# Alan Kay, '05 Columbia College Commencement

- #tags: #youtube #alan_kay #speech
- http://www.youtube.com/watch?v=1LBoNUPjEco


# 2017 AR in Action Keynote

- #tags: #alan_kay #conference #talk #youtube
- https://www.youtube.com/watch?v=TI7I0nvcQtg
- bootleg https://www.youtube.com/watch?v=nf5g3rblMK4
- hardware back in the day
- Ivan sutherland Paper on VR -  the ultimate display 1965 : http://worrydream.com/refs/Sutherland%20-%20The%20Ultimate%20Display.pdf
- 4 ideas about the dyna book: 1. in your pocket
- just a coincidence that head mounted display didn#t come earlier
- Nicolas Negroponte braclet idea
- focus issues
- what do we want to agument
- end quote: what we want is to exeed human wisdom over power
- David Smith - Reality Display

# Codemesh IO 2016 Interview with Joe Armstrong

- #tags: #alan_kay #conference #talk #youtube #joe_armstrong
- #book: Arthur Koestler - the act of Creation
- https://www.youtube.com/watch?v=fhOHn9TClXY&list=PLWbHc_FXPo2hGJHXhpgqDU-P4BArpCdh6&index=1
- some comments from twitter, mostly regarding his critique of monads: https://twitter.com/briantrice/status/802559025875324928
- https://twitter.com/deech/status/802537471514996736
- https://twitter.com/deech/status/802530106631716864

# CRESSTCon '16 Alan Kay Keynote - The Best Way to Predict the Future is to Invent It

- #tags: #alan_kay #conference #talk #youtube
- https://www.youtube.com/watch?v=yVw42wWZWrg
- 2016
- Talk on Education
- new slides ( look?)
- thinking fast and slow, sys 1 & 2
- https://youtu.be/yVw42wWZWrg?t=1969 But it is about Science, Systems and Society not simple algorithms
- Francis Bacon Novoum Organum, thinking Erros from: Species, Individuals, Language and Theater (Culture)
- Science as a method to get along whats wrong with our brains
- the environment trancents our desires ( for evil also , example : Television)
- facebook is about beeing a cave person 100.000 years ago

# 2017 Interview - Great Teachers

- #tags: #youtube #alan_kay #relationship #education
- https://www.youtube.com/watch?v=XgqLD8XhxoM&spfreload=1
- Books
- David Evans changing Alan by teaching about people
- 4th and 6th grade Teacher - the curious tinker table, realising that a research scientist and a kid are in a similar state

# 2007 - A Conversation with CMU Faculty & Students

- #tags: #youtube #alan_kay #science
- July 2007
- https://www.youtube.com/watch?v=PFc379hu--8
- short before the start of STEPS
- ~ 10:00 talks about the difficulties finding good researches in the US
- good researchers & artists get over the depresion of beeing dissatisfied
- I think in this conversation alan kay "gets" that science/research has changed ( market influences)
- 1:17 min AK: what has research/science to do with marketing? realy interesting exchange


# Back to the Future of Software Developement - VPRI 785 /23.04.2003

- #tags: #youtube #alan_kay #books #media #computer #smalltalk
- https://www.youtube.com/watch?v=pUoBSC3uoeo
- https://en.wikipedia.org/wiki/Aldus_Manutius AK Story: Aldus wanted to to a portable library so he went out and measured the size of contemporary hore saddles bags
- media history, printing press, printing revolution
- on the origins of comerlization of PC's
- instrumental reasoning 
- computer revolution?
- instrumental reasoning
- bit identical porting
- squeak car wheel and falling ball 
- many more squeak demos: rocket landing, car driving on a path, jumping face animation
- open croquet
- Late Binding

# The 40th Anniversary of the Dynabook / 05.11.2008

- #tags: #youtube #alan_kay #olpc
- https://youtu.be/tQg4LquY0uU?t=4697 1:18:17 as of today there are no reflective user interfaces
- title of alans Talk : "The power of context 40th Anniversary of the Dynabook Idea"
- 60s Marshall McLuhan , past present thinking , but no guidance , he stated facts
- the problem they are trying to solve the "education problem" - use technology to 
- QA  Charles Thacker and Mary Lou Jepsen
- QA: Display breaking is a problem? The Iphone 3GS was a Tank! Now Gorilla Glas
- Charles Thacker: in the US competing for attention TV kids , in preu kids want to learn to read
- QA: Why the dynabook wasn't build?, no good mentors in the developing world? SOLE possible?
- I think do thin SOLE's are good enough for the "better than a bad teacher" effort alan was talking about

# 2012 KEYNOTE 1: Alan Kay - Rethinking Design, Risk, and Software COFES Risk

- #tags: #youtube #alan_kay #steps_project
- AK Title Slides: The Intersection of Design and Risk in the Engineeering and Science of Software
- https://www.youtube.com/watch?v=QboI_1WJUlM
- found slides from YT comment! https://tinlizzie.org/IA/index.php/Rethinking_Design,_Risk,_and_Software_(2012)
- check that wiki and maybe collab? https://tinlizzie.org/IA !! I have a account now
- STEPS
- rant

# 2003 Turing Award Lecture

- #tags: #youtube #alan_kay #slides #turing_award #motivation
- https://www.youtube.com/watch?v=ymF94cFfzUQ
- with better sound and overlay slides: https://www.youtube.com/watch?v=aXC19T5sJ1U
- since 2019 there are slides available: https://tinlizzie.org/IA/index.php/Alan_Kay_Turing_Award_Lecture_%282004%29
- "So, little progress is being made but I think that whenever we say computer science or software engineering, especially whenever we think we're teaching it, the worst thing we could ever do is to pretend to the students **that we know what it is.**
Because the students are going to be the only ones that are going to save us.
So, we should teach the students what I was taught when I was in graduate school in the sixties: that is it isn't done yet. It's not even close to be done. You have to understand what the actual scope of computing is going to be and you have to **help us invent it.**"
- when Alan starts talking about motivations (about 15 min) he states, like "absolute"? categories? but they could be levels conscinouness ?
- the meme simulation... voting, chaning 
- quote : "You know, this group does not go to the polls to vote on the things they believe in.
It takes them a long time to change what they believe in."
- "The hubris of complexity"
- Alan talk about hte current curriculum, but shouldn't paperts idea apply here too? let the student explore on their own? in computer science land?
- Alan refers to "Art, Math, Science, Systems, Computing, engineering, evilization(?)"
- "We don't want to imprint them on, for God's sakes, is the data structures and algorithms - That was a great idea in the 50s and you had to understand it."
- About Violence (54min): "Most kids, boys and girls, put on big off-road tires like this, because part of the deal is feeling powerless, and wanting to feel empowered. This is something that video game manufacturers really understand. Why are those games so violent?" <- I think it is because to be able to execute violence is seen as a symbol of power
- this is really a interesting question, Seymour papert and Alan focused so much effort into making things better for children, but they failed to see the consequences of still leaving children in the context of current society, the violence is still here, the current context _also_ needs to change - Omni considering 
- etoys - squeak 
- croquet - opencroqouet.org

# 1986 The Dynabook - Past Present Future

- #tags: #youtube #alan_kay #history #perspective
- https://tinlizzie.org/IA/index.php/The_Dynabook_--_Past,_Present_and_Future_(1986)
- On scatchpad - 1986 https://www.youtube.com/watch?v=A6h-zDOggYQ
- https://www.youtube.com/watch?v=GMDphyKrAE8&pxtry=1
- https://en.wikipedia.org/wiki/Jerome_Bruner

# Alan Kay at TED2 (1990)

- #tags: #youtube #alan_kay #ted_talk #school #education #learning
- https://www.youtube.com/watch?v=2_E37CqpFLE
- learning with technology
- sea snails behaviour

# Alan Kay Episode of "Education in the Digital Age" / 1998

- #tags: #alan_kay #youtube #education
- https://www.youtube.com/watch?v=t2L2t-D3tDA
- some more deatailed personal background: mother musician, father scientist, grandfather author
- between kindergarten and gradschool negative experiences
- the influence of semour papert and douglas engelbart
- 1% of amaerican population produces all the tech 7 is scientific literate?!
- from 1400 10 in 1000 literate people to in the 1900 to 20 literate poeple
- mcluan global viliage is happening, AK: global civilization would be better
- why schools were invented: they were invented after the printing press because the natural way of human learning(village learning) was not enough to learn (reading and other higher stuff)
- like dijkstra he points ou thtat when the babyboomer population hit the schools there weren't enough (good) teachers
- what science really is

# Alan Kay - 1990 Interview

- #tags: #alan_kay #computer #history #civilization #perspective #purpose
- https://m.youtube.com/watch?v=275FQ9koAw8
- https://www.youtube.com/watch?app=desktop&v=275FQ9koAw8
- unwatched,.. I watched this even in parts
- nice ponderings on the purpose of computers
- Time stamp from comments:
- 14:57 ARPA
- 16:00 Vannevar Bush
- 16:40 pathfinding
- 28:59 early "personal" computers
- 48:18 books chained to desks; so too computers; computers are expensive -- they'll be valuable when we don't have to chain them to desks
- 49:20 illiteracy/literacy when skill can be practiced by lay people; no such thing as medicine/immedicine (il-medicine)
- 1:50:08 software/hardware
- 1:51:58 arithmetic as special case of logic gating
- 1:52:10 turing
- 1:53:25 good programs handle the organic chemistry so you can make the cake
- 1:57:30 "the analytical engine weaves algebraic patterns just as the jacard loom weaves patterns of silk" 
- some other comment with time stamps:
- at 01:00:00 Papert, Smalltalk kids, Piaget & Bruner ...
- at 01:15:00 Theatre...
- at 01:24:00 the first painting programme 1972
- at 01:38:00 VisiCalc
- at 01:56:00 Ada Augusta & Babbage first programming 
- at 02:00:00 Puppet theatre... object oriented programming 
- at 02:05:00 Tools & Agents ... manipulation(tools), management (agents), extensions, megamachines, flexible competence(A.I.)
- at 02:09:00 driving forces
- at 02:10:00 pervasive networking...
- at 02:11:00 newsfeeds ....
- at 02:12:00 Dynabook, intimacy, world wide infrastructure... 
- at 02:14:00 Children, Education, programming ecologies...
- at 02:18:00 Dewey-simulating the adult world...
- at 02:20:00 Computers, education and value systems...
- at 02:24:00 A.I. common sense, science, non-sense...
- at 02:30:00 virtual reality ...
- at 02:36:00 Book of Kells ...
- at 02:38:00 Civilisation, literacy ...


# 30.06.1988 - 19880630

- #tags: #youtube #alan_kay #experiments #logo
- https://www.youtube.com/watch?v=bQRxmX_-pR4 sadly offline (05052022)
- science camp , experiments with 6 year old, 12 year old and 15 year old - drawing with LOGO Turtles? reminded me of Seymour Paperts Piaget experiments


# Portable Portrait: Alan Kay (1990)

- #tags: #alan_kay #youtube #balance #harmony #stress #individual #society
- https://www.youtube.com/watch?v=1z37SMYM3H8
- this is quite a interesting video in which alan kay shows his "pocket he uses to travel, 
- I used to be a professional guitarist, AK: "for me the experience of playing music ... feeling like a conduit"
- using a transparent bag to avoid the messiness of the office
- What do you need for Balance (at the end)
- ALL the stuff on the table (he calls it stress reduction stuff) to make him feel he as a balanced day even when he doesn't , in fact he uses the word stress

# SD&M Konfernz (Germany) - Software Pioniere - year 2001 

- #tags: #talk #youtube #alan_kay #conference #computer #history #cross_reference
- #cross_reference: #konrad_hinsen #dialectic
- https://www.youtube.com/watch?v=MlK0IQmqmCQ 
- showing sketchpad on quicktime :)
- mother of demos
- simula and euler( taking algol nearer to lisp) https://en.wikipedia.org/wiki/Euler_(programming_language)
- 40:41 "using objects at the lowest level in the system and combining the language with this idea of **co-routines** so I realized that if you had co-routines and persistence **you didn't need files** because you basically just paused every structure that you wanted to stay around and just let it stay active there's a way of getting away from data structures so a lot of interesting ideas in this thing and on each iteration of these languages.."
- 41:30 "I'll just say one word that the more you can think biology when you're trying to understand objects the better off you are the more you think C or E or indeed any programming language with objects grafted on the further away you are from these things..."
- 46:00 "There is a big gap between technology and its powerful media" aldus story book size
- late binding to support learning (we don't know anything)
- Xerox PARC and PARC
- designing a character in 1972 (steve jobs and typography)
- showing some Xerox Parc videos , some of these should be online
- Objects and GUI: Objects should have been named Subjects
- Slide "Doing with Images makes Symbols" Jerome Bruner and Piaget Stages: Doing >> Images >> Symbols
- PARC GUI - WSYWYG
- Simulation UI Scripting
- Showing Nicolas Negropontes System from 1977
- had a nice discussion with konrad hinsen in ref to this video on FoC about the dialectic of computing
 
# OOPSLA 1997 - the Computer Revolution hasn't happend yet

- #tags: #alan_kay #talk #conference #youtube #slides #conference #programming #perspective
- https://www.youtube.com/watch?v=oKg1hTOQXoY
- some screenshots, slides annotated: https://catonmat.net/videos/the-computer-revolution-hasnt-happened-yet
- "00:23:50 HTML and the Internet has gone back to the dark ages because it presupposes that there should be a browser that understands its formats. This has to be one of the worst ideas since MSDOS. HTML is what happens when physicists decide to play with computers."
- "00:24:20 Browser wars either demonstrates a non-understanding of how to build complex systems or an attempt to gather territory."
- so the negogiate and grow metapher is way older than the STEPS project
- biology methaphors as an inspiration for objects: 30:00 : In an Ecoli bacteria, 120 million components of the total size of 120GB interact with each other in an informational way. 
- 48:00 Lisp Maxwell's equations of programming. 
- Meta Programming
-  The Art of Metaobject Protocol is the best book written in the last 10 years but it's hard to read. 
- #book https://www.goodreads.com/book/show/274495.The_Art_of_the_Metaobject_Protocol
- McLuhan - whater and fish
 
# extracting energy from the turing tarpit / 06.2012

- #tags: #alan_kay #talk #youtube #programing #computer #cross_reference #steps_project #alan_turing #fractal #pattern
- #cross_reference: #dijkstra #joe_armstrong
- alan is doing the presentation in the frank/STEPS project
- https://www.youtube.com/watch?v=Vt8jyPqsmxE
- ACM Turing laurate
- Alan Perlis :"Beware of the turing tarpit, where everything is possible but nothing is easy"
- energy extractor
- 7:00 "I don't think our main goal is writing millions of lines of code what we want it is for to do something.." this reads painfully reminding me of dijkstras effort to bring some methods to the  software industry, so for reasons people now DO actually write millions and millions lines of code, reminds me also of Joe Armstrongs (were in a mess talk): there was a time when there was too little , just about enough and now its just too much software
- tactics why not strategies
- simple tactics -> mere materials , pile up : pyramids
- strategies -> so that architecture dominates material
- sketchpad bridge
- demoing the frank system from about 15 minutes on
- 3 kinds of turing machines
- Alan Turing: The Chemical Basis of Morphogenesis: https://www.dna.caltech.edu/courses/cs191/paperscs191/turing.pdf
- deep math, trying to show simple systems of gradients : give rise to enourmous Morphogenesis structures >>> the fractal pattern of reality that we try to resonate with
- ants following gradient
- demoing Nile and Gezira
- ometa 
- Bret Victor JS demo
- Architecture to go from "whats" to "hows"
- whats: constraints, meanings , relations
- hows: solvers optimizations, pragmatics
- from turing tarpit to turing oportunity
- Q&A: Q: aren't yesterdays hows todays whats? A: yes its a funding invitation to go for the next step
- Q: predicionts : A: needs funding and people who can ideas into reality
- Q: Spam? Malware? Security? A: longer answer with reference to bio regeneration, take resilince into account 

# The computer revolution hasn't happened yet - October 2001

- #tags: #alan_kay #vimeo #history #education #computing #perspective #seymour_papert #logo #montessori #shinichi_suzuki #jean_piaget #society #democracy #organisation
- NYU School of Engineering
- http://vimeo.com/14965237
- Slide: There is a big gap between a Technology and its Powerful Media 
- No Page Numbers until 1560
- Harvard : reasons for Seasons
- Slide "Most people just "think normaly" tribalism comercialism
- Revenge of business people
- thinking when killing enemies
- Puck: "What fools these mortals be"
- Slide: Representing Ideas about the World
- Loki trickster creativity
- Changing the perspective on things in science
- Slide: Complexity 
- Slide:  The "Great Blue Book" : the scale of biology Ecoli
- Slide:  A "Flexible Personal Computer 1967
- Slide: A Happy thought : Why not just computers all the way down
- Slide: Seymour Papert and LOGO
- give kids vectors and stories geometry as a tool to explore calculus
- **computers as a language machine**
- Montessori: create good environments for kids to grow up in
- ak says montessori was after uncommon sense
- Shinichi Suzuki violin Method: Inner charakter motivation
- math and science as art froms ( I think AK is refering to the expression of the individual human being as a sacred thing/experience)
- you don't get math if you teach pattern matching, its an art form its a story of the person learning the math in a way it is expressed allowing the mathamatics acutally to happen
- Back to the gap slide making a closure for personal computing and the dyna book
- Slide: Xerox and PARC, Old Character Generator, Doing with Images makes Symbols (piaget stages)
- Slide: Media Interface 75
- Slide: Smalltalk int the schools
- Slide: Object Insights, Alan Showcasing his (squeak ) presentation environment, mentioning 3D environment, character linup demo
- ca. 1:008:00 Slide: "Not everyday Math" but "Meaningful Math" : car/wheel/track demo
- Slide: Epidemic demo, Aids, typhoid, statics  time scale and keyhole demonstration
- Slide: "Real World <+> "Math" -> "Science": Falling down get values through frame measure
- Slide: Our own Lander Game
- Slide: How does Floating work
- Slide: Gradient Following
- 1:26:00 Slide: Lots of Ants Seeking Food, pheremon trail, teaching a start of systems theory
- Slide: Sampling etoy
- Slide: Fantasium
- 1:28:00 Slide: Vivarium Demo : teaching by letting kids make ecological models, Drawing a 3D fish in it 
- Conclussion **When is the Computer Revolution going to happen: when the making and understaning of these models of these ideas that we can't do on conventional media**
- Q: Thats the display , how and where can we compile it, A: AK : squeak system "power point demo" 1:36:00
- when leaving disney they (VPRI team) developing squeakland then they left 
- blue plane: for the rest of use, super duper hyper card ( did it ever happen?)
- Alan tells about hte "unbelieveable" much support you have to do at apple for Hypercard, 40year olds won' do the 5 finger piano trainings they want tight away bach... 
- AK:"because we could not stand even happing a 1000 unhappy users
- Q: revolutionary about education/childrean learn and computers?
- A: AK: various ways of answerring that, there has been pretty much ( a lot) known about how to teach these things (in a certain way) for a bout 100 years ( a reference to montessori) , **incredibly difficult to get enough adults signed up (on these ideas)**
- Suziki as a success because he teached so many
- still Suziki and Montessori schools had a very hard time "in this country", adults/parents are not interested in music or expressiveness they want the kid to learn the instrument as fast as possible
- its difficult to find a really good montessori school in the us , there are somewhat polluted
- Logo was one of the great ideas in history, but not 1 elementry teacher in a 1000 in the US knows what the math is its (LOGO)  is pointing towards, they did not know anything about: calculus, cartesian coordinations and differential geometry
- If you give a talk about "how import math and science is" to a crowd ( of teachers) who is not fluent in math and science... your gonna get **pushback**
- they ( refereing to ignorant teachers) are perfetly sound and happy,  (they) forgetting all the thinking that went into that allowed them to be happy and sound
- its a very tough deal whenever a new literacy comes in (really? or is it just a failure of trust and communication?)
- the story about the survery for understanding the common sense book:
- #book Thomas Paine Common Sense https://www.goodreads.com/book/show/161744.Common_Sense
- 20% (adults) in the study could understand the book
- AK: thats pretty good unless you want to live in a democracy 
- But basically he is making the argument that everyone or a majority of people in a country should have very high fluency in math and science, but what if this is impossible? No way around trust!
- bell curve 
- for science its less then 5% for adults in america
- why is it better here in eruope/germany? not because we have smarter people but we have better trust
- Q: about cost A:global display industry making choises..
- old notes:
- kids will do things what they learned to be Normal
- What is science
- Einstein
- Montessori 
- when will it happend,listen to his answer
- what's that common sense book mentioned at the end? => Book, see above

# NITLE summit - Keynote - 07.09.2013 oder 2012

- #tags: #alan_kay #talk #vimeo
- http://vimeo.com/41089805
- teaching, thinking
- Why where book pages invented?
- To make a reference onto previous said things.
- writing -> books -> computer
- each step enhanced capabilities in terms of self-reference (see hofstaedter)
- so question is what is the next step and how can we use the computer to do so

# SRI -  40th anniversary of Douglas Engelbart's 1968 "Mother of All Demos" - 10.09.2013

- #tags: #alan_kay #talk #youtube #douglas_engelbart
- http://www.youtube.com/watch?v=CSXXfiFQqPI
- The problem with augmentation methods device is that they could easily replace/limit our existing capaiites.
- worry dream makes same reference: are they refering to the same person: our abstrations enhance and limit our vision at the same time => this is a principle, find the name 
- it took 150 years for printing press to spread chang, how long will it take for the computer to spread chang?
- about 20:00 SPJ idea of fixing ICT
- if you argue pro something you set a foot note and its fine, if you dislike something you have to justify, longe that the whole argument point you are trying to make?

# Grand Challenges for Computing and Interactive Digital Media - Lecture in Singapore NUS - 01.02.2007

- #tags: #alan_kay #talk #youtube
- https://www.youtube.com/watch?v=e5yTv_VdWLs
- Singapore
- Grand Challenges of the 60s
- Linux learning, not to be overwhelmed => later towards there is nothing *much* of interest in linux
- Doing research at vpri as in the 60 ties? Where does the incremental barrier in research come from?  Peer review?
- Absorbing progress?
- What should we teach our children: outlook, perspective, meta thinking
- The purpose of hardware should be to run software faster ( not to sell cheap instead?!)
- AK talks unusually open about linux as a distraction , and why files and operating systems prevail today
- You can draw the impossibile as long as you can express it in 2D -> that is the limitation

# 2012 SCIx Keynote Presentation

- #tags: #alan_kay #talk #youtube
- https://www.youtube.com/watch?v=BbwOPzxuJ0s
- origin , explanation of progress and the student exchange programm

# NATF - Keynote - 21.01.2013

- #tags: #alan_kay #talk #youtube
- Part I : https://www.youtube.com/watch?v=e0R0tAOf7KI
- The pachinko machine : Sensors,a Brain,  a Ghost and a Dream
- Part II: https://www.youtube.com/watch?v=CJdpseOdMBI
- http://www.fujitsu.com/downloads/SVC/fla/2013.NATF/NATF%202013%20Agenda%20FINAL.2.pdf
- Pondering on some old thoughts:
- Disney ? said cartoons where the art of the century, <= i didn’t munderstand that but he properbly meant it as a means to expressing
- Miazaki s Wind metaphor for “flying thoughts/thinking/creativity”
- Mc Luhan:
- I don’t see it until I believe it
- Puck says:
- Not only are we easily fooled, but we like to be fooled -  we pay to be fooled!
- http://de.wikipedia.org/wiki/Pogo_(Comic)

# SRII - Keynote Talk - 31.03.2011

- #tags: #alan_kay #talk #vimeo #youtube
- http://vimeo.com/22463791
- https://www.youtube.com/watch?v=jEOUVVvjKGU
- QA Session : http://vimeo.com/22477943

ein Paar gedanken zu Smalltalk:
der titel sagt schon einiges:
http://www.youtube.com/watch?v=YX3iRjKj7C0 - "What Killed Smalltalk Could Kill Ruby, Too"

Eines der größten Probleme unserer heutigen Software ist Komplexität. Und wenn Small talk zwar
1. die Möglichkeit bietet diese zu um gehen 
(in dem es verwendet wird "golden Boxes" zu bauen) aber 
2. jedoch leider für die meisten Programmierer zu einem Durcheinander führt ( daher das aufkommen der statisch stark typisierten sprachen)

Müssen wir uns doch fragen: Warum ist es gescheitert und wie können wir es besser machen.

Alle Präsentationen die alan kay nach 1996 gehalten hat sind in Smalltalk/Squeak geschrieben:
http://vimeo.com/10260548 ( seihe zeit index 43-48min für ein beispiel)
bei 1h erklärt er einiges zum Prinzip "golden Box"

Die Frage ist nun wo zum geier bekommt man den Code für all seine Präsentationen her und wie kann man eine golden Box bauen die auch andere (letztlich eine kritische Masse von) Menschen verstehen und benutzen können. 

Wenn man so spekulieren will wird das Internet mit der Entstehung davon einiges zu tun haben, das "Web" bzw. Javascript und HTML ggf. als eine Vorstufe für einen Bootstraping mechanismus (golden box)

Einerseits  hassen es die Menschen vom computer/Compiler bevormundet zu werden ( dynamische Sprachen fans) anderer Seits kann Ihnen dabei garnicht genug abgenommen , geprüft werden ( siehe dependent types, gerade oder ungerade Zahlen als typen beschränlung etc.)

Die Balance aus beidem wird wohl notwendig sein um vorwärts zukommen. :)

Es Gibt hier ja schon Ansätze die aber noch total in den Kinderschuhen stecken(gradual typing): siehe Dart und Clojure core.typed


# Normal considered harmful - 22.10.2009 

- #tags: #youtube #alan_kay #slides #talk #change #group #normal
- http://www.youtube.com/watch?v=FvmTSpJU-Xc
- found some slides!! http://www.slideshare.net/greenwop/normal-considered-harmful
- Human Universals ( next 10 mins): https://www.youtube.com/watch?v=FvmTSpJU-Xc&list=PL4tjVxK-UnHoHXZ1UhfKOjZ5xp11170Op#t=2415
- make some notes!!!!!!!!
- about 40 mins change in groups

**Change in Groups - Human universals**

- 95 % Instumental - Existing Goals most important 5 % Ideas and Tools most important
- 85 % Outer - Society most important 15 % inner self opinion most important   
- 14 % Inner and instrumental , manager, politicians, dictators of countries
- List - Universals:
    - coping
    - social
    - language
    - culture
    - fantasies
    - stories
    - tools, art, technologies
    - goals, plans
    - play & games
    - fixed rules flexible strategies
    - case based learning
    - case based reasoning
    - superstion
    - Religion/Magic
    - theater
    - differences over similarities
    - quick reactions to patterns
    - "the other"
    - supernormal responses
    - vendetta
    - value shortages
    - loud noises & snakes
- List - Non Universals:
    - progress ( an invention an idea!)
    - writing & reading
    - deductive astract math
    - model ased science
    - thought, thought, thought
    - equal rights
    - democracy
    - similarities over differences
    - slow deep thinking
    - legal systems vs vendetta
    - perspective drawing
    - theory of harmony

- TV as a fantasy amplifier
- Why hacks dont compose: example web browser design
- Learning, New and News
- Change in groups , rect. Diagram
- progress is a invention, not so old
- Education as a thermostate
- new and news perspectives
- change in groups
- these are not even close enough notes, rewatch AND take notes
- #book: Dan Ariely: Predictably Irrational / The Hidden Forces that Shape Our Decisions
- village by Richard Taylor <= I cant find that?

# Is it really "Complex"? Or did we just make it "Complicated"? - 30.10.2013

- #tags: #alan_kay #talk #youtube #christopher_alexander
- https://www.youtube.com/watch?v=ubaX1Smg6pY
- avaerage book person reads "10 books" a year
- What was the cell book?
- christopher alexander Notes on the Synthesis of Form (1964)
- Frederick P. Brooks: The Design of Design: Essays from a Computer Scientist
- Rigorous System Design: Joseph Sifakis <= A.k. heard about him at heidelberg , a talk should be available
- => there is no science in system design
- 47:36 => find those talks , slides?
- steve jobs, ipads
- the crew example 100,130, 2-4
- TCP/IP in Ometa Parser ~ 160 Loc
- Ometa website to play arround with?
- http://tinlizzie.org/ometa/
- https://github.com/alexwarth/ometa-js
- http://www.beecube.com/applications/ the programmable computer: BEE3
- Dan Amelang of Nile at the End

# Founder School Session: The Future Doesn't Have to Be Incremental - April 2014

- #tags: #alan_kay #talk #slides #youtube
- https://www.youtube.com/watch?v=gTAghAJcO1o
- human universals
- change in groups
- transscript with slides: https://github.com/matthiasn/talk-transcripts/blob/master/Kay_Alan/NonIncrementalFuture.md

# Keynote - Smalltalk Event - publised 24.11.2012

- #tags: #alan_kay #talk #youtube
- really bad Audio, but you can see the slides
- https://www.youtube.com/watch?v=0j6CEd1tpA8&app=desktop

# Interview with Adele Goldberg

- #tags: #youtube #adele_goldberg #interview #smalltalk #history #community
- https://www.youtube.com/watch?v=IGNiH85PLVg
- accepted in munic, housing problems and she went up to enjoy europe
- po(u)ndering what to do with this math degree, original plan: teach highschool math as her mother
- she needed fellowship, women wouldn't get fellowship (1967), war story about how it was ok in ww2 because the guys were all of to war..
- Chicago and standfort researcher 
- politics of university
- finishing degree in standford
- short teaching in Rio de Janeiro
- ACM 
- AG: "you gotta find ways to be not _**JUST**_ in your own building"
- community work
- I feel so reminded of my status post: https://our.status.im/computing-to-p2p-society/ Adoption and Community Centers , Diversity and Inclusion
- through her community work she met people already going to PARC
- LOGO 
- 1972 spend in brazil
- 1973 PARC
- she was suprised by the freedom at parc , she knew crocorporate workings from interns at IBM, tension with budgets came earlier than she thought
- smalltalk 72
- biggest influece seymour papert
- alan kay flex PHD thesis
- alan kays dynabook mockup
- old notes:
- worked with Alan Kay at PARC
- interview on PARC industry, american relationship between academic research, industrial sponsered research and ,
- starting business into corporate buisiness sponsored by ven. cap.
- developement of open source as a social developement, how alan kay did not do that

# Vannevar Bush symposium, 1995

- #tags: #alan_kay #talk #arthur_koestler
- https://archive.org/details/XD1941_9_95VannevarBushSymTape10_AlanKay 
- Arthur Koestler on Creativity - http://www.youtube.com/watch?v=8SHQAgJGr78  Alan Kay - Arthur Koestler on Creativity and Outlaw Thoughts (Vannevar Bush symposium, 1995) 
- too bad elon musk wont ever will hear this: "engineering is probably is one of the hardest fields to be creative in just because it's all about optimizing and you don't optimize without being very firmly anchored to the context that you're in so what we're talking about here is something that is not about optimization but actually in kind of rotating the point of point of view" 
- I think thats also the dijkstra arrogance talk?!
- Why generral Education is important - blue and pink context
- telling a joke : haha
- doing science: aha
- doing art: ahhh
- 60s interesting because there were no computer scientists : people came in from all kinds of fields
- Memex 1945
- Ivan Sutherland Thesis about sketchpad didn't got published?
- _Objects can act as anything: servers on a network, data structures, control structures in a programming language, valves in a refinery, physical object, bio cell_
- thought vectors and concept space (~ 39 min)
- Marvin Minsky
- Neil Postman : Media are the purposes that we put to that machine (techology)
- Aldus Manutius

# Alan Kay talk round with SAP guy -  bis 16:...

- #tags: #alan_kay #talk #youtube
- http://www.youtube.com/watch?v=IPsZyfGCaKs

# Aspen Institute/ Kennedy Center Arts Summit with Alan Kay and Sarah Lewis - 16.05.2015

- #tags: #alan_kay #talk #youtube
- https://www.youtube.com/watch?v=HTlE_OQPid4
- Book mentioned: thinking fast and slow : basically if System 1 isn't trained (good) enough System 2 never gets a chance! https://youtu.be/HTlE_OQPid4?t=1690
- so Which arts require which System 1 skills , how much of learning and practice do you need to be fluent in it
- principles of Non-Sense research, Science, its about making possible to find ways to percive what is out of reach of our senses
- Book: Maps of the mind
- HAHA!- A HA! - ummm! OH!
- biting class because it looks tasty
- Newtons book: https://en.wikipedia.org/wiki/Philosophi%C3%A6_Naturalis_Principia_Mathematica and how to read it, top 5 !
- getting System 1 fluend changes us cognitivly
- Computer:  "An instrument whose music is ideas" (make a computer out of ropes e.g.)
- operating systems for the people: constitution of america, -> need more itereative improvements, drafting with typeset

# Apple Advertising - A Conversation with Alan Kay 89/06/28 (VPRI 470)

- #tags: #alan_kay #youtube #interview
- https://www.youtube.com/watch?v=idBu9rf1aYU
- 1989
- to conceive computing to be capable of beeing personal
- but with emphasis on learning
- prediction of "mobile" and "inter-networked" society in 

# Inventing the Future

- #tags: #alan_kay #talk #youtube
- 2015
- https://www.youtube.com/watch?v=KVUGkuUj28o
- https://www.youtube.com/watch?v=M6ZHxUwqPVw
- What is culture
- How dows culture shape people? people you didn't met -> recognizeable
- Made up for coping
- writing invented for accounting
- invention of science -> big changes
- constituion of US only for 50 years, but they forgot to update it... power corrupts
- interest in eduction (of him) in the early 60s, to make the values of the US shine in a new and greater way then ever before
- jerry bernard? 8:24 8part1) culture?
- most cultures learn by looking what the adults do, secret societies -> EWDs "Guilds"
- How to read, How to learn from reading ( thats recursive)
- Math: think about time and space without doing
- Computer revolution hasn't happend yet ( critical mass of children lacking?)
- Computer is used for old Media
- cooperation trumphs competition

# Heidelberg Talk

- #tags: #alan_kay #talk
- http://www.heidelberg-laureate-forum.org/blog/video/lecture-friday-september-27-alan-kay/

# Alan Kay's talk for the 30th Anniversary of International Multimedia Forum (2019/12/13)

- #tags: #alan_kay #talk #youtube
- https://www.youtube.com/watch?v=SELkcwI_acg&
- media: making patterns using looms
- jaccard 1000pixel per inch
- ada and babbage description of processes not just patterns
- logo wikipedia : 2 problems
1. reading and writing is not the same
2. media is dead where dynamic content is needed
- ref ted nelon alan kay xerox demo
- HyperCard as a missed chance for a web interaction model
- climate "only the computer can represent, show and let us take a apart and put together many of the most important ideas we need to understand!"
- writing allowed us to spread our ideas our stories our oral communications over time and space
- mcLuhan ref- the medium is the massage/message
- Douglas engelbart groupware 50 years ago - google docs oin the web/JS OS?
- Augmentation
- gaining **wisdom** is necessary to exceed the power of their agencies <= that almost invoked sense-making terminology, sounding very similar to what daniel schmachtenberger says (ref war on sense-making)


# Interviews and Write ups

# Interview with Alan Kay - Gardner Campbell

- #tags: #alan_kay #education # 
- https://www.youtube.com/watch?v=TY-hBgYLJqc
- sadly no transcript from youtube for this video available :(
- most of the school teachers don'T have a handle what 
- the MOOC Gardner does: http://thoughtvectors.net/summer-2014-syllabus-thoughtvectors-1-0-the-pilot-episode/
- its 3h , i think i made notes for the first half but i seem to lost them...
- proficent readings purpose
- science as opinion, al gore
- cargo cult
- stackoverflow
- 1:17 tims phases
- teachers=autonomus chieftains
- congress not progress
- 1:51 what are schools for , how to learn
- highschool-> colloege
- verschulung _stuidium
- college supply diploma for money
- learning through culture
- game with huge stuff
- good teachers love teaching more than their subject
- Teaching As a Conserving Activity (Englisch) Gebundene Ausgabe – Juni 1979
- von Neil Postman  (Autor) - another book
- https://en.wikipedia.org/wiki/Jerome_Bruner books...theory..
- Media Gorillas Neil Postman
- advise on what to study, two majors?

# Powerful Ideas Need Love Too! via Worrydream site - 1995

- #tags: #bret_victor #alan_kay #article
- http://worrydream.com/refs/Kay%20-%20Powerful%20Ideas%20Need%20Love%20Too.html
- teaching as story telling ,bad? why in particular
- diese Art zu lernen ist ?common sense = “gesunder Menschenverstand”  
- isolated cases, stories to be trieved <-> not as a system of inter related arguments about what we think we know and how well we think we know it

# Thomas Paine Common Sense

- #tags: #alan_kay #context
- https://en.wikiquote.org/wiki/Thomas_Paine
- "Independence is my happiness, and I view things as they are, without regard to place or person; my country is the world, and my religion is to do good."

it will be very important for children to get fluent in thhe tree central forms of thinking that are now in use:
-stories
-logical arguments
-system dynamics

Hard to learn , but childrean learn anyway hard to learn things. They need reason ( and possibly a low range to sensomotoric experience) 
Difficulty is not the real issue here. Blonging to a culture and building a personal identity are.

This “create an embedded environment and support classrom teachers with visiting experts” ← SPJ fixing ICT ?
Making media on the internet that is self teaching the computer version of this will be able to find out how old and sophisticated is the sufer and instantly tailor a progression of learning experiences (**THATS what frickin wikipedia should be about**) that will have a much higher chance of introducing  each user to the “good stuff” that underlies most human knowledge.
a montessorri approach 

# Notes on a conversation with alan kay - 20.04.2013

- #tags: #alan_kay #interview
- find link....
- Some interesting points in this interview
- Peak Programming Languages
- Pattern regarding when in timeline programming langues appear which 'peak'
- 5-10 years? --> see alax payne blog post on emerging languages camp
- Programming Scala Education 
- Shakespeare -> TV
- CS -> boom of Personal Computing in 80s, expedience , dead line pressure
- Thoughts on User Interface
- "you never step in the same river twice" A user interface should be a learning environment, explore able in various ways. So it should adapt and change while using aiding in new contexts of the application.
- Also : Think of a programming language as a user interface.
- What was 'bad' about Smaltalk?
- Limited reflection
- C++ is a macro processor
- <= these reflections are technical only , social learning and conception principles are not regarded
- Microprocessor Architecture, Todays Microprocessor Architecture could be improved ( direction?)
- Language disposition
- There exist style languages
- APL, Lisp , Smalltalk
- agglutinative languages
- C++, Java..
- "Also, I think the style languages tend to be late-binding languages. The agglutinative languages are usually early-binding. That makes a huge difference in the whole approach. The kinds of bugs you have to deal with, and when you have to deal with them, is completely different."
- The has huge impacts on
- Late binding <-> Laziness
- W Cook states that there is a connection to algebras and co-algebras
- W. Cook on binding (=> found out via Gilad bracha that W cook has brain cancer :((( )
- Ideas, Good Ideas get discarded because they are not understood(B5000). Isolated processes
- Keep it simple, It is important to keep  a programming language in a state where children may use it without problems on their own. Zed Shaw seems to agree[2]

[1]http://queue.acm.org/detail.cfm?id=1039523
[2]http://vimeo.com/43380467

# Rethinking CS Education | Alan Kay, CrossRoads 2015

- #tags: #alan_kay #talk #youtube
- https://www.youtube.com/watch?v=N9c7_8Gp7gI
- #book: Human Universals Hardcover – May, 1991 by Donald E. Brown 
- Maps of the mind - charts and concepts of the mind and its labyrints / charles Hampden Turner
- Francis Bacon Novoum Organum - Scientific Method
- System 1/2 - re thinking fast and slow
- what is science
- steve jobs
- why learn programming
- do we choose to see/learn/live in culture
- 24 bias's : https://youtu.be/N9c7_8Gp7gI?t=3797

# King Lindberg Lecture - The Best Way to Predict the Future is to Create It. But Is It Already Too Late? 26.09.2018

- #tags: #alan_kay #youtube talk
-  https://www.youtube.com/watch?v=dTPI6wh-Lr0
-  children born in 2018 will be 82 in 2100
-  slogans -> also steve krouse interview
-  adultuts are very dangerous
-  studying normal, bacon
-  https://youtu.be/dTPI6wh-Lr0?t=903 ==>
-  1. What can they (children) learn?
-  2. What do they need to learn? 
-  3. How can we help them
-  teaching with rulers - physics => how to teach humanites?
-  Jerome Bruner : a course of human study
-  do doctors believe in bacteria?
-  scientits to schools -> go!  

# Scaling up the circular economy | Alan Kay, Summit 16 Oct 2019

- #tags: #alan_kay #talk #institutions #dame_ellen_macarthur #circular_economy
- https://www.youtube.com/watch?v=j9ZGFaIHegE
- https://internetat50.com/references/Kay_How.pdf such a wonderfull paper on the 50-60s and research communities print it!
- Ellen MacArthur Foundation
- https://www.janelia.org/
- 19 points from the paper:
- 1. The goodness of the results correlates most strong-ly with the goodness of the funders.
- 2. Visions instead of goals
- 3. Cosmic metaphors really help imagination
- 4. Fund people not projects
- 5. Fund problem-finding, not just problem-solving
- 6. No peer review (unreasonable and reasonable projects)
- 7. It’s baseball, not golf! (how to measure success - what failure rate si acceptable)
- 8. Fund great people (MacArthur for groups!)
- 9. It’s a research community not a research projec
- 10. Important results include new great people
- 11. Separate responsibility from control
- 12. Synergy requires constant messaging
- 13. "No one can have good ideas inside the Beltway" (away from washington?)
- 14. Train your successor and get back to work!
- 15. Argue to make progress, not to win
- 16. If  you  have  the  ability  to  invent  and  make  new tools  that  are  needed  for  your  problem,  then  you must.
- 17. Think and work in the future, not the present or past
- 18. Take an idea immediately 30 years out to evaluate
- 19. What Is Actually Needed (WIAN)

# Interview dr dobbs

- #tags: #interview #alan_kay
- http://www.drdobbs.com/architecture-and-design/interview-with-alan-kay/240003442
- childhood

# McLuhan

- #tags: #alan_kay #marshall_mcluhan
- for now no single entry maybe later more
- is mentioned quite often
- Alan Kay often refers to "the medium is the massage",  160 Seiten
- what is twittter:  https://twitter.com/abughat/status/423305262041006080
- world global village: https://www.youtube.com/watch?v=HeDnPP6ntic
- Medium is the message lecture https://www.youtube.com/watch?v=ImaH51F4HBw
- link to the video where alan explains that he close with neil postman and postman was a student of McLuhan

# Alan Kay's tribute to Ted Nelson at "Intertwingled" Fest

- #tags: #youtube #ted_nelson #alan_kay #smalltalk #xerox_alto #dynamic_media
- https://www.youtube.com/watch?v=AnrlSqtpOkw
- 31 Oct 2014
- Xerox Alto Smalltalk dynamic media example , combining animation and drawing tool
- ComputerLib Book
- one eyed metapher, a glimpse for selling

# Article: Alan kay on personal computing

- #tags: #twitter #alan_kay #paradigm_shift #computing
- via: https://twitter.com/swannodette/status/561570406382202880
- http://dorophone.blogspot.de/2011/07/duckspeak-vs-smalltalk.html
- really good quotes from AK:"... the user interface would have to become a learning environment along the lines of Montessori and Bruner; and [the] needs for large scope, reduction in complexity, and end-user literacy would require that data and control structures be done away with in favor of a more biological scheme of protected universal cells interacting only through messages that could mimic desired behavior.

... we were actually trying for a for a qualitative paradigm shift in belief structures -- a new Kuhnian paradigm in the same spirit as the invention of the printing press..."
- quote on pesonal computing: "I think one of the main consequences of the inventions of personal computing and the world wide Internet is that everyone gets to be a potential participant,
and this means that we now have the entire bell curve of humanity trying to be part of the action. This will dilute good design (almost stamp it out) until mass education can help most people get more savvy about what the new medium is all about. (This is not a fast process). What we have now is not dissimilar to the pop music scene vs the developed music culture (the former has almost driven out the latter -- and this is only possible where there is too little knowledge and taste to prevent it). Not a pretty sight."

# on APL notation

- #tags: #alan_kay #quora #notation
- https://www.quora.com/What-made-APL-programming-so-revolutionary?share=1
- how to fit on a t-shirt
- history of Maxwell’s Equations , from: 20 partial differential equations in x, y, z coordinates to : hiding coordinate systems with vectors, operators div/curl/gradient
- "You can think of the operators gradient: ∇, divergence: ∇•, 
- curl ∇×, as “meta”, that act a bit like macros to rewrite functions in a more complex decorated form."


# Books

- #tags: #alan_kay #books 
- http://en.wikipedia.org/wiki/Common_Sense_%28pamphlet%29
- the medium is the massage
- missing: his "system design" books mentioned in the heidelberg talk
- Calculus on Manifolds a modern Approach to classical theorems of advanced calculus
- Lickleider - the dream machine (biography)
- http://www.squeakland.org/resources/books/readingList.jsp source see comments here: https://www.youtube.com/watch?v=ubaX1Smg6pY
- http://c2.com/cgi/wiki?AlanKaysReadingList

# A Book for alan kay - 70th Birthday

- #tags: #alan_kay #article 
- http://vpri.org/pov/
- I think piumarta was on the STEPS team
- http://piumarta.com/pov/points-of-view.pdf

# missing

- #tags: #thoughts #alan_kay #howard_rheingold
- "Alan kay on how programming standards are contra productive" <- context for this was lost, maybe refind via transscript search
- find alan kays metapher : media gorilla (transscr)
- ... problem as a 2 dim problem space, programming as a N dim. problem space
- quality and quantity in reasearch
- Heidelberg talk 2014
- Alan kays Referenzen: lickleiter,
- from FF,reference? Context?: Charles Sanders Pierce, http://historymatters.gmu.edu/d/6268/ ,  http://www.rheingold.com/texts/tft/9.html
- investigate this playlist: http://www.youtube.com/watch?v=ymF94cFfzUQ&list=PLm7qZto1syFa8yNknStXs7Pt1yA_DgwQ-

# "Maxwell's equations of software" examined 

- #tags: #blog #programming #alan_kay #lisp
- Talks from Alan arround 2006 ? No youtube from that time...
- http://www.righto.com/2008/07/maxwells-equations-of-software-examined.html
- https://web.archive.org/web/20130510102916/http://bc.tech.coop/blog/060224.html
- https://en.wikipedia.org/wiki/Meta-circular_evaluator

# On SimCity 2007

- #tags: #alan_kay #article #soverinity
- via https://notes.andymatuschak.org/z8jg7T3YhvyXiEpy4humYAioLUEjnrdZgwHYs
- https://web.archive.org/web/20130904163228/https://www.donhopkins.com/drupal/node/134
- its so interesting and kind of strange to see that the notion of "Xenogaming" was there already present
- SICP chapter 4.1 


# WebBrowsers and Media

- #tags: #web #browser #media #alan_kay #quora
- https://www.quora.com/Should-web-browsers-have-stuck-to-being-document-viewers
- runars take on it? https://twitter.com/runarorama/status/1355572070378307585?s=20

# Keynote ETE 2021: Making Progress — Alan Kay

- #tags: #alan_kay #talk #conference #youtube #programming #learning #systems #jessica_kerr
- https://www.youtube.com/watch?v=9MqVfzxAp6A
- inspired/helped: Jessica Kerr
- deconstructing normal
- 2 things not/insuccfient 
- 1. Systems Thinker
- 2. premature optimization

# Correspondence with Alan Kay

- #tags: #alan_kay #design #culture #martin_luther
- https://mprove.de/visionreality/mail/kay.html
- "I think Martin Luther was one of the earliest great User Interface designers -- because he understood that you have to do much more than provide function to get large numbers of people to get fluent. You should always try to start with where the end-users are and then help them grow and change."

# Alan Kay on the context and catalysts of personal computing

- #tags: #alan_kay #decentralization #podcast #future_of_coding
- via future of coding slack
- https://notionrealized.com/blog/alan-kay

# Squeakers - DVD Documentary

- tags: #alan_kay #kim_rose #seymour_papert #youtube #
- via http://worrydream.com/AlanKaySqueaklandPosts/ 2004 Mailing list
- https://www.youtube.com/watch?v=ndkW61OUA20
- DVD Documentary on Squeak, some background on Xerox, Apple, dynabook
- Andreas Raab from germany already dead https://www.heise.de/developer/meldung/Squeak-und-Croquet-Entwickler-Andreas-Raab-gestorben-1791119.html
- I watched this in 2022 and I wondered, if they picked the right tools , context for the kids to learn
- it seems to me the kids needed much more cultural , historical , social and psychological topics to build more resilience
- how does squeak help them to be self sufficient farmers? to be Jornalists that can work against fake news?

# Being present

- #tags: #alan_kay
- after watching the latest alan kay talk, pulled between future ( how things could be) and past : xerox parc and model T

#  08 We Were So Lucky Alan Kay - 03.04.2023

- #tags: #alan_kay #change #computing #culture #progress
- https://www.youtube.com/watch?v=ktPCH_p80e4
- "the simple way of expressing what was going on back then is that the goodness of the results is most strongly correlated with the goodness of the funders there are smart people around every
generation but there aren't smart funders around every generation for instance we don't have them now"
- Vi hart " we must ensure human wisdom exceeds human power" - now th equestion is: is technology helpful or an obstacle to achieve this?
- alans conception of progress..
- 25:33 chart wisdom going up? But if the chart would show that the wisdom was so high before (antike, ...) and it would go down in modernity  would his notion of progress be challenged?
- human inventions - humans also invented social structures and practices - but technology and mechanistic thinking?
- **"wisdom requires a lot more context than tool making"**
- alan kays gang nach canossa das technologie einen kontext braucht... but would "education" suffice what about connection? meaning?
-  he says that you would need the three: "education, -humans-, powerful agencies "  to be a computer scientist, but what about all of those computer consumer people?
- "the world of the future is the world between human ears" ...
- "if wisdom means partly more awareness and larger of larger situations and consequences then this is one of the great Destinies for computing" ... **partly** yes sure we can simulate some effects but not the interesting ones, see complexity chapter sand talk
- so alan doesn't acknowledge that the focus on technology led to a **_believe_** in technology which led to stark decrease in wisdom, actually way before the computer