[[_TOC_]]

# Intro

- diffrent paths lead here: foundations of a programmable society
- futarchy
- DAO's
- liquid democracy
- better research => dashboard for progress

# Christian Lindner FDP (Host) - Lisa Neubauer - Brauchen wir Angst in der Klimafrage, Luisa Neubauer?

- #tags: #Staat #Top_Down #Veränderung #Strategie
- https://www.youtube.com/watch?v=wC_b-umGSwk
- Meta: der Staat schaft sich ab, in den Kommentaren bezeugen viele wieviel angenehmer als bei Lanz doch die Disskusion wäre
- mit dem Staat (policies!) ohne den Staat (der Markt regelt das)
- Also was wird mit den Steuergeldern/GEZ gemacht? Desinformiete, Unkonstruktive Hasspropaganda
- Sie erwähnt:
- https://en.wikipedia.org/wiki/Bill_McKibben
- Naomi Klein: https://www.goodreads.com/book/show/21913812-this-changes-everything
- George Monbiot https://www.theguardian.com/profile/georgemonbiot+environment/climate-change

# Rojava David Graeber - Syria, Anarchism and Visiting Rojava

- #tags: #State 
- #language:en
- https://www.youtube.com/watch?v=gqfoJvD0Ifg
- all orgs equal part using women
- getting rid of police for a (state) society

# Economy

**Eric Weinstein and his wife Pia Malaney on - Is Technology Killing Capitalism?**

- https://www.youtube.com/watch?v=SYsy6qbKp3Y
- loops get rid of ALL repetitive work, education institutions not prepared
- silicon valley will create new economic paradigm
- more social (guaranteed income) mor unjust

c - Maja Goepel
 
- https://www.wbgu.de/fileadmin/user_upload/wbgu/publikationen/hauptgutachten/hg2019/pdf/WBGU_HGD2019_Z.pdf Unsere gemeinsame
digitale Zukunft
- wir haben jetzt die wahl and der schwelle zur Veränderung => zu Homo deus, welche werte zählen ==> Grafik Seite 5
- https://www.youtube.com/watch?v=3vhuFlVGBeI
- Tilo zwar (wie immer) teilweise witzig nervt aber mit ständigen wiederholen und naivem unverständnis für komplexe Probleme( es gibt keine einfachen antworten etc..)
- => wenn wir alles wissen warum haben "wir" es nicht gemacht? history is not just, humans are contradictionary, every interview is a story so you can't go beyind stories (mcluan, alan kay simulations)

# political influence- social media - Cambridge analytica - price of influence 

- https://news.ycombinator.com/item?id=19893843

# Work - NewWork Podcast

- Found new work podcast(via twitter Markus andreazek):
- https://www.onthewaytonewwork.com/podcast

## 133 Dark Horse design thinking 

- partizipieren 
- Raum für Fehler 
- Geld nicht no. 1 Priorität 
- Hasso plattner Design School: founded dark horse afterwards , evolving , not fixed , value discussion starting ?

## 113 Dirk Ahlborn

- companies in Italy 
- believes in entrepreneurship, imposter syndrome?
- but it seems he genau
- Book recommend: the courage to be disliked (have found this before)
- psychology interested (why we like things) , how network effects and vitals works (human universals?)

## 112 Fabian Vogelsteller 

- Lukso
- VR Ethereum 
- people always overuse tech (TV...) then adapt 

## 90 Benedikt Herles 

- Buch kaputte Elite : https://m.spiegel.de/karriere/benedikt-herles-im-interview-ueber-sein-buch-die-kaputte-elite-a-926451.html
- die Elite in Deutschland: vornehmlich optimieren von vorhandenem
- Buch: Zukunftsblind, gute Ansätze aber er ist immer noch geldgläubig, keine Werte Diskussion?
- Bürger Blockchain aber kein Vergleich mit China ?
- hält aber trotzdem am national Staaten Model fest ?

## 53 Sasha Lobo

- Wir nennen es Arbeit 
- macht Vorträge zu AI und Crypto Währungen 

# SWR Doku - Weniger arbeiten

- https://www.youtube.com/watch?v=K1yZdUbgaRg
- SAP optimizing? Leadership Programme, Teilen von Führungspositionen über Angebot/Nachrage Börse, angeblich ginge es nicht ohne Hierarchie?
- gerald Hüther beim NWX Kongress in Hamburg
- https://twitter.com/AnkeHeines

# Sinneswandel Podcast

- Precht?

## 3 Brauchen wir eine Bildungsrevolution?

## 5 Was uns damals wie heute vom Glücklichsein abhält

- in sich Ruhen Seneca

## 12 Mission Utopia: Wie wollen wir in Zukunft lernen?

- die zukunftsbauer see notes Education.md

## 38 Christian Felber: Ist unsere Wirtschaft krank? Über die Notwendigkeit eines neuen Wirtschaftens (im Sinnes des Gemeinwohls)

- see notes Tobi.md

## 40 Greta Taubert: Wie lebt es sich als Zeitmillonärin?

- https://marilenaberends.de/2020/02/17/40/
- (kleine) Veränderungen sind bereits im Familienumfeld spürbar z. B.: Ernährung  
- ! mAKE more notes

## 49 Sabine Nuss: Hätte Marx die Digitalisierung als Chance begriffen?

- https://marilenaberends.de/2020/05/11/49/
- wie SN zu Marx gekommen ist ( weil in der - modernen? - VWL kein Platz für Sinn fragen ist)
- Grundeinkommen und Sinnstiftung
- Warum Marx noch heute relevant ist: es geht nicht nur um das ausgleichen von (Zahlen) Werten-Metriken sondern auch um die Bedeutung der Arbeit
- die Bedeutung der Arbeit für den Menschen als (ein notwendiges) sinnstifentendes Element
- ref Postkapitalismus.md war schwierig für mich zu lesen und einzuordnen , meine Zuordnung das es jedoch um die Bedeutung der Arbeit gehe halte ich immer noch für zutreffend
- der Podcast geht in eine Ähnliche richtung aber wie - häufig ? - schafft es Mariella nicht aus ihrer naiven Frage Rolle hinaus zu kommen - Systemtheorie? Umsetzungen/Agenten des Wandels?

## TOListen

- 45 Maja Göpel
- 47 Gerald Hüther

# DAOs

- Aragon - Ethereum
- https://blog.aragon.one/aragon-network-jurisdiction-part-1-decentralized-court-c8ab2a675e82
- Gupta Identity, dislike repuation systems like preiction marktes https://medium.com/humanizing-the-singularity/a-blockchain-solution-for-identity-51fbcae94caa

# Politics

**Software agents for better experiments in governance**

- A bold idea to replace politicians | César Hidalgo
- https://www.youtube.com/watch?v=CyGWML6cI_k&feature=share 

# Instiutions

- Robin Hansons book: The elephant in the brain, reflecting on instituions - how to rebuild them?
- crypto transforming academia: https://medium.com/@sherminvoshmgir/crypto3conomics-reseach-lab-335a6050d2f2
- https://news.ycombinator.com/item?id=16764321 Redesigning the Scientific Paper (theatlantic.com) , pushing more towards reproduceable science?


**podcast: academia, journalism**

- https://medium.com/conversations-with-tyler/tyler-cowen-balaji-srinivasan-tech-bitcoin-crytocurrency-silicon-valley-management-b8db383c9cef
- academia today: intransparent, review first publish after
- open source: transparent, publish first review after

# Economic models for a transition to a sustainable P2P Society

- P2PFoundation
- http://commonstransition.org/commons-transition-p2p-primer/
- https://primer.commonstransition.org/3-library
- book:
1. What is the Commons, what is P2P, and how do they interrelate?
2. What is commons-based peer production and how does it inform the P2P 
economy?
3.What are the politics of the Commons?
4. What is the strategy for a Commons transition, and what would be the 
result?
5. When does the Commons transition begin?

# Postwachstumsökonomie 

- Paech lehr Plurale Ökonomie in ..

- https://youtu.be/0xR2JeOpzug Nico Paech
- Theologie? siehe Nietzsche Gott ist tot, allerdings sieht sein Übermensch sehr genügsam aus
- Flugreisen entscheidend
- der gebildete Mensch leidet unter kognitiver dissoanz, schlechtem gewissen
- gute frage aus tilos comments: ohne smart phone hätte der nico nicht gefunden so ?
- Zeitknappheit als Geisel der Konsumgesellschaft	

** einige fragen paech weizäcker - wwerkstatt zukunft**

- https://www.youtube.com/watch?v=GgiFkp41268
- Generation Club of Rome trifft Paech
- Club of Rome Weizäcker, siehe die Menschheit schafft sich ab

**Die Wachstumsparty ist vorbei! - Niko Paech - Vortrag vom 30.01.2020, Pausluskirche Dortmund**

- https://www.youtube.com/watch?v=e-HSsUS3GZM&feature=share
- Suffizienz
- Gerechtigkeit
- von 12 auf 1 tonne
- von ca 60 kg Fleisch hinzu 24 kg
- die Politik kann nur **gerechte** Reduktion anleiten wenn die Technologie nicht genügt?
- Zeit Sinnes - Kognitive Kompression - Erhöhung des Wohlstandes auf Menge 
- global => runter , regional => rauf und lokal => rauf
- regionale Netzwerke die für wenigr Geldverbrauch und längere nutzbarkeit von Gütern sorgen
- Alfred Marshalls Prosumenten ( kritische Konsumtheorie)

**Rapunzel Events: Niko Paech Postwachstumsökonomie**

- https://www.youtube.com/watch?v=eeb3h9sBm0Y&feature=youtu.be
- Oktober 2016
- ~ 4:41min "Dogmenhistorie und Stand des Nachaltigkeitsdiskurses"
- Grünes Wachstum / Öko-Effizient - Kreisläufe/Erneuerbare? => Technischer Fortschritt - Expansion ( in desem Fall wäre techn. Fortschr. gleich zu setzen mit Kultureller stagnation?!)
- Postwachstumsökonomik / Suffiziente Lebenstile - Subsistenz => Kultureller Wandel - Kontraktion
- ca. 6t eigener CO2 Verbrauch pro Jahr?
- Konsumdemokratien im Depressions- und Erschöpfungszustand,Buch: Alain Ehrenberg - Das erscöpfte Selbst - Depression und Gesellschaft in der Gegenwart
- Produktion/Arbeitszeit nach dem Rückbau: 20h "normale" Erwerbsarbeit 20h marktfreie Versorgungszeit
- Halbierung der Industrie durch Prosumenten: Eigene Resources - 1. Marktfreie Zeit nutzen 2. Handwerkliche Kompetenzen 3. Aufbau sozialer Netze 
- Marktfreier Output: Dinge selbst produzieren, Dinge gemeinschaftlich nutzen, Dinge lange nutzen: Reparieren, Gesundheit und Selbstwirksamkeit
- nacent Projekt mit der Regierung, 1. Ebene: Mikrobiotope 2. Ebene : Lokale Gemeinschaften 3. Regionale Ökosysteme: , Praxispartner...

# Gemeinwohlökonomie

**Gemeinwohl-Ökonomie - Christian Felber: Wirtschaft neu denken**

- https://www.youtube.com/watch?v=PBxKPAu8lvA
- August 2017
- Krisenlandschaft (Übersicht)
    - Verteilungskrise - 8 % der Menschen besitzen mehr als 50%
    - Armutskrise - 122 in der EU armutsgefährdet
    - Hungerkrise - 805 Millionen Menschen unterernährt
    - Umweltkrise - "Welterschöpfung" im August
    - Sinnkrise (Meaning Crisis?) - 2/3 ohne Herzblut in der Arbeit
    - Wertekrise (Meaning Crisis?) - "Geiz ist geil" wirklich?
    - Demokratiekrise - 5 % Mitbestimmung durch Wahlen (DGOV)
    - Machtkrise: 147 Konzerne beherrschen die Welt  (DGOV)
- Disskusion: https://www.youtube.com/watch?v=5speasAi9lw
- Der Bürger als Souverän
- Abstimmungsexperiment am Ende: nicht Zustimmung messen sondern Vorschlag mit wenigsten Gegenstimmen gewinnt
- Abstimmung über das wievielfache sollte legitim sein? "üblicherweise gewinnt der Faktor 10"

# State

- The Rise and Decline of the State by Martin van Creveld - https://www.amazon.com/Rise-Decline-State-Martin-Creveld/dp/052165629X

**Snoden -Lessig - Birgitta Interview**

- https://www.youtube.com/watch?v=gfFn8u7Fb4A&feature=share
- lessig story of how he failed to convince bernie sanders and so the democratic party
- lessig: election money is the problem ( no it isnt)
- snoden: How did we get so far that everone is under surveilience , power 8 macht) as true reason for surv.
- ~22min lessig: the change of the corrupt system mus tbe part of the program ( to sanders)
- lessig: its not allowed to talk about the solution (even democrats are part of _the system_ and as agents of the system are afraid of possible change of the system)
- snowden: why dos thze media favor click baits etc, how to fight the pattern of strong counter reaction without reason in human nature?
- snowden: we need a better incentive system (!)  making moral actions overlay with self interest
- lessig: circumstances for Snowden’s courage 
- snowden: lots of benefit of change, stability is an illusion 

**Terry Patten: "Confronting the "Meta-Crisis": Criteria for Turning the Titanic" | Talks at Google**

- via http://www.whatisemerging.com/opinions/the-rise-of-the-emergentsia-meaning-making-in-a-time-between-worlds
- https://www.youtube.com/watch?v=jHxTvvPZUuI
- he explains, why only reforming politics (lessig! can't do it) Isn't working/ possible 
- also cross reference to Geoffrey west book: scale / model =>  west noted a _self accelerating_ pattern that is _self - terminating_ 
- systemic aproach - emergence
- the task for a collective zen koan
- book: a new republic of the heart

# Models to measure economy impact on environmental XDC

- https://www.heise.de/newsticker/meldung/Missing-Link-Klimawandel-und-Wirtschaft-ein-Index-der-oekologischen-Verantwortung-4436107.html

# Transparenz 


**Demokratie Dev ops**
- https://m.youtube.com/watch?v=hgHfd4sMWzc
- Transparenz und andere Missstände  
- wie schwer Wandel ist ..
- fragen zum Ende :  Normen - NICHT als Gemeingut..

# Liquid Democracy

- Dominik Schiener https://medium.com/organizer-sandbox/liquid-democracy-true-democracy-for-the-21st-century-7c66f5e53b6f

**experiments**

- Bundestags Avatar, https://motherboard.vice.com/en_us/article/59dnbb/colorado-political-candidate-promises-to-give-his-seat-to-an-app

The 
**DemocracyOS**

- morozov on DemoOS
- Santiago Siri : change from within, political party
- started in 2012

**From DemocracyOs to Democracy.earth**

- Democracy.earth
- https://www.youtube.com/watch?v=UajbQTHnTfM
- https://www.youtube.com/watch?v=yGmGWZCE4h0 y combinator france
- https://civichall.org/civicist/coding-world-run-liquid-democracy-powered-blockchains/

https://www.heise.de/newsticker/meldung/Missing-Link-Kontrollverlust-der-liberalen-Demokratien-Panik-ist-angebracht-4464581.html

Durch fortschreitende Privatisierung und Steuersenkung haben liberale Demokratien "faktisch alle Kontrolle über die Infrastrukturen verloren"

Island Piraten 

# Dashboard for Progress - better model for Research

- https://www.youtube.com/watch?v=iUVLuXjPAfg&feature=share Juan Benet


## US

- https://www.ribbonfarm.com/for-new-readers/
- https://www.ribbonfarm.com/you-are-here/ <== Map!!
- https://www.ribbonfarm.com/2016/02/25/the-epic-struggle-between-good-and-neutral/
- https://www.ribbonfarm.com/2017/03/09/another-green-world/
- ribbon colony.io
- de trans https://breakingsmart.com/de/staffel-1/software-ist-die-neue-hardware/

**bundestags Avatar in the US**

https://techcrunch.com/2018/02/24/liquid-democracy-uses-blockchain/

**The Bell Curve**

- Charles Murray interview with Sam Harris 
- building a bubble in a bubble by constructing groups by interest 

**interplanetary voting system**

- https://nvotes.com/elements-of-an-interplanetary-voting-system/

**Erik Weinstein - Andrew Yang Portal Podcast**

- https://www.youtube.com/watch?v=Sa2f0r9W2Mg
- "Amen" , "... they need to loose"

**Ulrike Guerot über die Utopie der "Europäischen Republik**

- https://www.youtube.com/watch?v=vIrdZjBBM5g&feature=share
- Jung & Naiv 331
- "Selbst Wahrnehmungs Probleme?" Franz. Zeitung vs FAZ süddeutsche?
- Linke Franz. Kandidaten anti Europa, in Wahrheit anti EU ...
- Liquid Democracy Kommentar
- 20min - Neopren Anzug wo kommt er her? 2008-2009 
- Was hat Deutschland nach 2008 gemacht ... exportieren von Arbeitslosigkeit...
- 32 min - Konkurrenz in Europa ist schlecht 
- Wer ist Export Weltmeister 
- Vorteile für Unternehmen aber nicht für Bürger ...
- Lybien - Frankreich als humanitärer Einsatz ??
- Sie sagt tiki sei jung aber kann man all ihre talks bei YouTube sehen?
- Wieviele Menschen in de verstehen ihre Argumente? Informationsasymmetrie 
- Widersprüche ohne Ende , sie will ganz Europa zu einer gemeinsamen Republik erziehen , kann sich aber selbiges nicht für Volksentscheide oder liquid democracy vorstellen ? 
- Briten finden den Exit vom brexit nicht 
- Why does BRD exist ?
- Welt Republik im dritten Jahr tausend 
- Drei Elefanten in der europäischen Politik 
- Amerikanisches System für die Vereinigten Staaten von Europa?!
- Tilos frage zum Schluss: nach dem letzten Jahr, IST das aber Utopie .. frage also wie kann man das System ändern?
- Sie erwähnt Junker und 5 Alternativen zum Schluss, !! Welche sind das ?
- https://de.qantara.de/inhalt/philosophie-kants-kosmopolitische-weltanschauung
- ref, nicht gesehen: https://www.youtube.com/watch?v=y4Tbye1ZXBQ
- ==> nicht zu unterschätzen - Tech Einfluss : hyperloop , neural Link 

**value of work**

- David graeber (bullshit jobs) 
- https://www.youtube.com/watch?v=tpoJIkqEXYo&feature=share

**concept of de-growth**

- https://www.jasonhickel.org/blog/2017/11/19/why-branko-milanovic-is-wrong-about-de-growth

## China 

- www.pwc.com/top100 (found July 2017)
- see DS tudy pdf
- from 2009 to 2017 changing value proposition in global society, from fossil fules ( petro china and exxon mobile to apple), also if China does the right moves in 3-5 years ahead of europe

https://www.heise.de/tp/features/China-Geduldige-Investoren-4084830.html
luftline.org

**games to society - corporations**

- https://www.golem.de/news/blizzard-community-macht-overwatch-heldin-zum-hongkong-symbol-1910-144412.html

## Deutschland

### Satire - Böhmermann 

- OK er hat etwas (positives?) bewirkt: https://www.golem.de/news/coinmaster-boehmermann-stoesst-pruefung-von-gluecksspielapps-fuer-kinder-an-1910-144428.html

# Grasswurzelbewegungen

## FridaysForFuture


**BPK: "Scientists for Future" zu den Protesten für mehr Klimaschutz - 12. März 2019**

- https://www.youtube.com/watch?v=OAoPkVfeTo0&feature=share
- BPK: "Scientists for Future" zu den Protesten für mehr Klimaschutz - 12. März 2019
- göping: kreislauf wirtschaft
- herhausen: Kerosin
- polotische entscheidungsfrage /wirtschafts wissenschaften

## Ernährung 

- https://m.youtube.com/watch?v=S5dvVAKBGic

WirGarten

- https://www.heise.de/tp/features/Ernaehrungsraete-Veraenderung-geht-nur-von-unten-3925392.html
- http://ernaehrungsraete.de



# Cities

- Morozov Studie, www.rosalux.de die smarte Stadt neu denken
- fab.city
- D-Cent
- open source circular city 
- thecityisopensource.bloglz.de 
- mission statement 
- Lars Zimmerman 
- to hack: repurpose 

**a plan for humanity**

- https://medium.com/future-literacy/a-plan-for-humanity-2bc04088e3d4
- using some cognitive science to fix some issues we have... 


# Africa - How Rwanda is Becoming the Singapore of Africa

- https://www.youtube.com/watch?v=xX0ozxrZlEQ&feature=youtu.be&t=392


# Meaning - Freedom of Ideas and Speech

**JRE Eric Weinstein**

- https://www.youtube.com/watch?v=_EWCN3CPhTI
- https://en.wikipedia.org/wiki/Octonion

# Attention Economy - Tristan Harris — Fighting Skynet and Firewalling Attention (#387)

- https://tim.blog/2019/09/19/tristan-harris/
- how language is shaping perception
- Tristan explains why attending a retreat on hypnosis, pickpocketing, and magic in Bali turned out to be one of the best life choices he’s ever made
- https://thework.com/ of byron katie
- Belief transformation, identity level propaganda, and the difference between Byron Katie’s work in psychological influence and Russia’s influence campaign during the 2016 US election. [28:58]
- **Waking up** from habitual processes, not believing everything we think, and the 21-Day No-Complaint Experiment. [35:10]
- The importance of gaining literacy for our minds — for ourselves and the sake of civilization. [47:49]
- Stanford Persuasive Technology Lab/ BJ Fogg, ( interestingly he doesn't mention it in the show notes and in the show they excuse like 3 times that the lab wasn't meant to be evil, or to create evil things? ha?)
- How can we stop software from, as Marc Andreessen once said, eating the world, and what would be a good use for funds generated by a proposed attention data tax? [1:23:03] from human curated saturday morning TV to youtube recommender
- in the process of optimization software isn't human and can hardly incorporate values so wwe have to be careful for software "simply optimizing" but creating poroblems like social collaps in the process

## The interlectual Dark Web

- origins of the term: https://samharris.org/podcasts/112-intellectual-dark-web/ via https://www.youtube.com/watch?v=_EWCN3CPhTI

**Future thinkers 101 on how it IDW changed over time**
 
-  https://www.youtube.com/watch?v=XLXOOY_I2fw
-   more depth => so from dark web to deep webn, even move out from binary dark/light to deep
-   How do yoiu express DMT trip?
-   Joe Rogan: canabis to deal with anxiety

**more on the origins-network**

- Weinsteins in 2009 - conference in Canada
- 2017 article by jordan green hall 
- Jim Rutt - Santa FE Institute also Trent Mc C.
- revival of Plan B


# Wirtschaft

**Ursachen der Naturzerstörung in der kapitalistischen Produktionsweise - Athanasios Karathanassis**

- https://www.youtube.com/watch?v=fvLMyXvFM3c Februar 2018
- Qualität <-> Quantität
- die Rolle der Gewerkschaften: Anstatt Arbeitszeitverkürzung => mehr Geld
- in den 50ern von einer Wachstumsökonomie zu einer _Wachstumskultur_,  wie konnte dies geschen?
- die Widersprüche zwischen Kapital und Natur
- Ökonomie ohne Kapital, ein großes Stück Arbeit
- Aristotelis unterschied Ökonomie (Haushalt) im Vergleich zur https://de.wikipedia.org/wiki/Chrematistik 
- siehe : https://de.wikipedia.org/wiki/Kapitalismuskritik 

# Gesellschaft

**How Culture Makes Us Feel Lost - Dr. Gabor Maté On Finding Your True Self Again**

- #tags: #karl_marx #separation #alienation
- https://www.youtube.com/watch?v=TIjvXtZRerY
- 1. from Nature
- 2. from other People
- 3. from our work
- 4. from ourselves
- https://en.wikipedia.org/wiki/Marx%27s_theory_of_alienation
- René Henriksen
5 months ago
" - The basis on economy is based on the loss of meaning in our lives."  Now someone has said it directly.
Actually its a bit of a misquote since the original used phrase is:  much of the economy is based on a loss of meaning in our culture

# Frankfurt Lokal

**Transitionnetwork.org**

- via Future thinkers discord
- https://frankfurt-im-wandel.de/ Regional währungen Simon? Solawi Ernährungsräte

# Human Connection

- startet nun (24.11.2019) nach 8? Jahren und 6 Technologie Iterationen
- https://www.youtube.com/watch?time_continue=14&v=jeyA24XyZ_k dennis hack und Ken Jebsen
- Dennis Hack hätte gerne 70000€ pro monat gehabt

# Sustainable Society - Africa - Burkina Faso

- Operndorf https://de.wikipedia.org/wiki/Operndorf_Afrika Christoph Schlingensief 
- Diébédo Francis Kéré - building with clay and community gando https://www.youtube.com/watch?v=1bTx_nftAj4
- http://kere-foundation.com/de/

# Africa by Kayak

- https://www.youtube.com/watch?v=UWnNjn1pki4&feature=share
- to know himself and the world better

# Paul graham: the incompetence of nation states as potential for improvement

- https://twitter.com/paulg/status/1164396598178111488?s=21    
- paul graham: the incompetence of nation states as potential for improvement - like crypto currencies 

# Fridays for Future - Deutschland trend die Wende

 - https://media.ccc.de/v/36c3-11227-fridays_for_future_de#t=714
 - https://www.youtube.com/watch?v=vLOmIyoRDBw
 - die Merhheit erkennt klima als ernst zu nehmendes Problem
 - die youtube kommentare (auch bei XR sind beklemmend)

# XR CCC

- mehr views UND mehr dislikes
- distanzierung von gründer Äußerungen
- dezentralisierung
- https://www.youtube.com/watch?v=eBgNn1MAIQg

# Daniele Ganser - Jahres Rückblick 2019

- https://www.youtube.com/watch?v=3CqZqfpYQ2g
- Venezuela gescheiterter
- Greta Thunberg Davos : positives Engagement für die Umwelt - Fridays For Future und andere werden gegründet
- ARD Framing Manuel wird veröffentlicht
- Beitragsverweigerer vs Beitragshinterzieher ==> Propaganda
- Assange in London verhaftet
- 9.11 Studie Alaska University: kein Feuer, 81 Stahlträger kollabieren simultan
- Deutschland ist seit 2002 im Krieg - Afghanistan (59 Tote Soldaten)
- Deutschland ist im Krieg mit Syrien seit 2015
- Syrien: CIA OP zur befaffnung u.a. von Terroristen
- Konflikte innerhalb der NATO Türkei <-> USA <-> Kurden
- Bolivien
- Weltall US Total Domination

# next decade (-> 2030) non crypto perspective from a crypto person

- Eva Beylin @evabeylin Jan 3rd 2020, 12 tweets, 4 min read
- https://twitter.com/evabeylin/status/1212897473947193346?s=20
- Someone asked me about next decade predictions for anything *but* crypto.
- While I believe crypto will touch everything in value & commerce, here are 10 events / innovations that I think are likely (& could also impact crypto) in the 2020s.
- In no particular order:  
- 1/ Users will start to exercise the 2nd amendment, the right to bear privacy.
- We'll see an evolution of high-quality default private products that let users charge a premium for the degree of public data sharing.
- This'll be easier when identity is a wallet address (ie. crypto). 
- 2/ **Transhumanism** will rise & the world will become starkly divided.
- For some, BMI tech & CRISPR will be subsidized, for others they will remain strictly illegal even if they reduce health risks. Either way, we'll see more black markets for gene-editing.
- 3/ The fall of nation states will become tangible as the first digital nations are legalized & citizens are allowed to own property in those nations.
- New legal frameworks will protect citizens in non-physical domains. Digital law unhinged from statism.
- 4/ At least one machine learning robot will kill its creator. 
- 5/ We’ll see the first national Space Race b/w SpaceX, Blue Origin & Virgin Galactic for space tourism & life exploration.
- The surveillance economy battleground will be in space, satellites the weapons.
- Space tech will be commoditized (fwd to teens doing space food muckbang). 
- 6/ Rockets will meet Moore's law. 👩‍🚀
- The space race will commercialize Rockets On Earth™ for public transit (eg. submarine, drone, avatar).
- If you can't get around by a rocket-powered machine, at least your packages will.
- 7/ There will be immense advancements in life extension (anti-aging & disease prevention).
- I theorize it'll be greatly driven by plant-based diet research & recipe innovations; that will improve health & begin dethroning meat/dairy producers.
- 8/ **Water wars** & noticeable shortages will rise.
- 70% of global freshwater is used for agriculture (4.8K 500ml bottles needed to make 1 hamburger!!)
- Population growth coupled with water depletion will lead to greater premiums/taxes on overuse & pollution.
- 9/ Censored "quiet space".
- Amazon Echo & Google Home can hear up to 10-25ft & 150 decibels. What happens when all cities are in range of FAGMA? When is it an invasion of property?
- Soundproof tech will rise but thought-reading tech already exists too.
- Sidebar - Google is currently giving away Google Homes for FREE if you have Google Fi.
- Why? They'll make back the cost+ from selling data & upgrades will improve range (surveillance) without users realizing.
- Btw finding stats on range was VERY difficult, I wonder why...👀 
- 10/ Dangerous identity & fact fraud.
- AI behavior matching, facial fraud, deepfakes & hacking will lead to indistinguishable identity theft/fraud.
- We'll need to begin using more complex metrics like social networking & memetic code for identity.

# Die Konsum-Aussteiger: Mit Kindern im Mini-Haus | WDR Doku

- in etwa um die gleiche Zeit gesehen wir die SWR Doku - weniger arbeiten
- https://www.youtube.com/watch?v=Hd3V_b3fT-Q
- über 3 jahre verfolgt
- Jurte mit Zwei Kindern: mehrere Stell/Zelt plätze sind später wieder zurück zum ersten Platz - Gemeinschaft?, Handling Jurte im Winter Kondenswasser, 
- selbst gebautes Tiny House 1 Kind , riesige Probleme einen Stellplatz zu finden, mit stell platz noch Container dazu
- beide mussten trotz der geringen Kosten, ca. 600-700 € ! pro Monat für Krankenkasse und laufende Kosten

# eye on the nile egypt

- https://media.ccc.de/v/36c3-10561-the_eye_on_the_nile

# Loosening the grip of control in the corporation

- via twitter markus Andr.
- https://codahale.com//work-is-work/
- see async communication favor

# State-sponsored networks of information manipulation

- https://twitter.com/cryptowanderer/status/1222802000997494786?s=20

# Die 2030er - Dekade des Umbruchs

- via Spiegel 01/2020
- "Wie geht Deutschland damit um das es älter wird, zugleich aber althergebrachtes immer kürzeren Bestand hat?"
- Erste Quelle: Beratungsfirma Progress (Berlin?)
- wichtige (laut quelle) Faktoren in den 30ern: Digitalisierung und Demografie

# Tiny House

- https://www.youtube.com/watch?v=uD3-IvWOzGg&feature=share
- im Fichtel gebirge grundstück gekauft
- Konsumgesellschaft, 1 Zimmerwohnung verwehrt
- 25 qm
- Schlafzimmer unten
- Geduld mit den Ämtern Freundlich bleiben aber Dickköpfig

# Jim Rutt Podcast Ep 43 Daniel Christian Wahl

- Changing the Perception of the Purpose Science from control to..
- Daniel is in the GameB Community
- the Style of his book - asking questions, changing the presupposeed stance from knowing to questioning
- Book: Regenerative Cultures <== !!

# Cardboard Box House - Wikkelhouse: pick your modular segments & click them together

- https://www.youtube.com/watch?v=C42Wg2JDRmc
- https://wikkelhouse.com
- start 30.000 € most houses between 50.000 and 80.000 €
- expected life cycle of 50 years minimum
- 100 % Recyclable
- No Foundation needed , Wind poses no danger 

# Jung und Naiv - Gemeinwohlökonomie mit Christian Felber

- https://www.youtube.com/watch?v=7mRe1ntgbj8&feature=share
- "die Schule ist die Mörderin des Kindes" !
- => zu erst ( gefühlsttechnisch) für sich sorgen danach auf mehr ein lassen     
- werte als Gefühle erfahren ( teilen Solidarität) ~ 16  min
- verfassungswerte und Beziehungswerte
- Beziehungswerte : wenn beziehungen gelingen - höchste Glückshormone
- die werte die Öknomen haben sind _alle_ dem Gelingen von Beziehungen entgegengerichtet: eigennutzen maximierung, Konkurrenz orientierung, materialismnus orientierung, erfolgsorieentierung
- ökonomische Erte Führen dazu das die Menschen unglücklich werden
- kapitalismus und freiheit miltion friedman
- Stiftungs proffessoren - Die bildung ist _nicht_ frei
- Tilos frage: woher kommt das Bildungs monopol
- tiefen ökologie, deutliche einflüsse von Charles eisenstein sacred economics
- Mankew : konsumenten maximieren ihren eigenen Nutzen, sie berücksichtigen nicht die interessen der anderen Konsumenten
- Homo economics ist ein mensch ohne Herz, ein MOnster
- erlebnisse von gelingenden Beziuehungen ( gerald hüther jargon?)
- Kapitalismus forschung: renditen, profite , preise , löhne und Brutto inlands produkt
- CF: definition kapitalismus : Wirtschaftssystem in dem die Mehrungs des Kapitals oberstes ziel ist
- im kapitalimus wird der unternehmerische erfolg primär an der finanz bilanz gemessen
- freihandel - euphemismus : mittel zum zweck - Handel wird auch dann gefördert wenn klima wandel , sklaven ...
- Wie zum Konzept der "Gemeinwohlökonomie" gekommen? Das Ende einer langen reise, Buch 50 Vorschläge für eine bessere Welt - Gegenargumente waren Konkurrenz, leistung etc.. => es geht um Werte
- Spitzensportler analogie 1:25h
- Gemeinwohl Produkt 
- Parteien - an der Basis überall bereits Gemeinwohlökonomie Konzepte , je weiter man nach oben kommt um so dünner !

# Tilo Jung Fridays for future 

- https://www.youtube.com/watch?v=i5PEF0RkaeE
- Die jungen sollen es richten?
- aber sie haben kein gewicht?
- zynisch  durch stagnation?
- politische sprache

# Happiness

- https://www.youtube.com/watch?v=e9dZQelULDk
- mouse animation story purpose life society eudamonia ikigai pill pleasure distraction

# IN-SHADOW - A Modern Odyssey - Animated Short Film

- https://www.youtube.com/watch?v=j800SVeiS5I
- trying to relatede something like this...
- bodies with holes instead of hearts, cube head
- baby chakras tree
- masks pills distraction man woman society 

# Begriffs genese - neues Vokabular für den Wandel

- Tilo Maja Göpel : Lebens bejaend
- Maja Göpel https://www.youtube.com/watch?v=dapQPzCZscE&feature=share Begriffe sind da  aber woher kommt der Wandel?
- Handlungs Paradox oder Paralyse? MG : berät nur Unternehmen und Politik? Struktur und Funktion? Politik und alte Institutionen  können Funktion anpassen?
- Ronny Lebens bejaend
- https://konzeptwerk-neue-oekonomie.org/themen/zeitwohlstand/
- https://www.zeitwohlstand.unibe.ch/

# Naval Ravikant - the modern struggle

- twitter.com/naval
- Naval (Blockchain) investtor from silicon valley , mindfullnes 
- he sees Blockchain potential but his views on freedom vs responsibility seem to lack a lot of perspective? commons? Charles Eisen stein?
- https://twitter.com/naval/status/1084739181593559040?s=12
- https://www.youtube.com/watch?v=OqlfWDyS1Io
- "The modern struggle"
- "Lone individuals summoning inhuman willpower,"
- "fasting, meditating, and exercising,"
- "up against armies of scientists & statisticians weaponizing abundant food, screens, & medicine into junk food, clickbait news, infinite porn, endless games & addictive drugs."


# Naval - Joe Rogan

- https://www.youtube.com/watch?v=kGMJ61Phs2s Joe Rogan | Why Socialism Has Yet to Work w/Naval Ravikat - so "free trade is build into us"? really?
- https://www.youtube.com/watch?v=3qHkcs3kG44
- comments contents: 
- [0:02:16] Do all what you can do (on jobs and hobbies)
- On reading 📖 
- [0:06:54] I read to satisfy my curiosity 
- [0:09:18] Social media 📱 
- [0:09:18] It doesn’t take a lot of insults to cancel out a good self image
- [0:09:37] Be Rich AND Anonymous
- [0:12:26] Creating wealth 💰 
- [0:15:16] You wanna be rich and CALM
- [0:16:22] You have to be consistent with your goals
- [0:16:39] Social contracts are very powerful
- [0:17:29] How happinesses can be a choice
- [0:18:28] Definition of happiness
- [0:20:05] The impacts of good decisions are much higher than what it used to be
- [0:21:22] Just relax and destress
- [0:22:25] We are closer to carnivores 
- [0:23:10] Humans are not meant to work 9to5
- [0:23:45] You have to own a piece of a business EQUITY
- [0:24:11] the information age is gonna REVERSE the industrial age
- [0:25:24] Ron-cos theorem
- [0:26:55] High quality work is gonna be available in a gig kind of fashion
- [0:29:02] On Ai & Automation 🤖 
- [0:29:16] Non-solution to a non-problem 
- [0:30:23] Society will always create new jobs
- [0:30:39] A record low employment 
- [0:30:50] How quickly can you reteach people
- [0:33:18] Coding is thinking 
- [0:35:12] Every intelligence is contextual. General Ai is waaaaaaay far away.
- [0:38:24] Dumbing Information on Ai is not enough. It has to have an environment to operate in it.
- [0:39:37] All of our basic needs is taken care of. And that’s GOOD!
- [0:40:41] Universal Basic Income
- [0:41:50] Capitalism 💵 
- [0:41:41] Free exchange and trade is intrinsic to the human nature
- [0:42:07] We should strive for equal opportunity.
- [0:43:21] Socialism is violent!
- #intersting 0:44:43  "With family I’m communist. With friends I’m socialist... and so on. "
- #intersting 0:46:07 
- [01:03:17] Social media 📱 
- [0:53:30] if you follow up the live news. You’ll get insane.
- #intersting 0:54:54 
- [0:57:57] news is commodity
-  #mhm 0:59:00  VERY IMPORTANT!!!!!
- [01:03:02] Politics and social media and power
- [01:06:57] People are afraid to speak!!
- [01:08:16] Technology empowers the individuals. Therefore technology helps shifting towards the left.
- [01:10:24] تقديس العلوم أفسدها
- [01:10:24] Social sciences are fake!  #intersting 01:10:24  
- [01:11:31] Biology will suffer the most.
- [01:11:39] FACTS ARE FACTS!
- [01:12:18] The gender discussions and like that are corrupted easily and heavily.
-  #intersting 01:13:59  The test for any system: hand it over to your enemy to run it for the next decade.
- [01:17:18] We are overexposed. The way to survive is retreat from society.
- [01:20:09] What is meditation 🧘‍♂️ 
- [01:24:41] The only way to get peace
- [01:26:48] The problem with environmentalism is that they don’t provide the solution
- [01:28:34] Align this goal with economic growth
- [01:28:49] Education 
- [01:33:13] You have to know the basics!!! #mhm 01:33:21  
- [01:34:42] When you memorize it’s an indication that you don’t understand 
- #intersting 01:37:52  
- [01:43:03] EDUCATION !!!!!  #mhm 01:43:03  
- [01:50:54] The most powerful money makers are individual brands !!!
 - #intersting 01:57:52  
- [02:05:55] meetings are useless :) 
- ========== other comment>
- - Read the same 100 books over and over
- Celebrities are the most miserable people in the World
- You’re not your public image
- Rich and anonymous > poor and famous
- Play and don’t overload your brain with too many information
- You want to work as an athlete, a mental athlete
- We’re not meant to work 9 to 5
- Own a business
- Work for yourself
- If someone can tell you when to work and what to where you’re not free
- Companies are smaller and smaller because they can externalize easily
- Maybe these gigs will include also the high-quality work
- Work will be organized in sprints and will be mission-based
- Electricity did to the work landscape what automation is doing today
- It’s impossible predicting what job is going to be created
- Universal basic income would bankrupt a country
- UBI doesn’t provide meaning to people
- UBI doesn’t take into account specific needs
- We should provide basic services instead
- You should educate yourself all your life
- Generalized AI is not coming in our lifetime
- We don’t even understand what really happens inside a cell, let alone the brain
- Intelligence is relevant only in its surrounding environment
- Equal opportunities > equal outcomes
- We all want to be socialist but with the head, we think capitalism
- White-collar vs blue-collar is the real battle going on
- Everyone can broadcast anything at anytime
- It’s also easy to set a mob and take someone out online
- Outraged people are the stupidest people on social media
- We have also anti-mob tactics 
- News has become commoditized, they’re not propaganda
- We will create decentralized media that cannot be suppressed
- Nowadays nobody has the privilege not to have political opinions
- Some technology is neutral, some are political
- Science is hard for people and often facts are not enough to make up their minds
- There’s no room for nuances, people are very polarised
- We live in a dictatorship of 51% so all your believes have to fit in one of the two sides. This doesn’t make you a clear thinker. 
- Being alone and enjoying it is a superpower aka the art of doing nothing
- Meditation is self-therapy 
- Peace is happiness at rest, happiness is peace in motion
- Peace is not about external problems but about giving up on the idea of problems itself
- It’s easier to change yourself than to change the world
- People are not going to giving up economic growth
- Lower the price of cleantech
- Being forced to articulate thoughts helps to understand them more than simply sit in a room and think about them
- Understanding the basics of reasoning is better than memorizing advanced concepts
- A good book you can read one page at night and then spend the rest of the night reasoning about it
- The meaning of life is a “why” questions and it’s endless
- You end up with Agrippa's trilemma where you have only the following 3 dead ends
- 1 Infinite regress
- 2 Circular reasoning
- 3 Axiom 
- The only answer is “because” meaning that we need to make up our own answer for the meaning of life
- All the great questions are paradoxes
- Thinking about them gives you insights that bring you a certain level of peace
- Everyone can be rich
- Imagine if tomorrow everybody was trained in software and engineering. We would create all the animation necessary not to work anymore and just focus on creativity. 
- Richness is about education
- The universe has infinite resources
- Lack of material possession can make you unhappy but material possession won’t make you happy
- If you are smart you should be able to figure out how to be happy
- Busy minds, called also monkey minds are not peaceful
- You have to develop peace FROM mind not peace OF mind
- There’s a fundamental delusion about thinking that there’s something external to us that can bring us peace and happiness
- Once you solve your money problem you stop sacrificing something today with the idea you’re gonna get something in return tomorrow
- Do something you love, do a lot of money or live like a monk so money will stop to be an issue
- Focus on being authentic, unique, creative and not replaceable 
- There are 2 great depictions: heroine and a monthly salary
- You have to ignore your peers
- “That’s easy for you to say” and “keeping up with the Jones” is a big trap
- Stop thinking it’s someone else’s fault
- If you judge others you’re gonna separate yourself, feel lonely and negative
- Reality is neutral and how you interpret it is your choice alone
- Life is short, you’re gonna die, choose to be happy
- Confucious: a man has 2 life, the second one is when you realize you only have one
- Desire is suffering. Choose wisely. Choose one desire at a time
- Force yourself to be positive until it becomes automatic
- Smile more, hug more, get sunlight, spend time in nature, meditate
- Trying to sound smart is a disease 
- Good question about it: “Would I be still interested in this thing if I couldn’t tell anyone?”
- If you are successful you’re gonna be inundated by inbound opportunities that you cannot manage and it’s harder to pick up something you by yourself
- You’re never gonna make more money than you think you’re worth
- Pick an aspiration hourly rate and be extremely jealous of your time
- This will help you define the cost of things like meetings or business travels
- Embrace what you’re doing at the moment 
- Do art, creativity, love. Do things for their own sake
- Work should feel like play to you so no-one can compete with you


# Kate Raworth - Trees As Infrastructure

- via https://twitter.com/cdlariviere/status/1270082149438365699?s=20
- "two very very different streams" money and other metrics like carbon footprint water usage etc.
- "redefining the language of power" (that is now money)
- "dashboard of metrics" dashboard for progress?
- https://www.goodreads.com/book/show/29214420-doughnut-economics

# Fourth Industrial Revolution (4IR) vs Global Peoples Revolution (GPR) 

- via this: https://twitter.com/leashless/status/1262207937864503297?s=20
- #tags: #narrative #society #industry #transformation #transition
- I came to https://twitter.com/michaelhaupt/status/1264553874670706689?s=20 and so to Michael Haupt medium
- following the two very very different difrections from Kate Raworth
- https://medium.com/society4/the-great-reset-3a7d10a34f94?source=---------4------------------
- Fourth Industrial Revolution (4IR) vs Global Peoples Revolution (GPR) 

# Daniel Schmachtenberger Modern Wisdom Podcast

- https://www.youtube.com/watch?v=tJQac_T_rPo&feature=share
- Daniel Schmachtenberger | Reality, Meaning & Self-Development | Modern Wisdom Podcast #179
- Eudamonia
- Science as a insufficient way to describe all of reality 
- DS: consciousness is not computational, not purely computational - fundamentally trans computational 
- DS:  "all of the contents of my consciousness came from the world so the idea that it's my consciousness is just kind of sloppy thinking and a misnomer " 
- I SELF - "my" consciousness 

# Vinay Gupta - Cutting through spiritual colonialism

- https://twitter.com/leashless/status/1262207937864503297
- essentially rant on "Cutting through spiritual colonialism"
- https://docs.google.com/document/d/1PwRpfJiqus_h8Hyj006wtsLJDClsdWcb/edit
- contains a chapter "The Space elite"
- last sentence: "By this metric, who among us is truly free? We must work together to build a moral base for human freedom."
- asked for feedback in DGOV but there was none

# Don't go back to normal 

- https://dontgobacktonormal.uk/
- by @solarpunkgirl from DGOV ... lets see what comes out of this


# IQ - Wissenschaft und Forschung Post-Wachstums-Ökonomie

- via: https://twitter.com/SustDigi/status/1275359077472915456
- https://www.br.de/radio/bayern2/programmkalender/sendung-2744382.html
- #tags: #Postwachstumsökonomie #Nico_Paech

# Sommergespräch mit Harald Lesch

- https://www.youtube.com/watch?v=B-tfyHPFpyU&feature=share&app=desktop
- Veränderungen in der Gesellschaft, Hirschhausen punkte:
- 40 Millionen Christen aber keine Mehrheit gegen Klimawandel
- widersprüche zwischen besseltheit und wissenschaftlichen Fakten
- Lesch: Argumente und Überzeugungen jenseits von wissenschaftlichen Fakten sind notwendig
- Wachstum notwendig oder nicht?

# Günter Gaus im Gespräch mit Rudi Dutschke (1967)

- https://www.youtube.com/watch?v=SeIsyuoNfOg&feature=youtu.be
- #tags: #Geschichte
- dezentrale organisation, lange Umsetzung, Forderung nach Frieden ( austritt NATO)
- RD: " in vergangenen epochen machten die revolutionäre ihre arbeit im wesentlichen unter nationalstaatlichen bedingungen wir machen unsere arbeit heute und der weltgeschichtlichen bedingungen"
- sehr guter Interviewer die Fragen danach wie sich bereits gemachte Fehler vermeiden lassen, 
- Auf die Frage danach wie RD die Korruption durch das Wachsen der Organisation verhindern will Antwortet er durch bewußte Reflektion beim Wachsen, die Grundlegende Fragestellung ausgehend vom "Wille zur Macht" - ref. Nietzsche steht immer im raum, 
- RD: "bei uns sind die entscheidenden komponenten selbsttätigkeit selbstorganisation entfaltung der initiative und der bewusstheit der menschen"
- RD - Gefängnisse als Gefängnisse zu beseitigen
- GG frage nach Christentum - RD : "was hat jesus christus da eigentlich getrieben wie wollte er seine gesellschaft verändern und welche mittel benutzt" <- "seine" Gesellschaft, ich bin immer wieder an die unangemessene Perspektive durch die Sprache erinnert wie sie ref. Sand talk (us-two us-all) einbringt
- RD: "wir sind denn ganz dezentrale eine ganz dezentralisiert organisation das ist ein großer vorteil"
- leider wird im Interview nicht über Entscheidungsprozesse (dGOV) oder Konfliktlösungen(Gefängnisse mal ausen vor) gesprochen

# "Political correctness" Issues have reached germany

- https://www.youtube.com/watch?v=_R0ywINFaOo
