[[_TOC_]]

#  worrydream - Closing panel 1995 - A conversation with Doug Engelbart, Alan Kay, Ted Nelson, and Tim Berners-Lee

- #tags: #bret_victor #website
- ref: http://worrydream.com/refs/Vannevar%20Bush%20Symposium%20-%20Closing%20Panel.html
- via: https://twitter.com/worrydream/status/447923062227623938
- extended refs: http://vimeo.com/81336768 NLS in Action , see mother of demos
- video: https://archive.org/details/XD1941_10_95VannevarBushSymTape11_2ndDayPanelDis
- Why did DEs Idea fail: Information SWAT teams? relateing to 
- TED http://www.ted.com/talks/bill_and_melinda_gates_why_giving_away_our_wealth_has_been_the_most_satisfying_thing_we_ve_done
- 2 important pooints to make progress:
- “use statistical data to see deficites, but you have to make the people use the vaccine”
- <= the same actually applies to DEs problem, his invention is a solution but people need to apply it, so you have to make sure they take the pill, otherwise the bootstrapping process cant start

# Page

- #tags: #homepage #bret_victor
- http://worrydream.com/
- Things i’m Thinking about….
- kill math
- http://worrydream.com/#!/Links
- Books like SICP, Structure of Scientific Revolutions
- Thoughts on Douglas Engelbart

# How to read and how to think into things

- #tags: #website #bret_victor
- http://worrydream.com/#!/Links2013
- How to gain perspective, see readin tip #1
- ref all mentions
- David hestens: Notes for a Modeling Theory of Science, Cognition and Instruction
- Alan Kay "Several “big idea” thinkers have shown up on this list, and I love them all, but Alan Kay's idea is bigger than any of them. It's almost too big an idea to see, and many people don't see it."

# Changing Minds

- #tags: #twitter #website #bret_victor
- https://twitter.com/worrydream/status/505119100171849728?refsrc=email
- Iconic example from diSessa of how representations -- the marks we see and manipulate -- determine what we can think.
- A cognitive View of Material Intelligence 
- http://worrydream.com/oatmeal/changing-minds.jpg
- explaning and maybe experiencing sapir whorf

# CSS

- #tags: #website #bret_victor #paul_chiusano
- http://pchiusano.github.io/2014-07-02/css-is-unnecessary.html
- on CSS: http://worrydream.com/MagicInk/#p255

# Ivan Sutherland

- #tags: #pdf #bret_victor #ivan_sutherland
- The Tyranny of the Clock
- http://worrydream.com/refs/Sutherland%20-%20Tyranny%20of%20the%20Clock.pdf

# podcast Dynamicland

- #tags: #podcast #bret_victor #dynamicland
- https://postlight.com/trackchanges/podcast/computing-is-everywhere
- what Dynamic Land is about ==> from make a product to build a Community Space .. create a culture 

# Inventing on principle (der erste ? den ich kenne) deswegen wohl etwas länger

- #tags: #vimeo #bret_victor #perspective #work #design
- http://vimeo.com/36579366
- fight/work for your belief, example: against modes 

# Future of Programming

- #tags: #vimeo #bret_victor #programming
- http://vimeo.com/71278954
- pseudo retro
- what models for parallel/concurrent programming work?
- "Can computing be liberatred from Von neuman architecture"?
 

# Media for the unthinkable

- #tags: #vimeo #how_to_think_an_impossible_thought #bret_victor
- http://vimeo.com/67076984
- see notes "how to think an impossible thought"
  
# Drawing Dynamic Visualisations

- #tags: #vimeo #visualisations #bret_victor
- http://vimeo.com/66085662

  
# Stop Drawing Dead Fish

- #tags: #vimeo #animation #bret_victor
- http://vimeo.com/64895205
- stories as a media
- the importance of animations, transitions in human perception

# Interactive Exploration of a Dynamical System

- #tags: #vimeo #bret_victor
- http://vimeo.com/23839605

  
# Seeing Spaces

- #tags: #vimeo #bret_victor
- http://vimeo.com/97903574
- das hier ist das Letzte Video(stand 11092014)
- und lässt alles etwas zusammenlaufen : media for the unthinkable, wie man dynamische software in einer art neuen “Labor” direct anwenden kann
- inspiriert maker spaces und seine Visualisierungs Ideen zu kombinieren um dynamische Interaktivität in eine "reale Werktstatt" zu bringen

# The Humane Representation of Thought

- #tags: #vimeo #bret_victor 
- http://vimeo.com/115154289
- erklärt die motivation hinter den bisherigen Videos und wofür man die Seeing Spaces verwenden könnte, "Aufmenting human intellect"
- http://worrydream.com/TheHumaneRepresentationOfThought/note.html
- after discussing this with adrian he mentions the similarity with the "holo deck from star trek", But if this will come mainstream , usage patterns emerge, like  it took until deep space nine, when these became a bar/renting like thing
- rewatch and take notes

# Seymour Papert und CodeDotOrg

- #tags: #article #seymour_papert #bret_victor #website
- http://worrydream.com/MeanwhileAtCodeOrg/

# History of Computing/Silicon Valley

- #tags: #books #bret_victor #twitter
- https://twitter.com/worrydream/status/556607120880050176
- #books: 
  - Engines of Logic - Martin Davis
  - The Idea Factory - John Gertner
  - Revolution in Miniature - Braun & macdonald
  - From Counterculture to Cyberculture - Fred Turner
  - What the Dormouse said - John markoff
  - Where Wizards stay up late - katie hafner matthew lyon
  - The Closed World - edwards
  - Dealer of Lighting - Michael A. Hiltzik
  - The man who invented the computer - Jane Smiley
  - The information / a History, a Theory, a Flood - james Gleick
  - Insanely Great - Steven levy
  - The dream machine - M. Mitchell waldrop
  - the soul of a new maschine - tracy kidder
  - revolutions in the valley/the insanely great story of how the mac was made
  - a few good men from univac - david E. Lundstrom
  - Geeks Bearing Gifts - Ted Nelson
  - Turing's Cathedral - George Dyson
  - Possiplex - Ted Nelson

# The web of alexandria

- #tags: #article #website #web #bret_victor #information #resilience
- http://worrydream.com/TheWebOfAlexandria/ IPFS? Maidsafe? web 3.0? , what about facebook and google, what if their "products" became open protocol informations
- which explains why the big siren server always control the w3c (andother) standards so that those "standard" - forcing protocols never can interfer with their business /product models
  
# More Books

- #tags: #books #bret_victor
- http://en.wikipedia.org/wiki/The_Educated_Mind
- mentioned in some of his talks, also: https://twitter.com/worrydream/status/472451306620080128
- http://en.wikipedia.org/wiki/Technics_and_Civilization
- see booklist/shelf in /books/List.md

# Why

- #tags: #article #bret_victor #technology #civilisation #website
- http://worrydream.com/why-2017-05-08/
- So good...
- loper-os
- STEPS
- #book Neil Postman - Technopoly: The Surrender of Culture to Technology recommend: https://www.goodreads.com/book/show/79678.Technopoly#:~:text=In%20this%20witty%2C%20often%20terrifying,of%20politics%2C%20art%2C%20education%2C
- from Tools to Technocracy , From Technocracy to Technopoly
- "I think what I like the most about Neil Postman's Technopoly is how he never denies the advances science has made, but points to the very existence and rise of technology and science and asks the extremely significant question, "What have we lost?" "

# Dynamicland

- #tags: #dynamicland #bret_victor
- finally everything coming together, but he still has funding problems!
- https://twitter.com/Dynamicland1
- See Hackernews comments for interesting feedback :
- 04/2019 https://news.ycombinator.com/item?id=19528468
- 2017 https://news.ycombinator.com/item?id=15962730

# Article on the History of Dynamicland

- #tags: #dynamicland #article #meaning_crisis #society_industry #alan_kay #bret_victor
- connecting dynamicland to alan kay (via CDG) : https://tashian.com/articles/dynamicland/
- "As a result, most professional programmers today spend their days editing text files inside an 80-column-wide command line interface first designed in the mid-1960s."
- They could follow intuitive hunches that were unlikely to lead to anything. They could spend long, quiet days reading and doing uninterrupted deep work in a direction of their choosing"

- http://tobyschachman.com/Shadershop/ harmonics
- us civil rights exercise with notes http://glench.com/EyesOnThePrize/
- the meaning crisis reached Dynamicland most interesting! http://glench.com/WhyIQuitTechAndBecameATherapist/
- In a talk he gave last year at Harvard, Victor laid out three major design principles from the research that were incorporated into Realtalk:
-   The medium should be communal and accessible. People should learn and collaborate through awareness, with no assumption of a single, isolated user sitting at a laptop with a keyboard and mouse.
-   The medium should allow people to think with their bodies, because we are more than fingers and hands.
-   _The medium should expand people’s agency and liberate their creativity; rather than being an app with a limited set of features defined by a corporation and imposed on people._
-    bret takes a sabatical from dynamic land he doesn't want that its tech gets outsold like with the GUI at Xerox Parc

# Future of Coding related talk

- #tags: #communal_computer #agency #soverignity
- https://marianoguerra.github.io/presentations/2020-berlin-bobkonf-programming-by-any-other-name/#101
- Dynamicland is a communal computer
- Designed for agency, not apps
- Where people can think like whole humans
- The computer of the future is not a product, but a place

# Tinyland, a very, very small Dynamicland

- #tags: #dynamicland #tinkering
- via: https://news.ycombinator.com/item?id=21555740
- https://emmasmith.me/tinyland/
- the Recurse Center
- Computing without keyboard, but communal is even more important!
- talked with Saad about it  see github AR.js project

# stories and games

- #tags: #bret_victor #twiter #games #alan_kay
- https://twitter.com/worrydream/status/1116773641088270336?s=12
- ref alan kay squeak land - minecraft?
- http://worrydream.com/AlanKaySqueaklandPosts/
- from April 2019

# RESEARCH AGENDA AND FORMER FLOOR PLAN Communications - Bret Victor March 10, 2014

- #tags: #bret_victor #twiter #andrek
- http://worrydream.com/cdg/ResearchAgenda-v0.19-poster.pdf
- andre k hatte einen tweet über useful kinds of communication geteilt ich musste da an bret victors einteilung denken
- Modes of Communication of Representations
- https://twitter.com/kischiman/status/1548970871737356288
- https://twitter.com/curious_reader/status/1549045766290780160


# Shawn Douglas - Nanoscale Instruments for Visualizing Small Proteins & Bret Victor - Dynamicland

- #tags: #dynamicland #bret_victor
- https://www.youtube.com/watch?v=_gXiVOmaVSo
- first application in a bio ( molecule?) lab of dynamic land
