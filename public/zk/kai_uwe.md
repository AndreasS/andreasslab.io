# Sehenswertes zu Jordan Peterson aus meinem zk:

- The Tragic Story of the Man-Child- Peter Pan - https://www.youtube.com/watch?v=JjfClL6nogo #tags: #jordan_peterson #youtube #peter_pan #carl_jung #developement #agent_arena
- zum Thema Dune : https://www.reddit.com/r/dune/comments/a20qzd/i_swear_jordan_peterson_believes_in_the/ #tags: #jordan_peterson #dune
- A Zen Master talks about Jordan Peterson & the Shadow - JP : as a projective figure between surfaced conflicts https://www.youtube.com/watch?v=ABWeA203Q-c #tags: #zen #jordan_peterson #youtube #rebel_wisdom 
- Vorlesung - Maps of meaning https://www.youtube.com/watch?v=I8Xc2_FtpHI&list=PL22J3VaeABQAT-0aSPq-OKOpQlHyR4k5h (zk austausch später gerne wenn du magst) 
- Vorlesung - Personality and its Transformations (Carl Jung- Lion King) https://www.youtube.com/watch?v=kYYJlNbV1OM&list=PL22J3VaeABQApSdW8X71Ihe34eKN6XhCi
- A good critique of JP via Contrapoints: https://www.youtube.com/watch?v=4LqZdkkBDas #tags: #youtube #jordan_peterson #critique
- John Vervaeke and JBP https://www.youtube.com/watch?v=DLg2Q0daphE #tags: #jordan_peterson #john_vervaeke #philosophy #psychology #meaning #transcription #knowledge #god
- Jonathan Pageau and JBP https://www.youtube.com/watch?v=2rAqVmZwqZM


# Sehenswertes zu John Vervaeke:

- JV and JBP Podcast :)
- Four kinds of knowing: John Vervaeke and a more grounded way to grow https://www.youtube.com/watch?v=Gyx5tyFttfA #tags: #john_vervaeke #knowing #youtube #knowledge
- John Vervaeke How agape transforms the world https://www.youtube.com/watch?v=KD_lt5kmXBQ 4 min,  full lecture episode (60min) is here: https://www.youtube.com/watch?v=Jbwm03djuJc Christianity and agape
- The Monastic Academy w/ Seishin - Voices with Vervaeke https://www.youtube.com/watch?v=ZjEAgCMQGTY&feature=emb_logo #tags: #monastic_academy #john_vervaeke #wisdom #knowledge #eightfold_path
- Vorlesung - Awakening from the meaning crisis https://www.youtube.com/watch?v=54l8_ewcOlY&list=PLND1JCRq8Vuh3f0P5qjrSdb5eC1ZfZwWJ  #john_vervaeke #meaning_crisis #erich_fromm #aristotle #plato #socrates #flow #youtube #aftmc #lecture #agent_arena #agape #buddha #sati #ecology_of_pracices

# Evolve - Lokal in Frankfurt

- https://www.evolve-magazin.de/
- Radio evolve 501 - The Symbolic World is Real (with Jonathan Pageau) / evolve magazine https://www.youtube.com/watch?v=t5acqiUMZHM #tags: #jonathan_pageau #evolve_magazine #podcast #youtube #attention #icon #symbol #cognitive_science #science
