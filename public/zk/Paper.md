# Der mediatisierte Lebenswandel - Permanently online, permanently connected (POPC) Peter Vorderer

- #tags: #peter_vorderer #article
- http://dx.doi.org/10.1007/s11616-015-0239-3 von 2015 Peter Vorderer Uni Mannheim
- Technische Innovation und der kommunikative Alltag permanenter Vernetzung
- POPC verändert den Prozess von Problemlösungen und das Leistungshandeln
    - Wissenszugang ersetzt Wissen
    - Vertrauen in die Gruppe ersetzt Selbstwirksamkeit
    - Crowd-Befragung ersetzt Kreativität
    - Big Data ersetzt Intuition
    - Die Erwartung effizienten Gelingens ersetzt die Freude am Gelingen 
- POPC verändert das Beziehungshandeln
    - Erreichbarkeit ersetzt räumliche Nähe
    - Latente Konversationsfäden ersetzen Gespräche
    - Persönliche Netzwerke ersetzen Freunde
    - Reversibilität ersetzt Verbindlichkeit
    - Soziale Kontrolle ersetzt Vertrauen
    - Aufmerksamkeit ersetzt Wertschätzung: "Wenn ein ganzer Freundes- und Bekanntenkreis das POPC-Modell lebt, werden alle permanent mit neuen sozialen Informationen überflutet. Unter diesen Umständen etabliert sich möglicherweise eine neue
Währung sozialer Anerkennung: Aufmerksamkeit – die Nutzerin und der Nutzer
dürfen sich freuen, wenn ihnen ihre Online-Kontakte überhaupt Aufmerksamkeit für
ihre Botschaften (Posts, Bilder usw.) schenken"
    - Dabeisein ersetzt Nacherzählungen
- POPC verändert Motivationen und Bedürfnisse
- "Die Lebensführung des POPC greift nicht nur in das Verhältnis des Subjekts zu seiner beruflichen und privaten sozialen Umwelt ein, sondern dürfte auch das innere
‚Mindset‘ umwälzen, konkret also erhebliche Folgen für die Bedürfnis- und Motivationslage mit sich bringen."
- "Am auffälligsten ist für Außenstehende vermutlich der
veränderte Umgang mit Situationen des Wartens und der Langeweile, in denen das
Smartphone offenbar verlässlich seinen Dienst als **Zeitfüller, Entertainer und Stimmungsregulator versieht.**"
    - Netzzugang als Voraussetzung situativen Wohlbefindens
    - Kurzweil ersetzt Langeweile (Muse?)
    - "Lost in surfing" ersetzt Tagträumen
    - Relativität ersetzt Sensation
    - **Zu viel Information verdrängt Neugier**
    - Alleinsein wird zum raren Gut
    - Flatrate-Denken ersetzt geplante Auswahl
- POPC verändert Selbstprozesse
- Performance ersetzt Authentizität: "Die Möglichkeiten und (gefühlten) Notwendigkeiten zur strategischen Selbstpräsentation... Es müssen fast schon perfekte Fotos, präzise
selbstironische Pointen sein, mit denen man online auftritt."
- Inszenierung ersetzt Spontanität
- (Selbst-)Transparenz ersetzt Geheimnisse
- Das Selbst qua Verbindungen ersetzt das Selbst qua Unterscheidung
- Sich einer Meinung anschließen statt eine Meinung zu formulieren
