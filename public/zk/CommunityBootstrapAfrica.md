[[_TOC_]]

# Bootstrapping Ideas and Communities, for Africa and all other interested People

 1. Finding Heros
 2. Preparing yourself
 3. Forging Alliences
---
 ## 1. Finding Heros
 
 - Who are your Heros? Why?
 - What is a forum for People (Heros)?
 ---
 ## 2. Preparing yourself
 
 - Hardware: Rasberry PI or One Laptop per Child
 - Internet
 - Wikipedia Zero
 - MOOCs
 - Digital Aristotle
 ---
 ## 3. Forging Alliences
 
 - Communites are Powerful
 - How to build Communities
 - Crowd Funding
 - Finding Consensus
---
## 4. Access

 - SpaceX internet for every school in Africa?
 - who could help organize? Sugata Mitra?
 - build a blockchain DAO yo make it real

# eyesonme