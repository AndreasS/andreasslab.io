# Unsere Welt Neu denken - Eine Einladung - Maja Göpel

- #tags: #maja_göpel #book #sustainability #change #narrative #frame #context
- Eine Einladung
- Eine neue Realität
- Natur und Leben
- Mensch und Verhalten
- Wachstum und Entwicklung
- Technologischer Fortschritt (Warum nicht Fortschritt und Bedürfnisse? :D)
- Konsum
- Markt, Staat und Gemeingut
- Gerechtigkeit
- Denken und Handeln
- Wer weitermachen will

# Unsere Welt Neu Denken , einige Notizen - Natur und Leben

- #tags: #maja_göpel #book #sustainability #change #narrative #notes #nature #relationship #civilization
- Natur und Leben
- S. 40 "Wie ein Kind, das sein Spielzeug auseinandernimmt, nahm der Mensch nun Stück für Stück die Natur auseinander und begann mit ihren Einzelteilen zu spielen. Er fand heraus, welche Aufgabe sie hatten. Er veränderte sie, tauschte sie gegeneinander aus oder setzte sie neu zusammen, in der **Überzeugung**, dass die Welt für ihn damit besser funktioniere als vorher. **Aus der Natur, deren Teil der Mensch gerade noch gewesen war, wurde nun die _Um-Welt_, von der er sich abgetrennt hatte und die ihn ab jetzt nur noch umgibt.**
- S. 53 "In unserem Verhältnis zur Natur zeigt sich die ganze Anmaßung menshclichen Wirtschaftens. Indem der Mensch die Natürlichen Systeme seinem Bedarf unterwirft, reduziert er ihre Vielfalt, macht sie verletzlicher und braucht einen immer größeren Aufwand, um sie zu stabilisieren.

# Unsere Welt Neu Denken , einige Notizen - Mensch und Verhalten

- #tags: #maja_göpel #book #sustainability #change #narrative #notes #adam_smith #david_ricardo #richard_easterlin #buddhism
- Mensch und Verhalten
- S. 58 "Ich erinnere mich, wie in einer dieser Vorlesungen ein Professor erklärte, dass Arbeiter immer dort hinreisen werden, wo sie den höchsten Lohn bekommen, auch wenn das bedeute, dass sie in ein anderen Land ziehen müssen. Als ich mich meldete und fragte, ab wieviel Armut vor Ort und Lohnunterschied Menschen denn ihre Familie verlassen würden und wie es sein kann, dass für einen solchen Aufwand aufseiten der Arbeiter keinerlei Kosten in dem Modell auflaufen würden, wurde es plötzlich still im Hörsaal. Der Professor sah seinen Assistenten an, und die Studenten starrten mich an. Schließlich ertönte: Sehr her, da spricht ja ein warmes Herz! Eine Beantwortung meiner Frage blieb. aus. **Seitdem beshcäftigte es mich, wieso die Wirtschaftswissenschaften sich gern eines kalten Herzens rühmen und was daran gut sein soll**
- Wirtschaftswissenschaften und die drei Männer aus England vor 200 Jahren: Adam Smithm David Ricardo
- Adam Smith: Wealth of Nations
- David Ricardo : Arbeitsteilung und Austausch auf Staaten Ebene
- Charles Darwin: Dekontextualisierung z.b. durch Herbert Spencer - "Auf der Beziehungsebene wurde Wirtschaft nun ein Kampf von jedem gegen jeden. Ein Kampf, bei dem nur die Stärksten überlebten."
- homo oeconomicus
- in den 70ern Richard Easterlin - Easterlin Paradox **"Verbessert Wirtschaftliches Wachstum das menschliche Los?"** Ab einem gewissen einkommen erhört sich die Zufriedenheit nicht mehr
- Adam Smith zweites großes Werk: "Die Theorie der ethischen Gefühle"
- S. 65 "Allen drei Vordenkern ist also gemein, dass Nachfolger ihre zentralen Ideen aus dem Kontext genommen und sie zu vermeintlich universellen Gesetzmäßigkeiten _der_ Ökonomie hochstilisiert haben.
- "Warum ist das wichtig?... **Wirtschaftswissenschaften schaffen das Bewertungsystem dafür, ob etwas wirtschaftlich ist oder nicht. Sie schreiben die Definition von Fortschritt**"
- S. 70 Im Buddhismus hingegen gilt Arbeit als etwas, das die Menschen darin unterstützt, ihr eFähigkeiten zu entwickeln. Sie verbindet sie miteinander und verhindert, dass sie sichin Selbstbezogenheit verlieren (Sinnstiftung). Sie stellt zudem Waren udn Dienstleistungen her, die für ein menschenwürdiges Dasein nätig und wünschenswert sind. Das Ideal einersolchen Welt wäre damit nicht Produktionssteigerung zu möglichst billigen Preisen, sondern eine sogenannte Tätigkeitsgesellschaft zur Sicherung von allgemeinen Wohlergehen. 
- #book Ernst Friedrich Schumacher - Small is Beautiful - Rückkehr zum menschlichem Maß


# Unsere Welt Neu Denken , einige Notizen - Wachstum und Entwicklung

- #tags: #maja_göpel #book #sustainability #change #narrative #frame #notes #attention #growth #metrics #value #steven_pinker #bill_gates
- Wachstum und Entwicklung
- S. 75 "Als Nachhaltigkeitswissenschaftlerin hatte ich das auch schon mal empfohlen, weil das Thema damit eine noch größere Wichtigkeit erhählt und in unserer täglichen **Aufmerksamkeitsarchitektur** einen Platz bei den relevanten Informationen ergattert."
- S. 76 Anders gesagt: Schrumpft die Wirtschaft, verlangsamt sich der Klimawandel. Wächst die Wirtschaft, beschleunigt er sich. **Oder noch einfacher ausgedrückt: Wirtschaftswachstum in seiner heutigen Form heißt Klimawandel**
- S. 79 "..Seitdem ist das BIP die Kennziffer geworden, um Wachstum und damit Wohlstand zu messen. Aus einem Konzept wird eine Zahl, aus einer Zahl folgen Entscheidungen, wird Politik, richtet sich eine Gesellschaft aus. **Wieviel _Wertverlust_ und _Schadschöpfung_ sich hinter der Zahl verbirgt, bleibt verborgen.**
- S. 80 "Aber erinnern sie Sie sich an das Easterlin-Paradox? Die Gleichung entkoppelt sich irgendwann, und jeder Euro und jedes Stück Besitz nehr hat dann nicht mehr den gleichen Mehrwert für Menschen wie die Euro und Besitzstücke bis zu diesem Sättigungsgrad. Genau das ist dem auf Wachstum getrimmten Wirtschaftssystem aber ziehmlich egal. **Die Frage, ob jemals ein _Genug erreicht sein könnte, ist darin nicht vorgesehen.**"
- Steven Pinker und Bill gates in Davos: But there is progress!
- 2011 festgelegter Standard von 1,90 Dollar pro Tag : in den Usa für Gesunde Ernährung, Behausung und Gesundheitsvorge?
- nimmt man stattdessen 7,40 Dollar leben 2019 4,2 Milliarden Menschen unter der Armutsgrenze, das sind mehr als 1981" Quelle im Buch:
- https://www.theguardian.com/commentisfree/2019/jan/29/bill-gates-davos-global-poverty-infographic-neoliberal
- https://twitter.com/BillGates/status/1086662632587907072
- "What Roser’s numbers actually reveal is that the world went from a situation where most of humanity had no need of money at all to one where today most of humanity struggles to survive on extremely small amounts of money."
- S. 87 "Das weltweite Bruttoinlandsprodukt ist in der gleichen Zeit von 28,4 Billionen Dollar auf 82,6 Billionen Dollar gestiegen. Aber von jedem Dollar mehr sind nur fünf Prozent bei den unteren sechzig Prozent der Weltbevölkerung angekommen. Und wissen Sie, wo di emeistne Personen leben, deren Lebenstandard sich seit 1981 über diese Armutsgrenze hinaus entwickelt hat? In china. Nehmen wir diese Anzahl Personen aus der Statistik heraus, sieht die marktradikale Variante des Wachstumsmodells nur noch wenig nach Trickle down aus.
- In Europa ist diese Entwicklung im Vergleich zum Rest der Welt weniger drastisch, aber auch in Deutschland steigen alle Indikatoren für Ungleichheit an.
- S. 89 Die Geschichte vom ewigen Wachstum des Konsums für alle ist nicht aufgegangen, weder ökologisch noch sozial. Schritt für Schritt ist hinter atemberaubenden Zahlen ein System entstanden , **das unseren Planeten zertört**, Eigentumsverhältnisse wieder denen im Feudalismus angleicht und das trotzdem immer weiterwachsen muss, um unter seinen Unwuchten nicht zusammenzubrechen.
- #book Mariana Mazzucato : Wie kommt der Wert in die Welt?
- S. 91 "Bis zum 19. Jahrhundert, also auch noch bei Adam Smith und David Ricardo, gab es stets so etwas wie eine objektive Basis für die Ermittlung von Wertschöpfung"
- S. 92 "Smith fand übrigens - entgegen den Bestrebungen der Financiers -, das die Vergüstung dafür eher gering gehalten werden sollte.
- "Diese Unterscheidung zwischen Wert und Preis ging dem Utilitarismus und der Mathematesierung der Ökonomie verloren: ein nutzenmaximierender _homo oeconomicus_ gibt nur so viel Geld für etwas her, wie es ihm an zusätzlichem Wert einbringt.
- "Der Wert von Dingen wird also durch ihren Preis auf dem Markt bestimmt und hat nichts mehr mit ihren Inhalten oder Qualität zu tun **Der Preis _ist_ der Wert. Subjektive Präferenzen (der Käufer) schlagen objektive Ressourcen, Tauschwert entkoppelt sich vom Nutzwert.** >== hier noch viel mehr Bedürfniswe, enkopplung , Entfremdung, quantifizierung
- **Auf diese Weise wurde Wertschöpfung durch reine Verabredung möglich**
- Recherchieren sie mal Preisprünge für Medikamente nach Unternehmensfusionen. Sie werden erstaunt sein, wie einige neue Eigentümer den Wert der übernommenen Angebote einstufen im Vergleich zum Vorgänger.
- S. 94 "In die Berechnung des BIP wurden die Aktivitäten des Finanzsektors seit den Siebzigerjahren aufgenommen, parallel zu den Deregulierungsmaßnahmen, die diesen Sektor unter geringere Kontrolle stellten. Der Aufstieg ist beindruckend. Aus einem unproduktiven Verschieben von Resourcen im Dienste der Realwirtschaft ist mit der Zeit ein hochlukratives neues Geschäftsmodell geworden. Machen sie soch nochmal klar, wie es funktioniert: **Es beeinflusst durch Renditeerwartungen, welche Produktionsprozesse, Vergütungsregeln und Technologienb sich in der Realwirtschaft durchsetzen.**"
- S. 95 Begriffe Und Wertvorstellungen zum Nachdenken:
- Vom Produkt zum Prozess
- Vom Förderband zum Kreislauf
- vom Einzelteil zum System
- vom Extrahieren zum Regenieren
- vom Wettkampf zur Zusammenarbeit
- von Unwucht zu Balance
- Vom Geld zum Wert

# Unsere Welt Neu Denken , einige Notizen - Technologischer Fortschritt

- #tags: #maja_göpel #book #sustainability #change #narrative #frame #note #value #meaning #purpose #attention #silicon_valley #progress
- Technologischer Fortschritt
- S. 108 "Die Vorstellung von Effizienz in den Wirtschaftswissenschaften besteht, salopp gesagt, darin, dass wir immer zwei zum Preis von einem bekommen. **Und wir haben gesehen, dass Vorstellungen die Eigenschaft besitzen , sich handfest auf die Realität auszuwirken (Absolut richtig, siehe maps of meaning, psyhology cognitive science ...) 
- #book S. 112 Phlipipp Staab : Falsche Versprechen
"Beim Kauf geht es in den Gesellschaften materiellen Überflusses selten nur um den Gebrauchswert von Produkten, sondern meist auch um deren **Distinktionspotential**, das heißt um die Möglichkeit, sich durch den Besitz knapper oder sozial spezifisch konotierter Produkte symbolisch von anderen abzusetzen. 
- Cybertruck von Tesla/Elon musk, symbolisch eher mad max entsprungen: mir ist egal was sich mir in den WEg stellt, ab morgen fahre ich hübsch gepanzert mit dem Vollgas der Sonne darüber
- "Da sind mir die Trends in Richtung Achtsamkeit, Yoga, Wandern, Waldbaden,  Digital Detox, Selbstverwirklichung und Zeitwohlstand doch lieber."
- John Maynard Keynes "Wirtschaftliche Möglichkeiten für unsre Enkelkinder" was wir machen wenn wir "genug" haben ( siehe auch Marx das maschinen Fragment)
- S. 114 "Was machen wir mit der ganzen frrei gewordenen Zeit? .. Klar , wir genießen das Leben"
- Georg Frank "Die ökonomie der Aufmerksamkeit" "Die Aufmerksamkeit anderer Menschen ist die unwiderstehlichste aller Drogen"
- S. 115 "Diese Form des technischen Fortschritts, so die Ansicht einiger Austeiger aus dem Silicon Valley, hat dazu geführt, dass wir heute die Veränderung menschlicher Aufmerksamkeit  und des Lernverhaltens, der sozialen Beziehungen und der Gesprächkultur diskutieren."
- S. 117 "Um in der neuen Realität gut zusammenleben zu können , müssen wir auch unsere Vorstellungen von Fortschritt ändern, sonst verschieben  wir die PRobleme einfach weiter in die Zukunft ( eine ernsthaft Auseinandersetzung mit unseren Bedürfnissen auf Kultureller ebene?)

# Unsere Welt Neu Denken , einige Notizen - Konsum

- #tags: #maja_göpel #book #sustainability #change #narrative #notes #attention #metrics
- Konsum
- "Zu viele Leute geben Geld aus, das sie nicht haben, um Dinge zu kaufen, die sie nicht brauchen, um damit LEute zu beeindrucken, die sie nicht mögen." Robert Quillen Humorist
- #book S. 122  Soziologe Stephan Lessenich : Neben uns die Sinnflut
- "Der Soziologe Stephan Lessenich, hat in seinem Buch : Neben uns die Sinnflut, erklärt wie der Wohlstand der westlichen Welt zu weiten Teilen darauf beruht, das wir seine wahren Kosten nicht selbst tragen, sondern anderen aufgehalst haben.
- Beispiel Bio Spricht, Raps Anbau flächen die nicht skalieren , importe die den Regenwald(palm öl) zerstören
- S. 125 Müll als Geschäftsmodell, Abfallwirtschaft Exportbranche  , "Laut einer Untersuchung der Universität Würzburg-Schweinfurt hat Deutschland im Jahr 2018 (in Tonnen gerechnet) mehr Müll ins Ausland geführt als Maschinen.
- S. 129 CO2 Bepreisung "Sie soll nicht nur ihre Entscheidung als Konsument beeinflussen, sondern auch auch genau den Innovationen Kostenvorteile verschaffen, die helfen, CO2-freie Produkte zu entwickeln. .. **Sie würde ökologische Schadschöpfung in der Preisgestaltung sichtbar machen - womit wir einer objektiveren Fassung von Wertschöpfung wieder näher kämen.**"
- #book Hartmut Rosa - Unverfügbarkeit
- S. 130 "Unsere moderne Gesellschaft ist so eingerichtet, das ihre Gegenwart stets versucht ist, ihre Vergangenheit zu übertrumpfen. Es herrscht ein stetet **Steigerungszwang** (Bernds Mühle/Hamsterrad), nicht nur im technologischen und wirtschaftlichen Bereich, auch im sozialen und sogar im räumlichen. **Jede Mode, jeder Job, jede Freude, jeder Urlaub könnte morgen schon wieder von gestern sein.** Und die **Aufmerksamkeitsökonomie** der ständigen Werbebotschaften, NAchrichten, Selbstdarstellungen und Informationswellen trägt verlässlich dazu bei, das dieses Verfallsdatum sich immer schneller nähert.
- Barry Swartz - Paradox of Choice - Die Freude an Entscheidungen vermehrt sich eben nicht automatisch mit einer Vergrößerung der Auswahl.
- S. 131 #book Psyhcologe Tim Kasser :  the high price of materialism
- "Er hat sich gefragt, wie sich unsere materialistische Orientierung auf unser Wohlbefinden und unser Selbstwertgefühl auswirkt, und herausgefunden, dass Materialismus sowohl Ausdruck als auch Ursache von Unsicherheit und Unzufriedenheit ist. Das ist so weil er(Materialismus) primär die **_extrinsiche_** also von außen kommende Motivation und Rückbestätigung von Menschen anspricht. Der PReis der Dinge oder Ausmaß der Aufmerksamkeit ( Ruhm, Likes, Clicks), die ich bekomme, spiegeln dann meinen Eigenwert wider.
- Materieller Verlust => Selbstwert in Gefahr

# Unsere Welt Neu Denken , einige Notizen - Markt, Staat und Gemeingut

- #tags: #maja_göpel #book #sustainability #change #narrative #notes #cooperation
- Markt, Staat und Gemeingut
- S. 140 "Wir haben es im Kapitel: Mensch und Verhalten - schon gesehen: Wettbewerbsfähigkeit, ursprünglich eine Idee, um Unternehmen miteinander zu vergleichen, ist heute zu einer Kategorie für Ländergeworden."
- "Inzwischen gibt es Anwaltsfirmen, die sich darauf spezialisiert haben, Staaten im Namen von Unternehmen für ihre Umwelt- oder Sozialpolitik zu verklagen, wenn sich die Gewinne nicht einstellen, die ihren Investitionsentscheidungen zugrunde lagen."
- S. 142 #book Mariana Mazzucato - The entrepreneurial State (Vergleich mit Glen Weyl - Radical Markets?)
- Betrachtungen über die Beziehung von Risiko und Invesitionen, der Staat spielte hier häufig(immer?) eine entscheidene Rolle (ähnlich chomskys quellen Aussagen über die Innovationskraft/Risiko bereischaft von STaat vs. Unternehmen)
- S. 144 "Das ist das Vertrackte an **der Tyrannei der kleinen Entscheidungen**: das sie keine übergeordnete Instanz kennt, die von einer höheren Perspektive aus überprüft, ob die Summe der Einzelinteressen tatsächlich für alle einen Nutzen herbeiführt." das wirkt für mich wie umkehert , tysons regel zur : value in the marignalised stories
- Gemeinwohlsicherung originäre Aufgabe des Staates ( wer ist der Staat? Institutionen? Interessen?)
- #book Thomas Beschorner : In schwindelerregender Gesellschaft
- "Er bennent sie als halbierten Liberalismus, in dem **Staat und Markt nicht mehr angemessen ihre komplementären Rollen ausüben**."
- S. 149 "Sie sollte auch Anreize setzen, eigennutzorientiertes Verhalten im Zaum zu halten und moralisches HAndeln zu fördern. Staat und Markt sind nicht trennbar."
- S. 151 ( kleine rmann was nun) in den letzten 50 Jahren Lebenmittel anteil von 25% auf 14 gesunken 
- Wohnausgaben seit 1993 von 27%  auf 39 gestiegen für die unteren 20% der Bevölkerung, für die reichsten 20% der Bevölkerung 9% weniger kosten für Wohnen
- S. 154 Garret Hardin - Tragedy of the commons

# Unsere Welt Neu Denken , einige Notizen - Gerechtigkeit

- #tags: #maja_göpel #book #sustainability #change #narrative #notes #dan_ariely
- Gerechtigkeit
- S. 157 "Insbesondere jüngere Menschen können Vielfliegeridentitäten als eine von Promineten geprägte soziale Norm ansehen"
- S. 163 "Wie will man die ökologische Frage lösen, wenn man sie nicht auch als soziale Frage versteht?"
- S. 165 Dan Ariely Verteilungsgerechtigkeit (siehe sein TED talk)
- S. 166 World Inequality Lab, seit 1980 zugenommen
- **"Mann kann es drehen und wenden, wie man will: von dem, was das Wirtschaftswachstum seit der Globalisierung an Vermögen geschaffen hat, sit bei vielen Armen etwas, bei sehr wenigen Reichen unfassbar viel und bei der großen Mittelschicht kaum bis gar nichts angekommen.**
- S. 171 "Gerechtigkeit heißt nicht allein Verteilungsgerechtigkeit, sie heißt auch Chancengerechtigkeit. 
- S. 174 **"Wie kommen wir aus dem Wettlauf Richtung Zerstörung der Welt raus?"**
- "Für mich lieft die Formel darin, dass wir aus der Zukunft denken. Und systemisch. ( warum nicht in der Gegenwart und systemisch?)

# Unsere Welt Neu Denken , einige Notizen - Denken und Handeln

- #tags: #maja_göpel #book #sustainability #change #narrative #notes
- Denken und Handeln
- S. 185 "Im Kapitel über die neue Realität, in dem ein Astronaut vom Weltall aus das erste Foto von unserem Planeten  schoss, habe ich gezeigt, wie wichtig es ist, **welches Bild wir Menschen uns von einer Sache machen.**"
- "Es bestimmt darüber, wie wir uns dieser Sache nähern, sie behandeln, in welche Beziehung wir uns zu ihr setzen."
- "In unseren Bildern davon, wie wir die Erde sehen, ihre Natur, wie wir Menschen sind oder nicht sind, wozu Fortschritt dient, wofür man Technik einsetzt und was einem gerecht erscheint, liegt die Deutungshoheit darüber, was in der Welt möglich ist und was nicht."
- **"Meine Einladung war es, einige dieser Bilder zu hinterfragen."**
- S. 188 "Wenn es etwas gibt, was in den Diskussionen über die Tiefen Treiber der nicht nachaltigen Entwicklung zu wenig Aufmerksamkeit bekommt, **dann ist es die Finanzialisierung unserer Welt und unserer Beziehungen**
- "Die Erfahrung von Selbstwirksamkeit..." (schade darauf sit sie recht wenig eingegangen falls überhaupt) Peter Vorderes Arbeit hier auch relevant

# Wer weitermachen will

- #tags: #maja_göpel #book #sustainability #change #narrative #notes
- viele gute anregungen, einige mir bekannte:
- gemeinwohlökonomie, kreislaufwirtschaft ellen mac arthur foundation, transitiontowns
