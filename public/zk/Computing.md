[[_TOC_]]

# Context -  About Computing

- #tags: #context #computing
So Computing how to go about it and how to explore it? How to measure? How to navigate? This document should have four major sections, which I will use to create a representation a thought model to think about computation. the four sections are:

- People
- Terms and Concepts
- Chasms and Trends / Human phenomena
- (rest)

People form Terms and Concepts which then evolve into Chasms and Trends. So the people form the start, why these people, but I think they offer interesting insights.
Alan Kay quotes Cicero: "To be ignorant of the past is to be forever a child." (from programming and scale, watch it if your haven't!!). 

# implementing ideas 

- #tags: #creativity #motivation #twitter
-  https://twitter.com/avsa/status/935896906483617793
- It’s impossible to hire a great developer to build your idea, because any dev worth their salt will have ideas of their own. Good products are built by people working together to merge their compatible ideas.

# Math

- #tags: #math #history #wikipedia
- Since math is very related to computing i put this here: 
- https://en.wikipedia.org/wiki/Algebraic_geometry#History
- Dijkstra mentioned something about wondering why gaus didn't proclaim non-euclidian geometry. So this is info for the map of math.


# Tracking 

- #tags: #ideas #programming #computing
- Conferences
- News Sites 
- Spectrum: Industry vs Academia
- lambda the ultimae: http://arcanesentiment.blogspot.de/2014/04/why-lambda-ultimate-doesnt-make-me-feel.html
- also : http://neverworkintheory.org <- whats this explore!
- books
- twitter -> people [communities]

# People

- #tags: #people #computing
- Dijkstra, Kay, Engelbart, McCarthy, Steele, Perlis, Hoare, Graham, Zawinski, Claude Shannon ( also see turing awards)

# Claude Shannon

- #tags: #claude_shannon #article #paper #wikipedia
- pupil of vannevar bush?
- Information theory, discovering a meta level, http://math.harvard.edu/~ctm/home/text/others/shannon/entropy/entropy.pdf
- https://en.wikipedia.org/wiki/Somatotype_and_constitutional_psychology
- http://spectrum.ieee.org/geek-life/history/a-man-in-a-hurry-claude-shannons-new-york-years how he thought the impossible
- http://nautil.us/issue/51/limits/how-information-got-re_invented

# Papers we love a mathematical Theory of Communication

- #tags: #youtube #claude_shannon #slides
- https://www.youtube.com/watch?v=UQJ1LQ-twho&feature=share
- https://speakerdeck.com/kiran/papers-we-love-too-shannons-mathematical-theory-of-communication
- 1. all communications is the same
- 2. Information is measureable
- 3. Information can be transmitted without error over noisy channels

# People

- #tags: #programming #community #functional #static #math
- robert harper, existential type
- Philip wadler
- Benjamin Pierce (definition Typesystems see TAPL)  the more interesting your types get, the less fun it is to write them down ( progress is tied to mathematical notation, EWD: invent new when necessary)
- runar, tony, brian mc kenna, ed kmett, jon sterling, adrej bauer, paul chiusano
- @chrisamaphone chris martens(they/them)
- nada amin
- stephen diehl
- Conal Elliott https://twitter.com/conal
- Oleg (but also scheme): http://okmij.org/ftp/

# Progress/Methods/Principles in Static Communities

- #tags: #programming #math 
- Terms: Category Theory, HoTT, Topology, Curry-Howard Correspondence
- Pushing Feedback loop to the most outer boundary, finding model(s) to represent _everything_
- Parametricity, Recursion Schemes and Codata -> Category Theory
- Scala Design Philosophie
- https://twitter.com/posco talk about _programming is not math_ http://www.youtube.com/watch?v=JF-ttZyNa84
- I had a chat with him a year later or so and ...
- robert Harper:_"PL's don't use type systems, they are type systems."_   http://www.quora.com/Why-do-programming-languages-use-type-systems/answer/Robert-Harper?srid=1sA&share=1
- **trinity: math, logic(think about gödels contribution!), computing** http://www.quora.com/Why-is-functional-programming-gaining-popularity-lately/answer/Robert-Harper?srid=1sA&share=1
- which comes down to what does it mean to programm? how to _DO_ programming, this relates directly to dijkstras: teach math and teaching _how to do math_
- the theoretic tower to enable this is enormus
- Category Theory not powerfull enugh to bridge types and computation -> HoTT -> http://www.heidelberg-laureate-forum.org/blog/a-new-type-of-mathematics/
- denotational design: http://conal.net/talks/lambdajam-2014.pdf
- paul phillips on future of programming: https://twitter.com/extempore2/status/525049636688363520
- https://lobste.rs/s/aiovv2/wadler_s_blog_propositions_as_types
- they see largly dependent types as a way forward: scala-dotty, Idris
- Cubical Type theory as an implementation? of HoTT

# Notation and Thought

- #tags: #links #notation #cognition 
- https://github.com/hypotext/notation/blob/master/README.md

# Lambda calculus

- #tags: #lambda_calculus #blog
- https://palmstroem.blogspot.com/2012/05/lambda-calculus-for-absolute-dummies.html

# Category Theory

- #tags: #category_theory #context
- good intro: https://cdsmith.wordpress.com/2012/04/18/why-do-monads-matter/
- 4 very useful Abstractions: Functor, Monoid, Monad, Applicative
- https://ncatlab.org/nlab/show/representable+functor briding Math and programming (CT + HoTT)

# Idris

- #tags: #programming #functional
- Edwin brady
- no monads for effects
- TDD book ( type driven developement)
- talks by raichoo and puffnfresh
- ethereum smart conctract and Idris, from DevCon1 : https://www.youtube.com/watch?v=H2uwUdzVDI9
- TDD type driven developement book: https://www.manning.com/books/type-driven-development-with-idris

# Propositions as Types - Paper by Phil Wadler

- #tags: #paper #youtube #functional #programming #math
- http://paperswelove.org/2015/video/michael-bernstein-propositions-as-types/
- strange lop 2015 talk : https://www.youtube.com/watch?v=IOiZatlZtGU Curry-Howard Correspondence, Terms, important static concept
- the logicans find the interesting stuff FIRST, ponder this, why?! ;)


# Philip Wadler on Computability

- #tags: #functional #programming #youtube
- https://www.youtube.com/watch?v=GnpcMCW0RUA


# Homotopy_type_theory

- #tags: #math #programming
- [Homotopy_type_theory](http://en.wikipedia.org/wiki/Homotopy_type_theory)
- from wikipedia: "HoTT allows mathematical proofs to be translated into a computer programming language for computer proof assistants much more easily than before."
- over twitter 
- which raises the question: how many of the today available proofs can be translated? Is there a repository/*pedia for _all_ proofs in mathematics?
- https://github.com/HoTT/HoTT

# Podcast

- #tags: #programming #functional #podcast
- http://typetheorypodcast.com/2015/01/episode-3-dan-licata-on-homotopy-type-theory/#t=16:47.024
- Higher type theory, Higher Category Theory, HoTT finding, exploring connections
- intensional Equality
- compare Mergesort/quicksort
- Extentional laws -> Functional Equivalence, Monad Laws
- Reflexivity
- Univalence Axion: behavior vs how things are
- TT as Expression language for what things are _and_ behave
- Higher dimensional Algebraic Structure (Groupoid)
- Khan Condition
- focus more on programming than proofs
- framework for operational semantics
- Types <-> Groupoids
- cubes with open lid <- get this part
- other shapes have been tried -> mathematican intuion!
- thoughts: connection, relation to CT and dependent types?

# Introduction to Type Theory - HoTT

- #tags: #math #programming
- https://github.com/jozefg/learn-tt
- https://www.youtube.com/watch?v=OupcXmLER7I Lambda Jam 2014 HoTT - Whats the Big idea

# HoTT for functional programmers

- #tags: #youtube #math #programming
- path intuitions, coercion 
- patches
- https://m.youtube.com/watch?v=caSOTjr1z18&feature=youtu.be


# Learning to learn - Ed kmett

- #tags: #learning #programming #talk slides #twiter #problem_solving #strategies
- https://twitter.com/kmett/status/552926129284481024
- slides https://slides.yowconference.com/yow2014/Kmett-StopTreadingWater.pdf
- video https://yow.eventer.com/yow-2014-1222/stop-treading-water-learning-to-learn-by-edward-kmett-1750
- Richard Feynmen
- Saunders Mac Lane survey of modern algebra, see also: wp: Philosopy pf mathematics, liked approachable texts, his : Categories for the Working Mathematician(1970), best intro to CT, yet!
- Alexander Grothendieck <- I heard of him before, multiple times, why are the stories of genius always so tragic?
- slide 58 The rising sea "A different image came to me a few weeks
ago. The unknown thing to be known
appeared to me as some stretch of earth of
hard marl, resisting penetration…the sea
advances insensibly in silence, nothing seems
to happen, nothing moves, the water is so far
off you hardly hear it...yet it finally surrounds
the resistant substance."
- Alexander Grothendieck, translated by Colin McLarty"
- use spaced repitition for learning
- how to crack a nut by knife
- or use grothendieck's method, surrounding whater 
- => the zettelkasten uses the method of the rising sea, put in notes and eventually the sea will rise, clusters will emerge

# Alexander Grothendieck

- #tags: #math #alexander_grothendieck #youtube #twitter #article
- I think I came to Grothendieck via ed kmetts talk learning to learn
- https://www.youtube.com/watch?v=ZW9JpZXwGXc on francais...
- https://news.ycombinator.com/item?id=10026385 -Reminiscences of Grothendieck and His School
- meeting him in 2012 https://twitter.com/evelynjlamb/status/650739419859648513
- http://webusers.imj-prg.fr/~leila.schneps/grothendieckcircle/biographic.php
- https://al3x.svbtle.com/alexander-grothendieck
- there are notes missing regarding his serious efforts and lifestyle change related to pacifism

# Seemingly impossible programs

- #tags: #programming #math
- http://lambda-the-ultimate.org/node/5074
- Topology in constructive mathematics (1920-1950)
- Topology in recursion theory (1950-1980)
- Topology in computer science (1960–present)
- Topology in computer science (1980–present)
- Topology in computer science (2000–present)
- Topology in operational semantics (2005)

# Haskell in the Large

- #tags: #trend #programming #twitter
- came up a couple of times up in twitter but i pick and read this one: https://twitter.com/ID_AA_Carmack/status/561656744976580609
- Functional Programming, types and abstractions FTW
- can write java in any language
- valuable lessons from erlang!, thats why they created a mini Erlang with haskell, -> this also explains why martin odersky put in actors from the start into scala you need both for "good" systems by todays standards
- more and more programming language as a Operating system running on a VM hypervisor appear(Mirage, OSv), those then are managed like "actors"? that smells like a trend

# podcast with puffnfresh

- #tags: #podcast #programming
- http://turing.cool/37/
- turing machines, RF, LC were a zeitgeist phenomenen too!
- types to the max, tests to types, derive implementation
- brian :http://strictlypositive.org/Idiom.pdf
- types as propositions , parametricity how tha tchanges your thoughts
- will people learn it?
- its important to keep trak of new papers-> papers we love : http://paperswelove.org

# Deep Spec Benjamin Pierce

- #tags: #project #youtube #industry #programming #logic
- https://www.youtube.com/watch?feature=youtu.be&v=0aqC3UgupLQ&app=desktop

# podcast with paul chiusano

- #tags: #podcast #unison
- http://futureofcoding.org/episodes/10-unisons-paul-chiusano-on-how-abstraction-will-save-distributed-computing.html
- word -> LINQpad
- Companies restricting -> P2P - Blockchain
- Programmers Sapir Whorf
- Brain Interface x86
- google discuvering the "functions" see joe armstrong coders at work, there is no technical reason its politcial, social , economical
- nix has project based hashes
- Content based hashes, on function level for unison - this should be the right level
- reasoning from first principles - see musk , the awerness of the context/frame - how to think an impossible thought

# how to use types with java/scala

- #tags: #blog #programming
- http://rea.tech/the-abject-failure-of-weak-typing/
- ken scambler , 2014

# from patterns to category theory

- #tags: #patterns #category_theory #blog #abstraction
- http://blog.ploeh.dk/2017/10/04/from-design-patterns-to-category-theory/
- " What's a good abstraction? As already quoted, it's a model that amplifies the essentials, etcetera. I think a good abstraction should also be intuitive.
What's the most intuitive abstractions ever?"
- "**Mathematics.**"

# Programming languages in the near future

- #tags: #programming #trends
- http://dev.stephendiehl.com/nearfuture.pdf
- via https://news.ycombinator.com/item?id=15582429
- https://github.com/mortberg/cubicaltt Cubical Type Theory: a constructive interpretation of the univalence axiom

# HoTT progress

- #tags: #programming #math
- All here: Robert Harper Emily Riehl ..
- http://uwo.ca/math/faculty/kapulkin/seminars/hottest.html
- a ver strong push towards runnable mathematics

# Rchain FP in scala , State STATE of the art

- #tags: #talk #programming #blockchain
- https://youtu.be/FupUBrXOLMc
- MTL, Finals tagless...

# Dynamic - data (driven) designing systems 

- #tags: #context #programming #communities
- LISP School
- John McCarthy 
- @samth
- Gilad bracha
- Abelson and Sussman
- william byrd and dan friedman - scheme and ***Kanren
- guy steele? -> but fortress?
- whilliam cook -> try to formalize OO
- efforts of chris granger? -> Datalog + Constraints + efficient implementation
- efforts of worrydream?
- swannodette smalltalk
- fogus - forth 

# Progress/Methods/Principles in Dynammic Communities

- #tags: #programming #communities
- trying to close the feedback loop through more imidiate response and _dynamic_ representation of intent
- emphasis on design and systems
- Actors as _real_ OO for System Design
- Clojure Design Philosophie
- Simple Made Easy
- Hammock Driven Developement
- Exploring the preo 90's computing techniques, see fogus
- on how comunities work: see twitter discussions on comparing language communities to american political parties/directions ( liberterian/conservative/republican), see in that context why steve yegge didn't go for clojure, blog by him?
- knowing what you want PL Design: http://tagide.com/blog/2012/03/research-in-programming-languages/
- clojure.spec feature, see Paper: parsing with derivates (Matt Might'), the blogpost bashes on static types although dependent types exactly go in that direction

# Actor Interaction Patterns

- #tags: #actors #infoq #talk #ui
- http://www.infoq.com/presentations/Actor-Interaction-Patterns
- humus language, JS vis, vis. exploring actor interaction patterns

# a blog in the old sense, deep written with experience and thought

- #tags: #blog #programming #meta 
- not only one post but the whole blog( at least what I can see form 2013)! http://www.cs.uni.edu/~wallingf/blog/
- http://www.cs.uni.edu/~wallingf/blog/archives/monthly/2013-03.html#e2013-03-12T16_30_36.htm
- ref: http://www.storytotell.org/essays/good-for-whom.html Dan Ingals ... readability, expres. and power
- Does Readability Give a False Sense of Understandability?
- http://www.cs.uni.edu/~wallingf/blog/archives/monthly/2014-05.html#e2014-05-07T15_39_20.htm "Programming is Social"
- http://www.cs.uni.edu/~wallingf/blog/archives/monthly/2013-03.html#e2013-03-27T12_46_03.htm "Programming Language as operating System"
- http://www.cs.uni.edu/~wallingf/blog/archives/monthly/2013-12.html#e2013-12-10T15_33_11.htm "Your Programming Language is Your Raw Material, Too"
- via https://twitter.com/wallingf/status/410530470804201472
- via comments: https://schneide.wordpress.com/2009/12/21/the-fallacy-of-the-right-tool/
- the right "material" instead "tool" for the job, material lasts longer then the tool
- see Code as Material metaphor from awelonblue


# (fourth RacketCon): Michael Fogus — Extracting a Goose from a Klein Bottle

- #tags: #talk #youtube #lisp
- https://www.youtube.com/watch?v=2ZrM0aYaqJM&list=PLXr4KViVC0qI9t3lizitiFJ1cFIeN2Gdh&index=1
- influences are important
- rackets killer appp : education
- ~ 10 quite mentioning the quantity vs quality issue in US academic papers
- very often the students don't know to learn
- 18:51 Alan Perlis: ""There will always be things we wish to say in our programs that in all known languages can only be said poorly"
- hash lang capability in racket
- ian piumarta : Mood Specific Language
- real world stuff, but for whom?
- ~34:40 _"its not enough to know 2 paradigms"_
- autodidacts 42:50 <- Ramajan? from GEB
- 47: 15 http://en.wikipedia.org/wiki/Aldous_Huxley
- A.E. Houseman "I , A Stranger and afraid in a world i never made" Kirk Quote?
- 51-45 the dust beneath the bridge: marketing, language wars, endless twitter streams of language wars

# Live Literate Programming

- #tags: #youtube #gilad_bracha #talk
- Gilad Bracha 
- https://news.ycombinator.com/item?id=14433687
- https://www.youtube.com/watch?v=HW7sqMjTkI0&feature=youtu.be
- new speak

# Guy steele on Computer science metanotation (CSM)

- #tags: #computer_science #science #change #media #youtube
- https://www.youtube.com/watch?v=dCuZkaaou0Q
- language of computer science papers, analyze scope over 30 to 50 years
- try to build a interpreter/compiler for it

# William Byrd, Greg Rosenblatt - Barliman: trying the halting problem backwards, blindfolded

- #tags: #talk #youtube #lisp
- https://www.youtube.com/watch?v=er_lLvkklsk&feature=share

# William Byrd on "The Most Beautiful Program Ever Written"

- #tags: #lisp #youtube #talk
- https://www.youtube.com/watch?v=OyfBQmvr2Hc
- equality - HoTT is not a trivial thing

# patterns why they failed

- #tags: #patterns #perspective #vimeo
- https://www.deconstructconf.com/2017/brian-marick-patterns-failed-why-should-we-care

# Comparing Scala and Clojure Design Approaches

- #tags: #programming #thoughts #design
- Scala definies itself as a OO(imperative) - FP hybrid, interestingly it had a actors library from the start, I think that is because MO sees actors as true objects and as a powefull computation model
- Clojure defines itself as a FP language, Simple Made Easy, Hamock Driven Developement, "Data" all the APIs, the value of values

# Hardware

- #tags: #programming #hardware
- loperOS dataflow hardware

# Transmeta Crusoe

- #tags: #hardware #wikipeida
- https://en.wikipedia.org/wiki/Transmeta_Crusoe
- Code Morphing Software

# Asynchronous Circuit Design

- #tags: #concurrency #hardware
- http://worrydream.com/refs/Sutherland%20-%20Tyranny%20of%20the%20Clock.pdf
- of course Intel bought attempts and killed them off.. https://news.ycombinator.com/item?id=11996634

# Mill CPU

- #tags: #hardware
- see ff bookmarks
- hardware SSA
- they realize that their slides make "no sense" without animation, still ppt...
- https://m.youtube.com/watch?v=QyzlEYspqTI - The belt is a hardware queue via : https://news.ycombinator.com/item?id=10111991
- Via google:alan kay on mill cpu architecture: https://m.reddit.com/comments/2yx4rq

# Quantum Computers

- #tags: #youtube #hardware #computer
- https://www.youtube.com/watch?v=JvIbrDR1G_c&feature=share
- he says google will try to make a 50 qbit computer in 2018 , when they succeed it could be possible to disprove critics
- Scott Aaronson https://www.scottaaronson.com/blog/ Book QC since democrit
- https://blog.ycombinator.com/scott-aaronson-on-computational-complexity-theory-and-quantum-computers/

# Terms and Concepts - (Fundamentales)

- #tags: #programming #concepts
- Abstraction Cube ( yes its N dimensional)
- important abstractions, terms, like s(f)-expresion
- Compiler, Interpreter, Parser
- so IS programming math?, and what does Math here mean(comes closer to Dijkstras def , symbolic computation and inventing new notation when necessary)?
- on Jörgs argument: it would be so much easier if there would be only one language
- how false and not even false can this argument be
- tonys not even false OOP argument comes to mind
- also the twitter wars of programming is not math tony vs. twitter scala lib authors see http://www.youtube.com/watch?v=JF-ttZyNa84
- Haskell Programm Visualizer, very poor UI but the fundamental Idea is good, also it runs the programm backwards to a certain extend: watch me: http://www.youtube.com/watch?v=pqtqaL_ojpk&feature=plcp
- https://github.com/scalaz/scalaz/issues/271
- What is a bug? Missing functionality or unexpected behavior of existing functions? See also dijkstra on this

# Property based testing

- #tags: #programming #testing
- quickCheck ( erlang, haskell) , hedgehog, theft for C, clojure spec, scalaCheck
- John Hughes Volvo https://www.youtube.com/watch?v=zi0rHwfiX1Q

# Hypercubes/Spheres

- #tags: #geometry #math #programming
- Higher dimensional shapes/geometry
- see Blockchain applications
- https://youtu.be/ciM6wigZK0w 8 dimensions viz paper

# RDBMS - CQRS/ES - Blockchain

- #tags: #programming #intuition #blockchain
- understanding ES principles a from a systems perspective, help understanding blockchains: http://adaptechsolutions.net/eventsourcing-why-are-people-into-that/

# Computability - Entscheidungsproblem - Halteproblem

- #tags: #programming
- see Phil Wadler Video

# Abstraction

- #tags: #programming #concept #meaning
- se Dijsktras definiton, see paul chuisanons blog , see gavin kings remark on reddit that hibernate was "designed" as leaky , jeff aldwood leaky?, 
- so what does that term mean, did it "evovle"? can/should it evolve? 
- on IDEs https://twitter.com/michaeljforster/status/414797954298298368 see later in that thread EWDs comment

# Expressiveness

- #tags: #programming #meaning
- What does that Term mean? see Ola's definition, maybe relation to Lockhardt etc, how subjective can it be and must it be?
- Expressiveness in Programming languages: http://www.sumsar.net/blog/2014/12/peter-norvigs-spell-checker-in-two-lines-of-r/
- Comparing (Matlab, Wolfram language) R with Julia?
- need an interactive model for exploring PL meta data ( possible source wikipedia, wikidata?!) , impl: Js, D3, unity, clj?

# Debugging

- #tags: #programming #twitter 
- the best debugging tool of all, tip by Rob Pike: https://twitter.com/ceejbot/status/582974795844861952

# Nachvollziehbarkeit, Vorhersagbarkeit von Programmen

- #tags: #programming #media
- the value of values
- the IDE as a value - chris granger http://www.chris-granger.com/2013/01/24/the-ide-as-data/
- data all the things
- free monads, make description of your programm and a interpreter to run it, the separation makes both easier to extend
- re-running your programm as a "List" of instructions, pause, resume, transfer to another machine, debug , can we find that blog that explained this with MMORPG example?

# Persistent datastructures - functional datastructures

- #tags: #programming
- http://cstheory.stackexchange.com/questions/1539/whats-new-in-purely-functional-data-structures-since-okasaki
- for a general introduction see rich hikey e.g.
- ideal hash trees and much more...

# Y Combinator /fix point - Jim Weirich

- #tags: #concept #programming #definition
- https://www.youtube.com/watch?v=FITJMJjASUs
- explaination in ruby, he also made a talk involving the term : conacouis ( see FF bookmarks)

# Parsing

- #tags: #programming #concept
- Do you know WHY it is so important?!
- what is an S-Expr, and why it is important! -> chris granger said that most non prog people dislike the concept of scope, since s-exps are always nested that could be an asthetic reason?
- see STEPS and Ometa, Early Parsing
- http://loup-vaillant.fr/tutorials/earley-parsing/what-and-why code in lua
- via http://www.mail-archive.com/fonc@vpri.org/msg04623.html

- Overview Parsing Techniques see MattMight Talk - Parsing with Derivates
- Overview Techniques and Tools: GLL

# library vs. frameworks

- #tags: #programming #structure #blog
- "frame" to frame, to limit
- if you think this through its a tools shape our thinking issue, programming lanugage, library , framework, how much constrains and express ability does each offer?
- Tim Perrett from Scala: http://timperrett.com/2016/11/12/frameworks-are-fundimentally-broken/

# Monadic design patterns for the web

- #tags: #math #programming #principle
- Asssociativity means flattening: from monadic pattern for the web
- a * ( b * c) =  c * (a * b)  also auch ganz flach: = a * b *  c

# Programming at the meta Level

- #tags: #programming #meta
- scala type level programming, turing complete programms
- the programming lanugage at the type level is diffrent from the value level in scala
- using the black scheme and other lisps you may avoid that
- programm as data
- data as programm
- types and proofs
- programming should eat itself
- type theory should eat itself
- homo type therory should eat itself
- see nada admin key note strange loop 2014
- why programming at the "meta" level can be useful: nada - to build up understanding and context of a problem to view a problem from above/meta level
- haskell needs the meta level too: https://twitter.com/GabrielG439/status/519301129289465857
- related to program synthesis
- Wie heilt man Feature ritis? Implementierung der kernfunktionalitat als kombinierbare Funktionen. Über das User Interface dann Werkzeuge zur Kombination zur Verfügung stellen.

# Equality

- #tags: #programming 
- http://jwesleysmith.bitbucket.org/set-functor.pdf 
- Sets, Set Datatype, map operation, see EWDs, define Set intensional or extensional

# Probabilistic Programming (Language)

- #tags: #programming_language #wikipedia #twitter #article
- http://en.wikipedia.org/wiki/Probabilistic_programming_language (cite_note-Anglican-14)
- https://twitter.com/beaucronin/status/587609942015651841 (follow up paulTypes)
- http://newsoffice.mit.edu/2015/better-probabilistic-programming-0413 -> PL:Julia -> Picture as a Julia Extension 

# Actors and Functional Programming

- #tags: #programming #actors
- seems that Erik Meijer for example understands that FP is not only immutability but aso reasoning about effects (see mostly functional programming doenst work)
- Tony morris, runar, Jessica Kerr about Actors and FP, jonas boner, Jonas Boner , CALM: https://twitter.com/jessitron/status/368147000673325056
- -> jim Duey CALM... that video he links to is the BLOOM project, evolving theory/practie for distributed systems , see and compare TLA+ ?

# Paralelism vs. Concurrency

- #tags: #programming #principle
- via Rob Pike, Concurrency: progrg. as the composition of independent processes / parallesim: prog. as simultanous execution of (possiblyy) related computations
- Joe Armstrong: http://joearms.github.io/2013/04/05/concurrent-and-parallel-programming.html
- Hickey: 
- _Concurrency_ Use Queues - solves Write Problem, but notification when ready to read?
- _Parallelism_ Use immutable Data Structures, Map and Reduce

# What is a bug , see Dijkstra and some recent twitter diskussion ( graham lea, puffn fresh ,bite, david barbour)**

- #tags: #programming #meaning 
- Defining : bug / correct programm
- https://twitter.com/curious_reader/status/448577107380428800
- rember how sets work: you can define them intensional and extensional ( can you find dijkstras ref?)

# Quines

- #tags: #programming #wikipedia
- Ein Programm gibt sich selbst aus
- http://de.wikipedia.org/wiki/Quine_(Computerprogramm)
- Bijektive Funktionen
- Siehe auch Fixpunkt kombinator ( Y Kombinator)

# values - types - kinds

- #tags: #programming #principle
- ref:http://de.slideshare.net/dgalichet/demystifying-scala-type-system
- Q:What is a type?
- "A type defines a set of values a variable can posses and a set of functions that can be applied to these values”so they form a relation?!"
- Sum Types - Product Types
- Q:What is a type system?
- ref:http://de.slideshare.net/djspiewak/high-wizardry-in-the-land-of-scala
"A type system is a tractable syntatic method proving the abcence of certain programm behaviors by classifying phrases according to the kinds of values they compute" benjamin pierce  ( also TAPL)
Q:What are "higher" kinds?
ref:http://de.slideshare.net/djspiewak/high-wizardry-in-the-land-of-scala
- haskell and scala have _kind_ systems, the .NET CLR for example can not represent those
----------------
kinds
------
types
----------
values
-----------
- type systems classify values
- kind systems classify types
- → Values are to types as types are to kinds 

# Functional Reactive Programming - FRP - Rx introduction

- #tags: #programming #youtube
- https://gist.github.com/staltz/868e7e9bc2a7b8c1f754
- https://t.co/sBO5xYfOoc
- https://speakerdeck.com/player/4fd7f5501f1b013238737e42b879906f#
- http://www.youtube.com/watch?feature=player_embedded&v=nCxBEUyIBt0

# What is a type provider?

- #tags: #programming #podcast
- via: http://www.podcast.org.il/the-scala-types-podcast-175568/episode-26-stefan-zeiger-on-slick-פרק-214639988/
- DataSource (Database or web defined schema.freebase?) with Types =>
- use typeprovider mapping for database.
- The compiler magic in F# and type macros in scala create then
- native (scala or F#) type instances of the DataSource Schema 
- scala/F# type mapping
- Its a Typ(system) mapping from 1 external defined Typsystem ( typed schema) to another (local) Typsystem
- CLOS offers a similar mechanism bridging
- C++ 'Objects' and Common Lisp 'Objects'
- https://groups.google.com/forum/#!topic/idris-lang/hAjO5yVl6nw/discussion

# Logic programming

- #tags: #programming
- binding of results into logic variables, backtracking 

# how-to-train-your-programmer

- #tags: #learning #programming #perspective #meaning
- via https://twitter.com/asciidisco/status/590544614043557888
- Presentation by https://twitter.com/ag_dubs ashley williams
- http://ashleygwilliams.github.io/how-to-train-your-programmer/
    - What does it mean to program?
    - What aspects of the developer experience should we consider?
    - What are the goals of teaching programming?
- Dijkstra - The effective exploitation of his powers of abstraction must be regarded as one of the most vital activities of a competent programmer. - The Humble Programmer (=> its itneresting to note how he himself saw his speech different couple of years later )
- Peter Van Roy and Hadidi - Concepts, Trechniques and Models of Computer Programming
- Benjamin C. Pierce - Types and Programming Languages
- Abelson and Sussman - Teach Programming *Concepts* NOT Language Features - SICP  
- Ron Jeffries XP YAGNI
- Carl Sagan "If you wish to make an apple pie from scratch, you must first invent the universe."
- Richard Feynman  "The real problem in speech is not precise language. The problem is clear language ... It is really quite impossible to say anything with absolute precision, unless that thing is so abstracted from the real world as to not represent any real thing."
- Pablo Picasso - There is no abstract art. You must always start with something. Afterward you can remove all traces of reality.
- Martin Heidegger - The most thought-provoking thing in our thought-provoking time is that we are still not thinking.  What is Called Thinking? [Was heisst Denken?] (1951–1952)
- Jacques Derrida - Deconstructed
- Thomas Kuhn - "Scientists work from models acquired through education and through subsequent exposure to the literature often without quite knowing or needing to know what characteristics have given these models the status of community paradigms"-  The Structure of Scientific Revolutions", pg 46 (1962)
- Igor Stravinksy - Themes and Conclusions, (1982) p. 188
- "What does it mean to program?"
- really good sketch notes(<- got this term from @chrisamaphone)!!! http://ashleygwilliams.github.io/how-to-train-your-programmer/images/lecture_notesv2.JPG

# Papers

- #tags: #programming #papers #learning
- I have read those but havn't taken notes..: Out of the Tarpit, No Silver Bullet

# 10-papers-every-programmer-should-read-at-least

- #tags: #programming #papers #learning
- http://michaelfeathers.tumblr.com/post/81489281/10-papers-every-programmer-should-read-at-least
-   'On the criteria to be used in decomposing systems into modules' - David Parnas
    'A Note On Distributed Computing’ - Jim Waldo, Geoff Wyant, Ann Wollrath, Sam Kendall
    'The Next 700 Programming Languages’ - P. J. Landin
    'Can Programming Be Liberated from the von Neumann Style?’ - John Backus
    'Reflections on Trusting Trust' - Ken Thompson
    'Lisp: Good News, Bad News, How to Win Big’ - Richard Gabriel
    'An experimental evaluation of the assumption of independence in multiversion programming’ - John Knight and Nancy Leveson
    'Arguments and Results' - James Noble
    'A Laboratory For Teaching Object-Oriented Thinking' - Kent Beck, Ward Cunningham
    'Programming as an Experience: the inspiration for Self' - David Ungar, Randall B. Smith
- compare with turing award list

# Zeitgeist and Trends 

- #tags: #trends #zeitgeist #programming
- Especially the essay from Paul graham: Beating the averages, can help understand some social relations which influence trends. Especially interesting here is the "blub" paradox. 
- Chasms and Trends are deeply connected to our human thought and behaviour model (communities-> tribes)

# thoughts on trends and representation

- #tags: #trends #thoughts
- need a time line with multiple/dimension views, overlay with chams e.g., 
- another over lay would be how those chasms manifest: media -> books, conferenes, blogs, 

# Global function/data space

- #tags: #zeitgeist #programming #computing #blog #youtube 
- remember, functions are just values!
- This mechanism would enable better software reuse than libraries! for better explanition see david barbour/luke palmer on programming singulairty
- What if there would be a global namespace for functions ?
- strangely enough more and more people pondering this(or is is just hte internet who let me finds it?)
- joe Armstrong erlang ML, see FF book marks http://erlang.2086793.n4.nabble.com/Why-do-we-need-modules-at-all-td3546413.html
- McCarthy (S-Expr)
- pchiusa and his Unison efforts http://pchiusano.blogspot.de/2013/05/the-future-of-software-end-of-apps-and.html
- david barbour and his awelon efforts, he has done alot of technical research, as spend time communicating
- see also gilad bracha on _ban on imports_
- tatsächlich sind Smalltalk ( und Image basierte Systeme Self?) genau hierfür ein Beispiel, das VPRI Projekt KWorld
- Stephen Wolfram hat nun so ein System gebaut welchen “Prinzipiell” unbegrenzte anpassungsfähigkeit ermöglicht.
- Die Anpassungsfähigkeit des Systems ist lediglich durch die Ausdrucks Kraft der Sprache und der eigenen Kenntnis mathematischer Modelle begrenzt!
- http://blog.wolframalpha.com/2013/11/13/something-very-big-is-coming-our-most-important-technology-project-yet/
- Rest Hypermedia APIs also?
- from luke palmer :  https://lukepalmer.wordpress.com/2010/07/22/programming-for-a-culture-approaching-singularity/ see his github notes
- BitCon apps wie ethereum, werden das erste globale system bilden: https://www.youtube.com/watch?v=o6D8Up411dI
- see disscusion with Joe Armstrong, puffnfhresh and John De Goes
- es ist wichtig zu verstehen was die motivation für diese Entwicklung ist:
- see the dependency problems from _all_ programming languages, see the "re" - use problem, see luke palmers graphic in his blog posthow re-use is done and even (only) possible today
- see Joe Armstrongs post on hash web ( reducing entropy!)
- see Ola Binis post on DNS and security

# Mark Hibberd - facts based dependency managemeent - annex

- #tags: #programming #zeitgeist #youtube #talk #experiments
- https://www.youtube.com/watch?v=D6JZiJTnPEI
- https://speakerdeck.com/markhibberd/towards-annex-a-fact-based-dependency-system-icfp
- no closed world asumptions, dependency costs too much
- introducing new terminology: fact, atom, world
- world changes over time
- zeitgeist observation: camlistore, datomic - clojure codeq,  mirage workflow and annex: hash based data store
- queries can be ditributed ( on dep graph) server OR Client
- deeper analytics -> the similarities with datomic, codeq are striking

# Stephen Diehl ( Haskell/Static)

- #tags: #programming #perspective
- http://dev.stephendiehl.com/nearfuture.pdf
- Its ALWAYS a funding problem, people are slowly realizing what Alan Kay has been saying for quite some time, but they dont see yet web3 as a alternative

# Reducing Entropy

- #tags: #programming #dialectic #tao
- Joe Armstrong talk: the mess we have created, the web of hashes
- john de goes : Ideal prog. Lang => Morte to _reduce_ entropy

# Morte

- #tags: #programming #experiments
- https://twitter.com/natpryce/status/609093048027504642 <- I made a ref here to morta
- http://www.haskellforall.com/2014/09/morte-intermediate-language-for-super.html
- http://www.haskellforall.com/2015/05/the-internet-of-code.html

# ipfs - The Permanent Web

- #tags: #programming #computing #zeitgesit #web3 #blockchain 
- ipfs.io
- references bittorrent and camlistore
- fusion concept of those and git ideas
- found via a tweet(thread) from joe armstrong talking about nix os
- alpha is available (14.04.2015)
- relation to camlistore?
- integration with ethereum?
- similar alternatives, via http://www.reddit.com/r/ethereum/comments/31iklh/ipfs_is_the_permanent_web_a_new_peertopeer/
    - TAHOE-LAFS
    - GnuNet
    - Freenet
    - Maidsafe
    - Storj
    - Oceanstore
    - Freehaven
- also, similar : https://forum.safenetwork.io/t/similar-projects/234

# the General conept

- #tags: #programming #concept
- bitcoin, Blockchain, ethereum
- the implications of this are profound, see ethereum notes
- IBM and Samsung use blockchain tech for IoT http://www.coindesk.com/ibm-reveals-proof-concept-blockchain-powered-internet-things/

# Stackoverflow founder blog

- #tags: #trends #learning #programming #perspective 
- see HPI Video , the _exact_ words of that teacher on why the students should read it and _when_ (sparetime)
- www.joelonsoftware.com/items/2014/07/24.html
- ponderings on excel, stable compiler, see FF bookmarks

# Trends, Alex Payne

- #tags: #programming #trends #programming_languages
- https://al3x.net/2014/09/16/thoughts-on-five-years-of-emerging-languages.html

# Waves of Chams( search for Alan Kay ref) from OO to FP

- #tags: #programming #trends #youtube
- http://www.youtube.com/watch?v=l-zL_RLGBq4#t=1485
- related Success for OO : http://www.quora.com/Was-object-oriented-programming-a-failure/answer/Michael-O-Church

# Future(dev. in) / trends of Computer Science

- http://www.heidelberg-laureate-forum.org/blog/video/lecture-friday-september-27-john-hopcroft/
If two fundamental compontens of computing may be: hardware and software, we may distinguish software further into code and data.
John hopcroft argues that in the time between now and the beginning of CS, its research focused on code (PLs, compilers,OS,algorithms,Databases) its now shifting towards Data analysis (how to analays “real” time “Big” data) ,in particular how to extra meaning out of (unstructured) data , which could be described as: quantifying meaning. In which way or how can we define interest 

# Stackoverflow Phenomena

- #tags: #programming #inidivudal #group #society #systems 
- What did let to the invention of stackoverflow?
- http://www.joelonsoftware.com/articl8es/fog0000000007.html
- His blog is a pearl but it's already ancient not widely known
- What solution do you prefer
- Sfo answer or storie?
- replicate data 2 example
- => there is a Video in the FF bookmarks which shows him about 5-6 monts after site launch 
- => in the HPI Video Link from spacy the teacher recommends his blog, AS A SIDE NOTE, not as mandatory reading, "if you have a rainy weekend or too..."
- => also look up why Sfo failed blog post (its here see: Communicating semantics and stackoverflow) and thoughts

# Communicating semantics and stackoverflow - why it doesnt solve

- #tags: #programming #culture #history #computing
- http://exple.tive.org/blarg/2013/10/22/citation-needed/
- => TODO this is more deeply related: future of programming , worse is better , relate this

# Worse is better

- #tags: #programming #software #social #scale #industry #society #systems
Original links:
- http://www.dreamsongs.com/WorseIsBetter.html
- http://www.dreamsongs.com/WIB.html
- http://www.dreamsongs.com/RiseOfWorseIsBetter.html
- Worse is better by SeanMcDirmid[1]:
"Programmers will not continue to carefully design, plan, and validate their programs before they write one line of code because...they don't even do that today. The style that most programmers follow will continue to be somewhat less than ideal and benefit from a "heads up display" to more quickly understand where they are actually going vs. where they thought they were going."
- So does this mean we are doomed because our ability to analyze and adapt is (inherently?) better or to a greater extend developed then the ability to plan for the future? I think this is the essence of worse is better. For more see here [2]
- http://pchiusano.github.io/2014-10-13/worseisworse.html
- see the HN article : https://news.ycombinator.com/item?id=8449680 
- What to do about WIB : http://www.dreamsongs.com/MFASoftware.html real CS/Informatik Education

# Tonsky Rant on the State of the Art 2018 - Worse is better, still even in 2018

- #tags: #programming #software #social #scale
- http://tonsky.me/blog/disenchantment/
- good examples, android size, reliability
- positive examples: Martin Thompson: LMAX Disruptor, SBE, Aeron, Xi editor by Raph Levien, Jonathan Blow compiler, 500k lines per second on his laptop

# Software Krise

- #tags: #programming #software #social #scale
- Auf wikipedia wird auf Komplexität und anderes verwiesen, Dijkstra zeigt aber sehr deutlich das die Firmen wenig interesse an Fehlerfreier Software haben. Ein Fall von worse is better?

# Peak Abstraction

- #tags: #principle #abstraction #heros_jorney #psychology #cognition #individual #experience
- personal growth wrongly understood/approached?
If we want to understand our problems we have to look closely at our behavior. And Peak Abstraction is an important phenomena in that process
- Fit in this category. Using peak abstraction as an example it is really interesting how much self reflection we apparently miss.
- is peak abstraction personal?
- [1]http://lambda-the-ultimate.org/node/4655#comment-73745
- [2]http://www.dreamsongs.com/WorseIsBetter.html
- [3]http://lambda-the-ultimate.org/node/4442

# OBT Trends academia

- #tags: #trends #programming
- these OBT's look interesting, even Bret Victor goes there? http://www.cs.rice.edu/~sc40/obt15/
- OBT 2014 We Need Real Tools for Generating Type Inferencers William E. Byrd , Nada Amin http://popl-obt-2014.cs.brown.edu/papers/inference.pdf, more
 on that topic nada says: http://swannodette.github.io/nominal%20logic/2013/02/08/the-simply-typed-lambda-calculus-in-20-lines-redux/

# Future Programming Languages - As we may program

- #tags: #ai #programming #vimeo #peter_norvig
- https://vimeo.com/215418110
- Peter Norvig Lisp NYC
- probabilistic programming languages
- https://en.wikipedia.org/wiki/Probabilistic_programming_language#List_of_probabilistic_programming_languages
- talk could also be titled: or how to make it(programming style neural nets) every bodies problem


# Unikernel Introduction

- #tags: #programming #abstraction
- http://slides.com/technolo-g/intro-to-unikernels-and-erlang-on-xen-ling-demo#/

# MirageOS

- #tags: #computing #twitter
- Do you even need a shell installed on your web server? https://twitter.com/gazoombo/status/515888910225129473?refsrc=email

# ponderings on OS and Computing history also engelbart

- #tags: #computing #blog #patterns #douglas_engelbart #history
- http://davidad.github.io/blog/2014/03/12/the-operating-system-is-out-of-date/

# OSv

- #tags: #twitter #computing
- via : https://twitter.com/TzachL/status/561542925297803264
- Java,C/C++,Ruby and "most" HV

# Clive

- #tags: #computing #twitter
- via https://twitter.com/justincormack/status/587286099183214592
- go based unikernel

# Unikernel JITs

- #tags: #computing #twitter
- https://twitter.com/attrc/status/596691548311408641
- https://www.usenix.org/system/files/conference/nsdi15/nsdi15-paper-madhavapeddy.pdf
- Jitsu: Just-In-Time Summoning of Unikernels
- http://anil.recoil.org/papers/2015-nsdi-jitsu.pdf


# Program Synthesis

- #tags: #programming
- http://en.wikipedia.org/wiki/Program_synthesis
- related: automatic programming, proof verification, 
- https://people.eecs.berkeley.edu/~bodik/Files/2012/CAV-2012.pptx Synthesizing Programs with Constraint Solvers - CAV 2012 , source https://twitter.com/lmeyerov/status/552516802644025344
- http://lara.epfl.ch/w/leon THE LEON SYNTHESIS AND VERIFICATION SYSTEM for scala

# A Generation Lost in the Bazaar/Cathedral

- #tags: #programming #group #individual  #jamie_zawinski
- http://www.jwz.org/blog/2012/08/a-generation-lost-in-the-bazaar/ complexity, 

# Programmer Speak/Language

- #tags: #programming #language #thoughts
- always remember/ be aware of sapir whorf!
- the could should be "clear" - "sauber"
- need to get this code working/ "muss das zum _fliegen_ bringen"

# Philosophy

- #tags: #programming #software #philosophy
- destroyallsoftware.com
- programmingisterrible.com/
- http://zackarymorris.tumblr.com/post/10973087527/the-state-of-the-art-is-terrible <- illusion of progress?

# philosophy-in-a-time-of-software

- #tags: #programming #software #philosophy
- https://groups.google.com/forum/#!forum/philosophy-in-a-time-of-software
- this groups is really good, I think I found it through fogus or david nolen, steve klabnik wrote also there
- its mostly inactive these days but it contains some real gems
- I found "software takes over" through it 
- https://s3-us-west-2.amazonaws.com/vulk-blog/The+Pervert%27s+Guide+to+Computer+Programming+Languages.pdf Lacan/Zizek and Computer Programming Choice

# Rest 

- #tags: #programming #blog #youtube #article #twitter
- Programming and the multi dimensional cube - Docker
- Peter Norvig as we May Programm (ML/AI)
- WebAssembly Text Representation , s-expressions https://developer.mozilla.org/en-US/docs/WebAssembly/Understanding_the_text_format
- history IPv4 IPv6 buses http://apenwarr.ca/log/?m=201708#10
- functions, methods , state closure https://news.ycombinator.com/item?id=14799267
- red lang smalltalk with UI focus http://www.red-lang.org/2016/12/incursion-into-explorable-explanations.html?m=1
- Carl hewit IoT https://t.co/nE50aIJoAN?ssr=true
- ranking-reputation PLs : https://www.slant.co/topics/101/~best-languages-that-compile-to-javascript
- http://grishaev.me/en/pg-to-datomic#remove-jdbcpostgres
- also privacy hardware https://www.bleepingcomputer.com/news/security/openbsd-will-get-unique-kernels-on-each-reboot-do-you-hear-that-linux-windows/
- https://www.youtube.com/watch?v=JhHMJCUmq28 Quantum computers in 7 min
- https://www.youtube.com/watch?v=WjJdaDXN5Vs react problems with package manager in JS, sadly no open source, box visual programming in browser, react via https://twitter.com/poeschko/status/805035716778532864
- http://perl.plover.com/yak/design/samples/ The interesting origin of patterns, christopher alexander, c++ vs perl, 
- http://notes.ericjiang.com/posts/751 NodeJS Argument against
- https://twitter.com/pvillega/status/792105960412160000?refsrc=email&s=11 Math programming getting closer, by @mandubian link https://t.co/kAgFRtZv1B
- http://www.cs.rice.edu/~vardi/papers/aaas99.jsl.pdf On the Unusual Effectiveness of Logic in Computer Science - Harper
- https://twitter.com/andrejbauer/status/785502546219630592?refsrc=email&s=11 Five stages of accepting constructive mathematics
- https://twitter.com/leastfixedpoint/status/788467135655936001?refsrc=email&s=11 actor model history
- https://twitter.com/jdegoes/status/791408795867938816?refsrc=email&s=11 FP and crypto
- http://philipnilsson.github.io/Badness10k/posts/2016-08-10-functional-patterns-monoid-morphism.html Monoid morphisms
- https://www.sitepoint.com/how-optional-breaks-the-monad-laws-and-why-it-matters/ java 8 optional problems
- https://twitter.com/joeerl/status/783402919739662337?refsrc=email&s=11 the problem with dependencies
- https://bernardopires.com/2013/10/try-logic-programming-a-gentle-introduction-to-prolog/
- https://twitter.com/jessitron/status/785591779978383360?refsrc=email&s=11 jessitron on abstraction
- https://arxiv.org/pdf/1606.07557.pdf - Dynamic Witnesses for Static Type Errors
- https://speakerdeck.com/ajantis/practical-demystification-of-crdts-curryon-rome-2016
- http://www.drmaciver.com/2016/07/contributors-do-not-save-time/
- http://frontside.io/blog/2016/07/07/the-conjoined-triangles-of-senior-level-development.html
- https://twitter.com/datasciencela/status/754123836942000128?refsrc=email&s=11 Automatic Machine Learning? | SciPy 2016 | Andreas Mueller https://www.youtube.com/watch?v=Wy6EKjJT79M
- https://twitter.com/mrb_bk/status/747859275813294080?refsrc=email&s=11 the perfect programming language reference to Borges
- http://stevelosh.com/blog/2016/06/symbolic-computation/ symbolic computation
- https://twitter.com/stubbornella/status/746761954350366720?refsrc=email&s=11 on TDD
- https://twitter.com/debasishg/status/746392152368779264?refsrc=email&s=11 scala What makes type classes better than traits?

# Harvey OS - Plan

- #tags: #twitter #programming
- https://twitter.com/BrianKrent/status/625109609255038977

# Software archology Etymology

- #tags: #programming #meaning #blog
- https://fgiesen.wordpress.com/2015/07/22/the-modulith/


# Programming terms

- #tags: #infoq #programming #meaning
- http://www.infoq.com/presentations/nosql-commonalities
- riak guy on why NoSQL is a bad term
- the rise of **data middleware**, copy features, adapt to situations

# programming concepts

- #tags: #programming #concepts
- http://www.haskellforall.com/2013/12/equational-reasoning.html
- via https://twitter.com/puffnfresh/status/416043416842018816

# Haskell-linguistic-relativity-PLs

- https://groups.google.com/forum/m/#!topic/haskell-cafe/V52ZpjsbaRw

# Escher Programming Language

- #tags: #programming #twitter
- https://twitter.com/raganwald/status/587384178066337792
- https://github.com/gocircuit/escher
- written in Go

# Interview with Go’s Russ Cox and Sameer Ajmani

- #tags: #programming #interview #twitter
- on GO: http://www.pl-enthusiast.net/2015/03/25/interview-with-gos-russ-cox-and-sameer-ajmani/
- stability, predictability
- software engineers having to explore new ideas to keep up with the hardware improvement
- via https://twitter.com/swannodette/status/580858903312928768
- Googles Hybrid Approach to research: http://static.googleusercontent.com/media/research.google.com/en/us/pubs/archive/38149.pdf
- "and there is significant research at companies across the tech nindustry, not just google" 
- in the comments GO's connection to OCCAM, very nice history connections, details to CSP: http://go-lang.cat-v.org/talks/slides/emerging-languages-camp-2010.pdf 

# Dependent Type Providers

- #tags: #programming #blog #twitter
- via https://twitter.com/debasishg/status/582097881852588032
- via : http://heather.github.io/posts/2015-03-27-Dependent%20Types.html
- http://itu.dk/people/drc/pubs/dependent-type-providers.pdf
- Comparison with F# Type providers, explains, lists advantages and disadvanttages of type providers in F# and Idris, alson comparison as a FFI Mechanism
- Rete Algorithm via Adrian

# how to implement a listp, M ake A L isp -> MAL

- #tags: #programming #experiments #lisp
- really good description of HOW to do it, implementation wise, many examples in diffrent languages
- https://github.com/kanaka/mal


# on Modularity

- #tags: #individual #group #meaning #zeitgeist #meaning
- regarding Modul  mechanisms, JVM uses "Classloaders" to implement "modules"
- was also tried with OSGi
- see gilads post where he uses racket's "Unit" Lib as an example how to do "real" modules
- Ultimately -> all these efforts point towards the global function/data space solution

# the power of wolfram language

- #tags: #talk #programming #perspective
- Dpp What kind of apps he sees written - slide dats predictability Clojure vs scala clojure x 2013
- the data munging can be done with wolfram language in a few lines see runars examples
- https://skillsmatter.com/skillscasts/4921-some-musings-on-scala-and-clojure-by-a-long-time-scala-dude

# power point critique

- #tags: #wikipedia #media 
- http://en.wikipedia.org/wiki/Edward_Tufte /is on twitter!
- “Instead, Tufte argues that the most effective way of presenting information in a technical setting, such as an academic seminar or a meeting of industry experts, is by distributing a brief written report that can be read by all participants in the first 5 to 10 minutes of the meeting.”
- Wieviel kann man in 7 minuten lesen?
- Drei viertel Seite bei schrift größe 10?
- http://de.wikipedia.org/wiki/Kaizen - kein PP

# SICP

- #tags: #sicp #programming
- http://programming-musings.org/2009/12/29/sicp-distilled/ 
- die verlinkte Pdf ist eine SICP kurzfassung

# To Mock a Mocking bird

- #tags: #problem_solving #programming
- Topics coverd in the book : puzels with combinatoric logic, possibly basics to understand typelevel programming:
- http://lambda.jstolarek.com/2013/02/to-mock-a-mockingbird-or-how-i-learned-to-stop-worrying-and-learned-combinatory-logic/

# Clearing - System Design

- #tags: #programming #system #meaning #thoughts
Manuelles Clearing=> ein Zeichen davon das es günstiger ist ein System Unvollständig(siehe worse is better) bzw. gut genug  zu spezifizieren. Alle übrig entstehenden Problem fälle werden manuell (von Menschen) abgearbeitet, mit optionaler Verbesserung der laufenden Prozesse um auftretende Fehler zu vermeiden.
- Wie kann man das manuelle Clearing minimieren und den Feedback Prozess über Maschinelearning automatisch verbessern. Verschiedene Trends in Global agierenden Firmen zeigen den Trend der verlagerung von Arbeitsplätzen in niedriglohn Regionen (Osteuropa, Indien, China).  Schluss endlich werden diese Tätigkeiten aber schließlich maschinell erledigt werden.
- ->  wie Passen BPMS , XSD , WSDL in das Bild , Modellierung von 80-90% und der Rest wird “manuell” mit Clearing abgedeckt?

# Programming social

- #tags: #programming #motivation
- http://de.slideshare.net/CamilleFournier1/keynote-talk-how-to-stay-in-love-with-programming-with-notes
- contrast with Mrb_oks obsession => find via conference tracker?
- dual obervation and obsession

# programming motivation

- #tags: #programming #motivation
- programmingisterrible.com - lessons learned from a life wasted
- NIH:http://programmingisterrible.com/post/73023853878/getting-away-with-rewriting-code-from-scratch

# Value of "Programmers" towards PLs

- #tags: #programming #blog #programming_languages
- ref: http://alarmingdevelopment.org/?p=826
- http://www.eecs.berkeley.edu/~lmeyerov/projects/socioplt/papers/oopsla2013.pdf
- Importance of different factors when picking a language
- Bars how standard Error: E= Extrinsic factor, I = Intrinsic, M=Mixed company size, source slashdot

# Programmer Kompetentz Matrix

- #tags: #stages_of_developement #programming
-Analyse - Auswertung, Kritik, Erweiterung(warum ist die matrix ungenügend -> generatives Testen inwiefern gestaltet sich die Beziehung zwischen Test und Beweis)!
- https://sijinjoseph.netlify.app/programmer-competency-matrix/
- welche blogs kennst du von diesen: http://danluu.com/programming-blogs/

# Wie kommuniziert man Semantik im Code über den Code?

- #tags: #meaning #programming #communication
- z.B.: mit Gesetzen: https://github.com/scalaz/scalaz/issues/278
- Nick Partridge Laws Video

# 3 values of Programmers -  Lary Wall

- #tags: #programming #youtube 
- laziness, percistence, hybris // Dijkstra would not have liked this ;)
- http://www.youtube.com/watch?v=G49RUPv5-NU
- But programming today IS balancing between ALL the hardware/software details and the user wish of program behavior 

# Relating OOP Design Pattern to FP Concepts - Peter Norvig Patterns

- #tags: #peter_norvig #slides #patterns
ref:http://norvig.com/design-patterns/
- insbesondere interessant wie norvig hier (slide 4) design patterns von design strategies abhebt. Hat kent beck diese differenzierung in “implementation Patterns (ref Book) aufgehoben?
-Agent Oriented Programming “ not a new idea ref to 1996
http://stackoverflow.com/questions/327955/does-functional-programming-replace-gof-design-patterns
http://www.defmacro.org/ramblings/fp.html

# SOLID / OO Principles - FP

- https://en.wikipedia.org/wiki/SOLID_%28object-oriented_design%29
- my answer from 2012 https://programmers.stackexchange.com/questions/165356/equivalent-of-solid-principles-for-functional-programming/166263#166263
- https://twitter.com/graninas/status/1382233813569392645?s=28 "Yes, SOLID principles are nicely applicable to FP and Haskell. They are not OOP specific no matter they were used there much more intensively."
- https://dev.to/patferraggi/do-the-solid-principles-apply-to-functional-programming-56lm

# Curry - Church Discussion  Sexy Types – Are We Done Yet? 

- #tags: #erik_meijer #simon_peyton_jones #discussion #youtube #programming #meaning
- ref: https://research.microsoft.com/apps/video/dl.aspx?id=150045
- https://www.youtube.com/watch?v=ov0roDPDBFs
- Erik Meijer tritt hier für optionale Typsysteme ein. Weil Typsysteme immer komplexer werden und Leute ihre Programme lieber zur Laufzeit mit VS debuggen. Letzteres ist wohl kaum ein argument (angeblich erfahrene Entwickler). Es ist lediglich eine Methode um den Zusammenhang zwischen Code und Data zu verstehen. Insbesondere das Zustandsmodell des vorliegenden Codes. Es gibt offensichtlich fundamentale Probleme mit unseren Programmiermodellen für das Phänomen Zeit. 
- Die Erwähnten Ornamente sind nichts weiteres als Reflection?

# sapir whorf für programmierer

- #tags: #programming #society
- http://andrewvos.com/2011/02/21/amount-of-profanity-in-git-commit-messages-per-programming-language/

# Diskusion nicht lineare ODER darwinistische entwicklung von PLs

- #tags: #programming #programming_languages #evolution
- Go vs Algol-68 - http://cowlark.com/2009-11-15-go/
- Folie: 13 http://de.slideshare.net/pcalcado/lisp-macros-in-20-minutes-featuring-clojure-presentation

# twitter anmeldung

- #tags: #programming #experiments
- https://github.com/MitchellMcKenna/twitter-rss-google-apps-script

# peter seibel on reading code - literal coding

- #tags: #programming #perspective
- http://www.gigamonkeys.com/code-reading/
- Code snippets are symbols so you can't (or just up to a certain degree) read but interpret them

# Interview Hal Abelson

- #tags: #programming #interview
- https://web.archive.org/web/20160304055249/http://codequarterly.com/2011/hal-abelson/

# Gotik als Methapher

- #tags: #metaphor #programming #slides
- http://de.slideshare.net/simonstl/the-allure-of-gothic-markup
- Folie 16 System A : OO / System B : FP /?

# Gedanke: Kingdom of nouns, Lesbarkeitsdebatte
 
- #tags: #thoughts #representation #programming #steve_yegge 
englische oder andere natürliche Sprache, anhäufung von verben 
Funktioniert für einfache use cases, skaliert aber nicht da die “Grammatik” fehlt , diese Form der höheren abstraktion bezeichnen wir im CS Umfeld: Algebra
=> siehe auch his Noobs Artikel

# Data - co data

- #tags: #programming #patterns #blog #reddit
- Daten -> Datenstrukturen
- Co Data -> verhalten
- http://debasishg.blogspot.in/2013/01/strict-recursion-non-strict-co-recursion.html
- http://www.reddit.com/r/haskell/comments/163vi2/strict_is_to_recursion_as_nonstrict_is_to/
- ref: the reflective programmer http://www.tele-task.de/archive/video/flash/19283/291

# stuart sierra - thinking in data

- #tags: #talk #conference #infoq #lisp
- http://www.infoq.com/presentations/Thinking-in-Data

# declarative programming

- #tags: #programming #blog #peter_van_roy
- http://michaelrbernste.in/2013/06/20/what-is-declarative-programming.html
- Direkt peter van roy book CTM, functional (‘declarative’) programming has no propertiers ( built in fo rocncurrency!), RT code has its origin in math? Origin?

# Functional Design - Data vs API - gehört zum Blog haskell / types <-> Lisp / data

- #tags: #lisp #programming #interview 
- ref: https://groups.google.com/group/clojure/browse_thread/thread/902597384d7f1b90
- found via: https://web.archive.org/web/20160330043139/http://www.codequarterly.com/2011/rich-hickey/
- sehr gutes rich hickey - fogus interview

# GUI Überlegungen

- #tags: #programming #thoughts
- Unix Kommandozeilen programme lassen sich über Pipes zusammenfügen.
- GUI Programme besitzen lediglich C&P !
- Also Kommunikationsmedium werden dateien verwendet, sie sind nicht im betrieb kombinierbar. hintereinander ausführen und zwischen ergebnisse über dateien vermitteln

# Objekte Aktoren <-> Comonade

- tags: #patterns #programing
- http://www.haskellforall.com/2013/02/you-could-have-invented-comonads.html
- Comonade als Objekte
- Objekte sollten keine getter/setter haben , alan kay
- http://patternsinfp.wordpress.com/2011/01/31/lenses-are-the-coalgebras-for-the-costate-comonad/
- inductive and co inductive types  http://www.cs.ut.ee/~varmo/papers/thesis.pdf
- http://stackoverflow.com/questions/16015020/what-does-coalgebra-mean-in-the-context-of-programming

# Haskell -  Modularity - Preludes

- #tags: #programming #blog
- Preface - explain what is a Prelude
- Advantages/Disadvantages
- The many Preludes http://www.yesodweb.com/blog/2013/01/so-many-preludes

#  Neil Ford on presentations

- #tags: #infoq #interview
- http://www.infoq.com/interviews/neal-ford-technical-presentations
- fade on inner topic slides 
- push or cube on chapter transitions
- => better examples wikidata presentation with sozi from 2014 at wikimania by that uni guy
- John bakcus - Function level Programming leads to J Programming Language
- Applicative Programming
- fundamental building blocks resemble LISP

# Testing and programming

- #tags: #progamming #testing #perspective
- http://programmers.stackexchange.com/questions/158052/are-unit-tests-really-that-useful/158057#158057 -> 1124 mal soviel testcode
- http://sqlite.org/testing.html

# FP-imperative

- #tags: #programming #thoughts
- ref Martin odersky Keynote devoxx fr (2015 or earlier)
- FP <-> imperative comparison 
- mutable vs FP transformations vs modificatipons
- Ich würde sagen unsere Vorstellung von zeit und sequetiellem Ablauf ist mit FP besser modelliert- 

# twitter wars - Erik Meijer vs agile / uncle bob

- #tags: #twitter #blog #erik_meijer #programming #society #culture #perspective #meaning
- First they ignore you, **then they laugh at you**, then they fight you, then you win:
- https://twitter.com/headinthebox/status/533564805161181184?refsrc=email
- http://blog.cleancoder.com/uncle-bob/2014/11/12/PutItInProduction.html
- http://blog.8thlight.com/uncle-bob/2014/01/27/TheChickenOrTheRoad.html
- notice I used the word "wars",  communication culture...

# Strange loop ovarian Cancer talk

- #tags: #cancer #infoq #lisp #conference #talk
- ref: http://www.infoq.com/presentations/ovarian-cancer
- 300 lines of production code ( many macros)
- Using multiple datasources ( matching algorithms?)
- see also steve yegge and SPARK video

# HPI Video und ponderings

- #tags: #programming #lecture #youtube
- HPI Material von spacy emfohlen
- http://www.tele-task.de/archive/video/flash/19251/0
- der Vortrag ist gar nicht so schlecht,
- allerdings ist sein Englisch schlecht und es gibt schon einige Mängel
- bei 20 minuten sagt er das das "runtime" system der beste ort sei über parallelisierung zu entscheiden,
- ich weiß was er damit sagen wollte aber der Ausdruck ist falsch
- denn tatsächlich kommt es nicht nur drauf an wieviel CPU Kerne du unten drunter hast sondern auf deine daten
- die daten entscheiden ob sich parallelisierunhg lohnt
- es gibt auch Probleme und algorithmen die sich nicht parallelisieren lassen
- einerseits jammert er über hypes andererseits sieht man das er den vortrag für sich erstellt hat, er hat also (zu) wenig darüber nachgedacht wie man die Konzepte vermitteln kann
- er verweßt  einfach auf seine vorherrigen Kurse (viel erzählen) irgendwas wird beim studenten schon hängen bleiben
- begrifflichkeiten wie: asoziative funktionen sind SEHR wichtig
- seine bemerkung das es Bildungs begründetes Problem ist
- ist schon mal ein schritt in die richtige richtung
- bei 41 sagt er schließlich das Data Parallelism sich nicht gut auf task parallesim anwenden läßt ( also im prinzip der Wiederspruch zu seiner aussage von 20, das "System sei der beste Ort um über "parallelisrung zu entscheiden
- wenn ich doch bevor ich mein problem lösen kann über legen muss welches paradigm a und framework ich verwende , macht es doch keinen sinn mich zu "freuen" das MapReduce "funktionoiert" , das problem wurde lediglich eine ebene weiter nach oben verschoben , was nicht unbedingt schlecht ist
- Die Unterscheidung wann welche Tools zu benutzen sind ist wichtig, hier eine bessere bespreibung/Unterteilung
http://www.youtube.com/watch?v=RVmY2lQ4DHE
- Bei 45 min resümiert er, aber nur mündlich er schafft es nicht in den Slides zu resümieren , das ist schlecht.
- LINQ ist ( was ich übrigens auch in meinem Vortrag erwähnte) eigentlich nur eine funktionale DSL in C#/ vb..
- Die unterscheidung zu FP ist also komplett nutlos das es genau das ist was er zuvor beschrieben hat map, reduce, runtime System...
- Der Titel " implzizierter " Paramllesium ist schlecht.
- denn die API hat ggf. 
- bei 52 ist er verwirrt implizit/explizit ist eher 
- imperativ / funktional
- und selbst beim funktionalen ( seinem "implizieten") kann man einiges konfigurieren und muss einiges beachen ( don't care ansatz klappt in einigen anwendungsfällen aber nicht allen,
- wenn du mehr hierzu wissen willst such bei stackoverflow nach den tags: scala parralel collections 
- Einerseites sagt er es sei gut das eine sprache aus der akademischen ecke kam
- andererseits sagt er nur Industrie VMs die 20 jahre alt sind taugen was, frage also -> warum gibt es keine vergleichbaren VMs aus der ak. Ecke?
- 1005 case classes "smell" like abstract classes -> ich denke nicht das die irgendwie riehcen
- Natürlich such man immer gemeinsamkeiten wenn man etwas neues lernt aber riechen...

# People

- #tags: #clojure #community #lisp #programming
- Rich Hickey, Stuart Halloway, David Nolen, Michael Fogus
- cursive: Collin fleming (newzealand)
- @aphyr Kyle Kingsburry, Rieman , podcast: http://thinkrelevance.com/blog/2014/01/30/kyle-kingsbury-cognicast-episode-048 I wonder if his leather thing devloped because of society or something other...
- Brian Beckman(see his interview with Rich hickey)
- podcast: relevance/cognicast

# Conferences

- #tags: #programming #conference
- strange loop
- conj

# @aphyr on programs and meaning

- #tags: #programming #meaning #social #relationship #twitter
- https://twitter.com/aphyr/status/576413782685036544
- https://twitter.com/aphyr/status/576415853412941824
- https://twitter.com/aphyr/status/576416256774905856 <- that can and should change right?

# Getting started

- #tags: #programming #lisp
- https://yogthos.github.io/ClojureDistilled.html

# Clojure React - OM

- #tags: #programming #twitter #lisp
- how to think an impossible thought, change
- https://swannodette.github.io/2013/12/17/the-future-of-javascript-mvcs
- react start popular: https://twitter.com/zpao/status/868211941856002052
- https://twitter.com/curious_reader/status/868374623808876544 https://twitter.com/curious_reader/status/868375154962952192

# Lisp Talks

- #tags: #programming #lisp
- Simple Made Easy
- Hammock Driven Developement
- see FF bookmarks
- https://github.com/matthiasn/talk-transcripts/tree/master/Hickey_Rich

# Spec-ulation Keynote - Rich Hickey

- #tags: #programming #lisp #youtube #twitter
- https://www.youtube.com/watch?v=oyLBGkS5ICk
- semantic versioning is broken, beyond repair
- use convention over configuration to fix the "lib problem" , this is in fact hte same problem as morte tries to fix
- names, Levels, Context , Scope
- functions, namespace, atrifacts
- provide vs required
- Growing your softwre: Accretion, Relaxtion, Fixation
- 'change' is not a thing (under specified)
- the value of FP: remove anxiety of mutability, foot :) https://youtu.be/oyLBGkS5ICk?t=2711
- rich says : its a social thing
- "we need to bring FP to the library ecosystem" - again morte?
- perspective from static people: https://twitter.com/ezyang/status/809917685479907328

# ClojureScript for Skeptics - Derek Slager

- #tags: #youtube #lisp #programming
- https://www.youtube.com/watch?v=gsffg5xxFQI
- state cljs tools 
- whats good and whats not

# Paul deGrandis - Unlocking data-driven systems

- #tags: #progamming #lisp #youtube #talk
- https://www.youtube.com/watch?v=BNkYYYyfF48
- @ohpauleez
- the value of values, a spectrum
- produce in hours not days or weeks
- constraint definition of a service
- programming with values, open for extension, code and data rollback(this is huge!), datalog enforced service wide properties
- data described clients, rendering in 10 ms => Apps like TV channels: Loaded/modified Live, versioned, rolled back, _queryed_ / analysed
- how did core.logic fit in _exactly_ ?
- project metrics: two developers, each project (3) took 16-24 days, kickoff to final delivery, functionally complete in 12-16 days, design time 4-8 days ( 1-2 weeks), ~ 50% Design Time!
- using, Clojure, clojureScript, datomic and core.logic allowed for exponential impact
- the workflow:"data driven" :  be exploraty, think hollistically , **write it down** <-this, is the critical part, writing it down thinking about it , discussing about it, constrain the design space, think critically, think slowly, envision outcomes and possibilities 
- => and unlock data-driven systems 

# super powers of datomic

- #tags: #progamming #lisp #youtube #talk
- https://www.youtube.com/watch?v=7lm3K8zVOdY

# Colin Fleming - Cursive: A different type of IDE

- #tags: #youtube #lisp #programming
- https://www.youtube.com/watch?v=vt1y2FbWQMg
- static analysis for clojure via IDEA

# Colin Fleming - podcast

- #tags: #programming #lisp #podcast
- http://blog.cognitect.com/cognicast/077
- IDE technology for Clj, debugging
- work less , find balance!

# Rich Hickey clojure conj 2017

- #tags: #progamming #lisp #youtube #talk
- https://www.youtube.com/watch?v=2V1FtfBDsLU&t=3127s
- terrible talk, lots of bias and misunderstanding
- types can help, yes they are complex
- at 30 clojure lang for cranky old programmers
- database person and mailing list person merge problem? A type problem? See guptas SQL Database post for salvation
- also isomorhisms do work and are useful, also free monad and other "interpreter-patterns"
- reactions to that talk: eric nomand - clojure programmer with some? haskell experience- he knows conals work, gave talk about it in clj but without explicit CT? - http://www.lispcast.com/clojure-and-types
- haskell programmers, implementing clojure(edn) in haskell: http://tech.frontrowed.com/2017/11/01/rhetoric-of-clojure-and-haskell/
- and yes seems right hickey doesn't know about 'lens' and other haskell tricks

# Joy Clark - Rich Hickey Interview

- #tags: #podcast #lisp #programming
- http://www.case-podcast.org/20-problem-solving-and-clojure-19-with-rich-hickey
- see Alan Kays why data is bad argument, Rich Hickey tries to fix the practice of industry, AK wants to move to a paradigm beyond "Industry"
- I think its related to what dijkstra had in mind when he thought about the relationship between academia and industry
- dependecy/classpath stuff and codeq sounds good
- JC needs perspective 
- what other problems are there to solve? ;)
- self 
- hickey: papers today are just math, scala - what is math/programming 
- brittle: what does he mean by that
- JS community adopting ideas from cljs
- history of programming languages 
- OO meaning, words and meaning

# FB Riddle solved in Prolog

- #tags: #code #twitter
- TODO: solve me in core.logic
- https://twitter.com/ohunt/status/589117665584619520

# Hash Map swap key values

- #tags: #code #lisp
- (defn flip-map [m] (zipmap (vals m) (keys m)))

# fizzbuss core.match

- #tags: #code #lisp
```
#!clojure
(use '[clojure.core.match :only (match)])

(defn fizzbuzz
  """
  Print each number from 1 to 100, but for multiples of 3 print "Fizz"
  instead, and for multiples of 5 print "Buzz". For multiples of both
  print "FizzBuzz
  """
  []
  (doseq [n (range 1 101)]
    (println
      (match [(mod n 3) (mod n 5)]
        [0 0] "FizzBuzz"
        [0 _] "Fizz"
        [_ 0] "Buzz"
        :else n))))
```
- https://github.com/swannodette/logic-tutorial
- http://www.reddit.com/r/dailyprogrammer/comments/v3afh/6152012_challenge_65_intermediate/
- https://gist.github.com/zerokarmaleft/3219448
- cheat sheet: http://clojure.org/cheatsheet

#  clojure notes

- #tags: #code #lisp
- Protocols unify typeclasses and classes
- Leiningen
- REPL
- statische Methoden mit :  (-main)
- Texteditor changes im REPL nachladen: 
- (use 'a-game-of-thrones.core :reload-all)
- to get sources from template to be included use: :main my-stuff.core
- if project is names: my-stuff and main source file: core.clj
- debugging macro - http://www.learningclojure.com/2010/09/clojure-macro-tutorial-part-i-getting.html

# Parallel ponderings , function elegance

- #tags: #programming #code
- https://groups.google.com/group/clojure/browse_thread/thread/f112828c5d34ba44/b883eae6455dcaf9?lnk=gst&q=Paul+butcher#
- https://github.com/paulbutcher/foldable-seq
- https://github.com/paulbutcher/foldable-seq-example
```
#!clojure
 (defn map-count [amap key] (assoc amap key (inc (get amap key 0))))

user=> (reduce map-count {} ["hi" "mum" "hi" "dad" "hi"])
```
- Parallel!

# SICP in Clojure

- #tags: #sicp #lisp #programming
- check on that later: https://www.kickstarter.com/projects/1751759988/sicp-distilled/posts?page=2
- SICP reworked: http://sicpebook.wordpress.com
- http://sarabander.github.io/sicp/html/index.xhtml
- finally! http://www.sicpdistilled.com/ at least the meta evaluator is online: http://www.sicpdistilled.com/section/4.1/
- Braucht: leinigen 
- Start: https://github.com/swannodette/lt-cljs-tutorial/

# Scala

- #tags: #programming #context
- I came to scala through daniel spiewaks blog code commit: http://www.codecommit.com/blog/scala/scala-collections-for-the-easily-bored-part-2
- I read the blog when it was written and even commented on that one again in 2010

# People

. #tags: #programming #community
- Scala is a big tent, quote by James Iry
- Greg M and MDP4TW
- 2 major groups of influential people in scala : comming from java seeking for "power", comming from FP seeking for "JVM"
- many people from the "comming from FP" camp , came from academia
- tony ( although he left ), runar, paul chiusano, russp, dpp, paul snively, daniel spiewak, debasish gosh,
  cedric (read his blog when he wrote it, he tried to delete it but wayback machine unveiled his shameful past, see Links for URL), 
  paul butcher (who went to clojure), akka: jonas Boner viktor klang, martin krasser, stephan schmidt (codemonkeyism blog), Daniel Sorbal,
  David Pollack and how he went to Clojure, Play (france) community - Mandubian, Nathan Ham. from Mettup (coderspiel blog), Jim McBeath,
  Lars Huppel went to innoq berlin ,Miles Sabin, travis Brown, john de Goes, chris nuttycom
- dr mac  iver : left Scala community early wrote some collection impls. 
- Geo communities: Strong FP community from australia (tony, they complain and still use Scala Ken scambler, brian McKenna  ) and underscore consulting in the UK, japanese community

# Conferences

- #tags: #programming #conferences
- ScalaDays: Early scala days looked liked academic conferences because it grew from academia, Videos took ages to publish, lately 8 since typesafe) more professional marketing
- went 2014 to ScalaDays Berlin
- North East Scala Symposium , small alternative conference
- Lanbda Jam in Ausi

# Debates

- #tags: #programming #meaning #twitter #youtube
- "Programming is Math"?! arround that time https://twitter.com/curious_reader/status/289347984297181184 jan 2013
- https://twitter.com/posco talk about _programming is not math_ http://www.youtube.com/watch?v=JF-ttZyNa84
- daniel spiewak, posco at NeScala 

# Talks

- #tags: #programming #conference #talk
- https://www.youtube.com/watch?v=DBu6zmrZ_50 scala in 2018 - talk from 2013
- that keynote from Spring rod...

# Category theory and machine learning

- #tags: #programming #code
- Scala , https://github.com/mandubian/neurocat

# Really good, real World Scala Web Experience Report

- #tags: #twitter #slides #programming
- @KenScambler , from australia, in the school of tony/ericT/..., he has a lot more of FP stuff, some good blogs
- https://twitter.com/evolvable/status/588661547846672385
- slides: http://www.slideshare.net/kenbot/2-years-of-real-world-fp-at-rea

# Akka critique

- #tags: #programming #actors #blog
- http://stew.vireo.org/posts/I-hate-akka/

# SBT History

- #tags: #programming #perspective #youtube
- Mark harrah founded the tool then went to typesafe
- wrote on apocalisp with runar
- designed SBT 0.7 "OO" - mutation style, there after with great suffers.. immutable FP...
- Problems with java dep. management: https://www.youtube.com/watch?v=TMQWFSM9_1A&index=14&list=PLndbWGuLoHeaOpTHoNhelI4NdnSqpeXA7 _
- founded as a response to that talk(last msg from may 2014...):  https://groups.google.com/forum/#!forum/adept-dev => ironically this points to the global function space solu

# how scala found its place ( ocaml and haskell regarded esoteric)

- #tags: #programming 
- http://channel9.msdn.com/Blogs/Charles/JAOO-2007-Bob-Martin-and-Chad-Fowler-Debating-Static-versus-Dynamic-Typing
- Chad Fowler -> Wunderlist

# Enterprise Scala by John de Goes

- #tags: #programming
- https://gist.github.com/jdegoes/83531f9bfc22578912e4

# learning scala

- #tags: #programmig
- http://www.scalakoans.org/finding-path
- properties tree build excercise

# Merging maps in Scala

- #tags: #programming #thoughts
- one can find numerous requets /questions on the scala ML for such a thing although there operations can be well defined in CT -> see scalaz monoid an semigroupo definitions for maps
- so the question is: do you like to write code? -> enlarging the entropy?!

# Reactive Streams SPI

- #tags: #programming
- via: https://twitter.com/jetbrains/status/560723883209531392
- rxJava - Netflix
- reactor - Pivotal
- vert.x redHat
- akka-streams - typesafe
- spray - spray.io, oracle
- scalaz-stream (pull) vs "ractives" (push?)

# tonys talks

- #tags: #programming #tony_morris
- https://github.com/tonymorris/talks
- Monad Transformers BFPG. 2017-06-13
- Let's Lens, Lambdaconf. Boulder, Colorado. 2017-05-26
- Introduction to Functional Programming, Lambdaconf. Boulder, Colorado. 2017-05-25
- Functional Programming in Aviation BFPG. Brisbane, Australia. 2016-08-09
- Functional Programming, Parametricity, Types YOW! West. Perth, Australia 2016-05-03
- The Expression Problem and Lenses Lambdajam. Brisbane, Australia 2016-04-28
- The Essential Tools of Open-Source : Functional Programming, Parametricity, Types linux.conf.au FP miniconf. Geelong, Australia 2016-02-02
- Perhaps There is a Much Better Way DjangoCon 2014 conference. Brisbane, Australia 2014-08-01
- Scalaz: the history, the motivation, the battles, the future LambdaJam 2014 conference. Chicago, USA 2014-07-23
- Parametricity, Types are Documentation YOW! West conference. Perth, Australia. 2014-05-13
- Parametricity, Types are Documentation LambdaJam conference. Brisbane, Australia. 2014-05-09
- A Modern History of Lenses LambdaJam conference. Brisbane, Australia. 2014-05-09
- Comonads, Applicative Functors, Monads and other principled things Codemania conference. Auckland, New Zealand. 2014-04-04
- Monad Transformers BFPG. Brisbane, Australia. 2013-08-27
- Zippers, Comonads & Data Structures in Scala Lambdajam. Brisbane, Australia. 2013-06-21
- Comonads, Applicative Functors, Monads and other principled things BFPG. Brisbane, Australia. 2014-03-25
- Explain List Folds to Yourself BFPG. Brisbane, Australia. 2013-04-23
- **==>** What Does Functional Programming Mean? BFPG. Brisbane, Australia. 2010-03-24
- **==>** Applicative Programming, Disjoint Unions, Semigroups and Non-breaking Error Handling BFPG, 2010 April
- **==>** What Does Monad Mean? BFPG. Brisbane, Australia. 2009-11-17
- **==>** Introduction to High-level Programming with Scala 2008-07-28


# Code

- #tags: #programming #code
```
#!scala
def countWords(text: Array[String]):Map[String,Int] = {  
  //getf is similar -assoc- function from clojure
  val getf = (m:Map[String,Int],key:String) => {val r=m.get(key); if(r.isDefined) r.get else 0 }
  
  text.foldLeft(Map.empty[String, Int]){ case(m,w)=> m + (w -> (getf(m,w) + 1))}
}
//import scala.collection.mutable.Map
def countWords(text: Array[String]) = {
          val counts = Map.empty[String, Int]
          for (word <- text) {
            val oldCount = 
              if (counts.contains(word))
                  counts(word)
              else
               0
            counts += (word -> (oldCount + 1))          }
          counts
        }

 P12/13
 def main(args: Array[String]): Unit = {
    
      
  val data=List((4, 'a), (1, 'b), (2, 'c), (2, 'a), (1, 'd), (4, 'e))
  
  
    //soll List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)
    println(encodeDirect(decode(data)))
  }
  
  def decode[A](xs:List[(Int,A)]) = {
    val n:List[A] = Nil
    xs.foldRight(n){ case(a,b) => List.fill(a._1)(a._2) ::: b}
  }
  
  def encodeDirect[A](xs:List[A]):List[(Int,A)] = {
       def accu(xxs:List[A],last:A,  acc: List[(Int,A)] ): List[(Int,A)]  = {
       if (xxs.isEmpty)
         acc
       else
       {
         if (acc.isEmpty){
           val h=xxs.head
           accu(xxs.tail, h,(1,h) :: Nil)
         }
         else
         {
           val h=xxs.head
           if(h != last){
             accu(xxs.tail,h,(1,h) :: acc)
           }else{
                val acc_h=acc.head
                accu(xxs.tail,h,(acc_h._1 +1,acc_h._2) :: acc.tail)
           }
             
          
         }
             
         
       }
      }
    
       val n :List[(Int,A)] = Nil
       if(xs.isEmpty)
           Nil
       else
    	   accu(xs,xs.head,n).reverse //prepend
  }
  

```

#  Code/Sw as a Media , Geek Culture

- #tags: #programming #thoughts
- https://twitter.com/jennschiffer/status/546858442288529412
- Geek Sublime, Vikram chandra
- found via twitter david nolen

# TDD - Testing

- #tags: #programming #thoughts
- watching recent talks generative testing etc... , it came to my mind that Testing mostly is about defining what your system can do,
 but _very_ much more importantly what it can not do, so I don't know if programming's dual is co-programming,
 or if this issue is about denotational semantics, but it has to do something with the limits what the programm can do 

# Transition Change Programming

- #tags: #programming #thoughts
- Craig david:  “transformation to FP programmer” - WHy it wasnt easy?
- Zeitgeist abstract or vague?
- Abstraction in the dijkstra sense means it is comprehensible because we can associate the abstract with the instance, instances of zeitgeist?

# REST

- #tags: #programming #thoughts
- REST betriebsubergreifendes Domain Model widerstand aus teilabteilungen

# Was bedeutet das Studium/Warum das studiert ?

- #tags: #programming #thoughts #meaning #institutiions #work #agent_arena_relationship
- prämisse: beim Studium wurde zuviel zu falsch unterrichtet
- zu wenig kommunikation ( keine übernommenen Projekte von anderen studenten, gesetzt den fall das es für Studenten die Pflicht gäbe Ein SW projekt zu erstellen was danach gewartet wird und ein projekt zu warten was erstellt wurde, was würde gewählt werden?
- zu wenig (bewußte) selbst reflektions techniken ( siehe Star Trek), reading writing Balance
- zu wenig reflektions techniken auf die Informatik selbst, was eine historische Betrachtung bedeutet: Was aus der Geschichte ist Relevant? Turing,Church, von Neumann, Zuse, McCarthy, Dijkstra, Paul graham , Joel Spoesky
=> identitfizieren von Individuen  und reflektieren über ihre essays mögliche Beispiele
- zu wenig einordnung in das gesamt gesellschaftliche bild, des
-zuviel MS Office, Amazon hat das partiell erkannt -> siehe SPJ und was CS unterreicht in england war
-zuviel konkretes muss auswendig gelernt werden
- Was bedeutet es programmier zu sein?
- Wie stell ich mir meine Arbeit Vor?
- Gibt es Code reuse
- Wie kann ich über meine Arbeit reflektieren
- langfristige Perspektive
- Zertifikate-Ersetzbarkeit
- => Was möchte ich erreichen und kann ich das alleine erreichen ,
- Was also wenn ich andere Leute erreichen will, Wie kann ich andere Leute ( programmierer) erreichen, verstehen?
- Individuum gegen Konzerne -> LISP , Dijkstra siehe aber auch MIK, 
- Generalist gegen Spezialist
- Alan Kays rinsaal Beispie wie langsam wir lernen, Die größten(effektiv,anderes) Fortschritte finden wir beim perspektiven wechsel, das - problem aus einer anderen Dimension betrachten
- Alan Kay Programming and Scale
‘The worst thing happening to a programer might be his first programming language’
‘the best class I took in programming...detroyed everything and rebuild from scratch ’
Was also tuen um die geistige Verschmelzung mit der ersten Programmiersprache zu verhinden? 2 oder mehr sprachen gleichzeitig lernen?
-> Warum nutzen große Firmen nicht das Potential ihrer Mitarbeiter bei großen Umschwenkungen? Die meisten Menschen sind zu unflexibel sich anzupassen, warum? Ich vermute das korreliert exakt mit dem Phänomen der Prägung durch die erste Programmiersprache. Ist es also ein inhärentes Problem oder lernen wir die Dinge nur falsch? →  lernen wir das “Lernen” falsch?

# Motivation

- #tags: #programming #thoughts #perspective #dialectic
- Ist es das Schicksal der jungen zu sagen: Der Status Quo ist gar nicht so Schlimm
- und das der alten zu sagen:	Früher war alles besser, der Status Quo ist eine Katastrophe
- Ausweg: Alan Kay: The best way to predict the Future is to invent it
- related
- Das konzept der Zirkularität schient den Menschen schon länger bekannt zu sein, der Begriff hier heißt: Ouroboros
- Dateiformate -Serialisierung
- vernachlässigtes Thema: DatenStruktur implementierungen
- Tags: Strings S-Expressions JSON,XML, mark-Up
-  push pull entwicklungs mechanismen
- Art der Organisation des Kontroll flusses
- Stichwörter: Push / Pull Modelle, Dependency Injection, dont call us, we call you, Hollywood pattern
