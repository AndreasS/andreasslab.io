// Compiled by ClojureScript 1.10.597 {:static-fns true, :optimize-constants true, :elide-asserts true}
goog.provide('simpleexample.core');
goog.require('cljs.core');
goog.require('cljs.core.constants');
goog.require('reagent.core');
goog.require('reagent.dom');
goog.require('clojure.string');
if((typeof simpleexample !== 'undefined') && (typeof simpleexample.core !== 'undefined') && (typeof simpleexample.core.timer !== 'undefined')){
} else {
simpleexample.core.timer = reagent.core.atom.cljs$core$IFn$_invoke$arity$1((new Date()));
}
if((typeof simpleexample !== 'undefined') && (typeof simpleexample.core !== 'undefined') && (typeof simpleexample.core.time_color !== 'undefined')){
} else {
simpleexample.core.time_color = reagent.core.atom.cljs$core$IFn$_invoke$arity$1("#f34");
}
if((typeof simpleexample !== 'undefined') && (typeof simpleexample.core !== 'undefined') && (typeof simpleexample.core.time_updater !== 'undefined')){
} else {
simpleexample.core.time_updater = setInterval((function (){
return cljs.core.reset_BANG_(simpleexample.core.timer,(new Date()));
}),(1000));
}
simpleexample.core.greeting = (function simpleexample$core$greeting(message){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.cst$kw$h1,message], null);
});
simpleexample.core.clock = (function simpleexample$core$clock(){
var time_str = cljs.core.first(clojure.string.split.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(simpleexample.core.timer).toTimeString()," "));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.cst$kw$div$example_DASH_clock,new cljs.core.PersistentArrayMap(null, 1, [cljs.core.cst$kw$style,new cljs.core.PersistentArrayMap(null, 1, [cljs.core.cst$kw$color,cljs.core.deref(simpleexample.core.time_color)], null)], null),time_str], null);
});
simpleexample.core.color_input = (function simpleexample$core$color_input(){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.cst$kw$div$color_DASH_input,"Time color: ",new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.cst$kw$input,new cljs.core.PersistentArrayMap(null, 3, [cljs.core.cst$kw$type,"text",cljs.core.cst$kw$value,cljs.core.deref(simpleexample.core.time_color),cljs.core.cst$kw$on_DASH_change,(function (p1__5748_SHARP_){
return cljs.core.reset_BANG_(simpleexample.core.time_color,p1__5748_SHARP_.target.value);
})], null)], null)], null);
});
simpleexample.core.simple_example = (function simpleexample$core$simple_example(){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.cst$kw$div,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [simpleexample.core.greeting,"Hello world, it is now"], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [simpleexample.core.clock], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [simpleexample.core.color_input], null)], null);
});
simpleexample.core.run = (function simpleexample$core$run(){
return reagent.dom.render.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [simpleexample.core.simple_example], null),document.getElementById("app"));
});
goog.exportSymbol('simpleexample.core.run', simpleexample.core.run);
