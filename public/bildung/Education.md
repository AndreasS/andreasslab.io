 [[_TOC_]]


# Tolstoi

- #tags: #vorbild #wikipedia #tolstoi
- http://de.wikipedia.org/wiki/Lew_Nikolajewitsch_Tolstoi
- Arbeits Moral
- Pädagogische Reformbestrebungen: http://de.wikipedia.org/wiki/Lew_Nikolajewitsch_Tolstoi#P.C3.A4dagogische_Reformbestrebungen
- https://en.wikipedia.org/wiki/Zeitgeist#Theory_and_leadership
- Briefwechsel mit Gandhi (via Jordan Peterson) https://en.wikisource.org/wiki/Correspondence_between_Tolstoy_and_Gandhi

# Konfuzius

- #tags: #konfuzius #wikipedia
- http://de.wikipedia.org/wiki/Konfuzius
- “Harmonie und Mitte, Gleichmut und Gleichgewicht“ galten ihm als erstrebenswert. Den Weg hierzu sah Konfuzius vor allem in der Bildung.”

# Noam Chomsky 

- #tags: #talks #education
- Education, What and for Whom
- Education Is a System of Indoctrination of the Young

# Maria Montessori

- #tags: #montessori #youtube
- Sketch Note explanation: https://www.youtube.com/watch?v=GcgN0lEh5IA
- AK refs


# TED Talk von Sugata Mitra -  Build a school in the cloud

- #tags: #ted #sugata_mitra #sole #education
- Inhalt:
    - Hole in the wall
    - SOLE Environments
    - impossible thought: knowing is obsolete?

- http://www.ted.com/talks/sugata_mitra_build_a_school_in_the_cloud.html
- https://www.theschoolinthecloud.org with videos from SOLE Experiments

# SOLE - Self Organized Learning Environment

- #tags: #ted #sugata_mitra #sole #education
-  is a place where children can work in groups, access the internet and other software, follow up on a class activity or project or take them where their interests lead them

# Future Learning

- #tags: #youtube #sugata_mitra #sole #sal_khan
- https://www.youtube.com/watch?v=qC_T9ePzANg Future Learning | Mini Documentary | GOOD  Sugata at the end :
    What future learners will need: 
    1. Reading comprehension skills 
    2. Search and retrieval skills 
    3. The ability to believe, as an armor against doctrine !!! ( I think thats just Alan Kays media Gorillas)

# The Future of Learning @ CUE ( some UK thing) 30.03.2015

- #tags: #youtube #sugata_mitra
- https://www.youtube.com/watch?v=y-e9WRMWcdI
- funny! His car stories
- 7 schools in the cloud
- some results of schools in the cloud, some more results in 18 months after, re check!

# School in the cloud book

- #tags: #twitter #sugata_mitra #book
- https://twitter.com/CorwinPress/status/1168917069158658048
- book : school in the cloud
- recap so far, from whole in the wall until today, recap on scool in the cloud experiments for india, other new efforts 

# 2019 Webniar recap

- #tags: #youtube #sugata_mitra #sole
- https://www.youtube.com/watch?time_continue=1182&v=aAsFGX458z8
- principles of SOLE - Self Organised Learning Environment:
- Meta I : Children can learn anything if:
- 1. they are unsupervised 
- 2. in heterogeous groups
- 3. using the internet
- 4. through large , visibile screens in a safe and public space
- Meta II : Children learn even faster if there is a “Granny - even remote “
- ideas to imrove current school -initiating change : 
- 1. Introduce Internetas a formal subject in school 
- 2. Introduce Complex Dynamical Systems as a subject
- 3. Divide existing curriclum in three parts: necessary for life, feels good to know, exam
- 4. Allow internet during exam
- from hole in the wall to end of nowing
- How to use a SOLE in class today: give a question let children explore then assess after - fill gaps and lift up where needed
- Best way of teaching a SOLE ist to try it out...

# KenFM im Gespräch mit: Gerald Hüther (“Mit Freude lernen”)

- #tags: #youtube #kenfm #gerald_hüther #book #youtube #lernen
- https://youtu.be/dzHhHs2bmvg (original broken)
- https://www.youtube.com/watch?v=6QMziaZ_CYw
- 45 min persöhnlich angegriffen
- schule eingebettet im System
- Maul halten
- kohärenz
- Würde
- “ eigentlich müssten die eltern allen aufstand machen und müssen sagen nicht wir wollen dass aus unseren dass kein kind in unserer schule diese angeborene freude am eigenen lernen und entdecken am gestalten verliert” https://youtu.be/dzHhHs2bmvg?t=3903
- Identität, Individualität, stufen 
- rauchen als ritual zur kreativarbeit
- singen als Erniedrigungs beispiel (martin)
- Koheränz, Zwei Grundbedürfnisse :
- Verbundenheit 
- Wachstum - Weiterentwicklung  

# #NWX19 - Gerald Hüther - Die Wiedererweckung von Intentionalität und Co-Kreativität

- #tags: #xing #newwork #gerald_hüther #co_kreativität 
- https://www.youtube.com/watch?v=66aQoRlF-eQ
- make some notes its only 25 min..

# School - Teachers

- #tags: #youtube #bill_gates #education
- Bill Gates, How US should catch up to china and others: Teachers need real feedback: https://www.youtube.com/watch?v=81Ub0SMxZQo

# Instituions

- #tags: #impossible_thought #deschooling_society #departments #institutions #education
- keine Fachbereiche, no departments, re from alan kay talk and found via p_e tweet, truly interdisciplinary http://www1.lehigh.edu/mountaintop/ https://www.youtube.com/watch?v=68176_mXukk
via : https://twitter.com/nikhilgoya_l/status/546766838223618048?refsrc=email&s=11

# Richard David Precht Bildung vs Wissen

- #tags: #youtube #bildung #wissen #richard_david_precht
- https://www.youtube.com/watch?v=ZXhug_e43tk
- Mathematik unterricht
- das Problem mit den Kultus ministern

# Digital Aristotle

- #tags: #youtube #aristotle #learning_agent

- I finally found a better metapher for tweedls/cards : digital aristotle !! <- couple this with Ted Nelsons everyting is intertwingled
- https://www.youtube.com/watch?v=7vsCAM17O-M
- https://subbable.com/cgpgrey <- I think he is successful because of his voice!

# Neil Stephenson

- #tags: #book #neil_stephenson #learning_agent
- #book#: Diamond age
- http://proto-knowledge.blogspot.de/2011/11/building-young-ladys-illustrated-primer.html?m=1
- found via: https://news.ycombinator.com/item?id=12469032

# scratch children programming

- #tags: #scratch #steve_krouse #blog
- https://stevekrouse.com/the-trick-to-teach-anonymous-functions-to-11-year-olds-558b697d7a53#.8urimd6cz
- link dead, maybe on github?

# Kindermedien

- #tags: #kinder #geschichten #book
- Pipi langstrumpf
- der kleine Prinz 
- gyges und sein Ring 
- Pinocchio 
- großes Busch Buch 
- Erzähl mir was
- Jules Verne
- Tausend und eine Nacht
- Peanuts für Kinder
- Hans Christian Andersen
- Hauffs Märchen (Karten?)
- Nordische Mythologie: Thor, Odin
- griechische/römische Mythologie
- australische traumzeit mythen
- Gulivers Reisen
- Alice im Wunderland (get annotated alice, kindle edition?, working through may be combine with: the victorian internet?)
- tausend und einer nacht- alf laila wa-laila:
- http://de.wikipedia.org/wiki/Tausendundeine_Nacht
- der Zauberer von OZ (50er version) Konflikte ohne Gewalt lösen
- Einige bücher (Cat?) von https://en.wikipedia.org/wiki/Dr._Seuss ?
- detective dot https://www.detectivedot.org/pages/faq
- tiptoi pen Ravensburger

# Lager Feuer

- #tags: #kinder #experimente #feuer
- Farben von kosmischen Supernoven am lagerfeuer erklären mit pulver aus: Natrium, stronzium, kupfer, calcium
- aus BBC doku ... der sterne teil 2

# MOOCS

- #tags: #mooc #gedanken 
- Abbrecher quoten und andere Probleme -> alternativ WOW als MOOC?
- “Selbständige Lerner bilden aber nur eine kleine Minderheit unter den Studierenden an den Hochschulen.
Die eigentliche Frage ist also, ob wir Studierende dazu erziehen können, autodidaktisch zu arbeiten.
Meine Studis jedenfalls interessieren sich eindeutig mehr für Fußball als für Differenzialgleichungen.
Die meisten wollen auch mal Party machen und fangen erst zwei Woche vor der Prüfung mit dem (sogenannten) Lernen an.
- Wie groß ist diese Minderheit?
der *drang* zu lernen, genetisch oder wodurch bedingt? ref: http://www.heise.de/tp/foren/S-Man-kann-Autodidakten-nicht-erzeugen/forum-284799/msg-25725593/read/:
- “So ca. 15% der typischen Studenten haben es, das sind die sehr guten, solange man ihnen denn etwas bietet.”


# Open Access Diskussion

- #tags: #open_access #twitter #discussion #papers
- (siehe heise)
- wenn alle veröffentlichungen vollständig “leicht” oder als unstrukturierter Text zur Verfügugn stehen  und statistische, analytische (systematische) methoden darauf angewendet werden können wird ein Umbruch stattfinden
-  recent:
- https://twitter.com/zooko/status/512299160415268864
- https://twitter.com/zooko/status/513319177445064704
- https://twitter.com/zooko/status/513318260666687488

# Sci Hub - Library Genesis

- #tags: #open_access #article #discussion #papers
- http://www.theatlantic.com/technology/archive/2016/02/the-research-pirates-of-the-dark-web/461829/?single_page=true

# Klassenraum(Lern) der Zukunft

- #tags: #youtube #learning #classroom #tedx
- ref: http://www.youtube.com/watch?v=Ah-SmLEMgis#t=491 (8:11)
- Die Software ist wohl eine Kombinaiton aus :  Coursera und Knewton
- more, new class room: https://www.youtube.com/watch?v=LcUBJjRWkBU&list=PLTP7oKl8qFmkOxDNMgDQmiwqk4MBtKvsl


# Elon musks school, batman example? , see khan academy

- #tags: #youtube #elon_musk #article #school #improvement
- http://www.heise.de/tp/artikel/40/40855/1.html
- ref interview: https://www.youtube.com/watch?v=vDwzmJpI4io 

# Mastery of Learning - Sal Khan

- #tags: #sal_khan #ted #mastery #education #improvement
- http://www.ted.com/talks/sal_khan_let_s_teach_for_mastery_not_test_scores
- instead of variable results (A,B,C,D) with fixed length, go the opposite: variablelength fixed outcome ( mastery)

# Bildung Disskusion Deutschland

- #tags: #discussion #deutschland #bayern #bildung
- Wir haben Probleme: http://www.heise.de/newsticker/foren/S-Macht-es-bitte-nicht/forum-283984/msg-25639165/read/
- http://www.heise.de/newsticker/foren/S-Tolle-Idee-noch-mehr-Mathematik/forum-283984/msg-25637429/read/
- Informatik in Bayern: http://www.isb-gym8-lehrplan.de/contentserv/3.1.neu/g8.de/index.php?StoryID=26193
- Nur Ein Post zu “Computing”? http://www.heise.de/newsticker/foren/S-Computing-statt-Informatik/forum-283984/msg-25634481/read/

# The Steve Jobs 95 Interview unabridged

- #tags: #education #steve_jobs #learning #papers
- first part on his education
- https://www.youtube.com/watch?v=M6Oxl5dAnR0
- kids need an individual who engages and with them 
- show how things work , take a part 
- the conspiracy about TV Shows 
- computers won’t help they are “reactive”
- teacher unions made it worse
- art and computers as a media
- work impact ratio
- give computers to school in the US (california worked)
- “never get fired for buying IBM”
- Xerox Parc Story at ~ 55 min
- pixar bought from george lucas
- what do you need for a start up

# On how to teach

- #tags: #teaching #gedanken #tobi
- Ist auch in Prinzipien enthalten, sollte an einem Ort sein?! nein tags :)
- Start with Taxonomie what is known =>
- Tobi: Lehrplan berlin, http://www.berlin.de/sen/bildung/unterricht/lehrplaene/
- => was wird gelehrt, was nicht? => Tech tree Viz siehe Meetup Ideas
- What about motivation?
- Idols? 
- Chomsky
- Dijkstra
- Alan Kay
- As we may think
- Why did douglas Engelbarts idea fail:?
- Who are you heros and why?
- What is more important, IQ, outlook?
- ref to alan kays prog and sale talk and to the book the structure of scientific revolutions
- Also media competence, McLuhan, What is media, can you see and sense it?

# Podcast über calliope

- #tags: #podcast #jöran #calliope  
- http://www.joeran.de/jra037kann-calliope-wirklich-unser-schulsystem-revolutionieren/
- Saarland ist dabei, wieviele BL folgen?
- http://www.zeit.de/digital/internet/2016-10/calliope-mikrocontroller-grundschule-dritte-klasse/komplettansicht

# Deutscher Schulpreis

- #tags: #schulpreis #jöran
- jöran
- http://de.wikipedia.org/wiki/Deutscher_Schulpreis#2013
- Kriterien
- http://schulpreis.bosch-stiftung.de/content/language1/html/8779.asp

**Titanpad diskussion

http://titanpad.com/ePYQkiy7rG
leider verloren gegangen :/

1.Digitale Impotenz statt Werkzeuge zur (DE) Konstruktion von Wissen
Here be Dragons von der Next 
Appstore
fimren kriti?
Content und software lock?

2. Digitale Elite statt Demokratisierung von Lernen und Wissen (Demokratisierung hier zu ungenau definiert)
- Internetnetnutzung in Afrika
- Mitmachen?! Wikipedia ofensichtlich , viele Web 2.0 Anwendungen
-Warum sind MOOCs nicht für alle da? Initiativen wie Wikipeida Zero auch gut in Afrika nutzbar
-OLPC?
-Peak -> Höhepunkt?
-Dijkstra, Kay, engelbart how fast cam society deal with progress?

3.Didaktische Konterrevolution –  Rattenkäﬁg statt Kompetenzentwicklung
-wer wird erwartet das Problem zu beheben? Staatliche Intitutionen scheinen in Deutschland wie in UK wenig Elan zu haben , was aber nicht Leute davon abhält das Problem zu lösen (für den Informatik unterricht):
http://www.infoq.com/presentations/education-IT-CS
Im Vortrag wird erwähnt das der größte “Beitrag” der Regierung war zu sagen: “Ihr könnt es machen aber wir können euch nicht helfen.”
Gamification als Konditionierung?-> Motivation ist aber auch ein Punkt!

4.Veränderungsresistente statt lernende Organisationen
Offensichtlich sind nicht die Organisationen leern resistent sondern die Leute welche die Organisationen betreiben, haben wir ein Bootstrapping Problem?
Kontroll verlust?

Sehnsucht nach Kontrolle und Ordnung
“Gesellschaft demokratisch oder doch nicht?”

5. Die Eigentlichkeit der Technik
Deal with it. Das ist eine Frage(warum funktioniert die Technik nur “eigentlich”?) welche eine Komplexe Antwort erfordert,technisch als auch historisch. Es gibt verschiedene Wege zur Linderung des Problems heute, als kurzfristige Lösung. Langfristig wird die Ausbildung von vielen mit entsprechender Kompetenz notwendig sein. 

Noch ein paar Gedanken zum Titel des Vortrag
Natürlich beziehst du du den Zustand der Computer bzw. Informationsindustrie auf auf dein Arbeitsumfeld. Die Ursache ist aber nicht speziell auf einen Bereich beschränkt,sondern alle Bereiche der Gesellschaft “leiden” unter den Umständen. 
Das Ganze ist im historischen Kontext zusehen. Wir befinden uns in einem vielschichtigen “bootstrap” Prozess (http://de.wikipedia.org/wiki/Bootstrapping_(Informatik)) in dem wir permanent erkennen was wir leisten könnten oder haben wollten aber die Umsetzung kollektive Anstrengung oder Übereinstimmung erfordert und daher niemals schritthalten kann.

Es sind wirklich verschiedene Phänomene hier am Werk z.B.: 
http://de.wikipedia.org/wiki/Sapir-Whorf-Hypothese
http://de.wikipedia.org/wiki/Zeitgeist
http://en.wikipedia.org/wiki/The_Structure_of_Scientific_Revolutions

Um den Bootstrap Prozess schneller vorran zu bringen müsste man das individuelle Verständnis steigern für die Phänomene die um uns herum passieren. Also genau das was Bildung ja eigentlich machen sollte.
Alan Kay hat hierzu verschiedene Methapern eine die mir gefällt ist die des “Media Gorilla”.

Der nächste Schritt in diesem Prozess wird von Alan Kay als “Computer Revolution” bezeichnet. Er vertritt die Meinung das diese noch nicht passiert ist. Es geht also nicht darum das die technischen Vorraussetzungen dafür vorhanden sind (Halbleiter Technik etc.)  sondern um die Auswirkungen auf das Menschliche Denken. Im Besonderen der Schritt mit dem wir den Computer nicht mehr dafür verwenden um vorhandene Medien zu imitieren sondern tatsächlich als Werkzeug zur Erweiterung des Denkens begreifen Ziemlich schwierige Sache da Software ersteinmal abstrakt ist. Erstaunlicherweise resonant zu deinem Vorwurf Digitale Unterrichgtsmedien würden nur schlecht analoge Imitieren.
(für elegantere Formulierung Siehe http://worrydream.com/Links2013/ - Abschnitt Alan Kay)

Noch ein paar Ideen zum Thema Bildung:
Deine Meinung zu Montessori Schulen?
Beziehen sich deine Kritikpunkte auf Ansätze die hier beschreiben werden oder siehst du diese als potentielle Lösungsansätze:
http://www.youtube.com/watch?v=quYDkuD4dMU
Hast du schon mal etwas vom “hole in the wall” experiment gehört? Deine Meinung hierzu: http://en.wikipedia.org/wiki/HiWEL  ?
Wie könnte der Klassenraum der Zukunft aussehen? Was hältst du von diesem Beispiel: http://www.youtube.com/watch?v=Ah-SmLEMgis#t=491

Was ich mich auch noch bei deinem Vortrag gefragt habe:
Für wen war er gedacht? Schüler, Lehrer? Person in der Gesellschaft? Konstruktive Anreize? Insgesamt war er mir doch einwenig zu WENIG konstruktiv.


# Jöran video

- unwatched  : https://twitter.com/joerande/status/597450711136378880


# from notes

- #tags: #teacher
https://www.edutopia.org/blog/maker-movement-shoulders-of-giants-sylvia-martinez

„It seeks to liberate learners from their dependency on being taught.”

„From constructivist theories of psychology, we take a view of learning as a reconstruction rather than as a transmission of knowledge. Then we extend the idea of manipulative materials to the idea that learning is most effective when part of an activity the learner experiences is constructing a meaningful product.” 

Social brain : yes - but has it to be a teacher? What is a teacher?
https://www.edutopia.org/video/what-social-brain

# Jöran Vortrag zu Digitalisierung und Bildung 2019

- #tags: #jöran #book #bildung #book
- Kuddelmuddeligkeit von Digitalisierung und Bildung - Univention Summit 2019 Jöran Muuß-Merholz
- 37:43 Hilft Zentralis ( pladoyer für dezentralisierung) https://youtu.be/WbN-Aqt-wK0?t=2263
- Die Vier dimensionen der Bildung: 
    -  Wissen
    -  Skills
    -  Charakter
    -  Meta Lernen (Seymor, Alan kay ..)
- #book: Die vier Dimensionen der Bildung, deutsche Übersetzung von Jöran https://www.amazon.de/Die-vier-Dimensionen-Bildung-Sch%C3%BClerinnen/dp/
- Veränderung ist immer das Problem
- von Kontrollverlust zu Emanzipation
- Der große Verstärker

# OER

- #tags: #education #oer #germany
- https://de.wikipedia.org/wiki/Open_Educational_Resources
- https://www.youtube.com/watch?v=AnD4XsLesZ4
- 2016 Status, MV is not on the chart... https://www.youtube.com/watch?v=uN3KRdMyTRM

# Montessori 

- #tags: #schule #montessori #erfahrungen
Wir waren heute in der Montessori Schule in Hofheim:
https://www.montessori-hofheim.de/aktuelles/uebersicht/detailansicht/tag-der-offenen-tuer-im-montessori-zentrum/

Wir haben uns Schulräume angesehen mit Lehrern als auch mit Eltern gesprochen.

Es war absolut fantastisch. Hier einige Sachen die mir gut gefallen haben:

- die Kinder dort gehen gerne in die Schule  
- Es gibt keine „Klassen” sondern Gruppen. Das heißt z. B.  In Gruppe A sind Kinder im Alter von 6 bis 9 Jahren zusammen
- 22 Kinder bei 2 Betreuerinnen 
- Es ist alles auf praktisches lernen mit allen Sinnen ausgelegt
- Keine Hausaufgaben 
- Noten ab der 8ten Klasse 
- Die Gestaltung der Räume ist beeindruckend...

# OER de Effort  - Edulabs

- #tags: #oer #blog #meetup #berlin
- https://edulabs.de/blog/projektstart-edulabs
- meetup berlin

# Simon Peyton Jones - Fixing ICT Crisis

- #tags: #infoq #simon_peyton_jones #education #ict
- http://www.infoq.com/presentations/education-IT-CS
- TEDx talk

# Freilerner, Unschooler, Homeschooler

- #tags: #youtube #article #education #alternative
- https://www.youtube.com/watch?v=K5qz1nlfz14
- http://www.heise.de/tp/artikel/40/40682/1.html
- finden einer Lösung ohne Staatliche Förderung/Mithilfe, Unterstützung lediglich in Form von “Duldung”
- Einführung des Fachs Computing bereits in der Grundschule -> welche Konzepte genau werden behandelt?

# Article - free thinkers

- #tags: #article #alternative #education
- http://www.wired.com/business/2013/10/free-thinkers/
- Personen erwähnt: Sugata Mitra Maria Montessori Waldorf
- Konzepte: selbständiges lernen, “Hole in the wall”

# Future Thinkers podcast 062, 063: Jordan Greenhall – Deep Code: Learning How To Learn

- #tags: #future_thinkers #podcast #jordan_hall #robert_kegan #learning #mastery #sovereignty
- https://futurethinkers.org/jordan-greenhall-deep-code/
- https://neurohacker.com/
- Robert Kegan (Evolving Self)
- the most important meta skill is to deconstruct learning
- Discernment 
- Attunement
- Coherence
- Clarity
- Insight
- Embodiment
- also mastery and sovereignty 

# proficiency mastery

- #tags: #mastery #finite_infinite_games
- game analogy/levels:
- 1. play game by rules 
- 2. Bend rules to beat the game
- 3. Invent own rules to create a new game 


# Video - Future of learning

- #tags: #youtube #learning #impossible_thought #sugata_mitra
- von Erricson gesponsort
- welche personen treten im video auf? Seth Godin Sugata Mitra Daphne Koller
- wie Revolutionen Funktionieren? Impossible thoughts?
- MOOC
- Coursera
- warum unser Normales Bildungsystem Obsolete ist -  Erläuterung von Sugata Mitra
- personalisierte bzw. individuelle Lernmethoden
    
http://www.youtube.com/watch?v=quYDkuD4dMU

0:14Everything looks bigger than life when you are five.
0:18So everything was big, everything was strange, and I remember feeling a bit scared.
0:25Like many children, I remember my school years fondly but the bits I remember fondly aren’t
0:29the bits I should have remembered.
0:31I remember the play and the sport and the naughtiness and the playfulness and the mischief.
0:37In fact, I remember the bits that were non-standard.
0:39Itís an incredible privilege for me to have had education be such an important and present
0:47part of my life.
0:48I remember being bored a lot. It didnít bring out the best in me, I got through it anyhow.
0:57I wasnít a great fit or the system wasn’t a great fit for me. Itís kind of crazy when
1:03you think about it.
1:04That we take all these children and we force them to try to adapt to this really complex
1:08bureaucracy system, the system should adapt to them.
1:12The origins of traditional education lie inside the military, to a large extent.
1:18They needed identical people; soldiers, administrators, and so on, so they produced this system.
1:23When the industrial revolution happened they too wanted identical people in their assembly
1:29lines.
1:29Even with consumers, they wanted consumers to be identical so that everybody would buy
1:33the same things.
1:35So if you look at school that way, if you look at the fact that we process twenty or
1:39thirty kids at a time, in a batch, just like in the factory.
1:42If you look at the fact that if you fail third grade, what do you do? We hold you back and
1:46we reprocess you.
1:48All matching with the factory works, we built it on purpose. And it was really useful for
1:55its function.
1:56But we donít have a shortage of factory workers any more.
2:00We are probably at the death of education right now.
2:04I think the structures and strictures of school, of learning from nine till three, working
2:10on your own, not working with others.
2:12I think thatís dead or dying. And I think learning is just beginning.
2:23I had ADD when I was growing up, like so many people now do.
2:49And thereís this feeling that there is something broken about the kid, because the kid doesnít
2:55conform to the system.
2:58So what we do is we medicate children to fit into the system, as opposed to saying; wait,
3:04the system is here for the kids.
3:07And there are lots of people who can quite easily sit still for eight hours and take
3:12notes, and then two weeks later say back what they
3:14wrote down.
3:16But there is also this huge population of extraordinarily talented and engaged people
3:21who canít learn that way.
3:23Thereís a very big difference between accessed information and school, they used to be the
3:30same thing.
3:32Information is there online to anyone of the billion people who have access to the Internet.
3:39So what that means is if we give access to a four year old or an eight year old or a
3:43twelve year old, they will get the information if they want it.
3:50Knowing something is probably an obsolete idea.
3:54You donít actually need to know anything, you can find out at the point when you need
3:59to know it.
4:00Itís the teacherís job to point young minds towards the right kind of question.
4:07A teacher doesnít need to give any answers, because answers are everywhere.
4:12And we know now from years of measurements that learners who find the answers for themselves
4:19retain it better than if they’re told the answer.
4:24Education is being very slow to look at data, to look at numbers, to look at analysis and
4:30whatís actually happening.
4:31We measure a test here, and an exam there, but the details of whatís happening we donít
4:36really have.
4:37That will be, for sure, the next important thing in our pockets, our ability to analyze
4:42wherever you are.
4:43Some of the people watching this will already be analyzing their health and their wellbeing
4:46and their sport.
4:47They will be analyzing their learning too soon.
4:49And then weíll be really good at it.
5:04Knewton is a data-mining and adaptive learning platform that allows anybody, anywhere to
5:10upload content.
5:11They could be a publisher, an individual teacher, or anything in-between.
5:15And produce a course that will be uniquely personalized to each student based on what
5:22she knows and how she learns best.
5:26The textbook of the future is going to be delivered on connected devices.
5:30What that means is the incredible amount of data that students have always produced, when
5:34they studied, are now capturable and useable.
5:37So Knewton and any product built on Knewton can figure out things like; you learn math
5:41best in the morning between 08:32 and 09:14 am.
5:46You learn science best in 40 minute bite sizes.
5:48At the 42 minute mark your click rate always begins to decline, we should pull that from
5:51you then and move you to something else to keep you engaged.
5:54That 35 minute burst you do at lunch everyday you’re not retaining any of that just
5:57hang out with your friends and do that stuff in the afternoon instead when you learn better.
6:01You learn this stuff best with short questions; this stuff best with complicated, difficult
6:05questions.
6:05We should return this type of material to you four days later for optimal retention.
6:08And here’s exactly the things you’re going to struggle with your homework tonight because
6:11you havenít learned some of the concepts that are embedded in that material.
6:15And we can go in real-time and grab you the perfect little bit of content, from last month,
6:19or last year, and put that seamlessly in front of you so that you don’t struggle.
6:23We can predict failure in advance and prevent it from happening.
6:27Weíre going to move from this kind of alienating and in some cases boring and in some cases
6:33frustrating model of everybody gets exactly the same stuff.
6:35They’re getting it at the exact same time, the exact same order, and the exact same difficulty
6:39level.
6:39For half of the class it’s too hard, for half of the class itís too boring.
6:43It’s going to get the most advanced kids, the most stimulating material.
6:49Itís going to get them to unlock their potential in a way that theyíre not today.
6:52But for every kid, no matter how much you are struggling, youíve got a path to success.
6:55It might take you a little longer, but youíll have a path to success no matter what.
6:59And also the system gets smarter and smarter as more people use it.
7:03Strategies compete against each other to be replicated in the next generation, so the
7:09strategy that is the most effective for you, once we find that; any kid in the future will
7:14have that strategy teed up.
7:16It’s a whole new thing.
7:20When the automobile was invented people werenít asking for the automobile they were asking
7:24for faster horses.
7:25And people arenít really asking for Knewton because they donít know what it is yet, but
7:31once they see it and experience it then they’ll get it right away.
7:38People say that education moves very slowly.
7:47Suddenly you just need to be connected.
7:50That changes everything; it changes the basis on which you can make a contribution, your
7:56brain can make a contribution at a distance.
7:59It’s one thing to sit here in the media lab and talk about the future.
8:06I often go into places that are about as different from a media lab as it can get.
8:11And I think to myself; whatís the value of all my ideas over here. But, there is one
8:18great hope.
8:20Wherever I go, the very first thing that I ask, or I take out my phone to check for,
8:26is; do I have that little bit of bandwidth to give me GPRS, or something equivalent to
8:32that?
8:34And in the middle of jungles I find that sometimes it says connected
8:41And I know then that everything that I’m saying can go anywhere, and work exactly the
8:47same way.
8:48Itís a question of time.
9:05Connectivity is actually opening up the world. If you open up a village, for example Bonsaaso,
9:12and the students can actually now communicate with other students, say in London.
9:18It means they start seeing the world in a different way.
9:22Educate a youth, and you educate a nation.
9:26Connect to Learn is a partnership between Ericsson, together with AF Institute in Columbia
9:31University, and the Millennium Promise.
9:33It’s twofold, it provides scholarships to girls, and Connect to Learn gives students
9:39computers and connectivity and shows them how to use it, and how to get information.
9:48Education was limited to what the teacher could tell the students, and the teacher was
9:52relying on a small textbook, or those few books, so the teacher was not getting very
9:57exposed.
9:57Now you are able to access a lot of information and the children start chatting and exchanging
10:05information, you can see that there is much more things for them to talk about because
10:09they feel like they are more exposed.
10:11And the children are more confident.
10:13They have the energy, they have quite a lifespan ahead of them and they are about to start
10:20thinking bigger.
10:21If you bring connectivity to them, they are actually able to do transactions and they
10:25can start small businesses, which will uplift them.
10:28So I’d say it’s actually opening up our villages, our country, and the whole continent.
10:34We’re rolling it out in as many countries as possible in Africa, and also in South America.
10:43It has the potential to be upscaled to any country.
11:06The way we solve interesting problems is we fail and we fail and we fail and we fail,
11:09until we succeed.
11:10And if youíve talked to people who have succeeded, what they almost all have in common is that
11:16theyíve failed a hundred times before they succeeded.
11:18And what separates them, from people who arenít successful, isnít that they succeeded, itís
11:23that they failed more that the other people did.
11:26Iím not sure itís okay for the schools to say; we have to optimize, to process as many
11:34people as we can to match this testing regime.
11:39You canít imagine, in a world where you sit down to do an exam and you ask yourself the
11:45question; I hope there are no surprises on the exam paper.
11:49And your teachers think; I hope I prepared him for everything.
11:51How would that prepare you then to go out into a world that everyday is going to surprise
11:56you.
11:57Itís full of the surprises of the economy, of society, of politics, of invention, of
12:03technology.
12:03Everyday is a surprise.
12:05Learning prepares you to cope with the surprises, education prepares you to cope with certainty,
12:10there is no certainty.
12:14The teacher stands between the child and the formal education. Trying to make the child
12:23face that system.
12:25And until that system breaks down, or disappears, she has an incredibly difficult job of keeping
12:32the childís curiosity alive, while at the same time saying; listen, by the time you
12:37are sixteen, youíll have to start memorizing certain things, so that you can go and sit
12:42for the examination, clear it, and get out of school properly.
12:48No one I know takes standardized tests for a living.
12:51So why are we using standardized test to see if you are going to be good when we donít
12:55have standardized tests after you take it.
12:57Itís infected the entire marketing eco-system of education, because famous colleges are
13:03famous because they are picky about your SAT scores.
13:07Parents want their kids to go to a famous college.
13:10Parents push their school to create kids who will get into a famous college by doing well
13:15on the SAT.
13:17All which is corrupting the entire reason we have education in the first place.
13:21If we can get parents, and teachers, and kids, and administrators, to have this conversation,
13:28to just talk about it, then if at school board meetings, or if, at ten year reviews the questions
13:34we are asking are not; how did your students do on the SAT.
13:40But instead we say; the SAT makes no sense, famous colleges are a scam, we need to create
13:45a different thing and we can have this conversation.
13:49Then change will start to happen.
14:00Coursera is a social entrepreneurship company that enables the best universities to take
14:10their best courses and put them out there, so that everyone around the world with an
14:14Internet connection can benefit from having access to a great education.
14:23As of today, which is the end of September we have 1.5 million students from 196 countries,
14:29itís a little debatable how you count countries.
14:31We have 195 courses from 33 universities.
14:36Our larger courses have an enrollment of 130,000, our smaller courses have an enrollment of
14:42only about 10,000, of course theyíre still growing, most of them havenít launched yet.
14:48A medium class, when it launches, has about 50- 60,000 students registered.
14:57Scale is interesting because it allows us to offer a high quality product at a very
15:05low marginal cost per student, which is what allows us to take people who really canít
15:10pay for an education and to provide them a free education.
15:14Education for free at the highest quality because the costs are so low per student.
15:21The student experience in Coursera is that the course starts on a given day and each
15:25week a student has access to numerous pieces.
15:28One piece is video lecture, and itís interactive videos, you donít sit there for an hour just
15:33watching video, you get to interact with the video.
15:35There is rigorous meaningful assessments of different kinds, not just multiple choice
15:40but real exercises with real depth.
15:42And there is a community of students that you get to interact with to ask questions
15:47and have those questions answered by your fellow students.
15:50So that you get both a better learning experience via peer teaching as well as a social experience,
15:56where you feel like there is a community of learners surrounding this intellectual activity.
16:03People often ask us whether universities are a thing of the past, whether universities
16:07are going to die out, and I definitely do not think so.
16:10There is something tremendous about getting people together in a place where serendipitous
16:15interactions can happen.
16:17Where you can have face-to-face mentoring between an instructor and students, where
16:21students can talk to each other, and create together, and learn to debate ideas.
16:25So this on-campus physical experience at the moment has no virtual substitute that is equally
16:33effective.
16:34Our goal here, and I think one needs to be pragmatic about this, is not to equalize necessarily
16:42the opportunity of students who currently donít have any access, and make it equal
16:48to what a fortunate Princeton student might have.
16:52Because that might be a really worthy goal but it’s not something that we can necessarily
16:55can achieve in the short time frame.
16:57What we’d like to do, is we’d like to bring both of these up to considerably better than
17:01where they are now, even if they donít end up being equal.
17:03If weíve improved a lot, of both the on-campus students, and the ones who currently donít
17:09have access, I think weíve done an amazing thing.
17:21So let me explain how revolutions work.
17:23Revolutions destroy the perfect and then they enable the impossible.
17:27They never go from everything is good to everything is good.
17:30There is a lot of noise in the middle.
17:33If we look at the music business; first it destroyed the record label business, the Internet.
17:38And only now is it enabling independent musicians to get heard.
17:43Education tends to move in stairstep functions, in terms of change, so when it does change,
17:48it explosively changes.
17:49The move from pre-printing press to post-printing press is a one-time transition in history
17:54of the world, in terms of education.
17:56Online education is going to be like that as well. And we want to make sure that, as
17:59a species, the human species gets it right.
18:02One of the revolutions that weíre going to see is where less and less of education is
18:07about a conveyer of content, because that is going to be a commodity, and hopefully
18:12one that’s going to be available to everyone around the world.
18:15And a lot more of what we think of as education is going to go back to its original roots
18:21of teaching.
18:21Where the instructor actually engages in a dialogue with the students and helps them
18:27develop thinking skills, problem solving skills, passion for the discipline.
18:31The kinds of things that are much easier to do in a face-to-face setting and a lot harder
18:37to do in an online format, but for which, really the college experience, as we know
18:43it; that it the right place where youíd like to put that kind of development of skills.
18:48Now what I want to see from schools is; get kids to want it.
18:54Create an environment where kids are restless until their need for information is satisfied.
19:06Every time I get a question right, I get immediate engagement. I think the teacher has to step
19:13back and say; today’s topic is this. Open your notebook and figure it out for yourself.
19:22What we need are teachers who will look people in the eye, and believe in them, and push
19:29them forward, and itís hard to do that on the Internet.
19:32It really needs to be done in person.
19:35Schools decide to be better because they see children being better.
19:38And teachers... what does it say on teacher’s t-shirts?
19:40It says: ‘We’re in it for the outcome, not the income!’.
19:44Teachers are there because they can see the change in their children.
19:48If you add up every child in history, more children will leave school in the next 30
19:52years, than they’ve ever left school in history.
19:56If I was going to make one change, I’d make their schooling just a little bit better.
20:03And that would change history faster than anything else.

# Minerva university

- #tags: #minerva_university
- https://www.youtube.com/watch?v=VSTHz18E49s schools in : San fransico, Berlin, Seuol , Buenos Aires ...
- teach meta skills, students can7should complement with “usual” MOOC’s but its mostly about self confidence and doing projects ( education etc...)
- via: steve crouse twitter: https://twitter.com/andy_matuschak https://twitter.com/andy_matuschak/status/1127268379901874176 minerva ref: https://twitter.com/andy_matuschak/status/1127785742501998592
- Book: building the inten
- unjaded jade(channel description: academia, self-growth, positiv vegan): taking a gap year before university, in uganda https://www.youtube.com/watch?v=QtM1m3oNhTQ
- cultural awareness already ! ( stuck in school grade thinking) https://www.youtube.com/watch?v=9OAzRT-2lkw

# Unjaded jade

- #tags: #youtube #unjaded_jade #self_censorship
- https://www.youtube.com/watch?v=aVXFi4Hp_KE
- took down video of minerva because of critique that is was a scam! ==> this is the _cost_ of change , if jade can’t explain it because she hasn’t visited it how can other not haveing done it critisize it?

# MY 5AM SCHOOL MORNING ROUTINE 2017

- #tags: #youtube #unjaded_jade
-  “intentions for the day I like to remind myself what I’m grateful for tell myself things like I am beautiful and worthy of happiness and success”
-  so basically shw is really/more awake when she gets to school

# first week university

- #tags: #minerva_university #unjaded_jade
- https://www.youtube.com/watch?v=-6HxEvnALpg
- 4 courses:
- 1. complex systems
- 2. formal analysis
- 3. empirical analysis 
- 4. multimodal communications
- she says: first year is teaching habits of thinking through context of different subjects
- pre class work instead of “home work”, 2 hours per class 
- city based assignments and civic projects
- comments point out: climate protests but food from amazon/apple -  called middle class hypocrisy

# room mates san fransisco

- #tags: #minerva_university #unjaded_jade
- https://www.youtube.com/watch?v=yZeNvk_Ql-c
- Minerva 10:01
- her culture perspective expands: kosovo, afghanistan...

# Connecting hustle culture with thomas aquines - motivation for live

- #tags: #connecting_dots
- josch bach thomas aquines — promise of afterlive —jade hustle culter always future
- also jordan peterson working for something promised 


# Typical Day Of A Minerva Student In Seoul

- #tags: #youtube #dinara #minerva_university
- https://www.youtube.com/watch?v=Z-SG9BZeoXk
-  she is quite a different charater from jade its good to have that perspective : 

# downside minerva

- #tags: #minerva_university #youtube #dinara
- https://www.youtube.com/watch?v=E5FmlthzeiQ
- she feels overwhelmed by the experience as a whole- counter? more tuition and different scale for individuals?)

# Typical Day of a Minerva Student in Berlin

- #tags: #minerva_university #youtube #dinara
- https://www.youtube.com/watch?v=WxIKlhp4WCA 
- waking up at 10? 
- foods looks good! African!
- But no physical exercise?

# Podcast Zukunftsbauer

- #tags: #podcast #zukunftsbauer #sinneswandel #selbstwirksamkeit #gestaltungskraft
- https://sinneswandel.art/2019/07/15/aileenmoeck/
- #book: empfohlene Bücher: Risikogesellschaft von Ulrich Beck , Resonanzpädagogik von Hartmut Rosa
- https://medium.com/zukunftsbauer-das-schulprojekt/arbeits-welten-von-morgen-die-zukunftsreise-als-open-educational-resources-oer-2172d6d414ce
- https://aileenmoeck.com/
- https://www.ewi-psy.fu-berlin.de/en/v/master-zukunftsforschung/index.html
- Selbstwirksamkeit/Gestaltungskraft

# What you didn’t learn in school

- #tags: #deschooling_society #purpose #school #possibilities #thoughts
- https://en.wikipedia.org/wiki/How_to_Read_a_Book
- How to Listen to Great Music: A Guide to Its History, Culture, and Heart (The Great Courses)
- How to think an impossible thought... - but what SHOULD you learn in school?

# Unschool Adventures

- #tags: #deschooling_society #unschooling #possibilities
- via https://twitter.com/stevekrouse/status/1222997370457051136?s=20
- Book: “Are you still sending your kids to school?”
- https://www.kickstarter.com/projects/blakeboles/why-are-you-still-sending-your-kids-to-school
- https://twitter.com/blakeboles
- https://www.unschooladventures.com/trips/spain-2018/
- like RDP - Anne schule - Ship sailing for teenagers

# Teaching - Programming Thoughts and Opinions

- #tags: #twitter #youtube #teaching #fail #programming
- request for feedback from her (unlikely) https://twitter.com/curious_reader/status/1234436738250104834
- in response to this talk: https://www.youtube.com/watch?v=g1ib43q3uXQ by https://twitter.com/felienne
- where she apparently critiques constructivism and seymour papert of “letting the child discover it instead of explaining it to the kid”
- I think its an interesting question, thank you @gman for bringing it up
- One of the Problems I see with answering it is how to measure if learning with kids “works” or doesn’t.  
- In Paperts book Mindstorms he explains some interesting aspects of his vision.
- Later in the book he also has this example of Samba school as an example of a learning environment.
- I think, or at least it seems to me that she took him too “literal” or too constructivist - dogmatic.
- Which is never a good idea. I I still think Papert and his work have valuable lessons in them.
- But for her as for a very very very large part of society its just too much of a leap from existing culture.
- Paperts words in Mindstorms regarding - Computer Cultures are real.
- But its not limited to Computers current “Mainstream” Civilization Cultures does quite a bad job at acknowledging its biases towards certain tendencies.
- So a bias towards explanation might not be easily spotted but still be there.
- I will just point to some effort which does also lean towards more self-directed learning
- and these are SOLES such as proposed by Sugata Mitra: https://en.wikipedia.org/wiki/Self_Organised_Learning_Environment 

# TipToi reverse engineering project

- #tags: #tiptoi #haskell #ccc
- https://github.com/entropia/tip-toi-reveng
- haskell
- tttool
- https://entropia.de/Hauptseite Karlsruhe Hacker- und Makerspace
- 2015 Vortrag CCC: https://www.youtube.com/watch?v=GzXtgR73icg
- 2019 https://www.youtube.com/watch?v=HX1Fpx4fer8

# History of Logo

- #tags: #logo #history #article #book
- https://escholarship.org/uc/item/1623m1p3
- #book: Turtle Geometry(Abelson and diSessa) https://books.google.de/books?id=3geYp44hJVcC&pg=PA30&hl=de&source=gbs_toc_r&cad=2#v=onepage&q&f=false

# Parenting Discussion on GameB Discord 17.06.2020

- #tags: #nickredmark #discord #gameb #parenting
- https://twitter.com/nickredmark/status/1273362072366186497?s=20
- notes from nick:
- Never occurred to me that someone could read a book about the local public school system.
- Antiauthoritarian parenting
- Free range parenting
- Unconditional parenting
- Punished by rewards
- Partnership vs domination
- consent-based education vs coercive
- student-led education
- Is #gameb aligned with this?
- recommends integralparenting.com
- I’m taking it :)
- Probably relevant: “love is that which enables choice” by Forrest Landry
- But. What about boundaries? Where to put boundaries? How to set boundaries?
- Encourage but don’t push
- Parents: a shared state of confusion. And guilt. And shame.
- Processing your trauma: hard to do once you have children. Parents need (and lack) support groups.
- And another dimension: the strain that children put on relationships, and little time to process them. Lots of relationships break up when children arrive. Difficult to find people to talk about it. Again, support groups.
- Integrate children in more of life.
- Great approach to accepting homeoffice with children: “this is life”. Allow children to be in the background of work calls, screaming. This is life.
- #gameb community is actually generally quite understanding.
- Book: mindstorms
- Author studied with Jean Piaget
- You best learn french in France, but to learn math where do you go?
- I can recommend this playlist **Peter Gray** : https://www.youtube.com/watch?v=G2BAJ_svbhA&list=PLeE6SEu5P6dgcofCbZL0NM4er3wGqOm-x
- Give them the tools they need and trust them 100% that they will learn.
- Sudbury schools
- Also interesting concept: unschooling cooperative
- Baltimore Unschooling Cooperative Live. Learn. Grow. https://baltimoreunschoolingcoop.com/
- The fixed size, and age of classrooms does seem absurd.
- Retreat centers should include kids.
- Adjacent possible: start having gatherings with local parents.
- We have to relearn to dwell in the local.
- Let’s not forget how #covid has made parents’ life harder.
- educationrevolution.org
- a good starting point
- arrow
- Zak Stein lecticalive.org VCoLs medium.com/@theo_dawson 
- Maybe there’s a tradeoff between efficient learning and self-motivation.
- Grades as a form of manipulation.
- What does it mean to force someone to do something?
- Brooklyn Free School is an independent school with sliding scale tuition and a mission of education for social justice. Governed by democratic decision-making, Brooklyn Free School invites young peop…
- https://www.brooklynfreeschool.org/
- Teacher liberation handbook
- North star model, north star school
- Adult facilitator
- Environment is important: if you’re surrounded by others passionate about learning...
- Homeschooling illegal in Germany :(
- Hope situation is better in Switzerland
- If you ask questions about the very room you are in you have learning possibilities/material for the rest of your life.
- Thanks @rhyscass @alejandramanes @curious_reader @truthnprogress for joining!
- https://cdfc2002.wixsite.com/educationarchitect
- Also relevant, the Game~B Parenting facebook group

# Podcast Zukunftsbauer 25.04.2020

- #tags: #podcast #zulunftsbauer #backgorund 
-  via: https://twitter.com/rec_tag/status/1253941903915208704?s=20
- https://www.rectag.eu/geil-montag-podcast/aileen-moeck-die-zukunftsbauer-wie-bekommt-man-lust-auf-die-zukunft?utm_source=twitter&utm_medium=organic&utm_content=geilmontag&utm_campaign=contentpost
- mpesa
- uni master -> schule
- workshop transition to online(corona) only
- ihr prof: de haan https://www.ewi-psy.fu-berlin.de/v/master-zukunftsforschung/schaufenster/FUture-Slam/2019_Videos/Gerd-de-Haan/index.html Massenmedien haben sehr viel größeren ( zeit) einfluss , Schule (institution) wird anfälliger
 
# Story of P2PU 

- via https://av.tib.eu/media/47552?q=Peer&f=series%3Bhttp://av.tib.eu/resource/collection/854
- Philip Schmidt MIT media lab
- https://shuttleworthfoundation.org/thinking/2017/12/18/thinking-philipp-schmidt-p2pu/
- Interessanterweise vom massiven MOOC Boom aufkeimende Idden zerstört ( es wurde gleich wieder versucht eine service industrie daraus zu machen), heute tot? zumindest das Portal? Was ist mit dem Format?

# Unjaded Jade

- #tags: #unjaded_jade #uncertain #future #youtube
- https://www.youtube.com/watch?v=CWwR1lYzl-s
-  why planing for live doesn’t work (never did) , who came up with 5 year plans anyway?

# “Ach, so geht das auch?” - Formate des Selbst- und des Peer-to-peer-Lernens für die bibliothekarische Fort- und Weiterbildung

- #tags: #jöran_und_konsorten #oer #p2p_lernen #bibliotheken #lernen
- Gabi Fahrenkrog
- https://av.tib.eu/media/47552?hl=Fahrenkrog
- P2P-Lernen Formate: 
    - Barcamps
    - Learning-Circles (P2PU) 
    - Stationen-Lernen
    - Twittter
    - Kollegiale Beratung
    - Hackathon & Booksprint
    - Stammtische und Meetups
    - Offene Teeküchen-Sprechstunde
    - Blogs
- Perspektiven für Kelkheim?

# Kosmischer Kalender

- #tags: #kinder #lernen #ideen #cosmos
- https://upload.wikimedia.org/wikipedia/commons/9/99/Cosmic_Calendar.png
- https://en.wikipedia.org/wiki/Cosmic_Calendar

# Plato Kinder Bücher

- #tags: #bücher #philosophie #book
- https://www.deutschlandfunk.de/philosophie-im-kinder-und-jugendbuch-denkansaetze-fuer.1202.de.html?dram:article_id=321269
- https://www.diaphanes.net/reihe/platon-co-40

# Kontext Bildung fürs Leben - Deschooling society

- #tags: #twitter #deschooling_society
- 19.08.2020
- https://twitter.com/JohannesAchill/status/1295439242026418176
-  Johannes Achill Niederhauser : “Studying philosophy for a degree (read: an overpriced piece of paper) is over. Studying philosophy for its own sake, to find truth and fulfilment - that’s beginning anew.”
- the “de-schooling of society” has begun :)

# Deutsche Schulakademie - Themen Woche digitale Schule - Online-Vortrag von Prof. Dr. Uta Hauck-Thum (Ludwig-Maximilians-Universität München)

- #tags: #conference #youtube
- via Radio Morgenkreis
- Bildung im kulturellen Wandel 
- https://www.youtube.com/watch?v=MYeH-b5ZBL8
- räume zur Verfügung gestellt aber nicht genutzt
- der Zusammenhang von Kultur und Bildung

# Deutsche Schulakademie - Themen Woche digitale Schule - Online-Vortrag von Prof. Dr. Jeanette Hofmann 

- #tags: #conference #youtube #demokratie 
- via Radio Morgenkreis
- Demokratie und digitaler Wandel 
- https://www.youtube.com/watch?v=mi2wrnY_Yq8&feature=emb_title
- Durchschnittsalter CDU : 61 Jahren
- Schwierige Rolle der Medien “mehr beteiligungs möglichkeit” für nicht immer zu mehr beteiligung - McLuhans Global Village Problem u.a.
- 3. Engagements Bericht: Projektbezogene Organisation, am gymnasium politisch in irgendeiner weise engagiert

# Radio Morgenkreis vom Donnerstag (24.09.2020) – “Digitale Schule? Auf den Kulturwandel kommt es an!”

- #tags: #conference #youtube #twitter
- via Jöran Twitter 25.09.2020
- https://www.youtube.com/watch?v=kvpVGtDH3ss
- überblick zur Themen Woche digitale Schule - 20 -26.9.2020

# Der Sonntagstalk in hr3 - Kati Ahl über notwendige Veränderungen an Schulen

- #tags: #podcast #h3 #schule 
- https://www.hr3.de/podcasts/sonntagstalk/index.html
- via Anna Louisas Muttier
- #book: schule verändern jetzt https://www.friedrich-verlag.de/shop/schule-veraendern-jetzt-31412
- schulen müssten Innovations Labore sein
- andere Fehlerkultur nötig, wir lernen durch Fehler
- Neuseeland 2015 40 seiten Änderungs
- Notenabschaffen liegt im gesetzlichen Rahmen in Deutschland
- Finnland: Fächer abgeschafft
- Altersübergreifender unterricht
- es fehlt an Mut
- Lehrpläne verschlanken, mehr tiefe statt Breite
- Montessori: mit Kopf Herz und Hand lernen
- Digitalisierung an der Schule, aber mit Deigital Detox raum
- http://www.ers-karlsruhe.de/gemeinschaftsschule#Gemeinschaftsschule öffnen der Räume
- 4K: Kritisches Denken, Kollaboration, Kreativität, Kommunikation
- in deutschland zu wenig zeit für Lehrer über den Tellerrand zu schauen
- nicht mehr lernen sondern anders vernetzen , einbringen
- Hackathon als Organisationsformat für Bildungsprojekte

# growth mindset: how it changed my approach to learning.

- #tags: #education #unjaded_jade #thoughts #mastery
- https://www.youtube.com/watch?v=UaDLuAkh62I
- I know now why I put unjaded jade here in education, its because I learn something everytime (almost) I watch her videos
- she described something very interesting about the appreciation of grades in school, its like she implicitly discovered sal kahns concept of mastery but came to it from the opposite direction
- I think many problems in the current education system come from this kind of modal confusion, good grades beeing good at education

# Werkstattpädagogik revolutioniert Bildung - Gerald Hüther & Christel van Dieken im Gespräch

- #tags: #education #hamburg #bottom_up #change #gerald_hüther #youtube
- https://www.youtube.com/watch?v=Ql_mfPIb8nM
- werkstatt kitas

# OER Update 2021

- #tags: #education #bildung #oer
- auffrischung OER im Kontext von Andys Kernel
- https://routenplaner-digitale-bildung.de/
- https://open-educational-resources.de/wie-und-wo-finde-ich-oer-repositorien/ berlin brandenburg , Hamburg, Badenwürtemberg, Niedersachen
- niederländisches Landesinstitut für schulische Qualitätsentwicklung - Medienberatung Niedersachsen https://www.youtube.com/channel/UCtr2F2lPjLlKT0yvLQD4nVw/videos keine Kommentar optionen
