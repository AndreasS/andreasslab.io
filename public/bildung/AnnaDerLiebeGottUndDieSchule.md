[[_TOC_]]

# Richard David Precht - Anna der liebe Gott und die Schule

- https://www.goodreads.com/book/show/51944799-anna-die-schule-und-der-liebe-gott

## Erste notizen

- Befindlichkeiten 
- Bildung , woher kommt Bildung ? Wer versteht unter Bildung was ?
- Ursprung 
- Die zwei fundamental Befindlichkeiten im menschlichen Wesen: Wandel und Stetigkeit 
- Institutionen bzw. Traditionen hindern Wandel? Hierarchie und zentrales Legacy
- Humboldt . . . 
- ==>> keine Bücher für Blockchain 
- Unterschicht in den 1960 und heute ...
- Bildungsexperimente gemeinsames lernen Hamburg 
- Bildung messen ?
- Unterschied Bildung - Ausbildung , zum Zweck des Staates  
- Privat Schulen 
- Pisa
- ==>> GDP?
- Lobby Konzerne ändern die Werte des Bildungssystemd
- G8
- Ein deutsches Amt Angestellter konkurriert nicht mit China ??
- Warum "Bildung für alle" nicht mehr genügt 
- Liquid democracy ref?
- Khan Academy aber kein OER?..2012
- Hole in the wall experiment 
- Lernwerkstatt 
- Heutiges Schulsystem: sozialer Status erhalt (der Eltern ?)
- 38
- Nachhilfe wird als "normal" angesehen, ebenso das Eltern mit ihren Kindern nach der Schule nach arbeiten: soziales aussieben
- Abschaffen der Noten 
- Warum precht das Buch und spätere geschrieben hat: der Wandel ist überall nicht nur im Bildungssystem 
- 42
- BWL % übrig vom lernen
- Wissenschaft Pädagogik vs künstlerische 
- Der Zustand der Lehrer , darf jeder ? Weiterbildung und Kontrolle 
- Autorität durch wissenshoheit , Bücher preis 
- Wikipedia besser / häufiger als Lehrer ?
- Digitale Kompetenz Mängel ==> es fehlt im Dream machine , machine Lib : BV Library 
- Pädagogik ist keine Wissenschaft 
- Lern-wert- selbst Wahrnehmung - begriff  
- Ohne intrinsische Motivation keine Selbstbestimmung ? Selbst wirksamkeits Erwartung 
- Schüler / Lehrer asynchron 
- Episodisches Gedächtnis - Schule Episode eigene?
- Stress schlecht : Mathe brendel Übungen 
- Übungen mit Sprache wichtig : 
- 43: Wichtigkeit des Scheiterns - die Angst vor dem scheitern 
- 47 Bildungsrevolution
- Lernen mit scheitern wird vom System UNMÖGLICH gemacht! Somit wird selbst Erfahrung nicht möglich 
- Werte der Wissensgesellschaft 
- Bildung wird DE- Institutionalisiert ! Online Kurse etc.   
- 51 RDP: Bilder aus Sprache zeichnen lassen vom NN
- Lern didaktik, neuro - bewegen und begreifen - Einstein gödel Spaziergänge
- Heute System verhindert, minimiert entwickeln intrinsischer motivaton   
- Mastery learning 
- Bewegen beim lernen
- 70
- Projekte , Fächer übergreifend , Leger Teams 
- 77 10 Prinzipien 
- 88 Handstand für Staat, Staat vs. Gemeinschaft 
- Der Staat , der Staat und nochmals der starke Staat als Antwort auf alles ...

# Notizen direkt an Inhalt - Buchgliederung

**Anna und die Schule**

# Die Bildungskatastrophe

## Was ist Bildung? S. 25

### Das deutscheste Wort

- Urpsrung 18 Jahrhunder Bildungsbürgertum
- Im Gegensatz zu Frankreich keine Berufsintellektuellen
- das Konzept der _deutschen_ Bildung , was bedeutet der Begriff im Zusammenhang => absolut überladen wie z.b. Blockchain oder KI oder oder....
- Bildung , woher kommt Bildung ? Wer versteht unter Bildung was ?
- Ursprung 
- Die zwei fundamental Befindlichkeiten im menschlichen Wesen: Wandel und Stetigkeit 
- Beispiele Zitate Kant und Pestalozzi
- für den Staat
- Befindlichkeiten 
- Institutionen bzw. Traditionen hindern Wandel? Hierarchie und zentrales Legacy

### Humbolds Traum

- bewegungs akspekte nicht mit einbeziehen
- keine oder zu wenig naturwissenschaften
- er gebildete Mensch vs. der taugliche Staatsdiener **HEUTE NOCH AKTUELL**

### Humbolds Erbe

- Bildung, Pädagogik und Didaktik
- Einfluss von Humbold heute eher gering, Protestantismus eher
- S. 42 Georg kerschensteiner
- was von Humbold übrig bleibt Bildung vs. Ausbildung S. 43
- Bildung für den Zusammenhalt der Gesellschaft ( jaja aber auch des Staates?) S. 45

## Klassenkampf in der Schule S. 50

### Es war einmal ... eine Revoution!

- 1964 Georg Picht - die deusche Bildungskatastrophe

### Alte Kameraden

- Einkommensungleichheit und Armut nimmt zu
- S. 57 die Bildungschancen sind in etwa  erneut so ungleich verteilt wie in der Adenauer Zeit, erfolg wieder abhängig vom Elternhaus
- Hauptschule, Realschule, Gymnasium
- "Das Buch, in dem jedes Kind liest, ist das Leben seiner Eltern"
- "Traum von einem spektakulären Leben in der erweiterten Drogenökonomie, das ihrem wunsch nach sozialer Größe entgegenkommt"
- Unterschicht in den 1960 und heute ...

### Geschlossene Gesellschaft

### Die heilige Dreifaltigkeit

### Rette sich wer kann!

## PISA, G8 und andere Dummheiten S. 81

### Humbold ist tot!

### Die falsch vermessene Schule

## Das Dilemma unserer Schulen S. 104

- "Das Dilemma unserer Schulen" Konformität , Taylor System von der geforderten Arbeitswelt in die Ausbildung / Schule  
- Heutiges Schulsystem: Konformität , bulemie lernen

### Hurra! Hurra!

### Das Unbehagen in der Schulkultur

### Gefüllte Fässer S. 112

- von über 13000 Unterrichtsstunden plus etwa 7000 Stunden Hausaufgaben bleiben Schätzungsweise 1% im Gedächtnis, Schätzung Thomas Städler


### Sieben und Sortieren S. 119

- "Wer das Niveau unserer Schulen heben will, muss die Stoffmenge reduzieren" 
- Nachhilfeunterricht

### Werten und Bewerten S. 126

### Liegestühle auf der Titanic S. 133

- würde herrn precht gerne fragen ob es 2018 jetzt besser ist...

## Lehrer als Beruf S. 138

### das schwedische Experiment

### Falsche Fronten S. 144

### Lehrer werden S. 150

### Müssen Lehrer lernen?

# Die Bildungsrevolution

## Bildung im 21. Jahrhundert S. 165

### Prognosen und andere Irrtümer

### Postpferde und Rennpferde

### Die Wissensgesellschaft S. 176

- 1995 Jeremy Rifkin, "Das Ende der Arbeit und ihre Zukunft"
- Robert Solo "Es werden aber auch immer neue **Jobs** geschaffen" <-- ach ja?
- S. 179 Erkenntnis und Eigentum...

### Lebendiges Wissen S. 180

- 1954 Peter F. Drucker "Management by objectives"
- (intrinsische) Motivation
- Energiewende und Wikipedia - was für schlechte Beispiele, obwohl wikipedia gute eigenschaften besitzt werden solche Beispiele immer viel zu leichtfertig gewählt, so auch hier
- - S. 185 Die bedeutung von Noten, Zertifikaten und Abschlüssen wird abnehmen, soso... gerne aber es wollen noch zu viele leute damit geschäft machen und teilen und herschen

### Lebenslanges Lernen S. 186

- Motivation, wozu, warum, lernen

### Eine neue Gesellschaft? S. 189

- :)

## Wie geht Lernen? S. 196

### Die Biologie des Lernens

### Gehirn und Entwicklung S. 203

- am stück konzentrieren mit 6 Jahren - 15min
- mit 9 - 20min, mit 11 - 30min
- die pubertät

### Mein Wollen & ich S. 208

### Gehringerechtes lernen S. 214

## Individualisiertes Lernen

### Lernen im Gleichschritt S. 223

### Washburne S. 226

- Mastery lerning 1922

### Das Schicksal einer guten Idee S. 230

- Skinner

### Die Khan-Akademie S. 235

- Milchmädchen kosten rechnung - was kostet das Internet/www pro minute...
- Mastery Learning Reloaded?
- Khan Zitat: ms paint "kostenfrei"? 
- Sugata Mitra - Hole-in-the-wall Experiment 1999, Minimal Invasive Education
- Khan Academy aber kein OER?..2012

### Bsp. Mathematikunterricht S. 240

- 57 % allen Nachhilfeunterrichts

## Jenseits von Fach und Note

### Körper und Geist S. 246

### Projekte statt Fächer S. 251

- Projekte oder Geschichten...?
- Mastery learning mit Bewegungspausen

### Beispiel Deutschunterricht S. 258

### Unterrichten im Team S. 264

### Lehrer aus dem Leben S. 268

- Simon peyton jones ICT initiative?
- Wisdom Teachers

### Die persöhnliche Note S. 275

## Bessere Schulen

### Eine optimale Schule? S. 282

### Zehn Prinzipien S. 288

### Kindergartenpflicht S. 296

- der Staat als Lösung für alle Probleme? Herr Precht...

### Die Integrative Schule S. 300

### Das achte Schuljahr S. 304

- Pubertät, Segeltour, Alan McArthur?

## Bildung für alle!

### Wohlstansbildung S. 309

### Bildungsrepublik? S. 313

- zum Glück steht hier ein Fragezeichen, eigentlich müsste Hernn Precht langsam dämmern das die Zeichen auf Dezentralisierung stehen

### Die große Blockade  S. 317

### Freiheit für Schulen S. 322

- finde ich eine gute Idee


### Revolution oder Transformation S. 326

- 

Segel Yachten für Lotti und Leo , Beispiele aus dem Buch 



